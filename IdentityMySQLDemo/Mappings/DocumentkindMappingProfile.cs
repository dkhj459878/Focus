﻿using AutoMapper;
using Focus.DAL.Common.DbModels;
using Focus.Web.Models;

namespace Focus.Web.Mappings
{
    /// <summary>
    /// Профиль сопоставления веб модели вида документа, подтверждающего полномочия, с моделью хранения данных типа документа, подтверждающего полномочия.
    /// </summary>
    public class DocumentkindMappingProfile : Profile
    {
        public DocumentkindMappingProfile()
        {
            CreateMap<DOCUMENTKIND, Documentkind>()
                .ForMember(cfg => cfg.DOCUMENT, act => act.Ignore())
                .MaxDepth(1);
        }
    }
}