﻿using AutoMapper;
using Focus.DAL.Common.DbModels;
using Focus.Web.Models;

namespace Focus.Web.Mappings
{
    /// <summary>
    /// Профиль сопоставления веб модели автомобиля с моделью хранения данных автомобиля.
    /// </summary>
    public class VersionMappingProfile : Profile
    {
        public VersionMappingProfile()
        {
            CreateMap<VERSION, Version>()
                .ForMember(cfg => cfg.FEATURE, act => act.Ignore())
                .MaxDepth(1);
            ;
        }
    }
}