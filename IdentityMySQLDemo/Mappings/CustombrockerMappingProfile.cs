﻿using AutoMapper;
using Focus.DAL.Common.DbModels;
using Focus.Web.Models;

namespace Focus.Web.Mappings
{
    /// <summary>
    /// Профиль сопоставления веб модели таможенного представителя с моделью хранения данных таможенного представителя.
    /// </summary>
    public class CustombrockerMappingProfile : Profile
    {
        public CustombrockerMappingProfile()
        {
            CreateMap<CUSTOMBROCKER, Custombrocker>()
                .ForMember(cfg => cfg.DOC, act => act.Ignore())
                .MaxDepth(1)
                .ReverseMap()
                .MaxDepth(1);
        }
    }
}