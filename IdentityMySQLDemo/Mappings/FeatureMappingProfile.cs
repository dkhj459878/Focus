﻿using AutoMapper;
using Focus.DAL.Common.DbModels;
using Focus.Web.Models;

namespace Focus.Web.Mappings
{
    /// <summary>
    /// Профиль сопоставления веб модели автомобиля с моделью хранения данных автомобиля.
    /// </summary>
    public class FeatureMappingProfile : Profile
    {
        public FeatureMappingProfile()
        {
            CreateMap<FEATURE, Feature>()
                .ForMember(cfg => cfg.VERSION, act => act.Ignore())
                .MaxDepth(1);
            ;
        }
    }
}