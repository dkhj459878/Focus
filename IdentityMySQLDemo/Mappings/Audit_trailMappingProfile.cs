﻿using AutoMapper;
using Focus.DAL.Common.DbModels;
using Focus.Web.Models;

namespace Focus.Web.Mappings
{
    /// <summary>
    /// Профиль сопоставления веб модели автомобиля с моделью хранения данных автомобиля.
    /// </summary>
    public class Audit_trailMappingProfile : Profile
    {
        public Audit_trailMappingProfile()
        {
            CreateMap<AUDIT_TRAIL, Audit_trail>();
            ;
        }
    }
}