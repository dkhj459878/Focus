﻿using AutoMapper;
using Focus.DAL.Common.DbModels;
using Focus.Web.Models;

namespace Focus.Web.Mappings
{
    /// <summary>
    /// Профиль сопоставления веб модели организации с моделью хранения данных организации.
    /// </summary>
    public class OrganizationMappingProfile : Profile
    {
        public OrganizationMappingProfile()
        {
            CreateMap<ORGANIZATION, Organization>()
                .ForMember(cfg => cfg.DOC, act => act.Ignore())
                .ForMember(cfg => cfg.DOCUMENT, act => act.Ignore())
                .MaxDepth(1);
        }
    }
}