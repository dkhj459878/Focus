﻿using AutoMapper;
using Focus.DAL.Common.DbModels;
using Focus.Web.Models;

namespace Focus.Web.Mappings
{
    /// <summary>
    /// Профиль сопоставления веб модели разрешения с моделью хранения данных разрешения.
    /// </summary>
    public class PermissionMappingProfile : Profile
    {
        public PermissionMappingProfile()
        {
            CreateMap<PERMISSION, Permission>()
                .ForMember(cfg => cfg.DOCUMENT, act => act.Ignore())
                .MaxDepth(1);
        }
    }
}