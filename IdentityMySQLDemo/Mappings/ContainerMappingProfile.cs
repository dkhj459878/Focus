﻿using AutoMapper;
using Focus.DAL.Common.DbModels;
using Focus.Web.Models;

namespace Focus.Web.Mappings
{
    /// <summary>
    /// Профиль сопоставления веб модели контейнера с моделью хранения данных контейнера.
    /// </summary>
    public class ContainerMappingProfile : Profile
    {
        public ContainerMappingProfile()
        {
            CreateMap<CONTAINER, Container>()
                .ForMember(cfg => cfg.DOC, act => act.Ignore())
                .MaxDepth(1);
            ;
        }
    }
}