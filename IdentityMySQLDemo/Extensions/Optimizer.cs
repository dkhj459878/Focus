﻿using System;
using System.Collections.Generic;
using Messages = IdentityMySQLDemo.Properties.Resources;
using System.Reflection;

namespace Focus.Web.Extensions
{
    internal class Optimizer<T> where T : class
    {
        internal bool IsError { get; set; } = false;
        internal string ErrorMessage { get; set; } = string.Empty;

        private IDictionary<PropertyInfo, IEnumerable<PropertyInfo>> NestedInitialProperties { get; set; } = new Dictionary<PropertyInfo, IEnumerable<PropertyInfo>>();

        internal Type EntityType { get; set; } = typeof(T);

        internal IEnumerable<T> Optimize(IEnumerable<T> entities)
        {
            // Присваиваем локальной переменной значение передаваемого параметра.
            IEnumerable<T> entitiesLocal = entities;
            // Проверяем на Null передаваемую коллекцию.
            // В случае, если коллекция равна Null, то, в свою очередь, также возвращаем Null коллекцию.
            if (entitiesLocal == null)
            {
                return null;
            }
            else
            {
                // Данная логическая ветка не предусмотрена.
            }

            if (IsList(entitiesLocal))
            {
                if (IsEmpty(entitiesLocal))
                {
                    // то возвращаем пустую коллекцию передаваемого типа.
                    return new List<T>();
                }
            }
            else
            {
                SetErrorNotList(typeof(T));
                return null;
            }

            // Далее...
            // Проверяем (получаем) тип уплотняемой сущности.
            Type entityType = typeof(T);
            // Получаем все свойства сущности передаваемой коллекции.
            PropertyInfo[] initialProperties = entityType.GetProperties();
            // Ищем все свойства-классы (вложенные сущности), то есть, не текст, не числа (целые, с плавающей запятой), не даты и тп.
            foreach (PropertyInfo initialProperty in initialProperties)
            {
                Type initialProperyType = initialProperty.PropertyType;
                if (initialProperty.IsNestedProperty())
                {
                    // Получаем все свойства сложного типа вложенного свойства передаваемой коллекции.
                    PropertyInfo[] secondaryProperties = initialProperyType.GetProperties();
                    List<PropertyInfo> secondaryNestedProperties = new List<PropertyInfo>();
                    foreach (PropertyInfo secondaryProperty in secondaryProperties)
                    {
                        if (secondaryProperty.IsNestedProperty())
                        {
                            secondaryNestedProperties.Add(secondaryProperty);
                        }
                    }

                    bool initialNestedPropertyHasNestedElements = secondaryNestedProperties.Count > 0;
                    if (initialNestedPropertyHasNestedElements)
                    {
                        NestedInitialProperties.Add(initialProperty, secondaryNestedProperties);
                    }
                }
            }
            // Исходя из структуры данных сущности формируем словарь, где в:
            // - качестве ключа - используются типы данных вложенных свойства-классов передаваемой сущности, а 
            // - качестdве значения - коллекцию его (ключа) вложенных свойств-классов.
            // Итеративно проходимся по элементам передаваемой коллекции и присваиваем всем вложенным сложным сущностям
            // сложных сущностей (свойств-классов) элементов передаваемой коллекции значение Null.
            return MinifyEntities(entities);
            // Все. Выходим из метода.
        }


        private IEnumerable<T> MinifyEntities(IEnumerable<T> entities)
        {
            List<T> entitiesMinifided = new List<T>();
            IEnumerable<T> localEntities = entities;
            foreach (T entity in localEntities)
            {
                T entityMinified = MinifyEntity(entity);
                entitiesMinifided.Add(entityMinified);
            }
            
            return entitiesMinifided;
        }

        private T MinifyEntity(T entity)
        {
            T minifidedEntity = entity;
            foreach (KeyValuePair<PropertyInfo, IEnumerable<PropertyInfo>>
                nestedInitialProperty in NestedInitialProperties)
            {
                PropertyInfo initialProperty = nestedInitialProperty.Key;
                IEnumerable<PropertyInfo> secondaryProperties = NestedInitialProperties[initialProperty];
                foreach (PropertyInfo secondaryProperty in secondaryProperties)
                {
                    minifidedEntity = SetPropertyToNull(minifidedEntity, initialProperty.Name, secondaryProperty.Name);
                }
            }
            return minifidedEntity;
        }

        private T SetPropertyToNull(object target,  string initialProperty, string secondaryProperty)
        {
            object targetInitial = target;
            const object NULL_OBJECT = null;

            PropertyInfo propertyToGet = targetInitial.GetType().GetProperty(initialProperty);
            object targetNested = propertyToGet.GetValue(targetInitial, null);

            if (IsNull(targetNested))
            {
                return targetInitial as T;
            }
            else
            {
                PropertyInfo propertyToSet = targetNested.GetType().GetProperty(secondaryProperty);
                propertyToSet.SetValue(targetNested, NULL_OBJECT, null);
                return targetInitial as T;
            }
        }

        private bool IsNull(object obj)
        {
            return obj == null;
        }

        private bool IsList(IEnumerable<T> entities)
        {
            return entities is List<T>;
        } 

        private bool IsEmpty(IEnumerable<T> entities)
        {
            IEnumerable<T> localEntities = entities;
            if (IsList(localEntities))
            {
                List<T> entitiesList = localEntities as List<T>;
                return entitiesList.Count == 0;
            }
            else
            {
                SetErrorNotList(typeof(T));
                return false;
            }
        }

        private void SetErrorNotList(Type typeEntity)
        {
            IsError = true;
            ErrorMessage = string.Format(Messages.ErrorOptimizerCollectionNotList, typeEntity.Name);
        }
        
    }
}