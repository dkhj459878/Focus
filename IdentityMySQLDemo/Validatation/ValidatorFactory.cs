﻿using FluentValidation;
using Focus.Web.Models;
using Ninject;
using System;
using System.Linq;

namespace Focus.Web.Validation
{
    public class ValidatorFactory : ValidatorFactoryBase
    {
        private readonly IKernel kernel;

        /// <summaryDoc>>()
        /// Initializes a new instance of the <see cref="NinjectValidatorFactory"/> class.
        /// </summary>
        /// <param name="kernel"/>The kernel.</param>
        public ValidatorFactory(IKernel kernel)
        {
            this.kernel = kernel;
            AddBinding();
        }

        private void AddBinding()
        {
            kernel.Bind<IValidator<Doc>>().To<DocValidator>();
            kernel.Bind<IValidator<Vehicle>>().To<VehicleValidator>();
        }

        /// <summary>
        /// Creates an instance of a validator with the given type using ninject.
        /// </summary>
        /// <param name="validatorType">Type of the validator.</param>
        /// <returns>The newly created validator</returns>
        public override IValidator CreateInstance(Type validatorType)
        {
            if (!kernel.GetBindings(validatorType).Any())
            {
                return null;
            }
            return kernel.Get(validatorType) as IValidator;
        }
    }
}