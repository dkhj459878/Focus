﻿using Focus.Web.Models;
using System.Collections.Generic;

namespace Focus.Web.ViewModels
{
    public class DocsViewModel
    {
        public IEnumerable<Doc> Docs { get; set; }

        public PageInfo PageInfo { get; set; }
    }
}