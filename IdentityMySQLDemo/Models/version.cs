﻿namespace Focus.Web.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public partial class Version
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Version()
        {
        }

        [Display(Name = "Код")]
        public short ID { get; set; }
        public string NUM { get; set; }
        public string DESCRIPTION { get; set; }
        public string SID { get; set; }
        public string USERINFO { get; set; }
        public DateTime? LAST_UPDATE { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Feature> FEATURE { get; set; }
    }
}
