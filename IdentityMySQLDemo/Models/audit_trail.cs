﻿namespace Focus.Web.Models
{
    using System;
    using System.ComponentModel.DataAnnotations;

    public partial class Audit_trail
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Audit_trail()
        {
        }

        [Display(Name = "Код")]
        public long ID { get; set; }
        [Display(Name = "Название объекта")]
        public string ENTITY { get; set; }
        [Display(Name = "Код объекта")]
        public Nullable<short> ENTITYID { get; set; }
        [Display(Name = "Поле объекта")]
        public string FIELD { get; set; }
        [Display(Name = "Тип изменений")]
        public short? TYPE { get; set; }
        [Display(Name = "Старое значение")]
        public string VALUEOLD { get; set; }
        [Display(Name = "Новое значение")]
        public string VALUENEW { get; set; }
        [Display(Name = "Идентификатор пользователя")]
        public string SID { get; set; }
        [Display(Name = "Сведения о пользователе")]
        public string USERINFO { get; set; }
        [Display(Name = "Дата изменений")]
        public DateTime? DATE_ { get; set; }
        [Display(Name = "Дата и время внесения изменений в таблицу аудита")]
        public string TIME_STAMP { get; set; }
    }
}
