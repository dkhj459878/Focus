﻿namespace Focus.Web.Models
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Web.Mvc;

    public partial class Vehicletype
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Vehicletype()
        {
        }

        [Display(Name = "Код")]
        public byte VEHICLETYPEID { get; set; }
        [Required(ErrorMessage = "Требуется ввести наименование типа транспортного средства.")]
        [Display(Name = "Наименование")]
        [Remote("NoRepeated", "vehicletypes", HttpMethod = "POST", AdditionalFields = "VEHICLETYPEID")]
        [MaxLength(45)]
        public string NAME { get; set; }
        [Display(Name = "Описание")]
        [MaxLength(155)]
        public string DESCRIPTION { get; set; }
        [Display(Name = "Пользователь")]
        [MaxLength(256)]
        public string USERID { get; set; }
        [Display(Name = "Обновлен")]
        public System.DateTime LAST_UPDATE { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Vehiclekind> VEHICLEKIND { get; set; }
    }
}
