﻿namespace Focus.Web.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public partial class Feature
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Feature()
        {
        }

        [Display(Name = "Код")]
        public short ID { get; set; }
        [Display(Name = "Название")]
        public string NAME { get; set; }
        [Display(Name = "Описание")]
        public string DESCRIPTION { get; set; }
        [Display(Name = "Идентификатор пользователя")]
        public string SID { get; set; }
        [Display(Name = "Сведения о пользователе")]
        public string USERINFO { get; set; }
        [Display(Name = "Дата и время внесения изменений в таблицу")]
        public DateTime? LAST_UPDATE { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Version> VERSION { get; set; }
    }
}
