﻿namespace Focus.Web.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Web.Mvc;

    public partial class Post
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Post()
        {
        }

        [Display(Name = "Код")]
        public byte ID { get; set; }
        [Required(ErrorMessage = "Требуется ввести код ПТО.")]
        [Display(Name = "Код ПТО")]
        [RegularExpression("06[0-9]{3}", ErrorMessage = "Формат кода ПТО должен начинаться со строки '06' и продолжаться трехназчным числом.")]
        [Remote("NotRepeated", "post", HttpMethod = "POST", AdditionalFields = "NAME,ID")]
        [MaxLength(5)]
        public string CODE { get; set; }
        [Required(ErrorMessage = "Требуется ввести наименование ПТО.")]
        [Display(Name = "ПТО")]
        [MaxLength(45)]
        public string NAME { get; set; }
        [Required(ErrorMessage = "Требуется ввести название отдела / таможенного поста.")]
        [Display(Name = "Отдел/таможенный пост")]
        [MaxLength(45)]
        public string DEVISION { get; set; }
        [Required(ErrorMessage = "Требуется ввести адрес ПТО.")]
        [Display(Name = "Адрес ПТО")]
        [MaxLength(155)]
        public string ADDRESS { get; set; }
        [Display(Name = "Пользователь")]
        [MaxLength(256)]
        public string USERID { get; set; }
        [Display(Name = "Обновлен")]
        public DateTime? LAST_UPDATE { get; set; }
        [Display(Name = "SID")]
        public string SID { get; set; }
        [Display(Name = "Сведения о пользователе")]
        public string USERINFO { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Inspector> INSPECTOR { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Storage> STORAGE { get; set; }
    }
}
