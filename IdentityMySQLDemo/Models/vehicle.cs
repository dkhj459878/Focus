﻿namespace Focus.Web.Models
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Web.Mvc;

    //[FluentValidation.Attributes.Validator(typeof(MyModelValidator))]
    public partial class Vehicle
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Vehicle()
        {
        }

        [Display(Name = "Код")]
        public short VEHICLEID { get; set; }
        [Display(Name = "Вид")]
        public byte VEHICLEKINDID { get; set; }
        [Display(Name = "Страна регистрации")]
        public string COUNTRYCODE { get; set; }
        [Display(Name = "Марка")]
        [MaxLength(45)]
        public string MARK { get; set; }
        [Display(Name = "Модель")]
        [MaxLength(45)]
        public string MODEL { get; set; }
        [Display(Name = "Описание")]
        [MaxLength(155)]
        public string DESCRIPTION { get; set; }
        [Required(ErrorMessage = "Требуется ввести государственный регистрационный номер транспортного средства.")]
        [Remote("NotRepeated", "vehicles", AdditionalFields = "COUNTRYCODE, VEHICLEID")]
        [Display(Name = "Рег. номер")]
        [MaxLength(45)]
        public string REGNUMBER { get; set; }
        [Display(Name = "Буквы рег. номера")]
        [MaxLength(45)]
        public string REGNUMBERLETTERS { get; set; }
        [Display(Name = "Цифры рег. номера")]
        [MaxLength(45)]
        public string REGNUMBERDIGITS { get; set; }
        [Display(Name = "Цифры после рег. номера")]
        [MaxLength(45)]
        public string REGNUMBEDIGITSAFTER { get; set; }
        [Display(Name = "Пользователь")]
        [MaxLength(256)]
        public string USERID { get; set; }
        [Display(Name = "Обновлен")]
        public System.DateTime LAST_UPDATE { get; set; }
        [Display(Name = "SID")]
        public string SID { get; set; }
        [Display(Name = "Сведения о пользователе")]
        public string USERINFO { get; set; }

        public virtual Country COUNTRY { get; set; }
        public virtual Vehiclekind VEHICLEKIND { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Doc> DOC { get; set; }
    }
}
