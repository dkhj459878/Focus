﻿namespace Focus.Web.Models
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Web.Mvc;

    public partial class Vehiclekind
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Vehiclekind()
        {
        }

        [Display(Name = "Код")]
        public byte VEHICLEKINDID { get; set; }
        [Required(ErrorMessage = "Требуется ввести тип транспортного средства.")]
        [Display(Name = "Тип")]
        public byte VEHICLETYPEID { get; set; }
        [Required(ErrorMessage = "Требуется ввести наименование вида транспортного средства.")]
        [Display(Name = "Наименование")]
        [Remote("NotRepeated", "vehiclekinds", HttpMethod = "POST", AdditionalFields = "VEHICLETYPEID, VEHICLEKINDID")]
        [MaxLength(45)]
        public string NAME { get; set; }
        [Display(Name = "Описание")]
        [MaxLength(155)]
        public string DESCRIPTION { get; set; }
        [Display(Name = "Пользователь")]
        [MaxLength(256)]
        public string USERID { get; set; }
        [Display(Name = "Обновлен")]
        public System.DateTime LAST_UPDATE { get; set; }
        [Display(Name = "SID")]
        public string SID { get; set; }
        [Display(Name = "Сведения о пользователе")]
        public string USERINFO { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Car> CAR { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Vehicle> VEHICLE { get; set; }
        public virtual Vehicletype VEHICLETYPE { get; set; }
    }
}
