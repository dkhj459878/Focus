﻿namespace Focus.Web.Models
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Web.Mvc;

    public partial class Storage
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Storage()
        {
        }

        [Display(Name = "Код")]
        public byte ID { get; set; }
        [Required(ErrorMessage = "Требуется ввести наименование СВХ.")]
        [Display(Name = "Наименование")]
        [MaxLength(45)]
        [Remote("NotRepeatedName", "Storage", HttpMethod = "POST", AdditionalFields = "ID")]
        public string NAME { get; set; }
        [Required(ErrorMessage = "Требуется ввести адрес УНП.")]
        [Display(Name = "УНП")]
        [MaxLength(9)]
        public string UNP { get; set; }
        [Required(ErrorMessage = "Требуется ввести адрес СВХ.")]
        [Display(Name = "Адрес")]
        [MaxLength(500)]
        public string ADDRESS { get; set; }
        [Required(ErrorMessage = "Требуется ввести номер СВХ.")]
        [Display(Name = "Номер")]
        [MaxLength(45)]
        [Remote("NotRepeatedNumber", "Storage", HttpMethod = "POST", AdditionalFields = "ID")]
        public string NUMBER_ { get; set; }
        [Display(Name = "Пользователь")]
        [MaxLength(256)]
        public string USERID { get; set; }
        [Display(Name = "Обновлен")]
        public System.DateTime LAST_UPDATE { get; set; }
        [Display(Name = "SID")]
        public string SID { get; set; }
        [Display(Name = "Сведения о пользователе")]
        public string USERINFO { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Post> POST { get; set; }
    }
}
