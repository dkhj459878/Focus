﻿namespace Focus.Web.Models
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Web.Mvc;
    using System; 

    public partial class Devicetype
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Devicetype()
        {
        }

        [Display(Name = "Код")]
        public byte DEVICETYPEID { get; set; }
        [Display(Name = "Наименование")]
        [Remote("NotRepeated", "devicetypes", HttpMethod = "POST", AdditionalFields = "DEVICETYPEID")]
        [Required(ErrorMessage = "Требуется ввести наименование типа технического средства таможенного контроля.")]
        [MaxLength(255)]
        public string NAME { get; set; }
        [Display(Name = "Пользователь")]
        [MaxLength(256)]
        public string USERID { get; set; }
        [Display(Name = "Обновлен")]
        public DateTime LAST_UPDATE { get; set; }
        [Display(Name = "SID")]
        public string SID { get; set; }
        [Display(Name = "Сведения о пользователе")]
        public string USERINFO { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Device> DEVICE { get; set; }
    }
}
