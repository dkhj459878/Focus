﻿namespace Focus.Web.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Web.Mvc;

    public partial class Inspector
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Inspector()
        {
        }

        [Display(Name = "Код")]
        public short ID { get; set; }
        [Display(Name = "Подразделение")]
        [Required(ErrorMessage = "Требуется выбрать подразделение.")]
        public byte? POSTID { get; set; }
        [Required(ErrorMessage = "Требуется ввести имя инспектора.")]
        [Display(Name = "Имя")]
        [Remote("NotRepeated", "Inspector", HttpMethod = "POST", AdditionalFields = "LASTNAME, FATHERNAME, ID")]
        [MaxLength(45)]
        public string FIRSTNAME { get; set; }
        [Required(ErrorMessage = "Требуется ввести фамилию инспектора.")]
        [Display(Name = "Фамилия")]
        [MaxLength(45)]
        public string LASTNAME { get; set; }
        [Required(ErrorMessage = "Требуется ввести отчество инспектора.")]
        [Display(Name = "Отчество")]
        [MaxLength(45)]
        public string FATHERNAME { get; set; }
        [Required(ErrorMessage = "Требуется ввести фамилия и инициалы инспектора.")]
        [Display(Name = "Фамилия, инициалы")]
        [MaxLength(45)]
        public string SHORTNAME { get; set; }
        [Required(ErrorMessage = "Требуется ввести должность инспектора.")]
        [Display(Name = "Должность")]
        [MaxLength(45)]
        public string TITLE { get; set; }
        [Display(Name = "Номер ЛНП")]
        [MaxLength(5)]
        public string LNP { get; set; }
        [Display(Name = "Номер пломбиратора")]
        [MaxLength(15)]
        public string SEAL { get; set; }
        [Display(Name = "Телефон")]
        [MaxLength(45)]
        public string PHONENUMBER { get; set; }
        [Display(Name = "Пользователь")]
        [MaxLength(256)]
        public string USERID { get; set; }
        [Display(Name = "Обновлен")]
        public DateTime LAST_UPDATE { get; set; }
        [Display(Name = "SID")]
        public string SID { get; set; }
        [Display(Name = "Сведения о пользователе")]
        public string USERINFO { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Doc> DOC { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Doc> DOC1 { get; set; }
        public virtual Post POST { get; set; }
    }
}
