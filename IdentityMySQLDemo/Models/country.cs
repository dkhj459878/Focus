﻿namespace Focus.Web.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Web.Mvc;

    public partial class Country : IEquatable<Country>, ICloneable
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Country()
        {
        }

        [Required(ErrorMessage = "Требуется двухзначный код страны (два символа).")]
        [Display(Name = "Код страны 2")]
        [Remote("NotRepeatedAbbr2", "countries", HttpMethod = "POST")]
        public string ABBR2 { get; set; }
        [Display(Name = "Наименование")]
        [Required(ErrorMessage = "Требуется ввести наименование страны.")]
        [Remote("NotRepeatedName", "countries", HttpMethod = "POST")]
        [MaxLength(40)]
        public string NAME { get; set; }
        [Display(Name = "Полное название")]
        [MaxLength(60)]
        public string NAMEFULL { get; set; }
        [Display(Name = "Название на англ. яз.")]
        [MaxLength(45)]
        public string NAMEEN { get; set; }
        [Display(Name = "Код страны 3")]
        public string ABBR3 { get; set; }
        [Display(Name = "Код буквы")]
        public string CODELETTERS { get; set; }
        [Display(Name = "Код цифры")]
        public short? CODEDIGITS { get; set; }
        [Display(Name = "Континент")]
        [MaxLength(10)]
        public string CONTINENTS { get; set; }
        [Display(Name = "Регион")]
        [MaxLength(28)]
        public string REGION { get; set; }
        [MaxLength(256)]
        public string USERID { get; set; }
        public DateTime LAST_UPDATE { get; set; }
        public string COUNTRYCOL { get; set; }
        [Display(Name = "SID")]
        public string SID { get; set; }
        [Display(Name = "Сведения о пользователе")]
        public string USERINFO { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Car> CAR { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Container> CONTAINER { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Custombrocker> CUSTOMBROCKER { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Vehicle> VEHICLE { get; set; }

        public object Clone()
        {
            return new Country()
            {
                NAME = NAME,
                NAMEFULL = NAMEFULL,
                NAMEEN = NAMEEN,
                ABBR3 = ABBR3,
                CODELETTERS = CODELETTERS,
                CODEDIGITS = CODEDIGITS,
                CONTINENTS = CONTINENTS,
                REGION = REGION,
                USERID = USERID,
                LAST_UPDATE = LAST_UPDATE,
                COUNTRYCOL = COUNTRYCOL
            };
        }

        public override bool Equals(object obj)
        {
            return Equals(obj as Country);
        }

        public bool Equals(Country other)
        {
            return other != null &&
                   ABBR2 == other.ABBR2 &&
                   NAME == other.NAME &&
                   NAMEFULL == other.NAMEFULL &&
                   NAMEEN == other.NAMEEN &&
                   ABBR3 == other.ABBR3 &&
                   CODELETTERS == other.CODELETTERS &&
                   EqualityComparer<short?>.Default.Equals(CODEDIGITS, other.CODEDIGITS) &&
                   CONTINENTS == other.CONTINENTS &&
                   REGION == other.REGION &&
                   USERID == other.USERID &&
                   LAST_UPDATE == other.LAST_UPDATE &&
                   COUNTRYCOL == other.COUNTRYCOL;
        }

        public override int GetHashCode()
        {
            var hashCode = 1227812764;
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(ABBR2);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(NAME);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(NAMEFULL);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(NAMEEN);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(ABBR3);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(CODELETTERS);
            hashCode = hashCode * -1521134295 + EqualityComparer<short?>.Default.GetHashCode(CODEDIGITS);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(CONTINENTS);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(REGION);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(USERID);
            hashCode = hashCode * -1521134295 + LAST_UPDATE.GetHashCode();
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(COUNTRYCOL);
            return hashCode;
        }

        public static bool operator ==(Country country1, Country country2)
        {
            return EqualityComparer<Country>.Default.Equals(country1, country2);
        }

        public static bool operator !=(Country country1, Country country2)
        {
            return !(country1 == country2);
        }
    }
}
