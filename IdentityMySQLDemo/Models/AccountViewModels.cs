﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace IdentityMySQLDemo.Models
{
    public class ExternalLoginConfirmationViewModel
    {
        [Required]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }

    public class ExternalLoginListViewModel
    {
        public string ReturnUrl { get; set; }
    }

    public class SendCodeViewModel
    {
        public string SelectedProvider { get; set; }
        public ICollection<System.Web.Mvc.SelectListItem> Providers { get; set; }
        public string ReturnUrl { get; set; }
        public bool RememberMe { get; set; }
    }

    public class UsersViewModel
    {
        [Required]
        [Display(Name = "Код")]
        public string Id { get; set; }

        [Required]
        [Display(Name = "Имя пользователя")]
        public string UserName { get; set; }

        [Display(Name = "Права")]
        public string Role { get; set; }

        [Display(Name = "Должность")]
        public string Title { get; set; }

        [Display(Name = "ФИО")]
        public string ShortName { get; set; }

        [Display(Name = "Код ПТО")]
        public string CodePTO { get; set; }

        [Display(Name = "ПТО")]
        public string PTOName { get; set; }

        [Display(Name = "Подразделение/отдела")]
        public string Division { get; set; }

        public DateTime? LockoutEndDate { get; set; }

        [Display(Name = "Телефон")]
        public string PhoneNumber { get; set; }
    }

    public class VerifyCodeViewModel
    {
        [Required]
        public string Provider { get; set; }

        [Required]
        [Display(Name = "Код")]
        public string Code { get; set; }
        public string ReturnUrl { get; set; }

        [Display(Name = "Запомнить этот браузер?")]
        public bool RememberBrowser { get; set; }

        public bool RememberMe { get; set; }
    }

    public class ForgotViewModel
    {
        [Required]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }

    public class LoginViewModel
    {
        [Required]
        [Display(Name = "Имя пользователя")]
        public string UserName { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Пароль")]
        public string Password { get; set; }

        [Display(Name = "Запомнить меня?")]
        public bool RememberMe { get; set; }
    }


    public class UserViewModel
    {
        [Required]
        [Display(Name = "Код")]
        public string Id { get; set; }

        [Required]
        [Display(Name = "Имя пользователя")]
        public string UserName { get; set; }

        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Пароль")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Подтвердите пароль")]
        [System.ComponentModel.DataAnnotations.Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        public DateTime LockoutEndDate { get; set; }

        [Required]
        [Display(Name = "Права")]
        public string Role { get; set; }
    }

    public class RegisterViewModel
    {
        [Required]
        [Display(Name = "Имя пользователя")]
        [Remote("doesUserNameExist", "User", HttpMethod = "POST", ErrorMessage = "user с таким именем существует. Пожалуйста, введите другое имя пользователя.")]
        public string UserName { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "Неверный пароль. Длина поля {0} должна быть минимум {2} символов.", MinimumLength = 8)]
        [RegularExpression("(?=.{8,})(?=.*?[0-9])(?=.*?[A-Z])(.*?[a-z])(?=.*?[`~!@\"#$;:%^&?*()_\\-\\+\\=/.,<>\\[\\]]).*", ErrorMessage = "Неверный пароль. Пароль должен содержать: символы латинского алфавита всех регистров, цифры и хотя бы один специальный символ.")]
        [DataType(DataType.Password)]
        [Display(Name = "Пароль")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Подтвердите пароль")]
        [System.ComponentModel.DataAnnotations.Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        [Required]
        [Display(Name = "Должностное лицо")]
        public int PERSONID { get; set; }

        [Required]
        [Display(Name = "Права")]
        public string Role { get; set; }
    }


    public class RegisterAdministratorViewModel
    {
        [Required]
        [Display(Name = "Имя пользователя")]
        //[Remote("doesUserNameExist", "User", HttpMethod = "POST", ErrorMessage = "user с таким именем существует. Пожалуйста, введите другое имя пользователя.")]
        public string UserName { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "Неверный пароль. Длина поля {0} должна быть минимум {2} символов.", MinimumLength = 8)]
        [RegularExpression("(?=.{8,})(?=.*?[0-9])(?=.*?[A-Z])(.*?[a-z])(?=.*?[`~!@\"#$;:%^&?*()_\\-\\+\\=/.,<>\\[\\]]).*", ErrorMessage = "Неверный пароль. Пароль должен содержать: символы латинского алфавита всех регистров, цифры и хотя бы один специальный символ.")]
        [DataType(DataType.Password)]
        [Display(Name = "Пароль")]
        public string Password { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Подтвердите пароль")]
        [System.ComponentModel.DataAnnotations.Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        [Required(ErrorMessage = "Требуется ввести имя инспектора.")]
        [Display(Name = "Имя")]
        [Remote("NotRepeated", "User", HttpMethod = "POST", AdditionalFields = "LASTNAME, FATHERNAME")]
        [MaxLength(45)]
        public string FIRSTNAME { get; set; }

        [Required(ErrorMessage = "Требуется ввести фамилию инспектора.")]
        [Display(Name = "Фамилия")]
        [MaxLength(45)]
        public string LASTNAME { get; set; }

        [Required(ErrorMessage = "Требуется ввести отчество инспектора.")]
        [Display(Name = "Отчество")]
        [MaxLength(45)]
        public string FATHERNAME { get; set; }

        [Required(ErrorMessage = "Требуется ввести фамилия и инициалы инспектора.")]
        [Display(Name = "Фамилия, инициалы")]
        [MaxLength(45)]
        public string SHORTNAME { get; set; }

        [Required(ErrorMessage = "Требуется ввести должность инспектора.")]
        [Display(Name = "Должность")]
        [MaxLength(45)]
        public string TITLE { get; set; }

        [Display(Name = "Номер ЛНП")]
        [MaxLength(5)]
        public string LNP { get; set; }

        [Display(Name = "Номер пломбиратора")]
        [MaxLength(15)]
        public string SEAL { get; set; }

        [Display(Name = "Телефон")]
        [MaxLength(45)]
        public string PHONENUMBER { get; set; }
    }


    public class ResetPasswordViewModel
    {
        [Required]
        [Display(Name = "Имя пользователя")]
        public string UserName { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "Неверный пароль. Длина поля {0} должна быть минимум {2} символов.", MinimumLength = 8)]
        [RegularExpression("(?=.{8,})(?=.*?[0-9])(?=.*?[A-Z])(.*?[a-z])(?=.*?[`~!@\"#$;:%^&?*()_\\-\\+\\=/.,<>\\[\\]]).*", ErrorMessage = "Неверный пароль. Пароль должен содержать: символы латинского алфавита всех регистров, цифры и хотя бы один специальный символ.")]
        [DataType(DataType.Password)]
        [Display(Name = "Пароль")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Подтвердите пароль")]
        [System.ComponentModel.DataAnnotations.Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        public string Code { get; set; }
    }

    public class ForgotPasswordViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }
}
