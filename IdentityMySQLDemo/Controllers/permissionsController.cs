﻿using AspNet.Identity.MySQL;
using AutoMapper;
using Focus.BL.Common.Services;
using Focus.DAL.Common.DbModels;
using Focus.Web.Models;
using IdentityMySQLDemo.Models;
using log4net;
using Microsoft.AspNet.Identity;
using System;
using Focus.Web.Helpers;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Net;
using System.Web.Mvc;

namespace Focus.Controllers
{
    [Authorize(Roles = "redactor, admin, auditor")]
    public class permissionsController : Controller
    {
        protected static readonly ILog _logger =
       LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private ApplicationDbContext db = new ApplicationDbContext("OracleDbContext");

        UserManager<ApplicationUser> userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(new ApplicationDbContext("OracleDbContext")));
        RoleManager<IdentityRole> RoleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(new ApplicationDbContext("OracleDbContext")));

        private IPermissionService _permissionService;
        private IDocumentService _documentService;
        private IDocumentkindService _documentkindService;
        private ICustombrockerService _custombrockerService;
        private IInspectorService _inspectorService;


        public permissionsController(IPermissionService permissionService,IDocumentService documentService, IDocumentkindService documentkindService, ICustombrockerService custombrockerService, IInspectorService inspectorService)
        {
            _permissionService = permissionService;
            _documentService = documentService;
            _documentkindService = documentkindService;
            _custombrockerService = custombrockerService;
            _inspectorService = inspectorService;
        }

        // GET: permissions
        public ActionResult Index()
        {
            var permissions = _permissionService.GetAllPermissions().ToList();
            return View(Mapper.Map<IEnumerable<PERMISSION>, IEnumerable<Permission>>(permissions));
        }

        // GET: permissions/Details/5
        public ActionResult Details(short id)
        {
            PERMISSION permission = _permissionService.GetPermission(id);
            if (permission == null)
            {
                return HttpNotFound();
            }
            return View(Mapper.Map<Permission>(permission));
        }

        public JsonResult JsonPermission(PERMISSION permission, string fullName, string workOfPlace, string post, string passport, string address, string phone, string fax)
        {
    
            //if (ModelState.IsValid)
            //{
                var custombrockers = new List<CUSTOMBROCKER>();
                            foreach (var custombrocker in permission.CUSTOMBROCKER)
                            {
                                custombrockers.Add(_custombrockerService.GetCustombrocker(custombrocker.CUSTOMBROCKERID));
                            }

                            permission.CUSTOMBROCKER = null;

                try
                {
                    permission.LAST_UPDATE = DateTime.Now;
                    _permissionService.AddPermission(Mapper.Map<PERMISSION>(permission));

                    permission.CUSTOMBROCKER = custombrockers;

                    permission.LAST_UPDATE = DateTime.Now;
                    _permissionService.UpdatePermission(Mapper.Map<PERMISSION>(permission));

                    // Your code...
                    // Could also be before try if you know the exception occurs in SaveChanges
                    
                    //db.SaveChanges();
                }
                catch (DbEntityValidationException e)
                {
                    foreach (var eve in e.EntityValidationErrors)
                    {
                        Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                            eve.Entry.Entity.GetType().Name, eve.Entry.State);
                        foreach (var ve in eve.ValidationErrors)
                        {
                            Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                                ve.PropertyName, ve.ErrorMessage);
                        }
                    }
                    throw;
                }

                var jsondata = _permissionService.GetPermission(permission.PERMISSIONID);
                return Json(
                    new
                    {
                        id = jsondata?.PERMISSIONID,
                        start = jsondata?.DATESTART == null ? "" : jsondata?.DATESTART.Value.ToShortDateString(),
                        finish = jsondata?.DATEFINISH == null ? "" : jsondata?.DATEFINISH.Value.ToShortDateString(),
                        valid = jsondata?.ISVALID == null ? false : Convert.ToBoolean(jsondata?.ISVALID.Value),
                        fullName,
                        workOfPlace,
                        post,
                        address,
                        passport,
                        phone,
                        fax
                    },
                    JsonRequestBehavior.AllowGet);
            //}
            //return Json(new { });
        }

           

        // GET: permissions/Create
        public ActionResult Create()
        {
            ViewBag.DOCUMENTID = new SelectList(Mapper.Map<IEnumerable<DOCUMENT>, IEnumerable<Document>>(_documentService.GetAllDocuments()), "DOCUMENTID", "NUMBER");
            ViewBag.DOCUMENTKINDID = new SelectList(Mapper.Map<IEnumerable<DOCUMENTKIND>, IEnumerable<Documentkind>>(_documentkindService.GetAllDocumentkinds()), "DOCUMENTKINDID", "NAME", 1);
            return View();
        }

        // GET: permissions/Create
        public ActionResult PartialCreate()
        {
            ViewBag.DOCUMENTID = new SelectList(Mapper.Map<IEnumerable<DOCUMENT>, IEnumerable<Document>>(_documentService.GetAllDocuments()), "DOCUMENTID", "NUMBER");
            return PartialView();
        }

        // POST: permissions/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "PERMISSIONID,DOCUMENTID,DATESTART,DATEFINISH,ISVALID,LAST_UPDATE")] Permission permission)
        {
            if (ModelState.IsValid)
            {
                // Block for audit. Setting needed variables and objects.
                string userId = User.Identity.GetUserId();
                ApplicationUser user = userManager.FindById(userId);
                Inspector inspector = Mapper.Map<Inspector>(_inspectorService.GetInspector((short)user.PERSONID));
                UserInforManager userInforManager = new UserInforManager(inspector, user.UserName);
                string userInfo = userInforManager.GetUserInfo();

                _permissionService.AddPermission(Mapper.Map<PERMISSION>(permission), userId: User.Identity.GetUserId() ?? string.Empty, userInfo: userInfo ?? string.Empty);

                // Set null for unused variables.
                userId = null;
                user = null;
                inspector = null;
                userInforManager = null;
                userInfo = null;

                return RedirectToAction("Index");
            }


            ViewBag.DOCUMENTKINDID = new SelectList(Mapper.Map<IEnumerable<DOCUMENTKIND>, IEnumerable<Documentkind>>(_documentkindService.GetAllDocumentkinds()), "DOCUMENTKINDID", "NAME", 1);
            ViewBag.DOCUMENTID = new SelectList(Mapper.Map<IEnumerable<DOCUMENT>, IEnumerable<Document>>(_documentService.GetAllDocuments()), "DOCUMENTID", "NUMBER", permission.DOCUMENTID);
            return View(permission);
        }

        // GET: permissions/Edit/5
        public ActionResult Edit(short id)
        {
            PERMISSION permission = _permissionService.GetPermission(id);
            if (permission == null)
            {
                return HttpNotFound();
            }
            ViewBag.DOCUMENTID = new SelectList(Mapper.Map<IEnumerable<DOCUMENT>, IEnumerable<Document>>(_documentService.GetAllDocuments()), "DOCUMENTID", "NUMBER", permission.DOCUMENTID);
            return View(Mapper.Map<Permission>(permission));
        }

        // POST: permissions/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "PERMISSIONID,DOCUMENTID,DATESTART,DATEFINISH,ISVALID,LAST_UPDATE")] Permission permission)
        {
            if (ModelState.IsValid)
            {
                // Block for audit. Setting needed variables and objects.
                string userId = User.Identity.GetUserId();
                ApplicationUser user = userManager.FindById(userId);
                Inspector inspector = Mapper.Map<Inspector>(_inspectorService.GetInspector((short)user.PERSONID));
                UserInforManager userInforManager = new UserInforManager(inspector, user.UserName);
                string userInfo = userInforManager.GetUserInfo();

                _permissionService.UpdatePermission(Mapper.Map<PERMISSION>(permission), userId: User.Identity.GetUserId() ?? string.Empty, userInfo: userInfo ?? string.Empty);

                // Set null for unused variables.
                userId = null;
                user = null;
                inspector = null;
                userInforManager = null;
                userInfo = null;

                return RedirectToAction("Index");
            }
            ViewBag.DOCUMENTID = new SelectList(Mapper.Map<IEnumerable<DOCUMENT>, IEnumerable<Document>>(_documentService.GetAllDocuments()), "DOCUMENTID", "NUMBER", permission.DOCUMENTID);
            return View(permission);
        }

        // GET: permissions/Delete/5
        public ActionResult Delete(short id)
        {
            PERMISSION permission = _permissionService.GetPermission(id);
            if (permission == null)
            {
                return HttpNotFound();
            }
            return View(Mapper.Map<Permission>(permission));
        }


        // GET: Permission/Delete/5
        public ActionResult PartialDelete(short id)
        {
            PERMISSION permission = _permissionService.GetPermission(id);
            if (permission == null)
            {
                return HttpNotFound();
            }
            return PartialView(Mapper.Map<Permission>(permission));
        }

        // POST: Permission/Delete/5
        [HttpPost, ActionName("PartialDelete")]
        [ValidateAntiForgeryToken]
        public ActionResult PartialDeleteConfirmed(short id)
        {
            PERMISSION permission = _permissionService.GetPermission(id);

            // Block for audit. Setting needed variables and objects.
            string userId = User.Identity.GetUserId();
            ApplicationUser user = userManager.FindById(userId);
            Inspector inspector = Mapper.Map<Inspector>(_inspectorService.GetInspector((short)user.PERSONID));
            UserInforManager userInforManager = new UserInforManager(inspector, user.UserName);
            string userInfo = userInforManager.GetUserInfo();

            _permissionService.DeletePermission(id, userId: User.Identity.GetUserId() ?? string.Empty, userInfo: userInfo ?? string.Empty);

            // Set null for unused variables.
            userId = null;
            user = null;
            inspector = null;
            userInforManager = null;
            userInfo = null;

            return PartialView("PermissionDeleted", Mapper.Map<Permission>(permission));
        }

        // POST: Permission/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult PermissionDeleted(short id)
        {
            if (_permissionService.PermissionExists(p => p.PERMISSIONID == id))
            {
                // Block for audit. Setting needed variables and objects.
                string userId = User.Identity.GetUserId();
                ApplicationUser user = userManager.FindById(userId);
                Inspector inspector = Mapper.Map<Inspector>(_inspectorService.GetInspector((short)user.PERSONID));
                UserInforManager userInforManager = new UserInforManager(inspector, user.UserName);
                string userInfo = userInforManager.GetUserInfo();

                _permissionService.DeletePermission(id, userId: User.Identity.GetUserId() ?? string.Empty, userInfo: userInfo ?? string.Empty);

                // Set null for unused variables.
                userId = null;
                user = null;
                inspector = null;
                userInforManager = null;
                userInfo = null;
            }
            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }
         

        // POST: permissions/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(short id)
        {
            PERMISSION permission = _permissionService.GetPermission(id);

            // Block for audit. Setting needed variables and objects.
            string userId = User.Identity.GetUserId();
            ApplicationUser user = userManager.FindById(userId);
            Inspector inspector = Mapper.Map<Inspector>(_inspectorService.GetInspector((short)user.PERSONID));
            UserInforManager userInforManager = new UserInforManager(inspector, user.UserName);
            string userInfo = userInforManager.GetUserInfo();


            _permissionService.DeletePermission(id, userId: User.Identity.GetUserId() ?? string.Empty, userInfo: userInfo ?? string.Empty);

            // Set null for unused variables.
            userId = null;
            user = null;
            inspector = null;
            userInforManager = null;
            userInfo = null;

            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _permissionService.Dispose();
                _documentService.Dispose();
                _documentkindService.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
