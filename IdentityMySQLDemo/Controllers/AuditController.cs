﻿using AutoMapper;
using Focus.BL.Common.Services;
using Focus.DAL.Common.DbModels;
using Focus.Web.Extensions;
using Focus.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace IdentityMySQLDemo.Controllers
{
    [Authorize(Roles = "admin, auditor")]
    public class AuditController : Controller
    {
        private readonly IAuditDocService _auditDocService;
        private readonly IAuditOrganizationService _auditOrganizationService;
        private readonly IAuditCustombrockerService _auditCustombrockerService;
        private readonly IAuditDocumentService _auditDocumentService;

        public AuditController(IAuditDocService auditDocService, IAuditOrganizationService auditOrganizationService, IAuditCustombrockerService auditCustombrockerService,
             IAuditDocumentService auditDocumentService)
        {
            _auditDocService = auditDocService;
            _auditOrganizationService = auditOrganizationService;
            _auditCustombrockerService = auditCustombrockerService;
            _auditDocumentService = auditDocumentService;
        }

        // GET: Audit
        public ActionResult Doc(short id)
        {
            IEnumerable<DOC> docVersions = _auditDocService.GetDocVersions(id);
            Optimizer<DOC> optimizer = new Optimizer<DOC>();
            IEnumerable<DOC> docsMinified = optimizer.Optimize(docVersions);
            IEnumerable<Doc> docs = Mapper.Map<IEnumerable<DOC>, IEnumerable<Doc>>(docsMinified).OrderByDescending(d => d.LAST_UPDATE);
            ViewBag.IsAuditor = false;
            return View(docs);
        }

        // GET: Audit
        public ActionResult PartialDoc(short id, List<string> markup)
        {
            // Select only checked versions.
            IEnumerable<DOC> docVersions = _auditDocService.GetDocVersions(id).Where(v => markup.Select(m => Convert.ToDateTime(m.Replace("_", " "))).Contains(v.LAST_UPDATE));
            // Optimize entity size.
            IEnumerable<Doc> docs = Mapper.Map<IEnumerable<DOC>, IEnumerable<Doc>>(docVersions).OrderByDescending(d => d.LAST_UPDATE);
            return PartialView(docs);
        }

        // GET: Audit
        public ActionResult Organization(short id)
        {
            IEnumerable<ORGANIZATION> organizationVersions = _auditOrganizationService.GetOrganizationVersions(id);
            IEnumerable<Organization> organizations = Mapper.Map<IEnumerable<ORGANIZATION>, IEnumerable<Organization>>(organizationVersions).OrderByDescending(d => d.LAST_UPDATE);
            ViewBag.IsAuditor = false;
            return View(organizations);
        }

        // GET: Audit
        public ActionResult PartialOrganization(short id, List<string> markup)
        {
            // Select only checked versions.
            IEnumerable<ORGANIZATION> organizationVersions = _auditOrganizationService.GetOrganizationVersions(id).Where(v => markup.Select(m => Convert.ToDateTime(m.Replace("_", " "))).Contains(v.LAST_UPDATE));
            // Optimize entity size.
            IEnumerable<Organization> organizations = Mapper.Map<IEnumerable<ORGANIZATION>, IEnumerable<Organization>>(organizationVersions).OrderByDescending(d => d.LAST_UPDATE);
            return PartialView(organizations);
        }

        // GET: Audit
        public ActionResult Custombrocker(short id)
        {
            IEnumerable<CUSTOMBROCKER> custombrockerVersions = _auditCustombrockerService.GetCustombrockerVersions(id);
            IEnumerable<Custombrocker> custombrockers = Mapper.Map<IEnumerable<CUSTOMBROCKER>, IEnumerable<Custombrocker>>(custombrockerVersions).OrderByDescending(d => d.LAST_UPDATE);
            ViewBag.IsAuditor = false;
            return View(custombrockers);
        }

        // GET: Audit
        public ActionResult PartialCustombrocker(short id, List<string> markup)
        {
            // Select only checked versions.
            IEnumerable<CUSTOMBROCKER> custombrockerVersions = _auditCustombrockerService.GetCustombrockerVersions(id).Where(v => markup.Select(m => Convert.ToDateTime(m.Replace("_", " "))).Contains(v.LAST_UPDATE));
            // Optimize entity size.
            IEnumerable<Custombrocker> custombrockers = Mapper.Map<IEnumerable<CUSTOMBROCKER>, IEnumerable<Custombrocker>>(custombrockerVersions).OrderByDescending(d => d.LAST_UPDATE);
            return PartialView(custombrockers);
        }

        // GET: Audit
        public ActionResult Document(short id)
        {
            IEnumerable<DOCUMENT> documentVersions = _auditDocumentService.GetDocumentVersions(id);
            IEnumerable<Document> documents = Mapper.Map<IEnumerable<DOCUMENT>, IEnumerable<Document>>(documentVersions).OrderByDescending(d => d.LAST_UPDATE);
            ViewBag.IsAuditor = false;
            return View(documents);
        }

        // GET: Audit
        public ActionResult PartialDocument(short id, List<string> markup)
        {
            // Select only checked versions.
            IEnumerable<DOCUMENT> documentVersions = _auditDocumentService.GetDocumentVersions(id).Where(v => markup.Select(m => Convert.ToDateTime(m.Replace("_", " "))).Contains(v.LAST_UPDATE));
            // Optimize entity size.
            IEnumerable<Document> documents = Mapper.Map<IEnumerable<DOCUMENT>, IEnumerable<Document>>(documentVersions).OrderByDescending(d => d.LAST_UPDATE);
            return PartialView(documents);
        }
    }
}