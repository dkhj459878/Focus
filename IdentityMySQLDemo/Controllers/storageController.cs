﻿using AspNet.Identity.MySQL;
using AutoMapper;
using Focus.BL.Common.Services;
using Focus.DAL.Common.DbModels;
using Focus.DAL.Extensions;
using Focus.Web.Models;
using IdentityMySQLDemo.Models;
using Focus.Web.Helpers;
using log4net;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Web.Mvc;
using Messages = IdentityMySQLDemo.Properties.Resources;

namespace Focus.Controllers
{
    [Authorize(Roles = "inspection, admin, auditor")]
    public class storageController : Controller
    {
        protected static readonly ILog _logger =
            LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private ApplicationDbContext db = new ApplicationDbContext("OracleDbContext");

        UserManager<ApplicationUser> userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(new ApplicationDbContext("OracleDbContext")));
        RoleManager<IdentityRole> RoleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(new ApplicationDbContext("OracleDbContext")));

        private readonly IPostService _postService;
        private readonly IStorageService _storageService;
        private readonly IInspectorService _inspectorService;

        public storageController(IPostService postService, IStorageService storageService, IInspectorService inspectorService)
        {
            _postService = postService;
            _storageService = storageService;
            _inspectorService = inspectorService;
        }

        // Remote diagnostics.
        // GET: storage /NotRepeatedName/
        public JsonResult NotRepeatedName(string NAME, int? ID)
        {
            if (IsEniqueName(NAME, ID))
            {
                return Json(true,
                    JsonRequestBehavior.AllowGet);
            }

            return Json(
                    string.Format(
                        Messages.ErrorRemoteStorageNameRepeated,
                        NAME ?? string.Empty),
                        JsonRequestBehavior.AllowGet);
        }

        // Remote diagnostics.
        // GET: storage /NotRepeatedNumber/
        public JsonResult NotRepeatedNumber(string NUMBER_, int? ID)
        {
            if (IsEniqueNumber(NUMBER_, ID))
            {
                return Json(true,
                    JsonRequestBehavior.AllowGet);
            }

            return Json(
                    string.Format(
                        Messages.ErrorRemoteStorageNumberRepeated,
                        NUMBER_ ?? string.Empty),
                        JsonRequestBehavior.AllowGet);
        }

        private bool IsEniqueName(string name, int? id)
        {
            if (id == null)
            {
                return !_storageService.StorageExists(
                    i => i.NAME == (name == string.Empty ? null : name));
            }

            return !_storageService.StorageExists(
                    i => i.NAME == (name == string.Empty ? null : name) &&
                    i.ID != id);
        }

        private bool IsEniqueNumber(string number, int? id)
        {
            if (id == null)
            {
                return !_storageService.StorageExists(
                    i => i.NUMBER_ == (number == string.Empty ? null : number));
            }

            return !_storageService.StorageExists(
                    i => i.NUMBER_ == (number == string.Empty ? null : number) &&
                    i.ID != id);
        }


        // GET: Storage
        public ActionResult Index()
        {
            IEnumerable<STORAGE> storage = _storageService.GetAllStorages(includeProperties: "post");
            return View(Mapper.Map<IEnumerable<STORAGE>, IEnumerable<Storage>>(storage.ToList()));
        }

        [HttpPost]
        public ActionResult Search(string name)
        {

            if (string.IsNullOrEmpty(name))
            {
                return Content($"<br /><p><b>Поисковая строка не должна быть пустой. Введите в поисковую строку тест, содержащий более <span style = 'background-color: #ffff00; color: Red;'>4</span> символов, исключая пробелы, и вновь нажмите клавишу «Ввод» либо кнопку «Поиск».</b></p>");
            }
            if (name.Trim().Length < 5)
            {
                return Content($"<br /><p><b>Поисковая строка должна содержать не менее 5 символов, за исключением пробела. Введите в поисковую строку тест, содержащий более <span style = 'background-color: #ffff00; color: Red;'>4</span> символов, исключая пробелы, и вновь нажмите клавишу «Ввод» либо кнопку «Поиск».</b></p>");
            }

            List<STORAGE> storages = _storageService.GetAllStorages(filter: o => o.NAME.Contains(name)).ToList();
            if (storages.Count <= 0)
            {
                return Content($"<br /><p><b>СВХ с частью наименований <span style = 'background-color: #ffff00;'>{name}</span> в базе данных не обнаружены.</b></p>");
            }
            return PartialView(Mapper.Map<IEnumerable<STORAGE>, IEnumerable<Storage>>(storages));
        }

        // GET: Storage/Details/5
        public ActionResult Details(byte id)
        {
            STORAGE storage = _storageService.GetStorage(id);
            if (storage == null)
            {
                return HttpNotFound();
            }
            return View(Mapper.Map<Storage>(storage));
        }

        public JsonResult JsonOrg(byte id)
        {
            var jsondata = _storageService.GetStorage(id);

            return Json(
                new
                {
                    id = jsondata?.ID,
                    storagename = jsondata?.NAME,
                    storageaddress = jsondata?.ADDRESS,
                    storagenumber = jsondata?.NUMBER_
                },
                JsonRequestBehavior.AllowGet);
        }

        public ActionResult PartialCreate()
        {
            ViewBag.POSTID = new SelectList(Mapper.Map<IEnumerable<POST>, IEnumerable<Post>>(_postService.GetAllPosts()), "ID", "CODE");
            return PartialView();
        }

        // POST: storage/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult PartialCreate([Bind(Include = "ID,NAME,ADDRESS,NUMBER,LAST_UPDATE")] Storage storage)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    // Block for audit. Setting needed variables and objects.
                    string userId = User.Identity.GetUserId();
                    ApplicationUser user = userManager.FindById(userId);
                    Inspector inspector = Mapper.Map<Inspector>(_inspectorService.GetInspector((short)user.PERSONID));
                    UserInforManager userInforManager = new UserInforManager(inspector, user.UserName);
                    string userInfo = userInforManager.GetUserInfo();

                    // Set last update date of the entity.
                    storage.LAST_UPDATE = DateTime.Now;

                    _storageService.AddStorage(Mapper.Map<STORAGE>(storage), userId: User.Identity.GetUserId() ?? string.Empty, userInfo: userInfo ?? string.Empty);

                    // Set null for unused variables.
                    userId = null;
                    user = null;
                    inspector = null;
                    userInforManager = null;
                    userInfo = null;
                }
                catch (DbEntityValidationException e)
                {
                    _logger.Error(e.GetDetails(), e);
                    throw;
                }
                ViewBag.POSTID = new SelectList(Mapper.Map<IEnumerable<POST>, IEnumerable<Post>>(_postService.GetAllPosts()), "ID", "CODE");
                return RedirectToAction("Index");
            }

            return PartialView(storage);
        }

        // GET: storage/Create
        public ActionResult Create()
        {
            ViewBag.POSTID = new SelectList(Mapper.Map<IEnumerable<POST>, IEnumerable<Post>>(_postService.GetAllPosts()), "ID", "CODE");
            return View();
        }

        // POST: storage/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,NAME,ADDRESS,NUMBER,LAST_UPDATE")] Storage storage)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    // Block for audit. Setting needed variables and objects.
                    string userId = User.Identity.GetUserId();
                    ApplicationUser user = userManager.FindById(userId);
                    Inspector inspector = Mapper.Map<Inspector>(_inspectorService.GetInspector((short)user.PERSONID));
                    UserInforManager userInforManager = new UserInforManager(inspector, user.UserName);
                    string userInfo = userInforManager.GetUserInfo();

                    // Set last update date of the entity.
                    storage.LAST_UPDATE = DateTime.Now;

                    _storageService.AddStorage(Mapper.Map<STORAGE>(storage), userId: User.Identity.GetUserId() ?? string.Empty, userInfo: userInfo ?? string.Empty);

                    // Set null for unused variables.
                    userId = null;
                    user = null;
                    inspector = null;
                    userInforManager = null;
                    userInfo = null;
                }
                catch (DbEntityValidationException e)
                {
                    _logger.Error(e.GetDetails(), e);
                    throw;
                }
                ViewBag.POSTID = new SelectList(Mapper.Map<IEnumerable<POST>, IEnumerable<Post>>(_postService.GetAllPosts()), "ID", "CODE");
                return RedirectToAction("Index");
            }
            
            return View(storage);
        }

        // GET: storage/Edit/5
        public ActionResult Edit(byte id)
        {
            STORAGE storage = _storageService.GetStorage(id);
            if (storage == null)
            {
                return HttpNotFound();
            }
            ViewBag.POSTID = new SelectList(Mapper.Map<IEnumerable<POST>, IEnumerable<Post>>(_postService.GetAllPosts()), "ID", "CODE", storage.POST.FirstOrDefault().ID);
            return View(Mapper.Map<Storage>(storage));
        }

        // POST: storage/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,NAME,ADDRESS,NUMBER,LAST_UPDATE")] Storage storage)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    // Block for audit. Setting needed variables and objects.
                    string userId = User.Identity.GetUserId();
                    ApplicationUser user = userManager.FindById(userId);
                    Inspector inspector = Mapper.Map<Inspector>(_inspectorService.GetInspector((short)user.PERSONID));
                    UserInforManager userInforManager = new UserInforManager(inspector, user.UserName);
                    string userInfo = userInforManager.GetUserInfo();

                    // Set last update date of the entity.
                    storage.LAST_UPDATE = DateTime.Now;

                    _storageService.UpdateStorage(Mapper.Map<STORAGE>(storage), userId: User.Identity.GetUserId() ?? string.Empty, userInfo: userInfo ?? string.Empty);

                    // Set null for unused variables.
                    userId = null;
                    user = null;
                    inspector = null;
                    userInforManager = null;
                    userInfo = null;

                }
                catch (DbEntityValidationException e)
                {
                    _logger.Error(e.GetDetails(), e);
                    throw;
                }
                ViewBag.POSTID = new SelectList(Mapper.Map<IEnumerable<POST>, IEnumerable<Post>>(_postService.GetAllPosts()), "ID", "CODE");
                return RedirectToAction("Index");
            }
            return View(storage);
        }

        // GET: storage/Edit/5
        public ActionResult PartialEdit(byte id)
        {
            STORAGE storage = _storageService.GetStorage(id);
            if (storage == null)
            {
                return HttpNotFound();
            }
            ViewBag.POSTID = new SelectList(Mapper.Map<IEnumerable<POST>, IEnumerable<Post>>(_postService.GetAllPosts()), "ID", "CODE", storage.POST.FirstOrDefault().ID);
            return PartialView(Mapper.Map<Storage>(storage));
        }

        // POST: storage/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult PartialEdit([Bind(Include = "ID,NAME,ADDRESS,NUMBER,LAST_UPDATE")] Storage storage)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    // Block for audit. Setting needed variables and objects.
                    string userId = User.Identity.GetUserId();
                    ApplicationUser user = userManager.FindById(userId);
                    Inspector inspector = Mapper.Map<Inspector>(_inspectorService.GetInspector((short)user.PERSONID));
                    UserInforManager userInforManager = new UserInforManager(inspector, user.UserName);
                    string userInfo = userInforManager.GetUserInfo();

                    // Set last update date of the entity.
                    storage.LAST_UPDATE = DateTime.Now;

                    _storageService.UpdateStorage(Mapper.Map<STORAGE>(storage), userId: User.Identity.GetUserId() ?? string.Empty, userInfo: userInfo ?? string.Empty);

                    // Set null for unused variables.
                    userId = null;
                    user = null;
                    inspector = null;  
                    userInforManager = null;
                    userInfo = null;
                }
                catch (DbEntityValidationException e)
                {
                    _logger.Error(e.GetDetails(), e);
                    throw;
                }
                ViewBag.POSTID = new SelectList(Mapper.Map<IEnumerable<POST>, IEnumerable<Post>>(_postService.GetAllPosts()), "ID", "CODE");
                return RedirectToAction("Index");
            }
            return PartialView(storage);
        }

        // GET: storage/Delete/5
        public ActionResult Delete(byte id)
        {
            STORAGE storage = _storageService.GetStorage(id);
            if (storage == null)
            {
                return HttpNotFound();
            }
            return View(Mapper.Map<Storage>(storage));
        }

        // POST: storage/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(byte id)
        {
            STORAGE storage = _storageService.GetStorage(id);

            // Block for audit. Setting needed variables and objects.
            string userId = User.Identity.GetUserId();
            ApplicationUser user = userManager.FindById(userId);
            Inspector inspector = Mapper.Map<Inspector>(_inspectorService.GetInspector((short)user.PERSONID));
            UserInforManager userInforManager = new UserInforManager(inspector, user.UserName);
            string userInfo = userInforManager.GetUserInfo();

            _storageService.DeleteStorage(id, userId: User.Identity.GetUserId() ?? string.Empty, userInfo: userInfo ?? string.Empty);

            // Set null for unused variables.
            userId = null;
            user = null;
            inspector = null;
            userInforManager = null;
            userInfo = null;

            return RedirectToAction("Index");
        }

        // GET: vehicles/JsonDelete/5
        public ActionResult JsonDelete(byte id)
        {
            STORAGE jsondata = _storageService.GetStorage(id);

            var storageJSON = $"{jsondata.NAME} {jsondata.NUMBER_} {jsondata.ADDRESS}";

            var storageCount = jsondata.POST?.Count;

            return Json(
               new
               {
                   storageId = id,
                   storageName = storageJSON,

                   storageCount,
                   posts = jsondata.POST.Select(o => $"ПТО с кодом '{o.CODE}'")
               },
               JsonRequestBehavior.AllowGet);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _postService.Dispose();
                _storageService.Dispose();
                if (db != null)
                {
                    db.Dispose();
                    db = null;
                }

                if (userManager != null)
                {

                    userManager.Dispose();
                    userManager = null;
                }

                if (RoleManager != null)
                {

                    RoleManager.Dispose();
                    RoleManager = null;
                }
            }
            base.Dispose(disposing);
        }
    }
}
