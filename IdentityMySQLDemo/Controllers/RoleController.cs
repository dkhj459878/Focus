﻿using AspNet.Identity.MySQL;
using IdentityMySQLDemo.Models;
using Microsoft.AspNet.Identity;
using System.Linq;
using System.Net;
using System.Web.Mvc;

namespace IdentityMySQLDemo.Controllers
{
    [Authorize(Roles = "admin")]
    public class RoleController : Controller
    {

        private ApplicationDbContext db = new ApplicationDbContext("OracleDbContext");

        RoleManager<IdentityRole> RoleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(new ApplicationDbContext("OracleDbContext")));

        public RoleController()
        {

        }

        // GET: IdentityRoles
        public ActionResult Index()
        {
            return View(RoleManager.Roles.ToList());
        }

        // GET: IdentityRoles/Details/5
        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            IdentityRole identityRole = RoleManager.FindById(id);
            if (identityRole == null)
            {
                return HttpNotFound();
            }
            return View(identityRole);
        }

        // GET: IdentityRoles/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: IdentityRoles/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(string Name)
        {
            if (ModelState.IsValid)
            {
                var role = new IdentityRole(Name);
                RoleManager.Create(role);
                return RedirectToAction("Index");
            }

            return View();
        }

        // GET: IdentityRoles/Edit/5
        public ActionResult Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            IdentityRole identityRole = RoleManager.FindById(id);
            if (identityRole == null)
            {
                return HttpNotFound();
            }
            return View(identityRole);
        }

        // POST: IdentityRoles/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Name")] IdentityRole identityRole)
        {
            if (ModelState.IsValid)
            {
                var role = RoleManager.FindById(identityRole.Id);
                if (role != null)
                {
                    RoleManager.Update(identityRole);
                }
                return RedirectToAction("Index");
            }
            return View(identityRole);
        }

        // GET: IdentityRoles/Delete/5
        public ActionResult Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            IdentityRole identityRole = RoleManager.FindById(id);
            if (identityRole == null)
            {
                return HttpNotFound();
            }
            return View(identityRole);
        }

        // POST: IdentityRoles/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            IdentityRole identityRole = RoleManager.FindById(id);
            if (identityRole != null)
            {
                RoleManager.Delete(identityRole);
            }
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (db != null)
                {
                    db.Dispose();
                    db = null;
                }

                if (RoleManager != null)
                {

                    RoleManager.Dispose();
                    RoleManager = null;
                }
               
            }

            base.Dispose(disposing);
        }
    }
}
