﻿using AspNet.Identity.MySQL;
using AutoMapper;
using Focus.BL.Common.Services;
using Focus.DAL.Common.DbModels;
using Focus.Web.Helpers;
using Focus.Web.Models;
using IdentityMySQLDemo.Models;
using log4net;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.Mvc;
using Messages = IdentityMySQLDemo.Properties.Resources;

namespace Focus.Controllers
{
    [Authorize(Roles = "inspection, admin, redactor, auditor")]
    public class CarController : Controller
    {
        protected static readonly ILog _logger =
            LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private ApplicationDbContext db = new ApplicationDbContext("OracleDbContext");

        UserManager<ApplicationUser> userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(new ApplicationDbContext("OracleDbContext")));
        RoleManager<IdentityRole> RoleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(new ApplicationDbContext("OracleDbContext")));

        private readonly ICarService _carService;
        private readonly ICountryService _countryService;
        private readonly IInspectorService _inspectorService;
        private readonly IVehiclekindService _vehiclekindService;

        public CarController(ICarService carService, ICountryService countryService, IVehiclekindService vehiclekindService, IInspectorService inspectorService)
        {
            _carService = carService;
            _countryService = countryService;
            _inspectorService = inspectorService;
            _vehiclekindService = vehiclekindService;
        }

        public enum MotorFuelItem
        {
            БЕНЗИНОВЫЙ,
            ДИЗЕЛЬНЫЙ,
            ГИБРИДНЫЙ
        }

        // Remote diagnostics.
        // GET: Car /NotRepeatedVIN/
        public ActionResult NotRepeatedVIN(string VIN, int? CARID)
        {
            if (IsUnique(VIN, CARID))
            {
                return Json(true, JsonRequestBehavior.AllowGet);
            }

            return Json(
                   string.Format(
                       Messages.ErrorRemoteCarRepeated,
                       VIN
                       ), JsonRequestBehavior.AllowGet);

        }

        private bool IsUnique(string vin, int? id)
        {
            if (id == null)
            {
                return !_carService.CarExists
                (filter: c => c.VIN == vin);
            }

            return !_carService.CarExists
                (filter: c => c.VIN == vin &&
                c.CARID != id);
        }

        // GET: Car
        public ActionResult Index()
        {
            var cars = _carService.GetAllCars(includeProperties: "country,vehiclekind");
            return View("Index", Mapper.Map<IEnumerable<CAR>, IEnumerable<Car>>(cars.ToList()));
        }

        [HttpPost]
        public ActionResult Search(string vin, int[] carSelectedIndexes)
        {
            int[] selectedIndexes = carSelectedIndexes;
            IEnumerable<CAR> carsFiltered;
            var cars = _carService.GetAllCars(filter: o => o.VIN.Contains(vin)).ToList();
            if (cars.Count <= 0)
            {
                return HttpNotFound();
            }
            if (selectedIndexes != null)
            {
                carsFiltered = cars.Where(x => !carSelectedIndexes.Contains(x.CARID)).ToArray();
                if (carsFiltered.Count() <= 0)
                {
                    return HttpNotFound();
                }
                return PartialView(Mapper.Map<IEnumerable<CAR>, IEnumerable<Car>>(carsFiltered));
            }
            return PartialView(Mapper.Map<IEnumerable<CAR>, IEnumerable<Car>>(cars));
        }

        public ActionResult PartialCreate()
        {
            //Creating generic list
            List<SelectListItem> ObjList = new List<SelectListItem>()
            {
                new SelectListItem { Text = "БЕНЗИНОВЫЙ", Value = "БЕНЗИНОВЫЙ" },
                new SelectListItem { Text = "ДИЗЕЛЬНЫЙ", Value = "ДИЗЕЛЬНЫЙ" },
                new SelectListItem { Text = "ГИБРИДНЫЙ", Value ="ГИБРИДНЫЙ"}

            };
            //Assigning generic list to ViewBag
            ViewBag.MOTORFUELTYPE = new SelectList(ObjList, "Value", "Text");
            ViewBag.COUNTRYCODE = new SelectList(Mapper.Map<IEnumerable<COUNTRY>, IEnumerable<Country>>(_countryService.GetAllCountries()), "ABBR2", "NAME");
            ViewBag.VEHICLEKINDID = new SelectList(Mapper.Map<IEnumerable<VEHICLEKIND>, IEnumerable<Vehiclekind>>(_vehiclekindService.GetAllVehiclekinds()), "VEHICLEKINDID", "NAME");
            return PartialView();
        }
            
        // POST: Car/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult PartialCreate([Bind(Include = "CARID, VEHICLEKINDID,COUNTRYCODE,VIN,DESCRIPTION,GOODSMARK,GOODSMODEL,MOTORFUELTYPE,ENGINECAPACITY,LAST_UPDATE")] Car car)
        {
            if (ModelState.IsValid)
            {
                // Block for audit. Setting needed variables and objects.
                string userId = User.Identity.GetUserId();
                ApplicationUser user = userManager.FindById(userId);
                Inspector inspector = Mapper.Map<Inspector>(_inspectorService.GetInspector((short)user.PERSONID));
                UserInforManager userInforManager = new UserInforManager(inspector, user.UserName);
                string userInfo = userInforManager.GetUserInfo();

                // Set last update date of the entity.
                car.LAST_UPDATE = DateTime.Now;

                car.CARID  = _carService.AddCar(Mapper.Map<CAR>(car), userId: User.Identity.GetUserId() ?? string.Empty, userInfo: userInfo ?? string.Empty);

                // Set null for unused variables.
                userId = null;
                user = null;
                inspector = null;
                userInforManager = null;
                userInfo = null;

                return PartialView("Search", new List<Car>() { car });
            }
            //Creating generic list
            List<SelectListItem> ObjList = new List<SelectListItem>()
            {
                new SelectListItem { Text = "БЕНЗИНОВЫЙ", Value = "БЕНЗИНОВЫЙ" },
                new SelectListItem { Text = "ДИЗЕЛЬНЫЙ", Value = "ДИЗЕЛЬНЫЙ" },
                new SelectListItem { Text = "ГИБРИДНЫЙ", Value ="ГИБРИДНЫЙ"}

            };
            //Assigning generic list to ViewBag
            ViewBag.MOTORFUELTYPE = new SelectList(ObjList, "Value", "Text");
            ViewBag.COUNTRYCODE = new SelectList(Mapper.Map<IEnumerable<COUNTRY>, IEnumerable<Country>>(_countryService.GetAllCountries()), "ABBR2", "NAME");
            ViewBag.VEHICLEKINDID = new SelectList(Mapper.Map<IEnumerable<VEHICLEKIND>, IEnumerable<Vehiclekind>>(_vehiclekindService.GetAllVehiclekinds()), "VEHICLEKINDID", "NAME");
            return PartialView(car);
        }

        public JsonResult Json(short id)
        {
            var jsondata = _carService.GetCar(id);
            jsondata.COUNTRY = _countryService.GetCountry(jsondata.COUNTRYCODE);
            jsondata.VEHICLEKIND = _vehiclekindService.GetVehiclekind(jsondata.VEHICLEKINDID);

            return Json(
                new
                {
                    id = jsondata?.CARID,
                    vin = jsondata?.VIN,
                    desc = jsondata?.DESCRIPTION,
                    vehicleKind = jsondata.VEHICLEKIND?.NAME,
                    vehicleKindId = jsondata.VEHICLEKINDID,
                    countryCode = jsondata.COUNTRY?.NAME,
                    countryCode2 = jsondata.COUNTRY?.ABBR2,
                    mark = jsondata?.GOODSMARK,
                    model = jsondata?.GOODSMODEL,
                    motorFuelType = jsondata?.MOTORFUELTYPE,
                    capacityEngine = jsondata?.ENGINECAPACITY
                },
                JsonRequestBehavior.AllowGet);
        }

        public JsonResult JsonOrg(short id)
        {
            var jsondata = _carService.GetCar(id);

            return Json(
                new
                {
                    id = jsondata?.CARID,
                    vin = jsondata?.VIN,
                    desc = jsondata?.DESCRIPTION,
                    vehicleKindId = jsondata?.VEHICLEKINDID,
                    vehicleKineName = jsondata?.VEHICLEKIND.NAME,
                    countryCode = jsondata?.COUNTRYCODE,
                    countryCode2 = jsondata.COUNTRY?.ABBR2,
                    mark = jsondata?.GOODSMARK,
                    model = jsondata?.GOODSMODEL,
                    motorFuelType = jsondata?.MOTORFUELTYPE,
                    capacityEngine = jsondata?.ENGINECAPACITY
                },
                JsonRequestBehavior.AllowGet);
        }


        // GET: Car/Details/5
        public ActionResult Details(short id)
        {
            CAR car = _carService.GetCar(id);
            if (car == null)
            {
                return HttpNotFound();
            }
            return View(Mapper.Map<Car>(car));
        }

        // GET: Car/Create
        public ActionResult Create()
        {
            //Creating generic list
            List<SelectListItem> ObjList = new List<SelectListItem>()
            {
                new SelectListItem { Text = "БЕНЗИНОВЫЙ", Value = "БЕНЗИНОВЫЙ" },
                new SelectListItem { Text = "ДИЗЕЛЬНЫЙ", Value = "ДИЗЕЛЬНЫЙ" },
                new SelectListItem { Text = "ГИБРИДНЫЙ", Value ="ГИБРИДНЫЙ"}

            };
            //Assigning generic list to ViewBag
            ViewBag.MOTORFUELTYPE = new SelectList(ObjList, "Value", "Text");
            ViewBag.COUNTRYCODE = new SelectList(Mapper.Map<IEnumerable<COUNTRY>, IEnumerable<Country>>(_countryService.GetAllCountries()), "ABBR2", "NAME");
            ViewBag.VEHICLEKINDID = new SelectList(Mapper.Map<IEnumerable<VEHICLEKIND>, IEnumerable<Vehiclekind>>(_vehiclekindService.GetAllVehiclekinds()), "VEHICLEKINDID", "NAME");
            return View();
        }

        // POST: Car/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "CARID,VEHICLEKINDID,COUNTRYCODE,VIN,DESCRIPTION,GOODSMARK,GOODSMODEL,MOTORFUELTYPE,ENGINECAPACITY,LAST_UPDATE")] Car car)
        {
            if (ModelState.IsValid)
            {
                // Block for audit. Setting needed variables and objects.
                string userId = User.Identity.GetUserId();
                ApplicationUser user = userManager.FindById(userId);
                Inspector inspector = Mapper.Map<Inspector>(_inspectorService.GetInspector((short)user.PERSONID));
                UserInforManager userInforManager = new UserInforManager(inspector, user.UserName);
                string userInfo = userInforManager.GetUserInfo();

                // Set last update date of the entity.
                car.LAST_UPDATE = DateTime.Now;
                
                _carService.AddCar(Mapper.Map<CAR>(car), userId: User.Identity.GetUserId() ?? string.Empty, userInfo: userInfo ?? string.Empty);

                // Set null for unused variables.
                userId = null;
                user = null;
                inspector = null;
                userInforManager = null;
                userInfo = null;

                // Go to index page.
                return RedirectToAction("Index");
            }
            
            
            
            //Creating generic list
            List<SelectListItem> ObjList = new List<SelectListItem>()
            {
                new SelectListItem { Text = "БЕНЗИНОВЫЙ", Value = "БЕНЗИНОВЫЙ" },
                new SelectListItem { Text = "ДИЗЕЛЬНЫЙ", Value = "ДИЗЕЛЬНЫЙ" },
                new SelectListItem { Text = "ГИБРИДНЫЙ", Value ="ГИБРИДНЫЙ"}

            };
            //Assigning generic list to ViewBag
            ViewBag.MOTORFUELTYPE = new SelectList(ObjList, "Value", "Text");
            ViewBag.COUNTRYCODE = new SelectList(Mapper.Map<IEnumerable<COUNTRY>, IEnumerable<Country>>(_countryService.GetAllCountries()), "ABBR2", "NAME");
            ViewBag.VEHICLEKINDID = new SelectList(Mapper.Map<IEnumerable<VEHICLEKIND>, IEnumerable<Vehiclekind>>(_vehiclekindService.GetAllVehiclekinds()), "VEHICLEKINDID", "NAME");

            return View(car);
        }

        // GET: Car/Edit/5
        public ActionResult Edit(short id)
        {
            CAR car = _carService.GetCar(id);
            if (car == null)
            {
                return HttpNotFound();
            }
            //Creating generic list
            List<SelectListItem> ObjList = new List<SelectListItem>()
            {
                new SelectListItem { Text = "БЕНЗИНОВЫЙ", Value = "БЕНЗИНОВЫЙ" },
                new SelectListItem { Text = "ДИЗЕЛЬНЫЙ", Value = "ДИЗЕЛЬНЫЙ" },
                new SelectListItem { Text = "ГИБРИДНЫЙ", Value ="ГИБРИДНЫЙ"}

            };
            //Assigning generic list to ViewBag
            ViewBag.MOTORFUELTYPE = new SelectList(ObjList, "Value", "Text", car.MOTORFUELTYPE);
            ViewBag.COUNTRYCODE = new SelectList(Mapper.Map<IEnumerable<COUNTRY>, IEnumerable<Country>>(_countryService.GetAllCountries()), "ABBR2", "NAME", car.COUNTRYCODE);
            ViewBag.VEHICLEKINDID = new SelectList(Mapper.Map<IEnumerable<VEHICLEKIND>, IEnumerable<Vehiclekind>>(_vehiclekindService.GetAllVehiclekinds()), "VEHICLEKINDID", "NAME", car.VEHICLEKINDID);
            return View(Mapper.Map<Car>(car));
        }

        // POST: Car/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "CARID,VEHICLEKINDID,COUNTRYCODE,VIN,DESCRIPTION,GOODSMARK,GOODSMODEL,MOTORFUELTYPE,ENGINECAPACITY,LAST_UPDATE")] Car car)
        {
            if (ModelState.IsValid)
            {
                // Block for audit. Setting needed variables and objects.
                string userId = User.Identity.GetUserId();
                ApplicationUser user = userManager.FindById(userId);
                Inspector inspector = Mapper.Map<Inspector>(_inspectorService.GetInspector((short)user.PERSONID));
                UserInforManager userInforManager = new UserInforManager(inspector, user.UserName);
                string userInfo = userInforManager.GetUserInfo();

                // Set last update date of the entity.
                car.LAST_UPDATE = DateTime.Now;

                _carService.UpdateCar(Mapper.Map<CAR>(car), userId: User.Identity.GetUserId() ?? string.Empty, userInfo: userInfo ?? string.Empty);

                // Set null for unused variables.
                userId = null;
                user = null;
                inspector = null;
                userInforManager = null;
                userInfo = null;

                return RedirectToAction("Index");
            }
            //Creating generic list
            List<SelectListItem> ObjList = new List<SelectListItem>()
            {
                new SelectListItem { Text = "БЕНЗИНОВЫЙ", Value = "БЕНЗИНОВЫЙ" },
                new SelectListItem { Text = "ДИЗЕЛЬНЫЙ", Value = "ДИЗЕЛЬНЫЙ" },
                new SelectListItem { Text = "ГИБРИДНЫЙ", Value ="ГИБРИДНЫЙ"}

            };
            //Assigning generic list to ViewBag
            ViewBag.MOTORFUELTYPE = new SelectList(ObjList, "Value", "Text");
            ViewBag.COUNTRYCODE = new SelectList(Mapper.Map<IEnumerable<COUNTRY>, IEnumerable<Country>>(_countryService.GetAllCountries()), "ABBR2", "NAME");
            ViewBag.VEHICLEKINDID = new SelectList(Mapper.Map<IEnumerable<VEHICLEKIND>, IEnumerable<Vehiclekind>>(_vehiclekindService.GetAllVehiclekinds()), "VEHICLEKINDID", "NAME");
            return View(car);
        }

        // GET: Car/Edit/5
        public ActionResult PartialEdit(short id)
        {
            CAR car = _carService.GetCar(id);
            if (car == null)
            {
                return HttpNotFound();
            }
            //Creating generic list
            List<SelectListItem> ObjList = new List<SelectListItem>()
            {
                new SelectListItem { Text = "БЕНЗИНОВЫЙ", Value = "БЕНЗИНОВЫЙ" },
                new SelectListItem { Text = "ДИЗЕЛЬНЫЙ", Value = "ДИЗЕЛЬНЫЙ" },
                new SelectListItem { Text = "ГИБРИДНЫЙ", Value ="ГИБРИДНЫЙ"}

            };
            //Assigning generic list to ViewBag
            ViewBag.MOTORFUELTYPE = new SelectList(ObjList, "Value", "Text", car.MOTORFUELTYPE);
            ViewBag.COUNTRYCODE = new SelectList(Mapper.Map<IEnumerable<COUNTRY>, IEnumerable<Country>>(_countryService.GetAllCountries()), "ABBR2", "NAME", car.COUNTRYCODE);
            ViewBag.VEHICLEKINDID = new SelectList(Mapper.Map<IEnumerable<VEHICLEKIND>, IEnumerable<Vehiclekind>>(_vehiclekindService.GetAllVehiclekinds()), "VEHICLEKINDID", "NAME", car.VEHICLEKINDID);
            return PartialView(Mapper.Map<Car>(car));
        }

        // POST: Car/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult PartialEdit(Car car)
        {
            if (ModelState.IsValid)
            {
                // Block for audit. Setting needed variables and objects.
                string userId = User.Identity.GetUserId();
                ApplicationUser user = userManager.FindById(userId);
                Inspector inspector = Mapper.Map<Inspector>(_inspectorService.GetInspector((short)user.PERSONID));
                UserInforManager userInforManager = new UserInforManager(inspector, user.UserName);
                string userInfo = userInforManager.GetUserInfo();

                // Set last update date of the entity.
                car.LAST_UPDATE = DateTime.Now;

                _carService.UpdateCar(Mapper.Map<CAR>(car), userId: User.Identity.GetUserId() ?? string.Empty, userInfo: userInfo ?? string.Empty);

                // Set null for unused variables.
                userId = null;
                user = null;
                inspector = null;
                userInforManager = null;
                userInfo = null;

                return PartialView("CarEdited", car);
            }
            //Creating generic list
            List<SelectListItem> ObjList = new List<SelectListItem>()
            {
                new SelectListItem { Text = "БЕНЗИНОВЫЙ", Value = "БЕНЗИНОВЫЙ" },
                new SelectListItem { Text = "ДИЗЕЛЬНЫЙ", Value = "ДИЗЕЛЬНЫЙ" },
                new SelectListItem { Text = "ГИБРИДНЫЙ", Value ="ГИБРИДНЫЙ"}

            };
            //Assigning generic list to ViewBag
            ViewBag.MOTORFUELTYPE = new SelectList(ObjList, "Value", "Text");
            ViewBag.COUNTRYCODE = new SelectList(Mapper.Map<IEnumerable<COUNTRY>, IEnumerable<Country>>(_countryService.GetAllCountries()), "ABBR2", "NAME");
            ViewBag.VEHICLEKINDID = new SelectList(Mapper.Map<IEnumerable<VEHICLEKIND>, IEnumerable<Vehiclekind>>(_vehiclekindService.GetAllVehiclekinds()), "VEHICLEKINDID", "NAME");
            return PartialView(car);
        }

        // GET: Car/Delete/5
        public ActionResult PartialDelete(short id)
        {
            CAR car = _carService.GetCar(id);
            if (car == null)
            {
                return HttpNotFound();
            }
            return PartialView(Mapper.Map<Car>(car));
        }

		// GET: vehicles/JsonDelete/5
		public ActionResult JsonDelete(short id)
        {
            CAR jsondata = _carService.GetCar(id);

            var anonArray = new[] { new {
                message = new MvcHtmlString($"<p><b>Данные об автомобиле, как о товаре, с VIN номером {jsondata.VIN ?? string.Empty}, " +
                        $" маркой {jsondata.GOODSMARK ?? string.Empty} моделью {jsondata.GOODSMODEL ?? string.Empty} не могут быть удалены, " +
                        $"поскольку от них зависят следующие таможенные документы:</b></p>"),
                name = $"VIN: {jsondata.VIN} {jsondata.COUNTRYCODE}",
                action =  @"\Car\Delete\" + id.ToString(),
                enitities  = jsondata.DOC.Select(d => $"Номер АТД '{d.NUM}' от {d.PERIODSTARTDATE.Value.ToShortDateString()} {d.CONSIGNEENAME}")}};
            
            return Json(
               new
               {
                   anonArray
               },
               JsonRequestBehavior.AllowGet);
        }

        // POST: Car/Delete/5
        [HttpPost, ActionName("PartialDelete")]
        [ValidateAntiForgeryToken]
        public ActionResult PartialDeleteConfirmed(short id)
        {
            CAR car = _carService.GetCar(id);
            if (car == null)
            {
                return new JsonResult();
            }

            // Block for audit. Setting needed variables and objects.
            string userId = User.Identity.GetUserId();
            ApplicationUser user = userManager.FindById(userId);
            Inspector inspector = Mapper.Map<Inspector>(_inspectorService.GetInspector((short)user.PERSONID));
            UserInforManager userInforManager = new UserInforManager(inspector, user.UserName);
            string userInfo = userInforManager.GetUserInfo();

            _carService.DeleteCar(id, userId: User.Identity.GetUserId() ?? string.Empty, userInfo: userInfo ?? string.Empty);

            // Set null for unused variables.
            userId = null;
            user = null;
            inspector = null;
            userInforManager = null;
            userInfo = null;

            return PartialView("CarDeleted", Mapper.Map<Car>(car));
        }

        // GET: Car/Delete/5
        public ActionResult Delete(short id)
        {
            CAR car = _carService.GetCar(id);
            if (car == null)
            {
                return HttpNotFound();
            }
            return View(Mapper.Map<Car>(car));
        }

        // POST: Car/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(short id)
        {
            // Block for audit. Setting needed variables and objects.
            string userId = User.Identity.GetUserId();
            ApplicationUser user = userManager.FindById(userId);
            Inspector inspector = Mapper.Map<Inspector>(_inspectorService.GetInspector((short)user.PERSONID));
            UserInforManager userInforManager = new UserInforManager(inspector, user.UserName);
            string userInfo = userInforManager.GetUserInfo();

            _carService.DeleteCar(id, userId: User.Identity.GetUserId() ?? string.Empty, userInfo: userInfo ?? string.Empty);

            // Set null for unused variables.
            userId = null;
            user = null;
            inspector = null;
            userInforManager = null;
            userInfo = null;

            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _carService.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
