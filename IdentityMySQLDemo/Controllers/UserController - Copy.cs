﻿using AspNet.Identity.MySQL;
using Focus.BL.Common.Services;
using IdentityMySQLDemo.Models;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using Focus.DAL.Common.DbModels;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace IdentityMySQLDemo.Controllers
{
    [Authorize(Roles = "admin, auditor")]
    public class UserController2 : Controller
    {

        private ApplicationDbContext db = new ApplicationDbContext("OracleDbContext");

        UserManager<ApplicationUser> userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(new ApplicationDbContext("OracleDbContext")));
        RoleManager<IdentityRole> RoleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(new ApplicationDbContext("OracleDbContext")));

        private readonly IInspectorService _inspectorService;
        private readonly IPostService _postService;

        public UserController2(IInspectorService inspectorService, IPostService postService)
        {
            _inspectorService = inspectorService;
            _postService = postService;
        }

        // GET: User
        public ActionResult Index()
        {

            var users = new List<UsersViewModel>();
            UsersViewModel user = null;
            List<ApplicationUser> usersList = userManager.Users.ToList();
            foreach (ApplicationUser item in usersList)
            {
                ApplicationUser localItem = userManager.FindById(item.Id);

                var userId = localItem.Id;
                string role = userManager.GetRoles(userId).FirstOrDefault();
                short inspectorId = (short)localItem.PERSONID;
                DateTime? lockoutEndDate = localItem.LockoutEndDateUtc;
                INSPECTOR inspector = _inspectorService.GetInspector(inspectorId);
                if (inspector != null)
                {
                    string title = inspector.TITLE;
                    string shortName = inspector.SHORTNAME;
                    string phoneNumber = inspector.PHONENUMBER;
                    byte postId = inspector.POSTID ?? 0;
                    POST post = _postService.GetPost(postId);
                    if (post != null)
                    {
                        string code = post.CODE;
                        string namePto = post.NAME;
                        string division = post.DEVISION;
                        user = new UsersViewModel() { Id = userId, UserName = localItem.UserName, Role = role, ShortName = shortName, CodePTO = code, PTOName = namePto , Division = division , Title = title, PhoneNumber = phoneNumber, LockoutEndDate = lockoutEndDate };
                        users.Add(user);
                        continue;
                    }
                    user = new UsersViewModel() { Id = userId, UserName = localItem.UserName, Role = role, ShortName = shortName, Title = title, PhoneNumber = phoneNumber, LockoutEndDate = lockoutEndDate };
                    users.Add(user);
                    continue;
                }

                user = new UsersViewModel() { Id = userId, UserName = localItem.UserName, Role = role, LockoutEndDate = lockoutEndDate };
                users.Add(user);
            }
            return View(users);
        }

        // GET: User/Edit/5
        public async Task<ActionResult> ChangePassword(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ApplicationUser applicationUser = await userManager.FindByIdAsync(id);
            var userId = applicationUser.Id;
            var role = userManager.GetRoles(userId).FirstOrDefault();
            var user = new RegisterViewModel() { UserName = applicationUser.UserName, Role = role };
            if (applicationUser == null)
            {
                return HttpNotFound();
            }

            return View(user);
        }

        // POST request.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ChangePassword(RegisterViewModel usermodel)
        {
            var user = await userManager.FindByNameAsync(usermodel.UserName);
            if (user == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            user.PasswordHash = userManager.PasswordHasher.HashPassword(usermodel.Password);
            var result = await userManager.UpdateAsync(user);
            if (!result.Succeeded)
            {
                return View(usermodel);
            }
            return RedirectToAction("Index");
        }

        public ActionResult LockAccount(string id)
        {
            ApplicationUser user = userManager.FindById(id);
            if (user == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            const int LOCKOUT_MINUTES_COUNT = 15;
            LockAccount(user.UserName, LOCKOUT_MINUTES_COUNT);

            return RedirectToAction("Index");
        }

       
        public ActionResult LockoutAccount(string Id)
        {
            ApplicationUser user = userManager.FindById(Id);
            if (user == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            const int SECONDS_BACKWARD = 5;
            LockoutAccount(user.UserName, -SECONDS_BACKWARD);

            return RedirectToAction("Index");
        }

        private void LockoutAccount(string userName, int seconds = 0)
        {
            var user = userManager.FindByName(userName);

            DateTime lockoutEndDate = DateTime.Now.ToUniversalTime();
            if (seconds == 0)
            {
                userManager.SetLockoutEndDate(user.Id, lockoutEndDate);
            }
            else
            {
                lockoutEndDate = DateTime.Now.AddSeconds(seconds);
                userManager.SetLockoutEndDate(user.Id, lockoutEndDate);
            }
        }

        private void LockAccount(string userName, int minutes = 0)
        {
            ApplicationUser user = userManager.FindByName(userName);

            DateTime lockoutEndDate = DateTime.Now.ToUniversalTime();
            if (minutes == 0)
            {
                const int LOCKOUT_YEARS_COUNT = 100;
                lockoutEndDate = lockoutEndDate.AddYears(LOCKOUT_YEARS_COUNT);
                userManager.SetLockoutEndDate(user.Id, lockoutEndDate);
            }
            else
            {
                lockoutEndDate = lockoutEndDate.AddMinutes(minutes);
                userManager.SetLockoutEndDate(user.Id, lockoutEndDate);
            }
        }

        [HttpPost]
        public JsonResult doesUserNameExist(string UserName)
        {

            var user = userManager.FindByName(UserName);
            if (user==null)
                return Json(true, JsonRequestBehavior.AllowGet);

            string suggestedUID = String.Format(CultureInfo.InvariantCulture,
                "user с имененем '{0}' уже существует. Пожалуйста, введите другое имя пользователя.", UserName);
            return Json(suggestedUID, JsonRequestBehavior.AllowGet);
        }

        // GET: User/Details/5
        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ApplicationUser applicationUser = userManager.FindById(id);
            if (applicationUser == null)
            {
                return HttpNotFound();
            }
            return View(applicationUser);
        }

        // GET: User/Create
        public ActionResult Create()
        {
            ViewBag.ROLE = new SelectList(RoleManager.Roles.ToList(), "Name", "Name");
            return View();
        }

        // POST: User/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(RegisterViewModel model)
        {
            if (ModelState.IsValid)
            {
                DateTime now = DateTime.Now.ToUniversalTime();
                var user = new ApplicationUser { UserName = model.UserName, Email = model.UserName + "@test.com", PERSONID = (short)model.PERSONID, LockoutEnabled = true, LockoutEndDateUtc = now };
                var result = await userManager.CreateAsync(user, model.Password);
                //if (!result.Succeeded)
                //{

                //    // For more information on how to enable account confirmation and password reset please visit https://go.microsoft.com/fwlink/?LinkID=320771
                //    // Send an email with this link
                //    // string code = await UserManager.GenerateEmailConfirmationTokenAsync(user.Id);
                //    // var callbackUrl = Url.Action("ConfirmEmail", "Account", new { userId = user.Id, code = code }, protocol: Request.Url.Scheme);
                //    // await UserManager.SendEmailAsync(user.Id, "Confirm your account", "Please confirm your account by clicking <a href=\"" + callbackUrl + "\">here</a>");

                //    return RedirectToAction("Index");
                //}
                var rolesForUser = userManager.GetRoles(user.Id);
                if (!rolesForUser.Contains(model.Role))
                {
                    var resultAddUserToRoleAdmin = userManager.AddToRole(user.Id, model.Role);
                }
                return RedirectToAction("Index");
            }

            // If we got this far, something failed, redisplay form
            ViewBag.ROLE = new SelectList(RoleManager.Roles.ToList(), "Name", "Name");
            return View(model);
        }

        // GET: User/Edit/5
        public ActionResult Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ApplicationUser applicationUser = userManager.FindById(id);
            if (applicationUser == null)
            {
                return HttpNotFound();
            }
            var userId = applicationUser.Id;
            var role = userManager.GetRoles(userId).FirstOrDefault();
            var user = new UsersViewModel() { Id = userId, UserName = applicationUser.UserName, Role = role };
            ViewBag.Role = new SelectList(RoleManager.Roles.ToArray(), "Name", "Name", role);
            return View(user);
        }

        // POST: User/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(UsersViewModel applicationUser)
        {
            if (ModelState.IsValid)
            {
                ViewBag.Role = new SelectList(RoleManager.Roles.ToArray(), "Name", "Name");
                var user = userManager.FindById(applicationUser.Id);
                if (user != null)
                {
                    var userId = applicationUser.Id;
                    var role = userManager.GetRoles(user.Id).FirstOrDefault();
                    userManager.RemoveFromRole(userId, role);
                    userManager.AddToRole(userId, applicationUser.Role);
                }
                return RedirectToAction("Index");
            }
            return View();
        }

        // GET: User/Delete/5
        public ActionResult Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ApplicationUser applicationUser = userManager.FindById(id);
            if (applicationUser == null)
            {
                return HttpNotFound();
            }
            var userId = applicationUser.Id;
            var role = userManager.GetRoles(applicationUser.Id).FirstOrDefault();
            var user = new UserViewModel() { Id = applicationUser.Id, UserName = applicationUser.UserName, Role = role };
            return View(user);
        }

        // POST: User/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            ApplicationUser applicationUser = userManager.FindById(id);
            userManager.Delete(applicationUser);
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (db != null)
                {
                    db.Dispose();
                    db = null;
                }

                if (userManager != null)
                {

                    userManager.Dispose();
                    userManager = null;
                }

                if (RoleManager != null)
                {

                    RoleManager.Dispose();
                    RoleManager = null;
                }

            }

            base.Dispose(disposing);
        }
    }
}
