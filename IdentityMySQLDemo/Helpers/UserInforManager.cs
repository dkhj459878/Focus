﻿using Focus.Web.Models;
using System;

namespace Focus.Web.Helpers
{
    public class UserInforManager
    {
        private readonly Inspector _inspector;
        private readonly string _userName;

        public UserInforManager(Inspector inspector, string userName)
        {
            _inspector = inspector;
            _userName = userName ?? string.Empty;
        }

        private string GetInspectorInfor()
        {
            if (_inspector == null)
            {
                return string.Empty;
            }
            string postInfo = _inspector.POST == null ? string.Empty : $", код ПТО - { _inspector.POST?.CODE}, { _inspector.POST?.DEVISION}";
            return $", {_inspector.LASTNAME} {_inspector.FIRSTNAME} {_inspector.FATHERNAME}{_inspector.TITLE}{postInfo}";
        }

        public string GetUserInfo()
        {
            return $"{_userName}{GetInspectorInfor()}";
        }
    }
}