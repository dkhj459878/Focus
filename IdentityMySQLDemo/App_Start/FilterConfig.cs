﻿using Focus.BL.Services;
using log4net;
using System;
using System.Diagnostics;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace IdentityMySQLDemo
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new CustomExceptionHandler());
            filters.Add(new CustomActionfilterAttribute());

        }
    }

    public class CustomExceptionHandler : HandleErrorAttribute
    {
        protected static readonly ILog _logger =
            LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public override void OnException(ExceptionContext filterContext)
        {
            base.OnException(filterContext);

            string method = filterContext.RouteData.Values["action"] as string;
            string type = filterContext.RouteData.Values["controller"] as string;
            string ip = filterContext.HttpContext.Request.UserHostAddress;
            string user = filterContext.HttpContext.User.Identity.Name;
            string errorMessage = filterContext.Exception.Message;
            string stackTrace = filterContext.Exception.StackTrace;

            string message = string.Empty;
            message = $"{type} {method} {ip} {user} {errorMessage} {stackTrace}";
            _logger.Error(message);
        }
    }

    public class CustomActionfilterAttribute : IActionFilter
    {
        private readonly AdService _adService;

        public CustomActionfilterAttribute()
        {
            var user = HttpContext.Current.User;
            if (user != null)
            {
                _adService = new AdService(user.Identity?.Name, string.Empty);
            }

        }

        protected static readonly log4net.ILog _logger =
            LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private Stopwatch _stopwatch;
        public void OnActionExecuted(ActionExecutedContext filterContext)
        {
            _stopwatch.Stop();
            Log(filterContext.RouteData, filterContext.HttpContext, _stopwatch.ElapsedMilliseconds, "AfterAction");
        }

        public void OnActionExecuting(ActionExecutingContext filterContext)
        {


            _stopwatch = new Stopwatch();
            var user = HttpContext.Current.User;
            if (user != null)
            {
            }
            Log(filterContext.RouteData, filterContext.HttpContext, 0, "BeforAction");
            _stopwatch.Start();
        }

        private void Log(RouteData routeData, HttpContextBase httpContextBase, long timeSpent, string eventCase)
        {
            string method = routeData.Values["action"] as string;
            string type = routeData.Values["controller"] as string;
            string ip = httpContextBase.Request.UserHostAddress;
            string user = httpContextBase.User.Identity.Name;

            string message = string.Empty;
            message = $"{type} {method} {ip} Elapsed time:{timeSpent} ms {user} {eventCase}";
            _logger.Info(message);
        }
    }

    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = false, Inherited = true)]
    public class PreventDuplicateRequestAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (HttpContext.Current.Request["__RequestVerificationToken"] == null)
                return;

            var currentToken = HttpContext.Current.Request["__RequestVerificationToken"].ToString();

            if (HttpContext.Current.Session["LastProcessedToken"] == null)
            {
                HttpContext.Current.Session["LastProcessedToken"] = currentToken;
                return;
            }

            lock (HttpContext.Current.Session["LastProcessedToken"])
            {
                var lastToken = HttpContext.Current.Session["LastProcessedToken"].ToString();

                if (lastToken == currentToken)
                {
                    filterContext.Controller.ViewData.ModelState.AddModelError("", "Looks like you accidentally tried to double post.");
                    return;
                }

                HttpContext.Current.Session["LastProcessedToken"] = currentToken;
            }
        }
    }

}
