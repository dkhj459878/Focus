﻿using System.Web;
using System.Web.Optimization;

namespace IdentityMySQLDemo
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));


            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                "~/Scripts/jquery.disabling.js",
                "~/Scripts/jquery.validate.js",
                "~/Scripts/jquery.validate.unobtrusive.js",
                "~/Scripts/antidoubleclick.js"
                ));

            bundles.Add(new ScriptBundle("~/bundles/disabling").Include(
                "~/Scripts/disabling.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryvalg").Include(
                "~/Scripts/jquery.validate.globalize.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryajax").Include(
                        "~/Scripts/jquery.unobtrusive-ajax*"));

            bundles.Add(new ScriptBundle("~/bundles/globalize").Include(
                        "~/Scripts/cldr*",
                        "~/Scripts/globalize*"
                        )
                        .IncludeDirectory("~/Scripts/cldr", "*.js")
                        .IncludeDirectory("~/Scripts/globalize", "*.js")
                        );

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at https://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/site.css"));

            bundles.Add(new StyleBundle("~/Content/loader").Include(
                     "~/Content/loader.css",
                     "~/Content/loading.css"));
        }
    }
}
