﻿using AutoMapper;

namespace Focus.Web
{
    public class MapperConfig
    {
        public static void Initialize()
        {
            Mapper.Initialize(cfg => cfg.AddProfiles(typeof(MapperConfig)));
        }
    }
}
