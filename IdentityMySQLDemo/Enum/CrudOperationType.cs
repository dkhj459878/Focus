﻿namespace Focus.Web.Enum
{
    public enum CrudOperation
    {
        Added,
        Modified,
        Deleted
    }
}