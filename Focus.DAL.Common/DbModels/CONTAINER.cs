//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Focus.DAL.Common.DbModels
{
    using System;
    using System.Collections.Generic;
    
    public partial class CONTAINER
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public CONTAINER()
        {
            this.DOC = new HashSet<DOC>();
        }
    
        public short CONTAINERID { get; set; }
        public string REGNUMBER { get; set; }
        public string NAME { get; set; }
        public string COUNTRYCODE { get; set; }
        public string DEMENSIONS { get; set; }
        public string USERID { get; set; }
        public System.DateTime LAST_UPDATE { get; set; }
        public string SID { get; set; }
        public string USERINFO { get; set; }
    
        public virtual COUNTRY COUNTRY { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DOC> DOC { get; set; }
    }
}
