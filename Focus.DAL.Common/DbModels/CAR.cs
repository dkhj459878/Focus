//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Focus.DAL.Common.DbModels
{
    using System;
    using System.Collections.Generic;
    
    public partial class CAR
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public CAR()
        {
            this.DOC = new HashSet<DOC>();
        }
    
        public short CARID { get; set; }
        public byte VEHICLEKINDID { get; set; }
        public string COUNTRYCODE { get; set; }
        public string VIN { get; set; }
        public string DESCRIPTION { get; set; }
        public string GOODSMARK { get; set; }
        public string GOODSMODEL { get; set; }
        public string MOTORFUELTYPE { get; set; }
        public Nullable<short> ENGINECAPACITY { get; set; }
        public string USERID { get; set; }
        public System.DateTime LAST_UPDATE { get; set; }
        public string SID { get; set; }
        public string USERINFO { get; set; }
    
        public virtual COUNTRY COUNTRY { get; set; }
        public virtual VEHICLEKIND VEHICLEKIND { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DOC> DOC { get; set; }
    }
}
