﻿using System.Collections.Generic;

namespace Focus.DAL.Common.DbModels
{
    public partial class DOC
    {
        public object Clone()
        {
            return new DOC()
            {
                DOCID = DOCID,
                CONSIGNEEID = CONSIGNEEID,
                CONSIGNEENAME = CONSIGNEENAME,
                CONSIGNEEADDRESS = CONSIGNEEADDRESS,
                CONSIGNEETIN = CONSIGNEETIN,
                FIRSTNAME = FIRSTNAME,
                INSPECTORID = INSPECTORID,
                INSPECTORTITLE = INSPECTORTITLE,
                INSPECTORSHORTNAME = INSPECTORSHORTNAME,
                INSPECTORDEVISION = INSPECTORDEVISION,
                ID = ID,
                LASTNAME = LASTNAME,
                FATHERSNAME = FATHERSNAME,
                PASSPORTNUMBER = PASSPORTNUMBER,
                PASSPORTISSUEDDATE = PASSPORTISSUEDDATE,
                PASSPORTAUTHORITYBODY = PASSPORTAUTHORITYBODY,
                PLACEOFWORK = PLACEOFWORK,
                POST = POST,
                TYPE = TYPE,
                ADDRESS = ADDRESS,
                PHONE = PHONE,
                FAX = FAX,
                PHOTOPAGES = PHOTOPAGES,
                PHOTOCOPIES = PHOTOCOPIES,
                ADDITIONALNUMBERS = ADDITIONALNUMBERS,
                ADDITIONALNAME1 = ADDITIONALNAME1,
                ADDITIONALPAGES1 = ADDITIONALPAGES1,
                ADDITIONALCOPIES1 = ADDITIONALCOPIES1,
                ADDITIONALNAME2 = ADDITIONALNAME2,
                ADDITIONALPAGES2 = ADDITIONALPAGES2,
                ADDITIONALCOPIES2 = ADDITIONALCOPIES2,
                CHIEFPHONENUMBER = CHIEFPHONENUMBER,
                ADDITIONALNAME3 = ADDITIONALNAME3,
                ADDITIONALPAGES3 = ADDITIONALPAGES3,
                ADDITIONALCOPIES3 = ADDITIONALCOPIES3,
                ADDITIONALNAME4 = ADDITIONALNAME4,
                ADDITIONALPAGES4 = ADDITIONALPAGES4,
                ADDITIONALCOPIES4 = ADDITIONALCOPIES4,
                ADDITIONALNAME5 = ADDITIONALNAME5,
                ADDITIONALPAGES5 = ADDITIONALPAGES5,
                ADDITIONALCOPIES5 = ADDITIONALCOPIES5,
                NUM = NUM,
                SAMPLINGNUM = SAMPLINGNUM,
                DECISIONNUM = DECISIONNUM,
                PERIODSTARTDATE = PERIODSTARTDATE,
                PEROIDFINISHDATE = PEROIDFINISHDATE,
                PERIODSTARTTIME = PERIODSTARTTIME,
                CUSTOMSPLACE = CUSTOMSPLACE,
                PERIODFINISHTIME = PERIODFINISHTIME,
                DOCDECLARATIONONGOODS = DOCDECLARATIONONGOODS,
                DOCCMRNUM = DOCCMRNUM,
                DOCCMRDATE = DOCCMRDATE,
                DOCCARNERNUM = DOCCARNERNUM,
                DOCCARNETDATE = DOCCARNETDATE,
                DOCNOTIFICATIONPLACEMENT = DOCNOTIFICATIONPLACEMENT,
                DOCPERMISSIONPLACEMENT = DOCPERMISSIONPLACEMENT,
                DOCINVOCENUM = DOCINVOCENUM,
                DOCINVOICEDATE = DOCINVOICEDATE,
                GOODSDESCRIPTION = GOODSDESCRIPTION,
                GOODSTOTALBRUTTO = GOODSTOTALBRUTTO,
                GOODSARRIVALCOUNTRY = GOODSARRIVALCOUNTRY,
                ONITSOWNWHEELS = ONITSOWNWHEELS,
                PACKAGEINITIALNAME = PACKAGEINITIALNAME,
                PACKAGENAME = PACKAGENAME,
                PLACESNAME = PLACESNAME,
                PLACESNUMBER = PLACESNUMBER,
                QUESTIONS = QUESTIONS,
                GOODSDES = GOODSDES,
                SAMPLEWEIGHT = SAMPLEWEIGHT,
                SAMPLECOST = SAMPLECOST,
                TOTALWEIGHT = TOTALWEIGHT,
                TOTALCOST = TOTALCOST,
                CURRENCYNAME = CURRENCYNAME,
                ISDANGER = ISDANGER,
                ISPERISHABLEPRODUCT = ISPERISHABLEPRODUCT,
                KINDOFSAMPLE = KINDOFSAMPLE,
                SAMPLESNUM = SAMPLESNUM,
                EXPERTINSINKIND = EXPERTINSINKIND,
                SEALNUM = SEALNUM,
                WEIGHINGACTUALNETTO = WEIGHINGACTUALNETTO,
                WEIGHINGACTUALBRUTTO = WEIGHINGACTUALBRUTTO,
                ISACTUAL = ISACTUAL,
                ISAVERAGE = ISAVERAGE,
                ISAVERAGEPLACESNUMBER = ISAVERAGEPLACESNUMBER,
                ISAVERAGEFIRST = ISAVERAGEFIRST,
                ISAVERAGESECOND = ISAVERAGESECOND,
                ISAVERAGETHIRD = ISAVERAGETHIRD,
                ISCALCULATIONDESCRIPTION = ISCALCULATIONDESCRIPTION,
                ISOTHER = ISOTHER,
                ISOTHERDESCRIPTION = ISOTHERDESCRIPTION,
                ISCALCULATION = ISCALCULATION,
                SIGNSSOFVIOLATIONS = SIGNSSOFVIOLATIONS,
                VIOLATIONSDESCRIPTION = VIOLATIONSDESCRIPTION,
                CUSTOMS = CUSTOMS,
                RADIATIONBACKGROUND = RADIATIONBACKGROUND,
                CUSTOMSRESULT = CUSTOMSRESULT,
                LAST_UPDATE = LAST_UPDATE,
                GROUND = GROUND,
                SEALS = SEALS,
                SEALSVIOLATED = SEALSVIOLATED,
                IDENTIFICATIONTOOLS = IDENTIFICATIONTOOLS,
                IDENTIFICATIONTOOLSNUMBER = IDENTIFICATIONTOOLSNUMBER,
                PACKEDATOURPRESENCE = PACKEDATOURPRESENCE,
                WHEATHER = WHEATHER,
                LIGHTING = LIGHTING,
                SAMPLINGPACKAGE = SAMPLINGPACKAGE,
                PROFILENUMBER = PROFILENUMBER,
                CHIEFID = CHIEFID,
                CHIEFSHORTNAME = CHIEFSHORTNAME,
                CHIEFPOST = CHIEFPOST,
                CHIEFTITLE = CHIEFTITLE,
                BASIS = BASIS,
                CONTROL = CONTROL,
                TASK = TASK,
                REWIEW = REWIEW,
                COMMONRATE = COMMONRATE,
                SPEEDUPRATE = SPEEDUPRATE,
                RATE3 = RATE3,
                RATE5 = RATE5,
                RATE4 = RATE4,
                TIMESTAMP_ON_CREATE = TIMESTAMP_ON_CREATE,
                ISCOULDBEPRENTED = ISCOULDBEPRENTED,
                USERID = USERID,
                SID = SID,
                USERINFO = USERINFO,
                CUSTOMBROCKER = null,
                INSPECTOR = null,
                INSPECTOR1 = null,
                ORGANIZATION = null,
                CAR = GetCars(),
                CONTAINER = GetContainers(),
                DEVICE = GetDevices(),
                VEHICLE = GetVehicles()
            };

        }

        private ICollection<CAR> GetCars()
        {
            if (CAR == null)
            {
                return null;
            }
            else
            {
                // There is no logic for branch.
            }

            List<CAR> cars = new List<CAR>();

            foreach (CAR car in CAR)
            {
                cars.Add(car);
            }

            return cars;
        }

        private ICollection<CONTAINER> GetContainers()
        {
            if (CONTAINER == null)
            {
                return null;
            }
            else
            {
                // There is no logic for branch.
            }

            List<CONTAINER> containers = new List<CONTAINER>();

            foreach (CONTAINER container in CONTAINER)
            {
                containers.Add(container);
            }

            return containers;
        }

        private ICollection<DEVICE> GetDevices()
        {
            if (DEVICE == null)
            {
                return null;
            }
            else
            {
                // There is no logic for branch.
            }

            List<DEVICE> devices = new List<DEVICE>();

            foreach (DEVICE device in DEVICE)
            {
                devices.Add(device);
            }

            return devices;
        }

        private ICollection<VEHICLE> GetVehicles()
        {
            if (VEHICLE == null)
            {
                return null;
            }
            else
            {
                // There is no logic for branch.
            }

            List<VEHICLE> vehicles = new List<VEHICLE>();

            foreach (VEHICLE vehicle in VEHICLE)
            {
                vehicles.Add(vehicle);
            }

            return vehicles;
        }
    }
}
