﻿using Focus.DAL.Common.DbModels;

namespace Focus.DAL.Common.Repositories
{
    /// <summary>
    /// Представляет репозиторий для должностных лиц таможни.
    /// </summary>
    public interface IDbInspectorRepository : IBaseRepository<INSPECTOR>
    {
    }
}