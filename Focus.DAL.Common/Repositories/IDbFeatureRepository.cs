﻿using Focus.DAL.Common.DbModels;

namespace Focus.DAL.Common.Repositories
{
    /// <summary>
    /// Представляет репозиторий для доп. функций программы.
    /// </summary>
    public interface IDbFeatureRepository : IBaseRepository<FEATURE>
    {
    }
}