﻿using Focus.DAL.Common.DbModels;

namespace Focus.DAL.Common.Repositories
{
    /// <summary>
    /// Представляет репозиторий для записей аудита.
    /// </summary>
    public interface IDbAudit_trailRepository : IBaseRepository<AUDIT_TRAIL>
    {
    }
}