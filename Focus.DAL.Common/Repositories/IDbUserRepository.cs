﻿using Focus.DAL.Common.DbModels;

namespace Focus.DAL.Common.Repositories
{
    /// <summary>
    /// Представляет репозиторий пользователей.
    /// </summary>
    public interface IDbUserRepository : IBaseRepository<USERS>
    {
    }
}