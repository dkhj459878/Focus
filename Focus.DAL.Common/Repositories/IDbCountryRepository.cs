﻿using Focus.DAL.Common.DbModels;

namespace Focus.DAL.Common.Repositories
{
    /// <summary>
    /// Представляет репозиторий стран.
    /// </summary>
    public interface IDbCountryRepository : IBaseRepository<COUNTRY>
    {
    }
}