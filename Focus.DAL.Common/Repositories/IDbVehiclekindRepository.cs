﻿using Focus.DAL.Common.DbModels;

namespace Focus.DAL.Common.Repositories
{
    /// <summary>
    /// Представляет репозиторий для видов транспортных средств.
    /// </summary>
    public interface IDbVehiclekindRepository : IBaseRepository<VEHICLEKIND>
    {
    }
}