﻿using Focus.DAL.Common.DbModels;

namespace Focus.DAL.Common.Repositories
{
    /// <summary>
    /// Представляет репозиторий для транспортных средств.
    /// </summary>
    public interface IDbVehicleRepository : IBaseRepository<VEHICLE>
    {
    }
}