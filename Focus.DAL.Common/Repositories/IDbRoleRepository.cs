﻿using Focus.DAL.Common.DbModels;

namespace Focus.DAL.Common.Repositories
{
    /// <summary>
    /// Представляет репозиторий для автомобилей, как товара.
    /// </summary>
    public interface IDbRoleRepository : IBaseRepository<ROLES>
    {
    }
}