﻿using Focus.DAL.Common.DbModels;

namespace Focus.DAL.Common.Repositories
{
    /// <summary>
    /// Представляет репозиторий для типов технических средств таможенного контроля.
    /// </summary>
    public interface IDbDevicetypeRepository : IBaseRepository<DEVICETYPE>
    {
    }
}