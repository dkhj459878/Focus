﻿using Focus.DAL.Common.DbModels;

namespace Focus.DAL.Common.Repositories
{
    /// <summary>
    /// Представляет репозиторий для пунктов таможенного оформления.
    /// </summary>
    public interface IDbPostRepository : IBaseRepository<POST>
    {
    }
}