﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Focus.DAL.Common.Extensions
{
    /// <summary>
    /// Collection of auxiliary extension methods using the reflection mechanism.
    /// </summary>
    public static class ReflectionManager
    {
        /// <summary>
        /// Returns objects type name without refferance on any assemblies.
        /// </summary>
        /// <param name="obj">Instance of being examined object.</param>
        /// <returns>Object type name without assemblies information.</returns>
        public static string GetTypeName(this object obj)
        {
            if (obj.CheckNull())
            {
                throw new NullReferenceException("Return null reference expection in 'GetTypeName()' method of ReflectionManager class.");
            }

            Type type = obj.GetType();

            return type.Name;
        }

        /// <summary>
        /// Checks either object has custom attribute with specified name or not.
        /// </summary>
        /// <param name="obj">Instance of being examined object.</param>
        /// <returns>True if being examined object has attribute
        /// with specified by <see cref="attributeName"/> attribute name.</returns>
        public static bool HasAttribute(this object obj, string attributeName)
        {
            const string ATTRIBUTE_WORD = "Attribute";
;
            if (obj.CheckNull())
            {
                return false;
            }

            if (string.IsNullOrEmpty(attributeName))
            {
                return false;
            }

            Type type = obj.GetType();

            attributeName = attributeName.Contains(ATTRIBUTE_WORD)
                ? attributeName
                : attributeName + ATTRIBUTE_WORD;

            bool hasAttribute = type.CustomAttributes.Any(a => a.AttributeType.Name == attributeName);

            return hasAttribute;
        }

        public static bool IsCollection(this PropertyInfo propertyInfo)
        {
            Type typeCollection = typeof(ICollection<>);
            return propertyInfo == null ?
                false : 
                propertyInfo.PropertyType.IsGenericType ? 
                typeCollection.IsAssignableFrom(propertyInfo.PropertyType.GetGenericTypeDefinition()) : 
                false;
        }

        /// <summary>
        /// Get methods names and corresponding values pairs dictionary.
        /// </summary>
        /// <param name="obj">Instance of the being getted content object.</param>
        /// <returns>Methods names and corresponding values pairs collection.
        public static Dictionary<string, object> GetContent(this object obj)
        {
            if (obj.CheckNull())
            {
                throw new NullReferenceException("Return null reference expection in 'GetContent()' method of ReflectionManager class.");
            }
            else
            {
                // There is no a logic branch.
            }
            
            Type objectType = obj.GetType();
            PropertyInfo[] propertiesInfor = objectType.GetProperties();
            Dictionary<string, object> objectContent = new Dictionary<string, object>();

            for (int i = 0; i < propertiesInfor.Length; i++)
            {
                PropertyInfo propertyInfo = propertiesInfor[i];

                string propertyName = propertyInfo.Name;
                object propertyValue = propertyInfo.GetValue(obj, new object[] { });

                objectContent.Add(propertyName, propertyValue);

                propertyName = null;
                propertyValue = null;
;            }
            objectType = null;
            propertiesInfor = null;

            return objectContent;
        }
    }
}
