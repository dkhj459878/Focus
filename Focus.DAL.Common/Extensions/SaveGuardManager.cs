﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Focus.DAL.Common.Extensions
{
    public static class SaveGuardManager
    {
        public static bool CheckNull(this object obj)
        {
            if (obj == null)
            {
                return true;
            }

            return false;
        }
    }
}
