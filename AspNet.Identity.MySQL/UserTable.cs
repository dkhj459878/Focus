﻿using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;

namespace AspNet.Identity.MySQL
{
    /// <summary>
    /// Class that represents the Users table in the MySQL Database
    /// </summary>
    public class UserTable<TUser>
        where TUser :IdentityUser
    {
        private MySQLDatabase _database;
        /// <summary>
        /// Constructor that takes a MySQLDatabase instance 
        /// </summary>
        /// <param name="database"></param>
        public UserTable(MySQLDatabase database)
        {
            _database = database;
        }
   
        /// <summary>
        /// Returns the user's name given a user id
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public string GetUserName(string userId)
        {
            string commandText = "Select Name from Users where Id = :id";
            Dictionary<string, object> parameters = new Dictionary<string, object>() { { ":id", userId } };

            return _database.GetStrValue(commandText, parameters);
        }

        /// <summary>
        /// Returns a User ID given a user name
        /// </summary>
        /// <param name="userName">The user's name</param>
        /// <returns></returns>
        public string GetUserId(string userName)
        {
            string commandText = "Select Id from Users where UserName = :name";
            Dictionary<string, object> parameters = new Dictionary<string, object>() { { ":name", userName } };

            return _database.GetStrValue(commandText, parameters);
        }

        /// <summary>
        /// Gets users.
        /// </summary>
        /// <returns>Collection of user's information - Id, UserName</returns>
        public ICollection<TUser> GetUsers()
        {
            string commandText = "Select * from USERS";
            ICollection<TUser> users = new List<TUser>();
            Dictionary<string, object> parameters = new Dictionary<string, object>() { };
            var users2 = _database.Query(commandText, parameters);
            foreach (var item in users2)
            {
                var user = (TUser)Activator.CreateInstance(typeof(TUser));
                user.Id = item["ID"];
                user.UserName = item["USERNAME"];
                user.PasswordHash = string.IsNullOrEmpty(item["PASSWORDHASH"]) ? null : item["PASSWORDHASH"];
                user.SecurityStamp = string.IsNullOrEmpty(item["SECURITYSTAMP"]) ? null : item["SECURITYSTAMP"];
                user.Email = string.IsNullOrEmpty(item["EMAIL"]) ? null : item["EMAIL"];
                user.EmailConfirmed = item["EMAILCONFIRMED"] == "1" ? true : false;
                user.PhoneNumber = string.IsNullOrEmpty(item["PHONENUMBER"]) ? null : item["PHONENUMBER"];
                user.PhoneNumberConfirmed = item["PHONENUMBERCONFIRMED"] == "1" ? true : false;
                user.LockoutEnabled = item["LOCKOUTENABLED"] == "1" ? true : false;
                user.LockoutEndDateUtc = string.IsNullOrEmpty(item["LOCKOUTENDDATEUTC"]) ? DateTime.Now : DateTime.Parse(item["LOCKOUTENDDATEUTC"]);
                user.AccessFailedCount = string.IsNullOrEmpty(item["ACCESSFAILEDCOUNT"]) ? 0 : int.Parse(item["ACCESSFAILEDCOUNT"]);
                user.PERSONID = string.IsNullOrEmpty(item["PERSONID"]) ? 0 : int.Parse(item["PERSONID"]);
                users.Add(user);
            }
            return users;
        }

        /// <summary>
        /// Returns an TUser given the user's id
        /// </summary>
        /// <param name="userId">The user's id</param>
        /// <returns></returns>
        public TUser GetUserById(string userId)
        {
            TUser user = null;
            string commandText = "Select * from Users where Id = :id";
            Dictionary<string, object> parameters = new Dictionary<string, object>() { { ":id", userId } };

            var rows = _database.Query(commandText, parameters);
            if (rows != null && rows.Count == 1)
            {
                var row = rows[0];
                user = (TUser)Activator.CreateInstance(typeof(TUser));
                user.Id = row["ID"];
                user.UserName = row["USERNAME"];
                user.PasswordHash = string.IsNullOrEmpty(row["PASSWORDHASH"]) ? null : row["PASSWORDHASH"];
                user.SecurityStamp = string.IsNullOrEmpty(row["SECURITYSTAMP"]) ? null : row["SECURITYSTAMP"];
                user.Email = string.IsNullOrEmpty(row["EMAIL"]) ? null : row["EMAIL"];
                user.EmailConfirmed = row["EMAILCONFIRMED"] == "1" ? true:false;
                user.PhoneNumber = string.IsNullOrEmpty(row["PHONENUMBER"]) ? null : row["PHONENUMBER"];
                user.PhoneNumberConfirmed = row["PHONENUMBERCONFIRMED"] == "1" ? true : false;
                user.LockoutEnabled = row["LOCKOUTENABLED"] == "1" ? true : false;
                user.LockoutEndDateUtc = string.IsNullOrEmpty(row["LOCKOUTENDDATEUTC"]) ? DateTime.Now : DateTime.Parse(row["LOCKOUTENDDATEUTC"]);
                user.AccessFailedCount = string.IsNullOrEmpty(row["ACCESSFAILEDCOUNT"]) ? 0 : int.Parse(row["ACCESSFAILEDCOUNT"]);
                user.PERSONID = string.IsNullOrEmpty(row["PERSONID"]) ? 0 : int.Parse(row["PERSONID"]);
            }

            return user;
        }

        /// <summary>
        /// Returns a list of TUser instances given a user name
        /// </summary>
        /// <param name="userName">User's name</param>
        /// <returns></returns>
        public List<TUser> GetUserByName(string userName)
        {
            List<TUser> users = new List<TUser>();
            string commandText = "Select * from Users where UserName = :name";
            Dictionary<string, object> parameters = new Dictionary<string, object>() { { ":name", userName } };

            var rows = _database.Query(commandText, parameters);
            foreach(var row in rows)
            {
                TUser user = (TUser)Activator.CreateInstance(typeof(TUser));
                user.Id = row["ID"];
                user.UserName = row["USERNAME"];
                user.PasswordHash = string.IsNullOrEmpty(row["PASSWORDHASH"]) ? null : row["PASSWORDHASH"];
                user.SecurityStamp = string.IsNullOrEmpty(row["SECURITYSTAMP"]) ? null : row["SECURITYSTAMP"];
                user.Email = string.IsNullOrEmpty(row["EMAIL"]) ? null : row["EMAIL"];
                user.EmailConfirmed = row["EMAILCONFIRMED"] == "1" ? true : false;
                user.PhoneNumber = string.IsNullOrEmpty(row["PHONENUMBER"]) ? null : row["PHONENUMBER"];
                user.PhoneNumberConfirmed = row["PHONENUMBERCONFIRMED"] == "1" ? true : false;
                user.LockoutEnabled = row["LOCKOUTENABLED"] == "1" ? true : false;
                user.TwoFactorEnabled = row["TWOFACTORENABLED"] == "1" ? true : false;
                user.LockoutEndDateUtc = string.IsNullOrEmpty(row["LOCKOUTENDDATEUTC"]) ? DateTime.Now : DateTime.Parse(row["LOCKOUTENDDATEUTC"]);
                user.AccessFailedCount = string.IsNullOrEmpty(row["ACCESSFAILEDCOUNT"]) ? 0 : int.Parse(row["ACCESSFAILEDCOUNT"]);
                user.PERSONID = string.IsNullOrEmpty(row["PERSONID"]) ? 0 : int.Parse(row["PERSONID"]);
                users.Add(user);
            }

            return users;
        }

        public List<TUser> GetUserByEmail(string email)
        {
            return null;
        }

        /// <summary>
        /// Return the user's password hash
        /// </summary>
        /// <param name="userId">The user's id</param>
        /// <returns></returns>
        public string GetPasswordHash(string userId)
        {
            string commandText = "Select PasswordHash from Users where Id = :id";
            Dictionary<string, object> parameters = new Dictionary<string, object>();
            parameters.Add(":id", userId);

            var passHash = _database.GetStrValue(commandText, parameters);
            if(string.IsNullOrEmpty(passHash))
            {
                return null;
            }

            return passHash;
        }

        /// <summary>
        /// Sets the user's password hash
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="passwordHash"></param>
        /// <returns></returns>
        public int SetPasswordHash(string userId, string passwordHash)
        {
            string commandText = "Update Users set PasswordHash = :pwdHash where Id = :id";
            Dictionary<string, object> parameters = new Dictionary<string, object>();
            parameters.Add(":pwdHash", passwordHash);
            parameters.Add(":id", userId);

            return _database.Execute(commandText, parameters);
        }

        /// <summary>
        /// Returns the user's security stamp
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public string GetSecurityStamp(string userId)
        {
            string commandText = "Select SecurityStamp from Users where Id = :id";
            Dictionary<string, object> parameters = new Dictionary<string, object>() { { ":id", userId } };
            var result = _database.GetStrValue(commandText, parameters);

            return result;
        }

        /// <summary>
        /// Inserts a new user in the Users table
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public int Insert(TUser user)
        {
            string commandText = @"Insert into Users (UserName, Id, PasswordHash, SecurityStamp,Email,EmailConfirmed,PhoneNumber,PhoneNumberConfirmed, AccessFailedCount,LockoutEnabled,LockoutEndDateUtc,TwoFactorEnabled, PERSONID)
                values (:name, :id, :pwdHash, :SecStamp,:email,:emailconfirmed,:phonenumber,:phonenumberconfirmed,:accesscount,:lockoutenabled,TO_DATE(:lockoutenddate, 'MM-DD-YYYY hh24:mi:ss'),:twofactorenabled, :PERSONID)";
            Dictionary<string, object> parameters = new Dictionary<string, object>();
            parameters.Add(":name", user.UserName);
            parameters.Add(":id", user.Id);
            parameters.Add(":pwdHash", user.PasswordHash);
            parameters.Add(":SecStamp", user.SecurityStamp);
            parameters.Add(":email", user.Email);
            parameters.Add(":emailconfirmed", user.EmailConfirmed ? "1" : "0");
            parameters.Add(":phonenumber", user.PhoneNumber);
            parameters.Add(":phonenumberconfirmed", user.PhoneNumberConfirmed ? "1" : "0");
            parameters.Add(":accesscount", user.AccessFailedCount);
            parameters.Add(":lockoutenabled", user.LockoutEnabled ? "1" : "0");
            parameters.Add(":lockoutenddate", user.LockoutEndDateUtc == null ? string.Empty : user.LockoutEndDateUtc.Value.ToString(@"MM-dd-yyyy HH:mm:ss"));
            parameters.Add(":twofactorenabled", user.TwoFactorEnabled ? "1" : "0");
            parameters.Add(":PERSONID", user.PERSONID);

            return _database.Execute(commandText, parameters);
        }

        /// <summary>
        /// Deletes a user from the Users table
        /// </summary>
        /// <param name="userId">The user's id</param>
        /// <returns></returns>
        private int Delete(string userId)
        {
            string commandText = "Delete from Users where Id = :userId";
            Dictionary<string, object> parameters = new Dictionary<string, object>();
            parameters.Add(":userId", userId);

            return _database.Execute(commandText, parameters);
        }

        /// <summary>
        /// Deletes a user from the Users table
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public int Delete(TUser user)
        {
            return Delete(user.Id);
        }

        /// <summary>
        /// Updates a user in the Users table
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public int Update(TUser user)
        {
            //string commandText = @"Update USERS SET
            //        PERSONID = :PERSONID
            //    WHERE Id = :userId";
            string commandText = @"Update USERS set USERNAME = :userName, PASSWORDHASH = :pswHash, SECURITYSTAMP = :secStamp, 
                EMAIL=:email, EMAILCONFIRMED=:emailconfirmed, PHONENUMBER=:phonenumber, PHONENUMBERCONFIRMED=:phonenumberconfirmed, 
                ACCESSFAILEDCOUNT=:accesscount, LOCKOUTENABLED=:lockoutenabled, 
                LOCKOUTENDDATEUTC=TO_DATE(:lockoutenddate, 'MM-DD-YYYY hh24:mi:ss'),
                TWOFACTORENABLED=:twofactorenabled, PERSONID = :PERSONID  
                WHERE Id = :userId";

            Dictionary<string, object> parameters = new Dictionary<string, object>();
            parameters.Add(":userName", user.UserName);
            parameters.Add(":pswHash", user.PasswordHash);
            parameters.Add(":secStamp", user.SecurityStamp);
            parameters.Add(":email", user.Email);
            parameters.Add(":emailconfirmed", user.EmailConfirmed ? "1" : "0");
            parameters.Add(":phonenumber", user.PhoneNumber);
            parameters.Add(":phonenumberconfirmed", user.PhoneNumberConfirmed ? "1" : "0");
            parameters.Add(":accesscount", user.AccessFailedCount);
            parameters.Add(":lockoutenabled", user.LockoutEnabled ? "1" : "0");
            parameters.Add(":lockoutenddate", user.LockoutEndDateUtc == null ? string.Empty : user.LockoutEndDateUtc.Value.ToString(@"MM-dd-yyyy HH:mm:ss"));
            parameters.Add(":twofactorenabled", user.TwoFactorEnabled ? "1" : "0");
            parameters.Add(":PERSONID", user.PERSONID);
            parameters.Add(":userId", user.Id);

            return _database.Execute(commandText, parameters);
        }
    }
}
