﻿using Focus.BL.Common.Validators;
using System;
using System.Linq;

namespace Focus.BL.Validators
{
    public class StringMaster : IStringMaster
    {
        public string ConvertSimilarLatins(string str)
        {
            if (string.IsNullOrEmpty(str))
            {
                return string.Empty;
            };

            char[] simbols = str.ToCharArray();
            simbols.Select(c => LetterSwicher(c));
            return new string(simbols);
        }

        private char LetterSwicher(char forSwitching)
        {
            switch (forSwitching)
            {
                case '\u0410':
                    return '\u0041'; // 'A'.
                case '\u0412':
                    return '\u0042'; // 'B'.
                case '\u0415':
                    return '\u0045'; // 'E'
                case '\u041A':
                    return '\u004B'; // 'K'.
                case '\u041C':
                    return '\u004D'; // 'M'.
                case '\u041D':
                    return '\u0048'; // 'H'.
                case '\u041E':
                    return '\u004F'; // 'O'.
                case '\u0420':
                    return '\u0050'; // 'P'.
                case '\u0421':
                    return '\u0043'; // 'C'.
                case '\u0422':
                    return '\u0054'; // 'T'.
                case '\u0425':
                    return '\u0058'; // 'X'.
                default:
                    return forSwitching;
            }
        }

        public string GetLastFolderName(string path)
        {
            if (string.IsNullOrEmpty(path))
            {
                return string.Empty;
            }

            string[] searchString = new string[] { @"\" };

            string[] foldersNames = path.Split(searchString, StringSplitOptions.RemoveEmptyEntries);

            if (foldersNames.Length == 0)
            {
                return string.Empty;
            }

            return foldersNames[foldersNames.Length - 1];
        }

    }
}
