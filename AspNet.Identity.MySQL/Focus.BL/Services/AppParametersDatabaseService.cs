﻿using Focus.BL.Common.Services;
using System;
using System.Collections.Generic;

namespace Focus.BL.Services
{
    public class AppParametersDatabaseService : IAppParametersService
    {
        public IEnumerable<object> GetCollectionParameters(string collectionParameterName)
        {
            throw new NotImplementedException();
        }

        public Dictionary<string, IEnumerable<string>> GetCollectionsParameters()
        {
            throw new NotImplementedException();
        }

        public object GetParameter(string parameterName)
        {
            throw new NotImplementedException();
        }

        public Dictionary<string, string> GetParameters()
        {
            throw new NotImplementedException();
        }

        public void SetCollectionParameters(string collectionParameterName, IEnumerable<object> collectionParameters)
        {
            throw new NotImplementedException();
        }

        public void SetCollectionsParameters(Dictionary<string, IEnumerable<object>> collectionsParameters)
        {
            throw new NotImplementedException();
        }

        public void SetParameter(string parameterName, string parameterValue)
        {
            throw new NotImplementedException();
        }

        public void SetParameters(Dictionary<string, string> collecitonParameters)
        {
            throw new NotImplementedException();
        }
    }
}
