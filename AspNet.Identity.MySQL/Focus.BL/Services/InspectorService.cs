﻿using Focus.BL.Common.Services;
using Focus.DAL.Common.DbModels;
using Focus.DAL.Common.UoW;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Focus.BL.Services
{
    /// <summary>
    /// Представляет сервис для работы с инспекторами
    /// в хранилище данных.
    /// </summary>
    public class InspectorService : BaseService, IInspectorService
    {
        /// <summary>
        /// Создает новый экземпляр <see cref="InspectorManagementService"/>
        /// с заданным unit of work.
        /// </summary>
        /// <param name="unitOfWork"> uow </param>
        /// <param name="inspectormanager"> manager </param>
        public InspectorService(IUnitOfWork unitOfWork)
            : base(unitOfWork)
        {
        }

        /// <summary>
        /// Возвращает всех инспекторов
        /// из хранилища данных.
        /// </summary>
        /// <param name="filter">Лямбда-выражение определяющее фильтрацию экземпляров.</param>
        /// <param name="orderBy">Лямбда-выражение определяющее сортировку экземпляров.</param>
        /// <param name="includeProperties">Список связанных свойств экземпляров, разделенный запятыми.</param>
        /// <returns>Коллекция экземпляров инспекторов для пейджинга.</returns>
        public IEnumerable<INSPECTOR> GetAllInspectors(
            Expression<Func<INSPECTOR, bool>> filter = null,
            Func<IQueryable<INSPECTOR>, IOrderedQueryable<INSPECTOR>> orderBy = null,
            string includeProperties = "")
        {
            return _unitOfWork.InspectorRepository.Get(filter, orderBy, includeProperties);
        }

        /// <summary>
        /// Возвращает инспектора с заданным идентификатором из хранилища данных.
        /// </summary>
        /// <param name="pageSize">Количество сведений о инспекторах, выводимых на страницу.</param>
        /// <param name="pageNumber">Номер выводимой страницы со сведениями о ТСТКх.</param>
        /// <param name="filter">Лямбда-выражение определяющее фильтрацию экземпляров.</param>
        /// <param name="orderBy">Лямбда-выражение определяющее сортировку экземпляров.</param>
        /// <param name="includeProperties">Список связанных свойств инспектора, разделенный запятыми.</param>
        /// <returns>Экземпляр инспектора.</returns>
        public IEnumerable<INSPECTOR> GetInspectorsPage(
            int pageSize,
            int pageNumber,
            Expression<Func<INSPECTOR, bool>> filter = null,
            Func<IQueryable<INSPECTOR>, IOrderedQueryable<INSPECTOR>> orderBy = null,
            string includeProperties = "")
        {
            return _unitOfWork.InspectorRepository.GetPage(pageSize, pageNumber, filter, orderBy, includeProperties);
        }

        /// <summary>
        /// Возвращает инспектора с заданным идентификатором из хранилища данных.
        /// </summary>
        /// <param name="id">Идентификатор инспектора.</param>
        /// <returns>Экземпляр инспектора.</returns>
        public INSPECTOR GetInspector(short id)
        {
            return _unitOfWork.InspectorRepository.GetById(id);
        }

        /// <summary>
        /// Добавляет заданного инспектора в хранилище данных
        /// и возвращает идентификатор добавленного инспектора.
        /// </summary>
        /// <param name="inspector">Экземпляр инспектора для добавления.</param>
        /// <returns>Идентификатор инспектора.</returns>
        public short AddInspector(INSPECTOR inspector,
            string userId = "",
            string userInfo = "")
        {
            _unitOfWork.InspectorRepository.Insert(inspector);
            _unitOfWork.SaveChanges(userId, userInfo);
            return inspector.ID;
        }

        /// <summary>
        /// Обновляет информацию заданного инспектора в хранилище данных.
        /// </summary>
        /// <param name="inspector">Экземпляр инспектора для обновления.</param>
        /// <returns> Task </returns>
        public void UpdateInspector(INSPECTOR inspector,
            string userId = "",
            string userInfo = "")
        {
            _unitOfWork.InspectorRepository.UpdateSimpleProperties(inspector, inspector.ID);
            _unitOfWork.SaveChanges(userId, userInfo);
        }

        /// <summary>
        /// Удаляет инспектора с заданным идентификатором из хранилища данных.
        /// </summary>
        /// <param name="id">Идентификатор инспектора.</param>
        /// <returns> Task </returns>
        public void DeleteInspector(short id,
            string userId = "",
            string userInfo = "")
        {
            _unitOfWork.InspectorRepository.Delete(id);
            _unitOfWork.SaveChanges(userId, userInfo);
        }

        /// <summary>
        /// Проверяет наличие инспектора в хранилище данных
        /// соответствующего заданному фильтру.
        /// </summary>
        /// <param name="filter">Лямбда-выражение определяющее фильтр для поиска инспектора.</param>
        /// <returns>Возвращает <see langword="true"/>, если инспектор существует в хранилище данных.</returns>
        public bool InspectorExists(Expression<Func<INSPECTOR, bool>> filter)
        {
            return _unitOfWork.InspectorRepository.Get(filter: filter)
                      .FirstOrDefault() != null;
        }
    }
}