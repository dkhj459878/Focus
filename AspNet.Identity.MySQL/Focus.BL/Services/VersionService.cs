﻿using Focus.BL.Common.Services;
using Focus.DAL.Common.DbModels;
using Focus.DAL.Common.UoW;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Focus.BL.Services
{
    /// <summary>
    /// Представляет сервис для работы с автомобилями
    /// в хранилище данных.
    /// </summary>
    public class VersionService : BaseService, IVersionService
    {
        /// <summary>
        /// Создает новый экземпляр <see cref="VersionManagementService"/>
        /// с заданным unit of work.
        /// </summary>
        /// <param name="unitOfWork"> uow </param>
        /// <param name="versionmanager"> manager </param>
        public VersionService(IUnitOfWork unitOfWork)
            : base(unitOfWork)
        {
        }

        /// <summary>
        /// Возвращает все автомобили
        /// из хранилища данных.
        /// </summary>
        /// <param name="filter">Лямбда-выражение определяющее фильтрацию экземпляров.</param>
        /// <param name="orderBy">Лямбда-выражение определяющее сортировку экземпляров.</param>
        /// <param name="includeProperties">Список связанных свойств экземпляров, разделенный запятыми.</param>
        /// <returns>Коллекция экземпляров автомобилей для пейджинга.</returns>
        public IEnumerable<VERSION> GetAllVersions(
            Expression<Func<VERSION, bool>> filter = null,
            Func<IQueryable<VERSION>, IOrderedQueryable<VERSION>> orderBy = null,
            string includeProperties = "")
        {
            return _unitOfWork.VersionRepository.Get(filter, orderBy, includeProperties);
        }

        /// <summary>
        /// Возвращает автомобиль с заданным идентификатором из хранилища данных.
        /// </summary>
        /// <param name="pageSize">Количество сведений об автомобилях, выводимых на страницу.</param>
        /// <param name="pageNumber">Номер выводимой страницы со сведениями об автомобилях.</param>
        /// <param name="filter">Лямбда-выражение определяющее фильтрацию экземпляров.</param>
        /// <param name="orderBy">Лямбда-выражение определяющее сортировку экземпляров.</param>
        /// <param name="includeProperties">Список связанных свойств экземпляров, разделенный запятыми.</param>
        /// <returns>Экземпляр автомобиля.</returns>
        public IEnumerable<VERSION> GetVersionsPage(
            int pageSize,
            int pageNumber,
            Expression<Func<VERSION, bool>> filter = null,
            Func<IQueryable<VERSION>, IOrderedQueryable<VERSION>> orderBy = null,
            string includeProperties = "")
        {
            return _unitOfWork.VersionRepository.GetPage(pageSize, pageNumber, filter, orderBy, includeProperties);
        }

        /// <summary>
        /// Возвращает автомобиль с заданным идентификатором из хранилища данных.
        /// </summary>
        /// <param name="id">Идентификатор автомобиля.</param>
        /// <returns>Экземпляр автомобиля.</returns>
        public VERSION GetVersion(short id)
        {
            return _unitOfWork.VersionRepository.GetById(id);
        }

        /// <summary>
        /// Добавляет заданный автомобиль в хранилище данных
        /// и возвращает идентификатор добавленного автомобиля.
        /// </summary>
        /// <param name="version">Экземпляр автомобиля для добавления.</param>
        /// <returns>Идентификатор автомобиля.</returns>
        public short AddVersion(VERSION version,
            string userId = "",
            string userInfo = "")
        {
            _unitOfWork.VersionRepository.Insert(version);
            _unitOfWork.SaveChanges(userId, userInfo);
            return version.ID;
        }

        /// <summary>
        /// Обновляет информацию заданного автомобиля в хранилище данных.
        /// </summary>
        /// <param name="version">Экземпляр автомобиля для обновления.</param>
        /// <returns> Task </returns>
        public void UpdateVersion(VERSION version,
            string userId = "",
            string userInfo = "")
        {
            _unitOfWork.VersionRepository.UpdateSimpleProperties(version, version.ID);
            _unitOfWork.SaveChanges(userId, userInfo);
        }

        /// <summary>
        /// Удаляет автомобиль с заданным идентификатором из хранилища данных.
        /// </summary>
        /// <param name="id">Идентификатор автомобиля.</param>
        /// <returns> Task </returns>
        public void DeleteVersion(short id,
            string userId = "",
            string userInfo = "")
        {
            _unitOfWork.VersionRepository.Delete(id);
            _unitOfWork.SaveChanges(userId, userInfo);
        }

        /// <summary>
        /// Проверяет наличие автомобиля в хранилище данных
        /// соответствующего заданному фильтру.
        /// </summary>
        /// <param name="filter">Лямбда-выражение определяющее фильтр для поиска автомобиля.</param>
        /// <returns>Возвращает <see langword="true"/>, если автомобиль существует в хранилище данных.</returns>
        public bool VersionExists(Expression<Func<VERSION, bool>> filter)
        {
            return _unitOfWork.VersionRepository.Get(filter: filter)
                      .FirstOrDefault() != null;
        }
    }
}