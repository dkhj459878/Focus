﻿using Focus.BL.Common.Services;
using Focus.DAL.Common.DbModels;
using Focus.DAL.Common.UoW;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Focus.BL.Services
{
    /// <summary>
    /// Представляет сервис для работы с видами документов
    /// в хранилище данных.
    /// </summary>
    public class DocumentkindService : BaseService, IDocumentkindService
    {
        /// <summary>
        /// Создает новый экземпляр <see cref="DocumentkindManagementService"/>
        /// с заданным unit of work.
        /// </summaryf
        /// <param name="unitOfWork"> uow </param>
        /// <param name="documentkindmanager"> manager </param>
        public DocumentkindService(IUnitOfWork unitOfWork)
            : base(unitOfWork)
        {
        }

        /// <summary>
        /// Возвращает все виды документов
        /// из хранилища данных.
        /// </summary>
        /// <param name="filter">Лямбда-выражение определяющее фильтрацию экземпляров.</param>
        /// <param name="orderBy">Лямбда-выражение определяющее сортировку экземпляров.</param>
        /// <param name="includeProperties">Список связанных свойств экземпляров, разделенный запятыми.</param>
        /// <returns>Коллекция экземпляров видов документов для пейджинга.</returns>
        public IEnumerable<DOCUMENTKIND> GetAllDocumentkinds(
            Expression<Func<DOCUMENTKIND, bool>> filter = null,
            Func<IQueryable<DOCUMENTKIND>, IOrderedQueryable<DOCUMENTKIND>> orderBy = null,
            string includeProperties = "")
        {
            return _unitOfWork.DocumentkindRepository.Get(filter, orderBy, includeProperties);
        }

        /// <summary>
        /// Возвращает вид документа с заданным идентификатором из хранилища данных.
        /// </summary>
        /// <param name="pageSize">Количество сведений о документах, выводимых на видов документовицу.</param>
        /// <param name="pageNumber">Номер выводимой видов документовицы со сведениями о видах документов.</param>
        /// <param name="filter">Лямбда-выражение определяющее фильтрацию экземпляров.</param>
        /// <param name="orderBy">Лямбда-выражение определяющее сортировку экземпляров.</param>
        /// <param name="includeProperties">Список связанных свойств экземпляров, разделенный запятыми.</param>
        /// <returns>Экземпляр вида документа.</returns>
        public IEnumerable<DOCUMENTKIND> GetDocumentkindsPage(
            int pageSize,
            int pageNumber,
            Expression<Func<DOCUMENTKIND, bool>> filter = null,
            Func<IQueryable<DOCUMENTKIND>, IOrderedQueryable<DOCUMENTKIND>> orderBy = null,
            string includeProperties = "")
        {
            return _unitOfWork.DocumentkindRepository.GetPage(pageSize, pageNumber, filter, orderBy, includeProperties);
        }

        /// <summary>
        /// Возвращает видов документову с заданным идентификатором из хранилища данных.
        /// </summary>
        /// <param name="id">Идентификатор вида документа.</param>
        /// <returns>Экземпляр вида документа.</returns>
        public DOCUMENTKIND GetDocumentkind(byte id)
        {
            return _unitOfWork.DocumentkindRepository.GetById(id);
        }

        /// <summary>
        /// Добавляет заданный вид документа в хранилище данных
        /// и возвращает идентификатор добавленного вида документа.
        /// </summary>
        /// <param name="documentkind">Экземпляр вида документа для добавления.</param>
        /// <returns>Идентификатор вида документа.</returns>
        public byte AddDocumentkind(DOCUMENTKIND documentkind,
            string userId = "",
            string userInfo = "")
        {
            _unitOfWork.DocumentkindRepository.Insert(documentkind);
            _unitOfWork.SaveChanges(userId, userInfo);
            return documentkind.DOCUMENTKINDID;
        }

        /// <summary>
        /// Обновляет информацию заданной вида документа в хранилище данных.
        /// </summary>
        /// <param name="documentkind">Экземпляр вида документа для обновления.</param>
        /// <returns> Task </returns>
        public void UpdateDocumentkind(DOCUMENTKIND documentkind,
            string userId = "",
            string userInfo = "")
        {
            _unitOfWork.DocumentkindRepository.UpdateSimpleProperties(documentkind, documentkind.DOCUMENTKINDID);
            _unitOfWork.SaveChanges(userId, userInfo);
        }

        /// <summary>
        /// Удаляет вид документа с заданным идентификатором из хранилища данных.
        /// </summary>
        /// <param name="id">Идентификатор вида документа.</param>
        /// <returns> Task </returns>
        public void DeleteDocumentkind(byte id,
            string userId = "",
            string userInfo = "")
        {
            _unitOfWork.DocumentkindRepository.Delete(id);
            _unitOfWork.SaveChanges(userId, userInfo);
        }

        /// <summary>
        /// Проверяет наличие вида документа в хранилище данных
        /// соответствующего заданному фильтру.
        /// </summary>
        /// <param name="filter">Лямбда-выражение определяющее фильтр для поиска вида документа.</param>
        /// <returns>Возвращает <see langword="true"/>, если вид документа существует в хранилище данных.</returns>
        public bool DocumentkindExists(Expression<Func<DOCUMENTKIND, bool>> filter)
        {
            return _unitOfWork.DocumentkindRepository.Get(filter: filter)
                      .FirstOrDefault() != null;
        }
    }
}