﻿using Focus.BL.Common.Services;
using Focus.BL.Extensions;
using Focus.DAL.Common.DbModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Focus.BL.Services
{
    public partial class AuditDocService : IAuditDocService
    {
        private Dictionary<short?, IEnumerable<TBase>> FillWith<TBase, TChild>(Dictionary<short?, IEnumerable<TBase>> baseTree, Dictionary<short?, IEnumerable<TChild>> childTree, Expression<Func<TChild, object>> sorting) where TBase : class where TChild : class
        {
            // Chech wether one of the trees are null values or empty.
            if (IsNullOrEmpty(baseTree))
            {
                return GetEmptyTree<TBase>();
            }
            else
            {
                if (IsNullOrEmpty(childTree))
                {
                    return baseTree;
                }
            }

            // Sorting child tree enumeration values.
            Dictionary<short?, IEnumerable<TChild>> childTreeSorted = new Dictionary<short?, IEnumerable<TChild>>();
            foreach (short? key in childTreeSorted.Keys)
            {
                childTreeSorted.Add(key, childTree[key].AsQueryable().OrderByDescending(sorting).ToList());
            }

            const string FIELD_NAME_LAST_UPDATE = "LAST_UPDATE";
            string childTypeName = typeof(TChild).Name;
            foreach (short? baseTreeKey in baseTree.Keys)
            {
                foreach (TBase baseEntityVersion in baseTree[baseTreeKey])
                {
                    TChild child = baseEntityVersion.GetValue(childTypeName) as TChild;
                    string childEntityForiegnKeyName = childTypeName + "ID";
                    short? childId = child.GetValue(childEntityForiegnKeyName) as short?;

                    if (childTree.ContainsKey(childId))
                    {
                        DateTime baseEntityVersionTimeStamp = (DateTime)baseEntityVersion.GetValue(FIELD_NAME_LAST_UPDATE);
                        foreach (TChild childEntityVersion in childTree[childId])
                        {
                            DateTime childEntityVersionTimeStamp = (DateTime)childEntityVersion.GetValue(FIELD_NAME_LAST_UPDATE);
                            if (baseEntityVersionTimeStamp >= childEntityVersionTimeStamp)
                            {
                                baseEntityVersion.SetValue(childTypeName, childEntityVersion);
                                break;
                            }
                        }
                    }
                }
            }
            return baseTree;
        }

        private Dictionary<short?, IEnumerable<TType>> FillWith<TType>(Dictionary<short?, IEnumerable<TType>> tree, ICollection<COUNTRY> countries) where TType : class
        {
            if (IsNullOrEmpty(tree))
            {
                return GetEmptyTree<TType>();
            }
            else
            {
                if (IsNullOrEmpty(countries))
                {
                    return tree;
                }
            }
            const string COUNTRY_TYPE_NAME = "COUNTRY"; 
            foreach (short? key in tree.Keys)
            {
                foreach (TType entityVersion in tree[key])
                {
                    COUNTRY country = entityVersion.GetValue(COUNTRY_TYPE_NAME) as COUNTRY;
                    if (country != null)
                    {
                        COUNTRY countryFilled = countries.Where(c => c.ABBR2 == country.ABBR2).FirstOrDefault();
                        entityVersion.SetValue(COUNTRY_TYPE_NAME, countryFilled);
                    }
                }
            }
            return tree;
        }
    }
}
