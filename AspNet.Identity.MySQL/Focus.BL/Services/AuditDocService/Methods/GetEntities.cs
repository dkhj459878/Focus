﻿using Focus.BL.Common.Services;
using System.Collections.Generic;

namespace Focus.BL.Services
{
    public partial class AuditDocService : IAuditDocService
    {
        private Dictionary<short?, IEnumerable<TType>> GetEntities<TType>(IEnumerable<short?> identities) where TType : class
        {
            if (IsNullOrEmpty(identities))
            {
                return GetEmptyTree<TType>();
            }

            string typeName = typeof(TType).Name;
            const string CAR_TYPE_NAME = "CAR"; 
            const string CONTAINER_TYPE_NAME = "CONTAINER";
            const string DEVICE_TYPE_NAME = "DEVICE";
            const string VEHICLE_TYPE_NAME = "VEHICLE";
            const string VEHICLEKIND_TYPE_NAME = "VEHICLEKIND";
            const string VEHICLETYPE_TYPE_NAME = "VEHICLETYPE";
            const string DEVICETYPE_TYPE_NAME = "DEVICETYPE";

            switch (typeName)
            {
                case CAR_TYPE_NAME:
                    return GetCars(identities) as Dictionary<short?, IEnumerable<TType>>;
                case CONTAINER_TYPE_NAME:
                    return GetContainers(identities) as Dictionary<short?, IEnumerable<TType>>;
                case DEVICE_TYPE_NAME:
                    return GetDevices(identities) as Dictionary<short?, IEnumerable<TType>>;
                case VEHICLE_TYPE_NAME:
                    return GetVehicles(identities) as Dictionary<short?, IEnumerable<TType>>;
                case VEHICLEKIND_TYPE_NAME:
                    return GetVehicleKinds(identities) as Dictionary<short?, IEnumerable<TType>>;
                case VEHICLETYPE_TYPE_NAME:
                    return GetVehicleTypes(identities) as Dictionary<short?, IEnumerable<TType>>;
                case DEVICETYPE_TYPE_NAME:
                    return GetDeviceTypes(identities) as Dictionary<short?, IEnumerable<TType>>;
                default:
                    return new Dictionary<short?, IEnumerable<TType>>();
            }
        }
    }
}
