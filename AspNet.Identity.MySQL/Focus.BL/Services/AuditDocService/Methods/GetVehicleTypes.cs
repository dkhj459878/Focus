﻿using Focus.BL.Common.Services;
using Focus.DAL.Common.DbModels;
using System.Collections.Generic;
using System.Linq;

namespace Focus.BL.Services
{
    public partial class AuditDocService : IAuditDocService
    {
        private const string FIELD_VEHICLETYPE_ENTITY_NAME = "VEHICLETYPE";
        private Dictionary<short?, IEnumerable<VEHICLETYPE>> GetVehicleTypes(IEnumerable<short?> vehicleTypeIdentities)
        {
            // Беру все по истории.
            List<AUDIT_TRAIL> auditTrails = _audit_TrailService.GetAllAudit_trails(filter: audit => audit.ENTITY == FIELD_VEHICLETYPE_ENTITY_NAME && !vehicleTypeIdentities.ToList().Contains(audit.ENTITYID)).ToList();
            // Смотрю то, чего нет в истории.
            IEnumerable<short?> vehicleTypesIdsNotInAudit = vehicleTypeIdentities.ToList().Intersect(auditTrails.Select(c => c.ENTITYID).Distinct());
            // Беру их bз базы.
            Dictionary<short?, IEnumerable<VEHICLETYPE>> vehicleTypesInDb = GetVehicleTypesFromDatabase(vehicleTypesIdsNotInAudit);
            Dictionary<short?, IEnumerable<VEHICLETYPE>> vehicleTypesInAudit = GetVehicleTypesFromAudit(auditTrails);
            Dictionary<short?, IEnumerable<VEHICLETYPE>> vehicleTypes = new Dictionary<short?, IEnumerable<VEHICLETYPE>>(vehicleTypesInDb);
            // Конкотеначу их.
            foreach (KeyValuePair<short?, IEnumerable<VEHICLETYPE>> vehicleType in vehicleTypesInDb)
            {
                vehicleTypes.Add(vehicleType.Key, vehicleType.Value);
            };
            return vehicleTypes;
        }

        private Dictionary<short?, IEnumerable<VEHICLETYPE>> GetVehicleTypesFromAudit(List<AUDIT_TRAIL> auditTrails)
        {
            Dictionary<short?, IEnumerable<VEHICLETYPE>> vehicleTypes = new Dictionary<short?, IEnumerable<VEHICLETYPE>>();
            Dictionary<short?, List<AUDIT_TRAIL>> audit_TrailsGroup = auditTrails.GroupBy(p => p.ENTITYID).ToDictionary(g => g.Key, g => g.ToList());
            foreach (short? vehicleTypeId in audit_TrailsGroup.Keys)
            {
                vehicleTypes.Add(vehicleTypeId, _retrievingEntityService.RetrieveEntityVersions<VEHICLETYPE>(auditTrails));
            }
            return vehicleTypes;
        }

        private Dictionary<short?, IEnumerable<VEHICLETYPE>> GetVehicleTypesFromDatabase(IEnumerable<short?> vehicleTypesIdEpsentInAuditTrails)
        {
            IEnumerable<VEHICLETYPE> vehicleTypes = _vehicletypeService.GetAllVehicletypes(filter: vehicleType => vehicleTypesIdEpsentInAuditTrails.ToList().Contains(vehicleType.VEHICLETYPEID));
            if (vehicleTypes == null || vehicleTypes.Count() <1)
            {
                return new Dictionary<short?, IEnumerable<VEHICLETYPE>>();
            }
            Dictionary<short?, IEnumerable<VEHICLETYPE>> vehicleTypesDictionary = new Dictionary<short?, IEnumerable<VEHICLETYPE>>();
            Dictionary<byte, List<VEHICLETYPE>> vehicleTypesGroup = vehicleTypes.GroupBy(p => p.VEHICLETYPEID).ToDictionary(g => g.Key, g => g.ToList());
            foreach (KeyValuePair<byte, List<VEHICLETYPE>> vehicleType in vehicleTypesGroup)
            {
                vehicleTypesDictionary.Add(vehicleType.Key, vehicleType.Value);
            }
            return vehicleTypesDictionary;
        }
    }
}
