﻿using Focus.BL.Common.Services;
using Focus.DAL.Common.DbModels;
using System.Collections.Generic;
using System.Linq;

namespace Focus.BL.Services
{
    public partial class AuditDocService : IAuditDocService
    {
        const string FIELD_VEHICLE_ENTITY_NAME = "VEHICLE";
        private Dictionary<short?, IEnumerable<VEHICLE>> GetVehicles(IEnumerable<short?> vehicleIdentities)
        {
            // Беру все по истории.
            List<AUDIT_TRAIL> auditTrails = _audit_TrailService.GetAllAudit_trails(filter: audit => audit.ENTITY == FIELD_VEHICLE_ENTITY_NAME && !vehicleIdentities.ToList().Contains(audit.ENTITYID)).ToList();
            // Смотрю то, чего нет в истории.
            IEnumerable<short?> vehiclesIdsNotInAudit = vehicleIdentities.ToList().Intersect(auditTrails.Select(c => c.ENTITYID).Distinct());
            // Беру их bз базы.
            Dictionary<short?, IEnumerable<VEHICLE>> vehiclesInDb = GetVehiclesFromDatabase(vehiclesIdsNotInAudit);
            Dictionary<short?, IEnumerable<VEHICLE>> vehiclesInAudit = GetVehiclesFromAudit(auditTrails);
            Dictionary<short?, IEnumerable<VEHICLE>> vehicles = new Dictionary<short?, IEnumerable<VEHICLE>>(vehiclesInDb);
            // Конкотеначу их.
            foreach (KeyValuePair<short?, IEnumerable<VEHICLE>> vehicle in vehiclesInDb)
            {
                vehicles.Add(vehicle.Key, vehicle.Value);
            };
            return vehicles;
        }

        private Dictionary<short?, IEnumerable<VEHICLE>> GetVehiclesFromAudit(List<AUDIT_TRAIL> auditTrails)
        {
            Dictionary<short?, IEnumerable<VEHICLE>> vehicles = new Dictionary<short?, IEnumerable<VEHICLE>>();
            Dictionary<short?, List<AUDIT_TRAIL>> audit_TrailsGroup = auditTrails.GroupBy(p => p.ENTITYID).ToDictionary(g => g.Key, g => g.ToList());
            foreach (short? vehicleId in audit_TrailsGroup.Keys)
            {
                vehicles.Add(vehicleId, _retrievingEntityService.RetrieveEntityVersions<VEHICLE>(auditTrails));
            }
            return vehicles;
        }

        private Dictionary<short?, IEnumerable<VEHICLE>> GetVehiclesFromDatabase(IEnumerable<short?> vehiclesIdEpsentInAuditTrails)
        {
            IEnumerable<VEHICLE> vehicles = _vehicleService.GetAllVehicles(filter: vehicle => vehiclesIdEpsentInAuditTrails.ToList().Contains(vehicle.VEHICLEID));
            if (vehicles == null || vehicles.Count() < 1)
            {
                return new Dictionary<short?, IEnumerable<VEHICLE>>();
            }
            Dictionary<short?, IEnumerable<VEHICLE>> vehiclesDictionary = new Dictionary<short?, IEnumerable<VEHICLE>>();
            Dictionary<short, List<VEHICLE>> vehiclesGroup = vehicles.GroupBy(p => p.VEHICLEID).ToDictionary(g => g.Key, g => g.ToList());
            foreach (KeyValuePair<short, List<VEHICLE>> vehicle in vehiclesGroup)
            {
                vehiclesDictionary.Add(vehicle.Key, vehicle.Value);
            }
            return vehiclesDictionary;
        }
    }
}
