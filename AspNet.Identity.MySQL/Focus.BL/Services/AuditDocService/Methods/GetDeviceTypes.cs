﻿using Focus.BL.Common.Services;
using Focus.DAL.Common.DbModels;
using System.Collections.Generic;
using System.Linq;

namespace Focus.BL.Services
{
    public partial class AuditDocService : IAuditDocService
    {
        private const string FIELD_DEVICETYPE_ENTITY_NAME = "DEVICETYPE";
        private Dictionary<short?, IEnumerable<DEVICETYPE>> GetDeviceTypes(IEnumerable<short?> deviceTypeIdentities)
        {
            // Беру все по истории.
            List<AUDIT_TRAIL> auditTrails = _audit_TrailService.GetAllAudit_trails(filter: audit => audit.ENTITY == FIELD_DEVICETYPE_ENTITY_NAME && !deviceTypeIdentities.ToList().Contains(audit.ENTITYID)).ToList();
            // Смотрю то, чего нет в истории.
            IEnumerable<short?> deviceTypesIdsNotInAudit = deviceTypeIdentities.ToList().Intersect(auditTrails.Select(c => c.ENTITYID).Distinct());
            // Беру их bз базы.
            Dictionary<short?, IEnumerable<DEVICETYPE>> deviceTypesInDb = GetDeviceTypesFromDatabase(deviceTypesIdsNotInAudit);
            Dictionary<short?, IEnumerable<DEVICETYPE>> deviceTypesInAudit = GetDeviceTypesFromAudit(auditTrails);
            Dictionary<short?, IEnumerable<DEVICETYPE>> deviceTypes = new Dictionary<short?, IEnumerable<DEVICETYPE>>(deviceTypesInDb);
            // Конкотеначу их.
            foreach (KeyValuePair<short?, IEnumerable<DEVICETYPE>> deviceType in deviceTypesInDb)
            {
                deviceTypes.Add(deviceType.Key, deviceType.Value);
            };
            return deviceTypes;
        }

        private Dictionary<short?, IEnumerable<DEVICETYPE>> GetDeviceTypesFromAudit(List<AUDIT_TRAIL> auditTrails)
        {
            Dictionary<short?, IEnumerable<DEVICETYPE>> deviceTypes = new Dictionary<short?, IEnumerable<DEVICETYPE>>();
            Dictionary<short?, List<AUDIT_TRAIL>> audit_TrailsGroup = auditTrails.GroupBy(p => p.ENTITYID).ToDictionary(g => g.Key, g => g.ToList());
            foreach (short? deviceTypeId in audit_TrailsGroup.Keys)
            {
                deviceTypes.Add(deviceTypeId, _retrievingEntityService.RetrieveEntityVersions<DEVICETYPE>(auditTrails));
            }
            return deviceTypes;
        }

        private Dictionary<short?, IEnumerable<DEVICETYPE>> GetDeviceTypesFromDatabase(IEnumerable<short?> deviceTypesIdEpsentInAuditTrails)
        {
            IEnumerable<DEVICETYPE> deviceTypes = _devicetypeService.GetAllDevicetypes(filter: deviceType => deviceTypesIdEpsentInAuditTrails.ToList().Contains(deviceType.DEVICETYPEID));
            if (deviceTypes == null || deviceTypes.Count() <1)
            {
                return new Dictionary<short?, IEnumerable<DEVICETYPE>>();
            }
            Dictionary<short?, IEnumerable<DEVICETYPE>> deviceTypesDictionary = new Dictionary<short?, IEnumerable<DEVICETYPE>>();
            Dictionary<byte, List<DEVICETYPE>> deviceTypesGroup = deviceTypes.GroupBy(p => p.DEVICETYPEID).ToDictionary(g => g.Key, g => g.ToList());
            foreach (KeyValuePair<byte, List<DEVICETYPE>> deviceType in deviceTypesGroup)
            {
                deviceTypesDictionary.Add(deviceType.Key, deviceType.Value);
            }
            return deviceTypesDictionary;
        }
    }
}
