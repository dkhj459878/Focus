﻿using Focus.BL.Common.Services;
using Focus.DAL.Common.DbModels;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Focus.BL.Services
{
    public partial class AuditDocService : IAuditDocService
    {
        private Dictionary<Type, Dictionary<short?, IEnumerable<VEHICLETYPE>>> GetVehicleTypesCompleted(Dictionary<Type, IEnumerable<short?>> graph)
        {
            const string FIELD_NAME_IN_AUDIT_TRAIL = "VEHICLETYPE";
            IEnumerable<AUDIT_TRAIL> auditTrails = new List<AUDIT_TRAIL>(_audit_TrailService.GetAllAudit_trails(filter: p => p.ENTITY == FIELD_NAME_IN_AUDIT_TRAIL && graph.Values.FirstOrDefault().Contains(p.ENTITYID)));
            Dictionary<short?, List<AUDIT_TRAIL>> auditTrailGroupById = GroupById(auditTrails.ToList());
            Dictionary<Type, Dictionary<short?, IEnumerable<VEHICLETYPE>>> vihecleTypesCompleted = new Dictionary<Type, Dictionary<short?, IEnumerable<VEHICLETYPE>>>();
            foreach (short? key in auditTrailGroupById.Keys)
            {
                IEnumerable<VEHICLETYPE> vehicleTypes = _retrievingEntityService.RetrieveEntityVersions<VEHICLETYPE>(auditTrailGroupById[key]);
                vihecleTypesCompleted.Add(typeof(AUDIT_TRAIL), new Dictionary<short?, IEnumerable<VEHICLETYPE>>() { { key, vehicleTypes } });
            }
            List<short?> keysEpsentInAudit = graph.Values.FirstOrDefault().Intersect(vihecleTypesCompleted.Values.FirstOrDefault().Keys).ToList();
            if (IsNullOrEmpty(keysEpsentInAudit))
            {
                return vihecleTypesCompleted;
            }

            foreach (short? key in keysEpsentInAudit)
            {
                VEHICLETYPE vehicleType = _vehicletypeService.GetVehicletype((byte)key);
                if (vehicleType != null)
                {
                    vihecleTypesCompleted.Add(typeof(AUDIT_TRAIL), new Dictionary<short?, IEnumerable<VEHICLETYPE>>() { { key, new List<VEHICLETYPE>() { vehicleType } } });
                }
            }

            return vihecleTypesCompleted;
        }
    }
}
