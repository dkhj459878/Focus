﻿using Focus.BL.Common.Services;
using Focus.DAL.Common.DbModels;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Focus.BL.Services
{
    public partial class AuditDocService : IAuditDocService
    {
        private Dictionary<Type, IEnumerable<short?>> GetGraph(List<AUDIT_TRAIL> audit_Trails)
        {
            Dictionary<DateTime?, List<AUDIT_TRAIL>> audit_TrailsResult = GroupByDateTime(audit_Trails);

            return _requestService.GetIdentitiesNextGeneration(audit_TrailsResult);
        }

        private Dictionary<Type, IEnumerable<short?>> GetGraph(Dictionary<Type, IEnumerable<short?>> firstGraph)
        {
            IEnumerable<AUDIT_TRAIL> auditTrails = RetrieveAuditTrails(firstGraph);

            if (IsNullOrEmpty(auditTrails))
            {
                return GetEmptyTree();
            }

            return GetGraph(auditTrails.ToList());
        }

        private IEnumerable<string> GetTypeNamsFromAuditTrails(IEnumerable<AUDIT_TRAIL> auditTrails)
        {
            if (IsNullOrEmpty(auditTrails))
            {
                return GetEmptyList<string>();
            }

            return auditTrails.Select(a => a.ENTITY).Distinct();
        }
    }
}
