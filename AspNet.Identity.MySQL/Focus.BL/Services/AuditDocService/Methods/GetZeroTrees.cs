﻿using Focus.BL.Common.Services;
using Focus.DAL.Common.DbModels;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Focus.BL.Services
{
    public partial class AuditDocService : IAuditDocService
    {
        private Dictionary<short?, IEnumerable<DOC>> GetZeroTrees(List<AUDIT_TRAIL> audit_Trails)
        {
            IEnumerable<DOC> docVersions = _retrievingEntityService.RetrieveEntityVersions<DOC>(audit_Trails);
            short? id = audit_Trails.FirstOrDefault().ENTITYID;
            Type type = typeof(DOC);

            return new Dictionary<short?, IEnumerable<DOC>>() { { id, new List<DOC>(docVersions) } } ;
        }
    }
}
