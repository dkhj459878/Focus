﻿using Focus.BL.Common.Services;
using Focus.DAL.Common.DbModels;
using System.Collections.Generic;
using System.Linq;

namespace Focus.BL.Services
{
    public partial class AuditDocService : IAuditDocService
    {
        private const string FIELD_ENTITY_NAME = "CAR";
        private Dictionary<short?, IEnumerable<CAR>> GetCars(IEnumerable<short?> carIdentities)
        {
            // Беру все по истории.
            List<AUDIT_TRAIL> auditTrails = _audit_TrailService.GetAllAudit_trails(filter: audit => audit.ENTITY == FIELD_ENTITY_NAME && !carIdentities.ToList().Contains(audit.ENTITYID)).ToList();
            // Смотрю то, чего нет в истории.
            IEnumerable<short?> carsIdsNotInAudit = carIdentities.ToList().Intersect(auditTrails.Select(c => c.ENTITYID).Distinct());
            // Беру их bз базы.
            Dictionary<short?, IEnumerable<CAR>> carsInDb = GetCarsFromDatabase(carsIdsNotInAudit);
            Dictionary<short?, IEnumerable<CAR>> carsInAudit = GetCarsFromAudit(auditTrails);
            Dictionary<short?, IEnumerable<CAR>> cars = new Dictionary<short?, IEnumerable<CAR>>(carsInDb);
            // Конкотеначу их.
            foreach (KeyValuePair<short?, IEnumerable<CAR>> car in carsInDb)
            {
                cars.Add(car.Key, car.Value);
            };
            return cars;
        }

        private Dictionary<short?, IEnumerable<CAR>> GetCarsFromAudit(List<AUDIT_TRAIL> auditTrails)
        {
            Dictionary<short?, IEnumerable<CAR>> cars = new Dictionary<short?, IEnumerable<CAR>>();
            Dictionary<short?, List<AUDIT_TRAIL>> audit_TrailsGroup = auditTrails.GroupBy(p => p.ENTITYID).ToDictionary(g => g.Key, g => g.ToList());
            foreach (short? carId in audit_TrailsGroup.Keys)
            {
                cars.Add(carId, _retrievingEntityService.RetrieveEntityVersions<CAR>(auditTrails));
            }
            return cars;
        }

        private Dictionary<short?, IEnumerable<CAR>> GetCarsFromDatabase(IEnumerable<short?> carsIdEpsentInAuditTrails)
        {
            IEnumerable<CAR> cars = _carService.GetAllCars(filter: car => carsIdEpsentInAuditTrails.ToList().Contains(car.CARID));
            if (cars == null || cars.Count() <1)
            {
                return new Dictionary<short?, IEnumerable<CAR>>();
            }
            Dictionary<short?, IEnumerable<CAR>> carsDictionary = new Dictionary<short?, IEnumerable<CAR>>();
            Dictionary<short, List<CAR>> carsGroup = cars.GroupBy(p => p.CARID).ToDictionary(g => g.Key, g => g.ToList());
            foreach (KeyValuePair<short, List<CAR>> car in carsGroup)
            {
                carsDictionary.Add(car.Key, car.Value);
            }
            return carsDictionary;
        }
    }
}
