﻿using Focus.BL.Common.Services;
using System;
using System.Collections.Generic;

namespace Focus.BL.Services
{
    public partial class AuditDocService : IAuditDocService
    {
        private Dictionary<Type, IEnumerable<short?>> GetEmptyGraph()
        {
            return new Dictionary<Type, IEnumerable<short?>>();
        }

        private List<TEntity> GetEmptyList<TEntity>() where TEntity : class
        {
            return new List<TEntity>();
        }

        private Dictionary<Type, IEnumerable<short?>> GetEmptyTree()
        {
            return new Dictionary<Type, IEnumerable<short?>>();
        }

        private Dictionary<short?, IEnumerable<TEntity>> GetEmptyTree<TEntity>() where TEntity : class
        {
            return new Dictionary<short?, IEnumerable<TEntity>>();
        }
    }
}
