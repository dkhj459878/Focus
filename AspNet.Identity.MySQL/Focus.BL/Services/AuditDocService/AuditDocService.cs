﻿using Focus.BL.Common.Services;
using Focus.DAL.Common.DbModels;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Focus.BL.Services
{
    public partial class AuditDocService : IAuditDocService
    {
        #region Startup.
        private readonly IAudit_trailService _audit_TrailService;
        private readonly IRequestService _requestService;
        private readonly IContainerService _containerService;
        private readonly ICarService _carService;
        private readonly IDeviceService _deviceService;
        private readonly IVehiclekindService _vehiclekindService;
        private readonly ICountryService _countryService;
        private readonly IRetrievingEntityOptimaService _retrievingEntityService;
        private readonly IVehicleService _vehicleService;
        private readonly IVehicletypeService _vehicletypeService;
        private readonly IDevicetypeService _devicetypeService;

        public AuditDocService(IAudit_trailService audit_TrailService, IRequestService requestService,
            IContainerService containerService, ICarService carService, IDeviceService deviceService,
            IVehiclekindService vehiclekindService, ICountryService countryService,
            IRetrievingEntityOptimaService retrievingEntityService,
            IVehicleService vehicleService,
            IVehicletypeService vehicletypeService,
            IDevicetypeService devicetypeService
            )
        {
            _audit_TrailService = audit_TrailService;
            _requestService = requestService;
            _carService = carService;
            _deviceService = deviceService;
            _vehiclekindService = vehiclekindService;
            _countryService = countryService;
            _retrievingEntityService = retrievingEntityService;
            _vehicleService = vehicleService;
            _vehicletypeService = vehicletypeService;
            _devicetypeService = devicetypeService;
            _containerService = containerService;
        }
        #endregion

        public List<DOC> GetDocVersions(short id)
        {
            string entityTypeName = typeof(DOC).Name;

            // Retrieving from database audit records.
            List<AUDIT_TRAIL> audit_Trails = _audit_TrailService.GetAllAudit_trails(filter: audit => audit.ENTITY == entityTypeName && audit.ENTITYID == id).ToList();

            // Check whether necessary data exist.
            if (IsNullOrEmpty(audit_Trails))
            {
                return GetEmptyList<DOC>();
            }
            else
            {
                // Конвертируем записи аудита в версии документа, представленные в виде, так называемого 
                // фрейма, содержащий в корне тип сущности, который ссылается на библиотеку связки ключа 
                // на его версию.
                Dictionary<short?, IEnumerable<DOC>> zeroTree = GetZeroTrees(audit_Trails);

                #region Getting countries.
                // Получаем страны. Их версии не перебираем, так как полагаем, что их названия и коды не изменятются.
                ICollection<COUNTRY> countries = _countryService.GetAllCountries().ToList();
                #endregion

                // Получаем первый граф.
                Dictionary<Type, IEnumerable<short?>> firstGraph = GetGraph(audit_Trails);
            
                if (IsNullOrEmpty(firstGraph))
                {
                    // Если граф пустой, ссылочных данных нет, возвращаем версии документов.
                    return  _retrievingEntityService.RetrieveEntityVersions<DOC>(audit_Trails).ToList();
                }

                // Получаем неполные сущности из первого уровня.
                Dictionary<short?, IEnumerable<CAR>> carsTree = GetEntities<CAR>(firstGraph[typeof(CAR)]);
                Dictionary<short?, IEnumerable<CONTAINER>> containersTree = GetEntities<CONTAINER>(firstGraph[typeof(CONTAINER)]);
                Dictionary<short?, IEnumerable<DEVICE>> devicesTree = GetEntities<DEVICE>(firstGraph[typeof(DEVICE)]);
                Dictionary<short?, IEnumerable<VEHICLE>> vehiclesTree = GetEntities<VEHICLE>(firstGraph[typeof(VEHICLE)]);
                Dictionary<short?, IEnumerable<DEVICETYPE>> deviceTypesTree = GetEntities<DEVICETYPE>(firstGraph[typeof(DEVICETYPE)]);
                Dictionary<short?, IEnumerable<VEHICLEKIND>> vehicleKindsTree = GetEntities<VEHICLEKIND>(firstGraph[typeof(VEHICLEKIND)]);
                Dictionary<short?, IEnumerable<VEHICLETYPE>> vehicleTypesTree = new Dictionary<short?, IEnumerable<VEHICLETYPE>>();

                // Получаем граф второго уровня.
                Dictionary<Type, IEnumerable<short?>> secondGraph = GetGraph(firstGraph);

                // Если граф второго уровня равен нулю или он пустой, то возвращаем документы из первого дерева.
                if (!IsNullOrEmpty(secondGraph))
                {
                    // Дополняем первое дерево вторым.
                    vehicleTypesTree = GetEntities<VEHICLETYPE>(secondGraph[typeof(VEHICLETYPE)]);
                    vehicleKindsTree = FillWith(vehicleKindsTree, vehicleTypesTree, t => t.LAST_UPDATE);
                }
                else
                {
                    // Дополняем первое деверов из первого.
                    carsTree = FillWith(carsTree, vehicleKindsTree, k => k.LAST_UPDATE);
                    carsTree = FillWith(carsTree, countries);

                    containersTree = FillWith(containersTree, countries);

                    devicesTree = FillWith(devicesTree, deviceTypesTree, t => t.LAST_UPDATE);

                    vehiclesTree = FillWith(vehiclesTree, vehicleKindsTree, t => t.LAST_UPDATE);
                    vehiclesTree = FillWith(vehiclesTree, countries);
                }
                
                // Дополняем нулевое дерево первым.
                zeroTree = FillWith(zeroTree, carsTree, c => c.LAST_UPDATE);
                zeroTree = FillWith(zeroTree, containersTree, c => c.LAST_UPDATE);
                zeroTree = FillWith(zeroTree, devicesTree, d => d.LAST_UPDATE);
                zeroTree = FillWith(zeroTree, vehiclesTree, v => v.LAST_UPDATE);

                return GetDocs(zeroTree);
            }
        }

        private List<DOC> GetDocs(Dictionary<short?, IEnumerable<DOC>> zeroTree)
        {
            return zeroTree.Values.FirstOrDefault().ToList();
        }

        #region  Some methods.
        /// <summary>
        /// Convert audit records to audit records groups grouped by the time stamp of the 
        /// CRUD operations under the entity.
        /// </summary>
        /// <param name="audit_Trails">Audit records for certain entity.</param>
        /// <returns>Audit records groups grouped by the time stamp of the 
        /// CRUD operations under the entity.</returns>
        private Dictionary<DateTime?, List<AUDIT_TRAIL>> GroupByDateTime(List<AUDIT_TRAIL> audit_Trails)
        {
            // Group audit records in groups where key is a DateTime value and ordering groups by DateTime of the creation.
            Dictionary<DateTime?, List<AUDIT_TRAIL>> audit_TrailsGroup = audit_Trails.GroupBy(p => p.DATE_).ToDictionary(g => g.Key, g => g.ToList());
            SortedDictionary<DateTime?, List<AUDIT_TRAIL>> audit_TrailsGroupSorted = new SortedDictionary<DateTime?, List<AUDIT_TRAIL>>(audit_TrailsGroup);

            // Pump audit records group get above in new collection.
            Dictionary<DateTime?, List<AUDIT_TRAIL>> audit_TrailsResult = new Dictionary<DateTime?, List<AUDIT_TRAIL>>();
            foreach (KeyValuePair<DateTime?, List<AUDIT_TRAIL>> audit_Trail in audit_TrailsGroupSorted)
            {
                audit_TrailsResult.Add(audit_Trail.Key, audit_Trail.Value);
            }
            return audit_TrailsResult;
        }

        /// <summary>
        /// Convert audit records to audit records groups grouped by the id of the 
        /// CRUD operations under the entity.
        /// </summary>
        /// <param name="audit_Trails">Audit records for certain entity.</param>
        /// <returns>Audit records groups grouped by the id of the 
        /// CRUD operations under the entity.</returns>
        private Dictionary<short?, List<AUDIT_TRAIL>> GroupById(List<AUDIT_TRAIL> audit_Trails)
        {
            // Group audit records in groups where key is a DateTime value and ordering groups by DateTime of the creation.
            Dictionary<short?, List<AUDIT_TRAIL>> audit_TrailsGroup = audit_Trails.GroupBy(p => p.ENTITYID).ToDictionary(g => g.Key, g => g.ToList());
            SortedDictionary<short?, List<AUDIT_TRAIL>> audit_TrailsGroupSorted = new SortedDictionary<short?, List<AUDIT_TRAIL>>(audit_TrailsGroup);

            // Pump audit records group get above in new collection.
            Dictionary<short?, List<AUDIT_TRAIL>> audit_TrailsResult = new Dictionary<short?, List<AUDIT_TRAIL>>();
            foreach (KeyValuePair<short?, List<AUDIT_TRAIL>> audit_Trail in audit_TrailsGroupSorted)
            {
                audit_TrailsResult.Add(audit_Trail.Key, audit_Trail.Value);
            }
            return audit_TrailsResult;
        }
        #endregion
    }
}
