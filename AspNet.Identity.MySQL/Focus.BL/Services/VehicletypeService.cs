﻿using Focus.BL.Common.Services;
using Focus.DAL.Common.DbModels;
using Focus.DAL.Common.UoW;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Focus.BL.Services
{
    /// <summary>
    /// Представляет сервис для работы с типами транспортных средств
    /// в хранилище данных.
    /// </summary>
    public class VehicletypeService : BaseService, IVehicletypeService
    {
        /// <summary>
        /// Создает новый экземпляр <see cref="VehicletypeManagementService"/>
        /// с заданным unit of work.
        /// </summary>
        /// <param name="unitOfWork"> uow </param>
        /// <param name="vehicletypemanager"> manager </param>
        public VehicletypeService(IUnitOfWork unitOfWork)
            : base(unitOfWork)
        {
        }

        /// <summary>
        /// Возвращает все типы транспортных средств
        /// из хранилища данных.
        /// </summary>
        /// <param name="filter">Лямбда-выражение определяющее фильтрацию экземпляров.</param>
        /// <param name="orderBy">Лямбда-выражение определяющее сортировку экземпляров.</param>
        /// <param name="includeProperties">Список связанных свойств экземпляров, разделенный запятыми.</param>
        /// <returns>Коллекция экземпляров типов транспортных средств для пейджинга.</returns>
        public IEnumerable<VEHICLETYPE> GetAllVehicletypes(
            Expression<Func<VEHICLETYPE, bool>> filter = null,
            Func<IQueryable<VEHICLETYPE>, IOrderedQueryable<VEHICLETYPE>> orderBy = null,
            string includeProperties = "")
        {
            return _unitOfWork.VehicletypeRepository.Get(filter, orderBy, includeProperties);
        }

        /// <summary>
        /// Возвращает тип транспортного средства с заданным идентификатором из хранилища данных.
        /// </summary>
        /// <param name="pageSize">Количество сведений о типе транспортного средства, выводимых на страницу.</param>
        /// <param name="pageNumber">Номер выводимой страницы со сведениями о типа транспортного средствах.</param>
        /// <param name="filter">Лямбда-выражение определяющее фильтрацию экземпляров.</param>
        /// <param name="orderBy">Лямбда-выражение определяющее сортировку экземпляров.</param>
        /// <param name="includeProperties">Список связанных свойств экземпляров, разделенный запятыми.</param>
        /// <returns>Экземпляр типа транспортного средства.</returns>
        public IEnumerable<VEHICLETYPE> GetVehicletypesPage(
            int pageSize,
            int pageNumber,
            Expression<Func<VEHICLETYPE, bool>> filter = null,
            Func<IQueryable<VEHICLETYPE>, IOrderedQueryable<VEHICLETYPE>> orderBy = null,
            string includeProperties = "")
        {
            return _unitOfWork.VehicletypeRepository.GetPage(pageSize, pageNumber, filter, orderBy, includeProperties);
        }

        /// <summary>
        /// Возвращает тип транспортного средства с заданным идентификатором из хранилища данных.
        /// </summary>
        /// <param name="id">Идентификатор типа транспортного средства.</param>
        /// <returns>Экземпляр типа транспортного средства.</returns>
        public VEHICLETYPE GetVehicletype(byte id)
        {
            return _unitOfWork.VehicletypeRepository.GetById(id);
        }

        /// <summary>
        /// Добавляет заданный тип транспортного средства в хранилище данных
        /// и возвращает идентификатор добавленного типа транспортного средства.
        /// </summary>
        /// <param name="vehicletype">Экземпляр типа транспортного средства для добавления.</param>
        /// <returns>Идентификатор типа транспортного средства.</returns>
        public byte AddVehicletype(VEHICLETYPE vehicletype,
            string userId = "",
            string userInfo = "")
        {
            _unitOfWork.VehicletypeRepository.Insert(vehicletype);
            _unitOfWork.SaveChanges(userId, userInfo);
            return vehicletype.VEHICLETYPEID;
        }

        /// <summary>
        /// Обновляет информацию заданного типа транспортного средства в хранилище данных.
        /// </summary>
        /// <param name="vehicletype">Экземпляр типа транспортного средства для обновления.</param>
        /// <returns> Task </returns>
        public void UpdateVehicletype(VEHICLETYPE vehicletype,
            string userId = "",
            string userInfo = "")
        {
            _unitOfWork.VehicletypeRepository.UpdateSimpleProperties(vehicletype, vehicletype.VEHICLETYPEID);
            _unitOfWork.SaveChanges(userId, userInfo);
        }

        /// <summary>
        /// Удаляет тип транспортного средства с заданным идентификатором из хранилища данных.
        /// </summary>
        /// <param name="id">Идентификатор типа транспортного средства.</param>
        /// <returns> Task </returns>
        public void DeleteVehicletype(byte id,
            string userId = "",
            string userInfo = "")
        {
            _unitOfWork.VehicletypeRepository.Delete(id);
            _unitOfWork.SaveChanges(userId, userInfo);
        }

        /// <summary>
        /// Проверяет наличие типа транспортного средства в хранилище данных
        /// соответствующего заданному фильтру.
        /// </summary>
        /// <param name="filter">Лямбда-выражение определяющее фильтр для поиска типа транспортного средства.</param>
        /// <returns>Возвращает <see langword="true"/>, если тип транспортного средства существует в хранилище данных.</returns>
        public bool VehicletypeExists(Expression<Func<VEHICLETYPE, bool>> filter)
        {
            return _unitOfWork.VehicletypeRepository.Get(filter: filter)
                      .FirstOrDefault() != null;
        }
    }
}