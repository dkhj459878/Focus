﻿using Focus.BL.Common.Services;
using Focus.DAL.Common.DbModels;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Focus.BL.Services
{
    public partial class AuditOrganizationService : IAuditOrganizationService
    {
        #region Startup.
        private readonly IAudit_trailService _audit_TrailService;
        private readonly ICountryService _countryService;
        private readonly IRetrievingEntityOptimaService _retrievingEntityOptimaService;

        public AuditOrganizationService(ICountryService countryService,
            IRetrievingEntityOptimaService retrievingEntityOptimaService, IAudit_trailService audit_TrailService
            )
        {
            _audit_TrailService = audit_TrailService;
            _countryService = countryService;
            _retrievingEntityOptimaService = retrievingEntityOptimaService;
        }
        #endregion

        public List<ORGANIZATION> GetOrganizationVersions(short id)
        {
            string entityTypeName = typeof(ORGANIZATION).Name;

            // Retrieving from database audit records.
            List<AUDIT_TRAIL> audit_Trails = _audit_TrailService.GetAllAudit_trails(filter: audit => audit.ENTITY == entityTypeName && audit.ENTITYID == id).ToList();

            if (audit_Trails == null || audit_Trails.Count < 1)
            {
                return new List<ORGANIZATION>();
            }

            List<ORGANIZATION> organizations = new List<ORGANIZATION>(_retrievingEntityOptimaService.RetrieveEntityVersions<ORGANIZATION>(audit_Trails).ToList());

            // Seed collection elements.

            return organizations;
        }
    }
}
