﻿using Focus.BL.Common.Services;
using Focus.DAL.Common.DbModels;
using Focus.DAL.Common.UoW;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Focus.BL.Services
{
    /// <summary>
    /// Представляет сервис для работы с ТСТК
    /// в хранилище данных.
    /// </summary>
    public class DeviceService : BaseService, IDeviceService
    {
        /// <summary>
        /// Создает новый экземпляр <see cref="DeviceManagementService"/>
        /// с заданным unit of work.
        /// </summary>
        /// <param name="unitOfWork"> uow </param>
        /// <param name="devicemanager"> manager </param>
        public DeviceService(IUnitOfWork unitOfWork)
            : base(unitOfWork)
        {
        }

        /// <summary>
        /// Возвращает все ТСТК
        /// из хранилища данных.
        /// </summary>
        /// <param name="filter">Лямбда-выражение определяющее фильтрацию экземпляров.</param>
        /// <param name="orderBy">Лямбда-выражение определяющее сортировку экземпляров.</param>
        /// <param name="includeProperties">Список связанных свойств экземпляров, разделенный запятыми.</param>
        /// <returns>Коллекция экземпляров ТСТК для пейджинга.</returns>
        public IEnumerable<DEVICE> GetAllDevices(
            Expression<Func<DEVICE, bool>> filter = null,
            Func<IQueryable<DEVICE>, IOrderedQueryable<DEVICE>> orderBy = null,
            string includeProperties = "")
        {
            return _unitOfWork.DeviceRepository.Get(filter, orderBy, includeProperties);
        }

        /// <summary>
        /// Возвращает ТСТК с заданным идентификатором из хранилища данных.
        /// </summary>
        /// <param name="pageSize">Количество сведений о ТСТКх, выводимых на страницу.</param>
        /// <param name="pageNumber">Номер выводимой страницы со сведениями о ТСТКх.</param>
        /// <param name="filter">Лямбда-выражение определяющее фильтрацию экземпляров.</param>
        /// <param name="orderBy">Лямбда-выражение определяющее сортировку экземпляров.</param>
        /// <param name="includeProperties">Список связанных свойств экземпляров, разделенный запятыми.</param>
        /// <returns>Экземпляр ТСТК.</returns>
        public IEnumerable<DEVICE> GetDevicesPage(
            int pageSize,
            int pageNumber,
            Expression<Func<DEVICE, bool>> filter = null,
            Func<IQueryable<DEVICE>, IOrderedQueryable<DEVICE>> orderBy = null,
            string includeProperties = "")
        {
            return _unitOfWork.DeviceRepository.GetPage(pageSize, pageNumber, filter, orderBy, includeProperties);
        }

        /// <summary>
        /// Возвращает ТСТК с заданным идентификатором из хранилища данных.
        /// </summary>
        /// <param name="id">Идентификатор ТСТК.</param>
        /// <returns>Экземпляр ТСТК.</returns>
        public DEVICE GetDevice(short id)
        {
            return _unitOfWork.DeviceRepository.GetById(id);
        }

        /// <summary>
        /// Добавляет заданный ТСТК в хранилище данных
        /// и возвращает идентификатор добавленного ТСТК.
        /// </summary>
        /// <param name="device">Экземпляр ТСТК для добавления.</param>
        /// <returns>Идентификатор ТСТК.</returns>
        public short AddDevice(DEVICE device,
            string userId = "",
            string userInfo = "")
        {
            _unitOfWork.DeviceRepository.Insert(device);
            _unitOfWork.SaveChanges(userId, userInfo);
            return device.DEVICEID;
        }

        /// <summary>
        /// Обновляет информацию заданного ТСТК в хранилище данных.
        /// </summary>
        /// <param name="device">Экземпляр ТСТК для обновления.</param>
        /// <returns> Task </returns>
        public void UpdateDevice(DEVICE device,
            string userId = "",
            string userInfo = "")
        {
            _unitOfWork.DeviceRepository.UpdateSimpleProperties(device, device.DEVICEID);
            _unitOfWork.SaveChanges(userId, userInfo);
        }

        /// <summary>
        /// Удаляет ТСТК с заданным идентификатором из хранилища данных.
        /// </summary>
        /// <param name="id">Идентификатор ТСТК.</param>
        /// <returns> Task </returns>
        public void DeleteDevice(short id,
            string userId = "",
            string userInfo = "")
        {
            _unitOfWork.DeviceRepository.Delete(id);
            _unitOfWork.SaveChanges(userId, userInfo);
        }

        /// <summary>
        /// Проверяет наличие ТСТК в хранилище данных
        /// соответствующего заданному фильтру.
        /// </summary>
        /// <param name="filter">Лямбда-выражение определяющее фильтр для поиска ТСТК.</param>
        /// <returns>Возвращает <see langword="true"/>, если ТСТК существует в хранилище данных.</returns>
        public bool DeviceExists(Expression<Func<DEVICE, bool>> filter)
        {
            return _unitOfWork.DeviceRepository.Get(filter: filter)
                      .FirstOrDefault() != null;
        }
    }
}