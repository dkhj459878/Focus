﻿using Focus.BL.Common.Services;
using Focus.DAL.Common.DbModels;
using Focus.DAL.Common.UoW;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Focus.BL.Services
{
    /// <summary>
    /// Представляет сервис для работы со странами
    /// в хранилище данных.
    /// </summary>
    public class RoleService : BaseService, IRoleService
    {
        /// <summary>
        /// Создает новый экземпляр <see cref="RoleManagementService"/>
        /// с заданным unit of work.
        /// </summary>
        /// <param name="unitOfWork"> uow </param>
        /// <param name="rolemanager"> manager </param>
        public RoleService(IUnitOfWork unitOfWork)
            : base(unitOfWork)
        {
        }

        /// <summary>
        /// Возвращает страны
        /// из хранилища данных.
        /// </summary>
        /// <param name="filter">Лямбда-выражение определяющее фильтрацию экземпляров.</param>
        /// <param name="orderBy">Лямбда-выражение определяющее сортировку экземпляров.</param>
        /// <param name="includeProperties">Список связанных свойств экземпляров, разделенный запятыми.</param>
        /// <returns>Коллекция экземпляров стран для пейджинга.</returns>
        public IEnumerable<ROLES> GetAllRoles(
            Expression<Func<ROLES, bool>> filter = null,
            Func<IQueryable<ROLES>, IOrderedQueryable<ROLES>> orderBy = null,
            string includeProperties = "")
        {
            return _unitOfWork.RoleRepository.Get(filter, orderBy, includeProperties);
        }

        /// <summary>
        /// Возвращает страну с заданным идентификатором из хранилища данных.
        /// </summary>
        /// <param name="pageSize">Количество сведений о странах, выводимых на страницу.</param>
        /// <param name="pageNumber">Номер выводимой страницы со сведениями о странах.</param>
        /// <param name="filter">Лямбда-выражение определяющее фильтрацию экземпляров.</param>
        /// <param name="orderBy">Лямбда-выражение определяющее сортировку экземпляров.</param>
        /// <param name="includeProperties">Список связанных свойств экземпляров, разделенный запятыми.</param>
        /// <returns>Экземпляр страны.</returns>
        public IEnumerable<ROLES> GetRolesPage(
            int pageSize,
            int pageNumber,
            Expression<Func<ROLES, bool>> filter = null,
            Func<IQueryable<ROLES>, IOrderedQueryable<ROLES>> orderBy = null,
            string includeProperties = "")
        {
            return _unitOfWork.RoleRepository.GetPage(pageSize, pageNumber, filter, orderBy, includeProperties);
        }

        /// <summary>
        /// Возвращает страну с заданным идентификатором из хранилища данных.
        /// </summary>
        /// <param name="id">Идентификатор страны.</param>
        /// <returns>Экземпляр страны.</returns>
        public ROLES GetRole(string id)
        {
            return _unitOfWork.RoleRepository.GetById(id);
        }

        /// <summary>
        /// Добавляет заданную страну в хранилище данных
        /// и возвращает идентификатор добавленной страны.
        /// </summary>
        /// <param name="role">Экземпляр страны для добавления.</param>
        /// <returns>Идентификатор страны.</returns>
        public string AddRole(ROLES role,
            string userId = "",
            string userInfo = "")
        {
            _unitOfWork.RoleRepository.Insert(role);
            _unitOfWork.SaveChanges(userId, userInfo);
            return role.ID;
        }

        /// <summary>
        /// Обновляет информацию заданной страны в хранилище данных.
        /// </summary>
        /// <param name="role">Экземпляр страны для обновления.</param>
        /// <returns> Task </returns>
        public void UpdateRole(ROLES role,
            string userId = "",
            string userInfo = "")
        {
            _unitOfWork.RoleRepository.Update(role);
            _unitOfWork.SaveChanges(userId, userInfo);
        }

        /// <summary>
        /// Удаляет страну с заданным идентификатором из хранилища данных.
        /// </summary>
        /// <param name="id">Идентификатор страны.</param>
        /// <returns> Task </returns>
        public void DeleteRole(string id,
            string userId = "",
            string userInfo = "")
        {
            _unitOfWork.RoleRepository.Delete(id);
            _unitOfWork.SaveChanges(userId, userInfo);
        }

        /// <summary>
        /// Проверяет наличие страны в хранилище данных
        /// соответствующего заданному фильтру.
        /// </summary>
        /// <param name="filter">Лямбда-выражение определяющее фильтр для поиска страны.</param>
        /// <returns>Возвращает <see langword="true"/>, если страна существует в хранилище данных.</returns>
        public bool RoleExists(Expression<Func<ROLES, bool>> filter)
        {
            return _unitOfWork.RoleRepository.Get(filter: filter)
                      .FirstOrDefault() != null;
        }
    }
}