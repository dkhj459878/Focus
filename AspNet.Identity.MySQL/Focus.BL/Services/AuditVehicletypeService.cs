﻿using Focus.BL.Common.Services;
using Focus.DAL.Common.DbModels;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Focus.BL.Services
{
    public partial class AuditVehicletypeService : IAuditVehicletypeService
    {
        #region Startup.
        private readonly IAudit_trailService _audit_TrailService;
        private readonly IRetrievingEntityOptimaService _retrievingEntityOptimaService;

        public AuditVehicletypeService(
            IRetrievingEntityOptimaService retrievingEntityOptimaService, IAudit_trailService audit_TrailService
            )
        {
            _audit_TrailService = audit_TrailService;
            _retrievingEntityOptimaService = retrievingEntityOptimaService;
        }
        #endregion

        public List<VEHICLETYPE> GetVehicletypeVersions(short id)
        {
            string entityTypeName = typeof(VEHICLETYPE).Name;

            // Retrieving from database audit records.
            List<AUDIT_TRAIL> audit_Trails = _audit_TrailService.GetAllAudit_trails(filter: audit => audit.ENTITY == entityTypeName && audit.ENTITYID == id).ToList();

            if (audit_Trails == null || audit_Trails.Count < 1)
            {
                return new List<VEHICLETYPE>();
            }

            List<VEHICLETYPE> vehicletypes = new List<VEHICLETYPE>(_retrievingEntityOptimaService.RetrieveEntityVersions<VEHICLETYPE>(audit_Trails).ToList());

            // Seed collection elements.

            return vehicletypes;
        }
    }
}
