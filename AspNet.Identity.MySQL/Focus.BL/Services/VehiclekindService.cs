﻿using Focus.BL.Common.Services;
using Focus.DAL.Common.DbModels;
using Focus.DAL.Common.UoW;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Focus.BL.Services
{
    /// <summary>
    /// Представляет сервис для работы с видами транспортных средств
    /// в хранилище данных.
    /// </summary>
    public class VehiclekindService : BaseService, IVehiclekindService
    {
        /// <summary>
        /// Создает новый экземпляр <see cref="VehiclekindManagementService"/>
        /// с заданным unit of work.
        /// </summary>
        /// <param name="unitOfWork"> uow </param>
        /// <param name="vehiclekindmanager"> manager </param>
        public VehiclekindService(IUnitOfWork unitOfWork)
            : base(unitOfWork)
        {
        }

        /// <summary>
        /// Возвращает все с виды транспортных средств
        /// из хранилища данных.
        /// </summary>
        /// <param name="filter">Лямбда-выражение определяющее фильтрацию экземпляров.</param>
        /// <param name="orderBy">Лямбда-выражение определяющее сортировку экземпляров.</param>
        /// <param name="includeProperties">Список связанных свойств экземпляров, разделенный запятыми.</param>
        /// <returns>Коллекция экземпляров видов транспортного средства для пейджинга.</returns>
        public IEnumerable<VEHICLEKIND> GetAllVehiclekinds(
            Expression<Func<VEHICLEKIND, bool>> filter = null,
            Func<IQueryable<VEHICLEKIND>, IOrderedQueryable<VEHICLEKIND>> orderBy = null,
            string includeProperties = "")
        {
            return _unitOfWork.VehiclekindRepository.Get(filter, orderBy, includeProperties);
        }

        /// <summary>
        /// Возвращает с видаы транспортных средств с заданным идентификатором из хранилища данных.
        /// </summary>
        /// <param name="pageSize">Количество сведений о видах транспортных средств, выводимых на страницу.</param>
        /// <param name="pageNumber">Номер выводимой страницы со сведениями о видах транспортных средств.</param>
        /// <param name="filter">Лямбда-выражение определяющее фильтрацию экземпляров.</param>
        /// <param name="orderBy">Лямбда-выражение определяющее сортировку экземпляров.</param>
        /// <param name="includeProperties">Список связанных свойств экземпляров, разделенный запятыми.</param>
        /// <returns>Экземпляр вида транспортного средства.</returns>
        public IEnumerable<VEHICLEKIND> GetVehiclekindsPage(
            int pageSize,
            int pageNumber,
            Expression<Func<VEHICLEKIND, bool>> filter = null,
            Func<IQueryable<VEHICLEKIND>, IOrderedQueryable<VEHICLEKIND>> orderBy = null,
            string includeProperties = "")
        {
            return _unitOfWork.VehiclekindRepository.GetPage(pageSize, pageNumber, filter, orderBy, includeProperties);
        }

        /// <summary>
        /// Возвращает вид транспортного средства с заданным идентификатором из хранилища данных.
        /// </summary>
        /// <param name="id">Идентификатор  вида транспортного средства.</param>
        /// <returns>Экземпляр вида транспортного средства.</returns>
        public VEHICLEKIND GetVehiclekind(byte id)
        {
            return _unitOfWork.VehiclekindRepository.GetById(id);
        }

        /// <summary>
        /// Добавляет заданный вида транспортного средства в хранилище данных
        /// и возвращает идентификатор добавленного вида транспортного средства.
        /// </summary>
        /// <param name="vehiclekind">Экземпляр вида транспортного средства для добавления.</param>
        /// <returns>Идентификатор вида транспортного средства.</returns>
        public byte AddVehiclekind(VEHICLEKIND vehiclekind,
            string userId = "",
            string userInfo = "")
        {
            _unitOfWork.VehiclekindRepository.Insert(vehiclekind);
            _unitOfWork.SaveChanges(userId, userInfo);
            return vehiclekind.VEHICLEKINDID;
        }

        /// <summary>
        /// Обновляет информацию заданного вида транспортного средства в хранилище данных.
        /// </summary>
        /// <param name="vehiclekind">Экземпляр вида транспортного средства для обновления.</param>
        /// <returns> Task </returns>
        public void UpdateVehiclekind(VEHICLEKIND vehiclekind,
            string userId = "",
            string userInfo = "")
        {
            _unitOfWork.VehiclekindRepository.UpdateSimpleProperties(vehiclekind, vehiclekind.VEHICLEKINDID);
            _unitOfWork.SaveChanges(userId, userInfo);
        }

        /// <summary>
        /// Удаляет вида транспортного средства с заданным идентификатором из хранилища данных.
        /// </summary>
        /// <param name="id">Идентификатор вида транспортного средства.</param>
        /// <returns> Task </returns>
        public void DeleteVehiclekind(byte id,
            string userId = "",
            string userInfo = "")
        {
            _unitOfWork.VehiclekindRepository.Delete(id);
            _unitOfWork.SaveChanges(userId, userInfo);
        }

        /// <summary>
        /// Проверяет наличие вида транспортного средства в хранилище данных
        /// соответствующего заданному фильтру.
        /// </summary>
        /// <param name="filter">Лямбда-выражение определяющее фильтр для поиска вида транспортного средства.</param>
        public bool VehiclekindExists(Expression<Func<VEHICLEKIND, bool>> filter)
        {
            return _unitOfWork.VehiclekindRepository.Get(filter: filter)
                      .FirstOrDefault() != null;
        }
    }
}