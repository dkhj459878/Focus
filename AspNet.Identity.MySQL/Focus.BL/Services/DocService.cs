﻿using Focus.BL.Common.Services;
using Focus.DAL.Common.DbModels;
using Focus.DAL.Common.UoW;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Linq.Expressions;


namespace Focus.BL.Services
{
    /// <summary>
    /// Представляет сервис для работы с пользователями
    /// в хранилище данных.
    /// </summary>
    public class DocService : BaseService, IDocService
    {
        /// <summary>
        /// Создает новый экземпляр <see cref="UserManagementService"/>
        /// с заданным unit of work.
        /// </summary>
        /// <param name="unitOfWork"> uow </param>
        /// <param name="usermanager"> manager </param>
        public DocService(IUnitOfWork unitOfWork)
            : base(unitOfWork)
        {

        }

        /// <summary>
        /// Возвращает все документы
        /// из хранилища данных.
        /// <param name="filter">Лямбда-выражение определяющее фильтрацию экземпляров.</param>
        /// <param name="orderBy">Лямбда-выражение определяющее сортировку экземпляров.</param>
        /// <param name="includeProperties">Список связанных свойств экземпляров, разделенный запятыми.</param>
        /// </summary>
        /// <returns>Коллекцию всех экземпляров пользователей.</returns>
        public IEnumerable<DOC> GetAllDocs(
            Expression<Func<DOC, bool>> filter = null,
            Func<IQueryable<DOC>, IOrderedQueryable<DOC>> orderBy = null,
            string includeProperties = "")
        {
            return _unitOfWork.DocRepository.Get(filter, orderBy, includeProperties);
        }

        /// <summary>
        /// Возвращает пользователей
        /// из хранилища данных для пейджинга.
        /// </summary>
        /// <param name="pageSize">Количество сведений о пользователях, выводимых на страницу.</param>
        /// <param name="pageNumber">Номер выводимой страницы со сведениями о пользователях.</param>
        /// <param name="filter">Лямбда-выражение определяющее фильтрацию экземпляров.</param>
        /// <param name="orderBy">Лямбда-выражение определяющее сортировку экземпляров.</param>
        /// <param name="includeProperties">Список связанных свойств экземпляров, разделенный запятыми.</param>
        /// <returns>Коллекция экземпляров пользователей для пейджинга.</returns>
        public IEnumerable<DOC> GetDocsPage(
            int pageSize,
            int pageNumber,
            Expression<Func<DOC, bool>> filter = null,
            Func<IQueryable<DOC>, IOrderedQueryable<DOC>> orderBy = null,
            string includeProperties = "")
        {

            return _unitOfWork.DocRepository.GetPage(pageSize, pageNumber, filter, orderBy, includeProperties);
        }

        /// <summary>
        /// Возвращает пользователя с заданным идентификатором из хранилища данных.
        /// </summary>
        /// <param name="id">Идентификатор пользователя.</param>
        /// <returns>Экземпляр пользователя.</returns>
        public DOC GetDoc(short id)
        {
            return _unitOfWork.DocRepository.GetById(id);
        }

        /// <summary>
        /// Добавляет заданного пользователя в хранилище данных
        /// и возвращает идентификатор добавленного пользователя.
        /// </summary>
        /// <param name="user">Экземпляр пользователя для добавления.</param>
        /// <returns>Идентификатор пользователя.</returns>
        public short AddDoc(DOC doc,
            string userId = "",
            string userInfo = "")
        {
            var cars = new List<CAR>();
            foreach (var car in doc.CAR)
            {
                cars.Add(_unitOfWork.CarRepository.Find(car.CARID));
            }

            var vehicles = new List<VEHICLE>();
            foreach (var vehicle in doc.VEHICLE)
            {
                vehicles.Add(_unitOfWork.VehicleRepository.Find(vehicle.VEHICLEID));
            }

            var devices = new List<DEVICE>();
            foreach (var device in doc.DEVICE)
            {
                devices.Add(_unitOfWork.DeviceRepository.Find(device.DEVICEID));
            }

            var containers = new List<CONTAINER>();
            foreach (var container in doc.CONTAINER)
            {
                containers.Add(_unitOfWork.ContainerRepository.Find(container.CONTAINERID));
            }
            doc.CAR = null;
            doc.VEHICLE = null;
            doc.DEVICE = null;
            doc.CONTAINER = null;

            _unitOfWork.DocRepository.Insert(doc);

            try
            {
                _unitOfWork.SaveChanges(userId, userInfo);

                doc.CAR = cars;
                doc.VEHICLE = vehicles;
                doc.DEVICE = devices;
                doc.CONTAINER = containers;

                //Your code...
                // Could also be before try if you know the exception occurs in SaveChanges

                _unitOfWork.SaveChanges(userId, userInfo);
            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);
                    }
                }
                throw;
            }
            return doc.DOCID;
        }

        /// <summary>
        /// Обновляет только одно поле документа с заданным идентификатором.
        /// </summary>
        /// <param name="id">Идентификатор экземпляра документа.</param>
        /// <param name="field">Лямбда-выражение, определяющее имя поля.</param>
        /// <param name="value">Обновляемое значение.</param>
        public void UpdateOneField(short id, Expression<Func<DOC, string>> field, string value)
        {
            DOC doc = _unitOfWork.DocRepository.GetById(id);
            if (doc != null)
            {
                _unitOfWork.DocRepository.UpdateOneField(id, field, value);
                _unitOfWork.SaveChanges();
            }

        }

        /// <summary>
        /// Обновляет информацию заданного пользователя в хранилище данных.
        /// </summary>
        /// <param name="user">Экземпляр пользователя для обновления.</param>
        /// <returns> Task </returns>
        public void UpdateDoc(DOC modelDoc,
            string userId = "",
            string userInfo = "")
        {
            if (modelDoc == null)
            {
                return;
            }

            modelDoc.ISCOULDBEPRENTED = IsValidForPrinting(modelDoc) ? Convert.ToByte(1) : Convert.ToByte(0);

            //db.Entry(_unitOfWork.DocRepository.Find(modelDoc.DOCID)).CurrentValues.SetValues(modelDoc);
            _unitOfWork.DocRepository.UpdateSimpleProperties(modelDoc, modelDoc.DOCID);

            // Insert children.
            // Insert new cars.

            foreach (var modelCar in modelDoc.CAR)
            {
                var existsCarEntity = _unitOfWork.DocRepository.Find(modelDoc.DOCID).CAR.Any(d => d.CARID == modelCar.CARID);

                if (!existsCarEntity)
                {
                    _unitOfWork.DocRepository.Find(modelDoc.DOCID).CAR.Add(_unitOfWork.CarRepository.Find(modelCar.CARID));
                }
            }

            // Insert new devices.

            foreach (var modelDevice in modelDoc.DEVICE)
            {
                var existsDeviceEntity = _unitOfWork.DocRepository.Find(modelDoc.DOCID).DEVICE.Any(d => d.DEVICEID == modelDevice.DEVICEID);

                if (!existsDeviceEntity)
                {
                    _unitOfWork.DocRepository.Find(modelDoc.DOCID).DEVICE.Add(_unitOfWork.DeviceRepository.Find(modelDevice.DEVICEID));
                }
            }

            // Insert new containers.

            foreach (var modelContainer in modelDoc.CONTAINER)
            {
                var existsContainerEntity = _unitOfWork.DocRepository.Find(modelDoc.DOCID).CONTAINER.Any(d => d.CONTAINERID == modelContainer.CONTAINERID);

                if (!existsContainerEntity)
                {
                    _unitOfWork.DocRepository.Find(modelDoc.DOCID).CONTAINER.Add(_unitOfWork.ContainerRepository.Find(modelContainer.CONTAINERID));
                }
            }

            // Insert new vehicles.

            foreach (var modelVehicle in modelDoc.VEHICLE)
            {
                var existsVehicleEntity = _unitOfWork.DocRepository.Find(modelDoc.DOCID).VEHICLE.Any(d => d.VEHICLEID == modelVehicle.VEHICLEID);

                if (!existsVehicleEntity)
                {
                    _unitOfWork.DocRepository.Find(modelDoc.DOCID).VEHICLE.Add(_unitOfWork.VehicleRepository.Find(modelVehicle.VEHICLEID));
                }
            }
            // Update parent entity.
            _unitOfWork.SaveChanges(userId, userInfo);
        }

        /// <summary>
        /// Возвращает количество документов,
        /// соответствующих заданному фильтру.
        /// </summary>
        /// <param name="filter">Лямбда-выражение определяющее фильтрацию экземпляров документов <see cref="TEntity"/>.</param>
        /// <returns>Количество документов.</returns>
        public int DocsCount(Expression<Func<DOC, bool>> filter = null)
        {
            return _unitOfWork.DocRepository.ItemsCount(filter);
        }

        /// <summary>
        /// Удаляет пользователя с заданным идентификатором из хранилища данных.
        /// </summary>
        /// <param name="id">Идентификатор пользователя.</param>
        /// <returns> Task </returns>
        public void DeleteDoc(short id,
            string userId = "",
            string userInfo = "")
        {
            _unitOfWork.DocRepository.Delete(id);
            _unitOfWork.SaveChanges(userId, userInfo);
        }

        /// <summary>
        /// Удаляет дкоумент с заданным идентификатором из хранилища данных,
        /// но перед этим отмечает идентификатор пользователя, который удаляет 
        /// документ.
        /// </summary>
        public void DeleteDocWithUserIdMarking(short id, string userId)
        {
            DOC doc = _unitOfWork.DocRepository.GetById(id);
            doc.USERID = userId;
            _unitOfWork.DocRepository.UpdateSimpleProperties(doc, id);
            DeleteDoc(id);
        }

        /// <summary>
        /// Проверяет наличие пользователя в хранилище данных
        /// соответствующего заданному фильтру.
        /// </summary>
        /// <param name="filter">Лямбда-выражение определяющее фильтр для поиска пользователя.</param>
        /// <returns>Возвращает <see langword="true"/>, если пользователь существует в хранилище данных.</returns>
        public bool DocExists(Expression<Func<DOC, bool>> filter)
        {
            return _unitOfWork.DocRepository.Get(filter: filter)
                      .FirstOrDefault() != null;
        }

        /// <summary>
        /// Checks whether doc instance is valid for
        /// printing.
        /// </summary>
        /// <param name="doc">Instance of doc type object.</param>
        /// <returns></returns>
        private bool IsValidForPrinting(DOC doc)
        {
            var isDeclarationNeeded = string.IsNullOrEmpty(doc.DOCDECLARATIONONGOODS ?? string.Empty) &&
                string.IsNullOrEmpty(doc.DOCTRANSITDECLARATION);
            var isDocumentNeeded = string.IsNullOrEmpty(doc.DOCINVOCENUM ?? string.Empty) &&
                string.IsNullOrEmpty(doc.DOCCMRNUM);

            var isInspectionValid = (!isDeclarationNeeded) &&
                (!isDocumentNeeded) &&
                (doc.PERIODSTARTDATE != null) &&
                (doc.PERIODSTARTTIME != null) &&
                (doc.PEROIDFINISHDATE != null) &&
                (doc.PERIODFINISHTIME != null) &&
                (doc.TRANSFERRINGDATE != null) &&
                (doc.TRANSFERRINGTIME != null) &&
                !string.IsNullOrEmpty(doc.CUSTOMSRESULT ?? string.Empty) &&
                !string.IsNullOrEmpty(doc.NUM ?? string.Empty) &&
                !string.IsNullOrEmpty(doc.GOODSDESCRIPTION ?? string.Empty) &&
                (doc.GOODSTOTALBRUTTO != null) &&
                !string.IsNullOrEmpty(doc.GOODSARRIVALCOUNTRY ?? string.Empty);

            var isExpertiseValid = string.IsNullOrEmpty(doc.SAMPLINGNUM ?? string.Empty) ||
                (!string.IsNullOrEmpty(doc.DECISIONNUM ?? string.Empty) &&
                !string.IsNullOrEmpty(doc.SAMPLINGNUM ?? string.Empty) &&
                !string.IsNullOrEmpty(doc.CHIEFSHORTNAME ?? string.Empty) &&
                !string.IsNullOrEmpty(doc.CHIEFTITLE ?? string.Empty) &&
                !string.IsNullOrEmpty(doc.CHIEFPOST ?? string.Empty) &&
                !string.IsNullOrEmpty(doc.CHIEFPHONENUMBER ?? string.Empty) &&
                !string.IsNullOrEmpty(doc.QUESTIONS ?? string.Empty) &&
                !string.IsNullOrEmpty(doc.GOODSDES ?? string.Empty) &&
                (doc.SAMPLEWEIGHT != null) &&
                (doc.SAMPLECOST != null) &&
                (doc.TOTALWEIGHT != null) &&
                (doc.TOTALCOST != null) &&
                !string.IsNullOrEmpty(doc.CURRENCYNAME ?? string.Empty) &&
                !string.IsNullOrEmpty(doc.SAMPLESNUM ?? string.Empty) &&
                !string.IsNullOrEmpty(doc.SAMPLINGPACKAGE ?? string.Empty) &&
                !string.IsNullOrEmpty(doc.SEALNUM ?? string.Empty));

            return isInspectionValid && isExpertiseValid;
        }
    }
}