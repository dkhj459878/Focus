﻿using Focus.BL.Common.Services;
using Focus.DAL.Common.DbModels;
using Focus.DAL.Common.UoW;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Focus.BL.Services
{
    /// <summary>
    /// Представляет сервис для работы с ПТО
    /// в хранилище данных.
    /// </summary>
    public class PostService : BaseService, IPostService
    {
        /// <summary>
        /// Создает новый экземпляр <see cref="PostManagementService"/>
        /// с заданным unit of work.
        /// </summary>
        /// <param name="unitOfWork"> uow </param>
        /// <param name="postmanager"> manager </param>
        public PostService(IUnitOfWork unitOfWork)
            : base(unitOfWork)
        {
        }

        /// <summary>
        /// Возвращает все ПТО
        /// из хранилища данных.
        /// </summary>
        /// <param name="filter">Лямбда-выражение определяющее фильтрацию экземпляров.</param>
        /// <param name="orderBy">Лямбда-выражение определяющее сортировку экземпляров.</param>
        /// <param name="includeProperties">Список связанных свойств экземпляров, разделенный запятыми.</param>
        /// <returns>Коллекция экземпляров ПТО для пейджинга.</returns>
        public IEnumerable<POST> GetAllPosts(
            Expression<Func<POST, bool>> filter = null,
            Func<IQueryable<POST>, IOrderedQueryable<POST>> orderBy = null,
            string includeProperties = "")
        {
            return _unitOfWork.PostRepository.Get(filter, orderBy, includeProperties);
        }

        /// <summary>
        /// Возвращает ПТО с заданным идентификатором из хранилища данных.
        /// </summary>
        /// <param name="pageSize">Количество сведений о ПТО, выводимых на страницу.</param>
        /// <param name="pageNumber">Номер выводимой страницы со сведениями о ПТО.</param>
        /// <param name="filter">Лямбда-выражение определяющее фильтрацию экземпляров.</param>
        /// <param name="orderBy">Лямбда-выражение определяющее сортировку экземпляров.</param>
        /// <param name="includeProperties">Список связанных свойств экземпляров, разделенный запятыми.</param>
        /// <returns>Экземпляр ПТО.</returns>
        public IEnumerable<POST> GetPostsPage(
            int pageSize,
            int pageNumber,
            Expression<Func<POST, bool>> filter = null,
            Func<IQueryable<POST>, IOrderedQueryable<POST>> orderBy = null,
            string includeProperties = "")
        {
            return _unitOfWork.PostRepository.GetPage(pageSize, pageNumber, filter, orderBy, includeProperties);
        }

        /// <summary>
        /// Возвращает ПТО с заданным идентификатором из хранилища данных.
        /// </summary>
        /// <param name="id">Идентификатор ПТО.</param>
        /// <returns>Экземпляр ПТО.</returns>
        public POST GetPost(byte id)
        {
            return _unitOfWork.PostRepository.GetById(id);
        }

        /// <summary>
        /// Добавляет заданный ПТО в хранилище данных
        /// и возвращает идентификатор добавленного ПТО.
        /// </summary>
        /// <param name="post">Экземпляр ПТО для добавления.</param>
        /// <returns>Идентификатор ПТО.</returns>
        public byte AddPost(POST post,
            string userId = "",
            string userInfo = "")
        {
            _unitOfWork.PostRepository.Insert(post);
            _unitOfWork.SaveChanges(userId, userInfo);
            return post.ID;
        }

        /// <summary>
        /// Обновляет информацию заданного ПТО в хранилище данных.
        /// </summary>
        /// <param name="post">Экземпляр ПТО для обновления.</param>
        /// <returns> Task </returns>
        public void UpdatePost(POST post,
            string userId = "",
            string userInfo = "")
        {
            _unitOfWork.PostRepository.UpdateSimpleProperties(post, post.ID);
            _unitOfWork.SaveChanges(userId, userInfo);
        }

        /// <summary>
        /// Удаляет ПТО с заданным идентификатором из хранилища данных.
        /// </summary>
        /// <param name="id">Идентификатор ПТО.</param>
        /// <returns> Task </returns>
        public void DeletePost(byte id,
            string userId = "",
            string userInfo = "")
        {
            _unitOfWork.PostRepository.Delete(id);
            _unitOfWork.SaveChanges(userId, userInfo);
        }

        /// <summary>
        /// Проверяет наличие ПТО в хранилище данных
        /// соответствующего заданному фильтру.
        /// </summary>
        /// <param name="filter">Лямбда-выражение определяющее фильтр для поиска ПТО.</param>
        /// <returns>Возвращает <see langword="true"/>, если ПТО существует в хранилище данных.</returns>
        public bool PostExists(Expression<Func<POST, bool>> filter)
        {
            return _unitOfWork.PostRepository.Get(filter: filter)
                      .FirstOrDefault() != null;
        }
    }
}