﻿using Focus.BL.Common.Services;
using Focus.DAL.Common.DbModels;
using Focus.DAL.Common.UoW;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Focus.BL.Services
{
    /// <summary>
    /// Представляет сервис для работы со странами
    /// в хранилище данных.
    /// </summary>
    public class UserService : BaseService, IUserService
    {
        /// <summary>
        /// Создает новый экземпляр <see cref="UserManagementService"/>
        /// с заданным unit of work.
        /// </summary>
        /// <param name="unitOfWork"> uow </param>
        /// <param name="usermanager"> manager </param>
        public UserService(IUnitOfWork unitOfWork)
            : base(unitOfWork)
        {
        }

        /// <summary>
        /// Возвращает страны
        /// из хранилища данных.
        /// </summary>
        /// <param name="filter">Лямбда-выражение определяющее фильтрацию экземпляров.</param>
        /// <param name="orderBy">Лямбда-выражение определяющее сортировку экземпляров.</param>
        /// <param name="includeProperties">Список связанных свойств экземпляров, разделенный запятыми.</param>
        /// <returns>Коллекция экземпляров стран для пейджинга.</returns>
        public IEnumerable<USERS> GetAllUsers(
            Expression<Func<USERS, bool>> filter = null,
            Func<IQueryable<USERS>, IOrderedQueryable<USERS>> orderBy = null,
            string includeProperties = "")
        {
            return _unitOfWork.UserRepository.Get(filter, orderBy, includeProperties);
        }

        /// <summary>
        /// Возвращает страну с заданным идентификатором из хранилища данных.
        /// </summary>
        /// <param name="pageSize">Количество сведений о странах, выводимых на страницу.</param>
        /// <param name="pageNumber">Номер выводимой страницы со сведениями о странах.</param>
        /// <param name="filter">Лямбда-выражение определяющее фильтрацию экземпляров.</param>
        /// <param name="orderBy">Лямбда-выражение определяющее сортировку экземпляров.</param>
        /// <param name="includeProperties">Список связанных свойств экземпляров, разделенный запятыми.</param>
        /// <returns>Экземпляр страны.</returns>
        public IEnumerable<USERS> GetUsersPage(
            int pageSize,
            int pageNumber,
            Expression<Func<USERS, bool>> filter = null,
            Func<IQueryable<USERS>, IOrderedQueryable<USERS>> orderBy = null,
            string includeProperties = "")
        {
            return _unitOfWork.UserRepository.GetPage(pageSize, pageNumber, filter, orderBy, includeProperties);
        }

        /// <summary>
        /// Возвращает страну с заданным идентификатором из хранилища данных.
        /// </summary>
        /// <param name="id">Идентификатор страны.</param>
        /// <returns>Экземпляр страны.</returns>
        public USERS GetUser(string id)
        {
            return _unitOfWork.UserRepository.GetById(id);
        }

        /// <summary>
        /// Добавляет заданную страну в хранилище данных
        /// и возвращает идентификатор добавленной страны.
        /// </summary>
        /// <param name="user">Экземпляр страны для добавления.</param>
        /// <returns>Идентификатор страны.</returns>
        public string AddUser(USERS user,
            string userId = "",
            string userInfo = "")
        {
            _unitOfWork.UserRepository.Insert(user);
            _unitOfWork.SaveChanges(userId, userInfo);
            return user.ID;
        }

        /// <summary>
        /// Обновляет информацию заданной страны в хранилище данных.
        /// </summary>
        /// <param name="user">Экземпляр страны для обновления.</param>
        /// <returns> Task </returns>
        public void UpdateUser(USERS user,
            string userId = "",
            string userInfo = "")
        {
            _unitOfWork.UserRepository.Update(user);
            _unitOfWork.SaveChanges(userId, userInfo);
        }

        /// <summary>
        /// Удаляет страну с заданным идентификатором из хранилища данных.
        /// </summary>
        /// <param name="id">Идентификатор страны.</param>
        /// <returns> Task </returns>
        public void DeleteUser(string id,
            string userId = "",
            string userInfo = "")
        {
            _unitOfWork.UserRepository.Delete(id);
            _unitOfWork.SaveChanges(userId, userInfo);
        }

        /// <summary>
        /// Проверяет наличие страны в хранилище данных
        /// соответствующего заданному фильтру.
        /// </summary>
        /// <param name="filter">Лямбда-выражение определяющее фильтр для поиска страны.</param>
        /// <returns>Возвращает <see langword="true"/>, если страна существует в хранилище данных.</returns>
        public bool UserExists(Expression<Func<USERS, bool>> filter)
        {
            return _unitOfWork.UserRepository.Get(filter: filter)
                      .FirstOrDefault() != null;
        }
    }
}