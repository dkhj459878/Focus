﻿using Focus.BL.Common.Services;
using System;
using System.Collections.Generic;
using System.DirectoryServices;
using System.DirectoryServices.AccountManagement;
using System.Linq;
using DirectoryEntry = System.DirectoryServices.DirectoryEntry;

namespace Focus.BL.Services
{
    public class AdService : IAdService
    {
        public string UserName { get; set; } = string.Empty;
        public string UserDomain { get; set; } = string.Empty;
        public bool IsInitialized { get; set; } = false;

        public AdService(string userName, string userDomain = "")
        {
            if (userName.Contains("\\") || userName.Contains("/"))
            {
                UserName = userName.Split(new char[] { '\\', '/' })[0];
                UserDomain = userName.Split(new char[] { '\\', '/' })[1];
                IsInitialized = true;
            }
            else
            {
                if (string.IsNullOrEmpty(userName) || string.IsNullOrEmpty(userDomain))
                {
                    return;
                }

                UserName = userName;
                UserDomain = userDomain;
                IsInitialized = true;
            }
        }

        public AdService()
        {

        }

        public void CreateGroup(string groupName)
        {
            throw new NotImplementedException();
        }

        public void DeleteGroup(string groupName)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<string> GetGroupsName()
        {
            if (!UserNameAndDomainNameAreValid())
            {
                return null;
            }
            else
            {
                // Branch is not foreseeing.
            }

            List<string> result = new List<string>();
            const string GROUP_INDETIFIER = "cn";

            using (PrincipalContext domainContext = new PrincipalContext(ContextType.Domain, UserDomain))
            {
                using (UserPrincipal user = UserPrincipal.FindByIdentity(domainContext, UserName))
                {
                    DirectoryEntry directoryEntry = user.GetUnderlyingObject() as DirectoryEntry;
                    if (directoryEntry != null)
                    {
                        result.AddRange((directoryEntry.Properties[GROUP_INDETIFIER].Value as List<object>)
                            .Select(o => o.ToString()));
                    }
                }
            }
            return result;
        }

        public string GetUserDepartment()
        {
            throw new NotImplementedException();
        }

        public bool IsUserInGroup(string groupName)
        {
            throw new NotImplementedException();
        }

        public string GetUserProperty(string propertyIdentifier)
        {   

            if (string.IsNullOrEmpty(propertyIdentifier))
            {
                return string.Empty;
            }

            string result = string.Empty;
            if (!UserNameAndDomainNameAreValid())
            {
                return string.Empty;
            }
            else
            {
                List<string> groups = new List<string>();
                const string GROUP_INDETIFIER = "cn";
                using (PrincipalContext domainContext = new PrincipalContext(ContextType.Domain, UserDomain))
                {
                    using (UserPrincipal user = UserPrincipal.FindByIdentity(domainContext, UserName))
                    {
                        DirectoryEntry directoryEntry = user.GetUnderlyingObject() as DirectoryEntry;
                        if (directoryEntry != null)
                        {
                            if (directoryEntry.Properties.Contains(GROUP_INDETIFIER))
                            {
                                return directoryEntry.Properties[propertyIdentifier][0].ToString();
                            }
                        }
                        else
                        {
                            return result;
                        }
                    }
                }
                return result;
            }
        }

        private bool UserNameAndDomainNameAreValid()
        {
            if (!string.IsNullOrEmpty(UserName) && !string.IsNullOrEmpty(UserDomain))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
