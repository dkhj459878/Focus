﻿using Focus.BL.Common.Services;
using Focus.DAL.Common.DbModels;
using Focus.DAL.Common.UoW;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Focus.BL.Services
{
    /// <summary>
    /// Представляет сервис для работы с СВХ
    /// в хранилище данных.
    /// </summary>
    public class StorageService : BaseService, IStorageService
    {
        /// <summary>
        /// Создает новый экземпляр <see cref="StorageManagementService"/>
        /// с заданным unit of work.
        /// </summary>
        /// <param name="unitOfWork"> uow </param>
        /// <param name="storagemanager"> manager </param>
        public StorageService(IUnitOfWork unitOfWork)
            : base(unitOfWork)
        {
        }

        /// <summary>
        /// Возвращает все СВХ
        /// из хранилища данных.
        /// </summary>
        /// <param name="filter">Лямбда-выражение определяющее фильтрацию экземпляров.</param>
        /// <param name="orderBy">Лямбда-выражение определяющее сортировку экземпляров.</param>
        /// <param name="includeProperties">Список связанных свойств экземпляров, разделенный запятыми.</param>
        /// <returns>Коллекция экземпляров СВХ для пейджинга.</returns>
        public IEnumerable<STORAGE> GetAllStorages(
            Expression<Func<STORAGE, bool>> filter = null,
            Func<IQueryable<STORAGE>, IOrderedQueryable<STORAGE>> orderBy = null,
            string includeProperties = "")
        {
            return _unitOfWork.StorageRepository.Get(filter, orderBy, includeProperties);
        }

        /// <summary>
        /// Возвращает СВХ с заданным идентификатором из хранилища данных.
        /// </summary>
        /// <param name="pageSize">Количество сведений о СВХ, выводимых на страницу.</param>
        /// <param name="pageNumber">Номер выводимой страницы со сведениями о СВХ.</param>
        /// <param name="filter">Лямбда-выражение определяющее фильтрацию экземпляров.</param>
        /// <param name="orderBy">Лямбда-выражение определяющее сортировку экземпляров.</param>
        /// <param name="includeProperties">Список связанных свойств экземпляров, разделенный запятыми.</param>
        /// <returns>Экземпляр СВХ.</returns>
        public IEnumerable<STORAGE> GetStoragesPage(
            int pageSize,
            int pageNumber,
            Expression<Func<STORAGE, bool>> filter = null,
            Func<IQueryable<STORAGE>, IOrderedQueryable<STORAGE>> orderBy = null,
            string includeProperties = "")
        {
            return _unitOfWork.StorageRepository.GetPage(pageSize, pageNumber, filter, orderBy, includeProperties);
        }

        /// <summary>
        /// Возвращает СВХ с заданным идентификатором из хранилища данных.
        /// </summary>
        /// <param name="id">Идентификатор СВХ.</param>
        /// <returns>Экземпляр СВХ.</returns>
        public STORAGE GetStorage(byte id)
        {
            return _unitOfWork.StorageRepository.GetById(id);
        }

        /// <summary>
        /// Добавляет заданный СВХ в хранилище данных
        /// и возвращает идентификатор добавленного СВХ.
        /// </summary>
        /// <param name="storage">Экземпляр СВХ для добавления.</param>
        /// <returns>Идентификатор СВХ.</returns>
        public byte AddStorage(STORAGE storage,
            string userId = "",
            string userInfo = "")
        {
            _unitOfWork.StorageRepository.Insert(storage);
            _unitOfWork.SaveChanges(userId, userInfo);
            return storage.ID;
        }

        /// <summary>
        /// Обновляет информацию заданного СВХ в хранилище данных.
        /// </summary>
        /// <param name="storage">Экземпляр СВХ для обновления.</param>
        /// <returns> Task </returns>
        public void UpdateStorage(STORAGE storage,
            string userId = "",
            string userInfo = "")
        {
            _unitOfWork.StorageRepository.Update(storage);
            _unitOfWork.SaveChanges(userId, userInfo);
        }

        /// <summary>
        /// Удаляет СВХ с заданным идентификатором из хранилища данных.
        /// </summary>
        /// <param name="id">Идентификатор СВХ.</param>
        /// <returns> Task </returns>
        public void DeleteStorage(byte id,
            string userId = "",
            string userInfo = "")
        {
            _unitOfWork.StorageRepository.Delete(id);
            _unitOfWork.SaveChanges(userId, userInfo);
        }

        /// <summary>
        /// Проверяет наличие СВХ в хранилище данных
        /// соответствующего заданному фильтру.
        /// </summary>
        /// <param name="filter">Лямбда-выражение определяющее фильтр для поиска СВХ.</param>
        /// <returns>Возвращает <see langword="true"/>, если СВХ существует в хранилище данных.</returns>
        public bool StorageExists(Expression<Func<STORAGE, bool>> filter)
        {
            return _unitOfWork.StorageRepository.Get(filter: filter)
                      .FirstOrDefault() != null;
        }
    }
}