﻿using Focus.BL.Common.Services;
using Focus.BL.Common.Validators;
using Focus.BL.Mappers;
using Focus.DAL.Common.DbModels;
using System;
using log4net;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Word = Microsoft.Office.Interop.Word;

namespace Focus.BL.Services
{
    class FileDocumentService : IFileDocumentService
    {
        Object missingObj = System.Reflection.Missing.Value;
        Object replaceTypeObj = Word.WdReplace.wdReplaceAll;
        Object documenttype = Word.WdNewDocumentType.wdNewBlankDocument;
        Object trueObj = true;
        Object falseObj = false;
        Object visible = true;
        Object fileformat = Word.WdSaveFormat.wdFormatDocument97;

        IStringMaster _stringMaster;

        protected static readonly ILog _logger =
            LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);


        public FileDocumentService(IStringMaster stringMaster)
        {
            _stringMaster = stringMaster;
        }

        public string GetDocumentFilePath(DOC document, string templatePath)
        {
            Word.Application wordapp = null;
            Word.Document worddoc = null;
            try
            {
                wordapp = new Word.Application();
                if (wordapp == null)
                {
                    _logger.Debug("Приложение Word не инициализировано.");
                }
                else
                {
                    _logger.Debug("Приложение Word инициализировано.");
                }
                worddoc = wordapp.Documents.Add(templatePath + "АктЭкспертиза.dot", missingObj, documenttype, visible);
                if (wordapp == null)
                {
                    _logger.Debug("Документ Word не открыт.");
                }
                else
                {
                    _logger.Debug("Документ Word открыт.");
                }
                Change(wordapp, new MapperTemplate(document).Exchanger);
                if (wordapp == null)
                {
                    _logger.Debug("Документ Word не изменен.");
                }
                else
                {
                    _logger.Debug("Документ Word изменен.");
                }
                string newPath = GenerateFileName(document, templatePath);
                if (string.IsNullOrEmpty(newPath))
                {
                    _logger.Debug("Путь '" + newPath + "' не инициализирован.");
                }
                else
                {
                    _logger.Debug("Путь '" + newPath + "' не инициализирован.");
                }
                worddoc.SaveAs(newPath, fileformat,
                    missingObj, missingObj, missingObj, missingObj, missingObj, missingObj, missingObj, missingObj, missingObj);
                if (wordapp == null)
                {
                    _logger.Debug("Документ Word не сохранен.");
                }
                else
                {
                    _logger.Debug("Документ Word сохранен.");
                }
                return newPath;
            }
            finally
            {
                worddoc?.Close(ref falseObj, ref missingObj, ref falseObj);
                System.Runtime.InteropServices.Marshal.ReleaseComObject(worddoc);
                wordapp?.Quit(ref falseObj, ref missingObj, ref missingObj);
                CleanDocs(GetDocsDirectoryPath(templatePath));
            }
        }

        private string GetDocsDirectoryPath(string templatePath)
        {
            string result = string.Empty;
            try
            {
                result = templatePath.Replace(_stringMaster.GetLastFolderName(templatePath), "DocTemp");
            }
            catch (Exception error)
            {
                string message = $"{error.Message}";
                _logger.Error(message);
            }


            return result;
        }

        private void CleanDocs(string path)
        {
            var dirInfo = new DirectoryInfo(path);
            var dateTimeNow = DateTime.Now;
            foreach (var file in dirInfo.GetFiles().Where(f => f.CreationTime.AddMinutes(1) < dateTimeNow))
            {
                try
                {
                    file.Delete();
                }
                catch
                {
                }
            }
        }

        private string GenerateFileName(DOC document, string templatePath)
        {
            return $@"{GetDocsDirectoryPath(templatePath)}{DateTime.Now.Ticks}{document.DOCID}.doс";
        }

        private void Change(Word.Application wrdDoc, Dictionary<string, string> revolversLocal)
        {
            const int maxLenght = 150;

            Object strToFindObj;
            Object replaceStrObj;

            foreach (var item in revolversLocal)
            {
                strToFindObj = item.Key; replaceStrObj = item.Value;

                var replace = replaceStrObj.ToString();
                var find = strToFindObj.ToString();
                var replaceLenght = replace.Length;

                if (replaceLenght <= maxLenght)
                {
                    ChangeItem(wrdDoc, strToFindObj, replaceStrObj);
                }
                else
                {

                    List<Portion> portions = Portions(maxLenght, replaceLenght);

                    var portionsCount = portions.Count;

                    for (int i = 0; i < portionsCount - 1; i++)
                    {
                        var replaceItem = (object)(replace.Substring(portions[i].StartPosition, portions[i].Lenght) + find);

                        ChangeItem(wrdDoc, strToFindObj, replaceItem);
                    }

                    var replaceItemComplete = (object)replace.Substring(portions[portionsCount - 1].StartPosition, portions[portionsCount - 1].Lenght);

                    ChangeItem(wrdDoc, strToFindObj, replaceItemComplete);
                }
            }
        }

        private void ChangeItem(Word.Application wrdDoc, object strToFindObj, object replaceStrObj)
        {
            Object missingObj = System.Reflection.Missing.Value;
            Object replaceTypeObj = Word.WdReplace.wdReplaceAll;

            wrdDoc.Selection.Find.ClearFormatting();
            wrdDoc.Selection.Find.Replacement.ClearFormatting();
            wrdDoc.Selection.Find.Execute(ref strToFindObj, ref missingObj, ref missingObj,
                                            ref missingObj, ref missingObj, ref missingObj,
                                            ref missingObj, ref missingObj, ref missingObj,
                                            ref replaceStrObj, ref replaceTypeObj, ref missingObj,
                                            ref missingObj, ref missingObj, ref missingObj);
        }

        private List<Portion> Portions(int maxLenght, int stringLenght)
        {
            var portions = new List<Portion>();

            if (stringLenght <= maxLenght)
            {
                portions.Add(new Portion() { StartPosition = 0, Lenght = stringLenght });
            }
            else
            {
                var portionsCount = stringLenght / maxLenght;

                if (portionsCount * maxLenght == stringLenght)
                {
                    for (int i = 0; i < portionsCount; i++)
                    {
                        portions.Add(new Portion() { StartPosition = i * maxLenght, Lenght = maxLenght });
                    }
                }
                else
                {
                    for (int i = 0; i < portionsCount; i++)
                    {
                        portions.Add(new Portion() { StartPosition = i * maxLenght, Lenght = maxLenght });
                    }
                    portions.Add(new Portion() { StartPosition = portionsCount * maxLenght, Lenght = stringLenght - portionsCount * maxLenght });
                }
            }
            return portions;
        }
    }
}
