﻿using Focus.BL.Common.Services;
using Focus.BL.Exceptions;
using Focus.BL.Extensions;
using Focus.DAL.Common.DbModels;
using Focus.DAL.Common.UoW;
using Focus.DAL.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace Focus.BL.Services
{
    /// <summary>
    /// Представляет сервис для работы с автомобилями
    /// в хранилище данных.
    /// </summary>
    public class Audit_trailService : BaseService, IAudit_trailService
    {
        private readonly IMapperService _mapperService;

        #region Something that done
        /// <summary>
        /// Создает новый экземпляр <see cref="Audit_trailManagementService"/>
        /// с заданным unit of work.
        /// </summary>
        /// <param name="unitOfWork"> uow </param>
        /// <param name="audit_trailmanager"> manager </param>
        public Audit_trailService(IUnitOfWork unitOfWork, IMapperService mapperService)
            : base(unitOfWork)
        {
            _mapperService = mapperService;
        }

        /// <summary>
        /// Возвращает все автомобили
        /// из хранилища данных.
        /// </summary>
        /// <param name="filter">Лямбда-выражение определяющее фильтрацию экземпляров.</param>
        /// <param name="orderBy">Лямбда-выражение определяющее сортировку экземпляров.</param>
        /// <param name="includeProperties">Список связанных свойств экземпляров, разделенный запятыми.</param>
        /// <returns>Коллекция экземпляров автомобилей для пейджинга.</returns>
        public IEnumerable<AUDIT_TRAIL> GetAllAudit_trails(
            Expression<Func<AUDIT_TRAIL, bool>> filter = null,
            Func<IQueryable<AUDIT_TRAIL>, IOrderedQueryable<AUDIT_TRAIL>> orderBy = null,
            string includeProperties = "")
        {
            return _unitOfWork.Audit_trailRepository.Get(filter, orderBy, includeProperties);
        }

        /// <summary>
        /// Возвращает автомобиль с заданным идентификатором из хранилища данных.
        /// </summary>
        /// <param name="pageSize">Количество сведений об автомобилях, выводимых на страницу.</param>
        /// <param name="pageNumber">Номер выводимой страницы со сведениями об автомобилях.</param>
        /// <param name="filter">Лямбда-выражение определяющее фильтрацию экземпляров.</param>
        /// <param name="orderBy">Лямбда-выражение определяющее сортировку экземпляров.</param>
        /// <param name="includeProperties">Список связанных свойств экземпляров, разделенный запятыми.</param>
        /// <returns>Экземпляр автомобиля.</returns>
        public IEnumerable<AUDIT_TRAIL> GetAudit_trailsPage(
            int pageSize,
            int pageNumber,
            Expression<Func<AUDIT_TRAIL, bool>> filter = null,
            Func<IQueryable<AUDIT_TRAIL>, IOrderedQueryable<AUDIT_TRAIL>> orderBy = null,
            string includeProperties = "")
        {
            return _unitOfWork.Audit_trailRepository.GetPage(pageSize, pageNumber, filter, orderBy, includeProperties);
        }

        /// <summary>
        /// Возвращает автомобиль с заданным идентификатором из хранилища данных.
        /// </summary>
        /// <param name="id">Идентификатор автомобиля.</param>
        /// <returns>Экземпляр автомобиля.</returns>
        public AUDIT_TRAIL GetAudit_trail(long id)
        {
            return _unitOfWork.Audit_trailRepository.GetById(id);
        }

        /// <summary>
        /// Добавляет заданный автомобиль в хранилище данных
        /// и возвращает идентификатор добавленного автомобиля.
        /// </summary>
        /// <param name="audit_trail">Экземпляр автомобиля для добавления.</param>
        /// <returns>Идентификатор автомобиля.</returns>
        public long AddAudit_trail(AUDIT_TRAIL audit_trail)
        {
            _unitOfWork.Audit_trailRepository.Insert(audit_trail);
            _unitOfWork.SaveChanges();
            return audit_trail.ID;
        }

        /// Добавляет заданный автомобиль в хранилище данных
        /// и возвращает идентификатор добавленного автомобиля.
        /// </summary>
        /// <param name="audit_trail">Экземпляр автомобиля для добавления.</param>
        /// <returns>Идентификатор автомобиля.</returns>
        public int AddAudit_trails(IEnumerable<AUDIT_TRAIL> audit_trails)
        {
            _unitOfWork.Audit_trailRepository.InsertRange(audit_trails);
            return _unitOfWork.SaveChanges();
        }

        /// <summary>
        /// Обновляет информацию заданного автомобиля в хранилище данных.
        /// </summary>
        /// <param name="audit_trail">Экземпляр автомобиля для обновления.</param>
        /// <returns> Task </returns>
        public void UpdateAudit_trail(AUDIT_TRAIL audit_trail)
        {
            _unitOfWork.Audit_trailRepository.Update(audit_trail);
            _unitOfWork.SaveChanges();
        }

        /// <summary>
        /// Удаляет автомобиль с заданным идентификатором из хранилища данных.
        /// </summary>
        /// <param name="id">Идентификатор автомобиля.</param>
        /// <returns> Task </returns>
        public void DeleteAudit_trail(long id)
        {
            _unitOfWork.Audit_trailRepository.Delete(id);
            _unitOfWork.SaveChanges();
        }

        /// <summary>
        /// Проверяет наличие автомобиля в хранилище данных
        /// соответствующего заданному фильтру.
        /// </summary>
        /// <param name="filter">Лямбда-выражение определяющее фильтр для поиска автомобиля.</param>
        /// <returns>Возвращает <see langword="true"/>, если автомобиль существует в хранилище данных.</returns>
        public bool Audit_trailExists(Expression<Func<AUDIT_TRAIL, bool>> filter)
        {
            return _unitOfWork.Audit_trailRepository.Get(filter: filter)
                      .FirstOrDefault() != null;
        }
        #endregion

        private TEntity MicroClone<TEntity>(TEntity entity)
        {
            TEntity entityCloned = Activator.CreateInstance<TEntity>();
            List<PropertyInfo> properties = entity.GetType().GetMyProperties(p => !p.PropertyType.IsClass || p.PropertyType == typeof(string)).ToList();
            foreach (PropertyInfo property in properties)
            {
                entityCloned.SetValue(property.Name, entity.GetValue(property.Name));
            }

            return entityCloned;
        }

        private TEntity ConvertToEntity<TEntity>(IEnumerable<AUDIT_TRAIL> audit_Trails)
        {
            TEntity entity = Activator.CreateInstance<TEntity>();
            List<string> entitiesPropertyNames = entity.GetType().GetProperties().Select(p => p.GetType()).Except(_mapperService.GetCollection[entity.GetType()]).Select(p => p.Name).ToList();

            foreach (AUDIT_TRAIL audit_Trail in audit_Trails)
            {
                string propertyNameAudit = audit_Trail.FIELD;

                bool auditPropertyExist = entitiesPropertyNames.Contains(propertyNameAudit);

                if (auditPropertyExist)
                {
                    entity.SetValue(propertyNameAudit, audit_Trail.VALUENEW);
                }
            }

            // Seed collection properties.
            foreach (Type propertyCollectionType in (_mapperService.GetCollection[entity.GetType()]))
            {
                if (audit_Trails.Select(p => p.FIELD).Contains(propertyCollectionType.Name))
                {
                    string propertyValue = audit_Trails.
                        Where(p => p.FIELD == propertyCollectionType.Name).FirstOrDefault().VALUENEW;
                    if (string.IsNullOrEmpty(propertyValue))
                    {
                        continue;
                    }
                    else
                    {
                        short[] identityCollectionOfValues = propertyValue.
                        Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries).
                        Select(p => short.Parse(p)).
                        OrderBy(p => p).
                        ToArray();
                        ICollection<TEntity> collection = entity.GetCollection<TEntity>();
                        foreach (var identityValue in identityCollectionOfValues)
                        {
                            TEntity entityFromCollection = Activator.CreateInstance<TEntity>();
                            string identityName = $"{typeof(TEntity).Name}ID";
                            entityFromCollection.SetValue(identityName, identityValue);
                            collection.Add(entityFromCollection);
                        }
                        entity.SetValue(propertyCollectionType.Name, collection);
                    }
                }
                else
                {
                    continue;
                }
            }

            string entityIdName = entity.GetEntityIdFieldName();

            object entityIdValue = entity.GetValue(entityIdName);

            bool entityIdentityNotInitialized = entityIdValue == null || !(entityIdValue is short?) || (entity.GetValue(entityIdName) as short?).Value == 0;

            if (entityIdentityNotInitialized)
            {
                throw new EntityIdentityIsNotSetException();
            }

            return entity;
        }
    }
}