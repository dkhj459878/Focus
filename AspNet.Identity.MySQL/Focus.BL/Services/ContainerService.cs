﻿using Focus.BL.Common.Services;
using Focus.DAL.Common.DbModels;
using Focus.DAL.Common.UoW;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Focus.BL.Services
{
    /// <summary>
    /// Представляет сервис для работы с контейнерами
    /// в хранилище данных.
    /// </summary>
    public class ContainerService : BaseService, IContainerService
    {
        /// <summary>
        /// Создает новый экземпляр <see cref="ContainerManagementService"/>
        /// с заданным unit of work.
        /// </summary>
        /// <param name="unitOfWork"> uow </param>
        /// <param name="containermanager"> manager </param>
        public ContainerService(IUnitOfWork unitOfWork)
            : base(unitOfWork)
        {
        }

        /// <summary>
        /// Возвращает все контейнеры
        /// из хранилища данных.
        /// </summary>
        /// <param name="filter">Лямбда-выражение определяющее фильтрацию экземпляров.</param>
        /// <param name="orderBy">Лямбда-выражение определяющее сортировку экземпляров.</param>
        /// <param name="includeProperties">Список связанных свойств экземпляров, разделенный запятыми.</param>
        /// <returns>Коллекция экземпляров контейнеров для пейджинга.</returns>
        public IEnumerable<CONTAINER> GetAllContainers(
            Expression<Func<CONTAINER, bool>> filter = null,
            Func<IQueryable<CONTAINER>, IOrderedQueryable<CONTAINER>> orderBy = null,
            string includeProperties = "")
        {
            return _unitOfWork.ContainerRepository.Get(filter, orderBy, includeProperties);
        }

        /// <summary>
        /// Возвращает контейнер с заданным идентификатором из хранилища данных.
        /// </summary>
        /// <param name="pageSize">Количество сведений о контейнерах, выводимых на страницу.</param>
        /// <param name="pageNumber">Номер выводимой страницы со сведениями о контейнерах.</param>
        /// <param name="filter">Лямбда-выражение определяющее фильтрацию экземпляров.</param>
        /// <param name="orderBy">Лямбда-выражение определяющее сортировку экземпляров.</param>
        /// <param name="includeProperties">Список связанных свойств экземпляров, разделенный запятыми.</param>
        /// <returns>Экземпляр контейнера.</returns>
        public IEnumerable<CONTAINER> GetContainersPage(
            int pageSize,
            int pageNumber,
            Expression<Func<CONTAINER, bool>> filter = null,
            Func<IQueryable<CONTAINER>, IOrderedQueryable<CONTAINER>> orderBy = null,
            string includeProperties = "")
        {
            return _unitOfWork.ContainerRepository.GetPage(pageSize, pageNumber, filter, orderBy, includeProperties);
        }

        /// <summary>
        /// Возвращает контейнер с заданным идентификатором из хранилища данных.
        /// </summary>
        /// <param name="id">Идентификатор контейнера.</param>
        /// <returns>Экземпляр контейнера.</returns>
        public CONTAINER GetContainer(short id)
        {
            return _unitOfWork.ContainerRepository.GetById(id);
        }

        /// <summary>
        /// Добавляет заданный контейнер в хранилище данных
        /// и возвращает идентификатор добавленного контейнера.
        /// </summary>
        /// <param name="container">Экземпляр контейнера для добавления.</param>
        /// <returns>Идентификатор контейнера.</returns>
        public short AddContainer(CONTAINER container,
            string userId = "",
            string userInfo = "")
        {
            _unitOfWork.ContainerRepository.Insert(container);
            _unitOfWork.SaveChanges(userId, userInfo);
            return container.CONTAINERID;
        }

        /// <summary>
        /// Обновляет информацию заданного контейнера в хранилище данных.
        /// </summary>
        /// <param name="container">Экземпляр контейнера для обновления.</param>
        /// <returns> Task </returns>
        public void UpdateContainer(CONTAINER container,
            string userId = "",
            string userInfo = "")
        {
            _unitOfWork.ContainerRepository.UpdateSimpleProperties(container, container.CONTAINERID);
            _unitOfWork.SaveChanges(userId, userInfo);
        }

        /// <summary>
        /// Удаляет контейнер с заданным идентификатором из хранилища данных.
        /// </summary>
        /// <param name="id">Идентификатор контейнера.</param>
        /// <returns> Task </returns>
        public void DeleteContainer(short id,
            string userId = "",
            string userInfo = "")
        {
            _unitOfWork.ContainerRepository.Delete(id);
            _unitOfWork.SaveChanges(userId, userInfo);
        }

        /// <summary>
        /// Проверяет наличие контейнера в хранилище данных
        /// соответствующего заданному фильтру.
        /// </summary>
        /// <param name="filter">Лямбда-выражение определяющее фильтр для поиска контейнера.</param>
        /// <returns>Возвращает <see langword="true"/>, если контейнер существует в хранилище данных.</returns>
        public bool ContainerExists(Expression<Func<CONTAINER, bool>> filter)
        {
            return _unitOfWork.ContainerRepository.Get(filter: filter)
                      .FirstOrDefault() != null;
        }
    }
}