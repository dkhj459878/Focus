﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Focus.BL.Mappers
{
    /// <summary>
    /// Describes portion as set of its parameters.
    /// </summary>
    internal class Portion
    {
        public int StartPosition { get; set; }

        public int Lenght { get; set; }
    }
}
