﻿using Focus.BL.Common.Helpers;
using System;
using System.Collections.Generic;

namespace Focus.BL.Helpers
{
    public class Generation<TType>: IGeneration<TType> where TType: Type
    {
        public TType Type { get; set; }

        public ICollection<Version<TType>> Versions { get; set; }

        public bool IsNextGeneration { get; set; }
    }
}
