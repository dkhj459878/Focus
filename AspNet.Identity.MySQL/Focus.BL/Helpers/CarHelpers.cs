﻿using Focus.DAL.Common.DbModels;
using System.Collections.Generic;
using System.Linq;

namespace Focus.BL.Helpers
{
    public static class CarHelpers
    {
        public static string GetCarDataAsc(this List<CAR> cars)
        {
            int length = cars.Count;
            if (cars.Count == 0)
            {
                return string.Empty;
            }

            string[] arrayIndexex = cars.OrderBy(c => c.CARID).Select(c => c.CARID.ToString()).ToArray();
            return string.Join(",", arrayIndexex);
        }
    }
}
