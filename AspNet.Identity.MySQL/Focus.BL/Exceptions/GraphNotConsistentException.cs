﻿using System;

namespace Focus.BL.Exceptions
{
    internal class GraphNotConsistentException : Exception
    {
        public GraphNotConsistentException()
        {

        }

        public GraphNotConsistentException(string message)
            : base(message)
        {

        }

        public GraphNotConsistentException(string message, Exception inner)
            : base(message, inner)
        {

        }
    }
}
