﻿using Focus.DAL.Common.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Focus.BL.Extensions
{
    internal static class ReflectionExtensions
    {
        internal static IEnumerable<PropertyInfo> GetMyProperties(this Type entityType, Func<PropertyInfo, bool> filter = null)
        {
            PropertyInfo[] propertyInfos = entityType.GetProperties();

            // If filter parameter equals null we return all the properties of the examined object type.
            if (filter == null)
            {
                return propertyInfos;
            }
            // Initialize a new list for containing returning values.
            List<PropertyInfo> properties = new List<PropertyInfo>();

            // Get object properties which are not classes, but String type including.
            
            foreach (PropertyInfo propertyInfo in propertyInfos.Where(filter))
            {
                properties.Add(propertyInfo);
            }
            propertyInfos = null; // Clear an unused object for security purposes.

            return properties;
        }

        internal static string GetEntityIdFieldName(this object obj)
        {
            const string NULL_REDERENCE_EXCEPTION_TEXT = "Null reference exception occurs when 'GetEntityIdFieldName' extension method of the 'ReflectionExtensions' class were been run.";

            if (obj == null)
            {
                throw new NullReferenceException(NULL_REDERENCE_EXCEPTION_TEXT);
            }

            string entityName = obj.GetType().Name;

            const string ID_FIELD_NAME = "ID";

            string entityIdNameFull = $"{entityName}{ID_FIELD_NAME}";
            string entityIdNameShort = $"{ID_FIELD_NAME}";

            PropertyInfo[] propertyInfos = obj.GetType().GetProperties();

            return propertyInfos.Any(p => p.Name == entityIdNameFull) ? entityIdNameFull :
                (propertyInfos.Any(p => p.Name == entityIdNameShort) ? entityIdNameShort : throw new NullReferenceException(NULL_REDERENCE_EXCEPTION_TEXT)); 

        }

        internal static object GetValue(this object obj, string propertyName)
        {
            //We handle situation when passed parameters is not initialized.
            if (string.IsNullOrEmpty(propertyName))
            {
                return null;
            }

            

            if (obj == null)
            {
                const string NULL_REDERENCE_EXCEPTION_TEXT = "Null reference exception occurs when 'GetValue' extension method of the 'ReflectionExtensions' class were been run.";
                throw new NullReferenceException(NULL_REDERENCE_EXCEPTION_TEXT);
            }

            bool propertyIsExist = obj.GetType().GetProperties().Any(p => p.Name == propertyName);

            if (!propertyIsExist)
            {
                return null;
            }
            else
            {
                PropertyInfo propertyInfo = obj.GetType().GetProperty(propertyName);
                return propertyInfo.GetValue(obj);
            }

        }

        internal static bool IsNullableType(Type type)
        {
            return type.IsGenericType &&
                type.GetGenericTypeDefinition().Equals(typeof(Nullable));
        }

        internal static bool IsCollectionProperty(this PropertyInfo propertyInfo)
        {
            if (propertyInfo == null)
            {
                const string NULL_REDERENCE_EXCEPTION_TEXT = "Null reference exception occurs when 'IsCollection' extension method of the 'ReflectionExtensions' class were been run.";
                throw new NullReferenceException(NULL_REDERENCE_EXCEPTION_TEXT);
            }
            else
            {
                return typeof(ICollection<>).IsAssignableFrom(propertyInfo.PropertyType);
            }
        }

        internal static void SetValue(this object obj, string propertyName, object value)
        {
            const string NULL_REDERENCE_EXCEPTION_TEXT = "Null reference exception occurs when 'SetValue' extension method of the 'ReflectionExtensions' class were been run.";

            if (obj == null)
            {
                throw new NullReferenceException(NULL_REDERENCE_EXCEPTION_TEXT);
            }

            if (value == null)
            {
                return;
            }

            bool propertyIsExist = obj.GetType().GetProperties().Any(p => p.Name == propertyName);

            if (!propertyIsExist)
            {
                return;
            }
            else
            {
                Type objType = obj.GetType();

                //TODO - Refactoring is needed.
                const string DATE_TIME_NAIL = ",000000000";
                
                value = value.ToString().Contains(DATE_TIME_NAIL) ? value.ToString().Replace(DATE_TIME_NAIL, string.Empty) : value;

                PropertyInfo propertyInfo = objType.GetProperty(propertyName);

                Type propertyType = propertyInfo.PropertyType;

                Type targetType = Nullable.GetUnderlyingType(propertyType);

                targetType = targetType ?? propertyType;

                value = targetType.Name == typeof(decimal).Name ? value.ToString().Replace(".", ",") : value;

                if (propertyInfo.IsCollection())
                {
                    propertyInfo.SetValue(obj, value, null);
                }
                else
                {
                    propertyInfo.SetValue(obj, Convert.ChangeType(value, targetType), null);
                }
            }
        }

        internal static ICollection<TEntity> GetCollection<TEntity>(this object obj)
        {
            Type type = typeof(ICollection<TEntity>);

            return Activator.CreateInstance(type) as ICollection<TEntity>;
        }
    }


}
