﻿using Focus.DAL.Common.DbModels;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Focus.BL.Extensions
{
    public static class DocumentExtensions
    {
        public static string GetNextDocNumber(this IEnumerable<DOC> docs, byte postId, string postCode)
        {
            //const int POSITION_OF_UNDERSCOPE_IN_ARTIFICIAL_NUMBER = 5;

            DateTime currDate = DateTime.Now;

            if ((docs == null) || (docs.Count() == 0) || string.IsNullOrEmpty(postCode))
            {
                return postCode + "_" + currDate.ToString("yyyyMMdd") + "_1";
            }
            List<string> num = docs.Where(d => d.INSPECTOR1.POSTID == postId).Select(d => d.NUM).ToList();

            int lastNumber = Convert.ToInt32((DateTime.Now.Ticks).ToString().Substring(DateTime.Now.Ticks.ToString().Length - 9));

            return postCode + "_" + currDate.ToString("yyyyMMdd") + "_" + (++lastNumber).ToString();
        }

        public static byte ConvertToByte(this bool BoleanValue)
        {
            return BoleanValue ? Convert.ToByte(1) : Convert.ToByte(0);
        }
    }
}