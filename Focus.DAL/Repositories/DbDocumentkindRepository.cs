﻿using Focus.DAL.Common.DbModels;
using Focus.DAL.Common.Repositories;

namespace Focus.DAL.Repositories
{
    public class DbDocumentkindRepository : BaseRepository<DOCUMENTKIND>, IDbDocumentkindRepository
    {
        public DbDocumentkindRepository(Entities focusContext) : base(focusContext)
        {
        }
    }
}