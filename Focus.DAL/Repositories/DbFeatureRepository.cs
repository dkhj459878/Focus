﻿using Focus.DAL.Common.DbModels;
using Focus.DAL.Common.Repositories;

namespace Focus.DAL.Repositories
{
    public class DbFeatureRepository : BaseRepository<FEATURE>, IDbFeatureRepository
    {
        public DbFeatureRepository(Entities focusContext) : 
            base(focusContext)
        {
        }
    }
}
