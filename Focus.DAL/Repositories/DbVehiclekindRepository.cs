﻿using Focus.DAL.Common.DbModels;
using Focus.DAL.Common.Repositories;

namespace Focus.DAL.Repositories
{
    public class DbVehiclekindRepository : BaseRepository<VEHICLEKIND>, IDbVehiclekindRepository
    {
        public DbVehiclekindRepository(Entities focusContext) : base(focusContext)
        {
        }
    }
}