﻿using Focus.DAL.Common.DbModels;
using Focus.DAL.Common.Repositories;

namespace Focus.DAL.Repositories
{
    public class DbCustombrockerRepository : BaseRepository<CUSTOMBROCKER>, IDbCustombrockerRepository
    {
        public DbCustombrockerRepository(Entities focusContext) : base(focusContext)
        {
        }
    }
}