﻿using Focus.DAL.Common.DbModels;
using Focus.DAL.Common.Repositories;

namespace Focus.DAL.Repositories
{
    public class DbDevicetypeRepository : BaseRepository<DEVICETYPE>, IDbDevicetypeRepository
    {
        public DbDevicetypeRepository(Entities focusContext) : base(focusContext)
        {
        }
    }
}