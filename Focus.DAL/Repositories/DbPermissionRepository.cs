﻿using Focus.DAL.Common.DbModels;
using Focus.DAL.Common.Repositories;

namespace Focus.DAL.Repositories
{
    public class DbPermissionRepository : BaseRepository<PERMISSION>, IDbPermissionRepository
    {
        public DbPermissionRepository(Entities focusContext) : base(focusContext)
        {
        }
    }
}