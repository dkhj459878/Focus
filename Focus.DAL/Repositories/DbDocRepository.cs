﻿using Focus.DAL.Common.DbModels;
using Focus.DAL.Common.Repositories;

namespace Focus.DAL.Repositories
{
    public class DbDocRepository : BaseRepository<DOC>, IDbDocRepository
    {
        public DbDocRepository(Entities focusContext) : base(focusContext)
        {
        }
    }
}