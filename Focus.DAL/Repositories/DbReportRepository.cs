﻿using Focus.DAL.Common.DbModels;
using Focus.DAL.Common.Repositories;

namespace Focus.DAL.Repositories
{
    public class DbReportRepository : BaseRepository<REPORT>, IDbReportRepository
    {
        public DbReportRepository(Entities focusContext) : base(focusContext)
        {
        }
    }
}