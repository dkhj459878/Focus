﻿using Focus.DAL.Common.DbModels;
using Focus.DAL.Common.Repositories;

namespace Focus.DAL.Repositories
{
    public class DbContainerRepository : BaseRepository<CONTAINER>, IDbContainerRepository
    {
        public DbContainerRepository(Entities focusContext) : base(focusContext)
        {
        }
    }
}
