﻿using Focus.DAL.Common.DbModels;
using Focus.DAL.Common.Repositories;

namespace Focus.DAL.Repositories
{
    public class DbUserRepository : BaseRepository<USERS>, IDbUserRepository
    {
        public DbUserRepository(Entities focusContext) : base(focusContext)
        {
        }
    }
}
