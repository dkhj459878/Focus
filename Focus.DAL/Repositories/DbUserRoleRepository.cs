﻿using Focus.DAL.Common.DbModels;
using Focus.DAL.Common.Repositories;

namespace Focus.DAL.Repositories
{
    public class DbUserRoleRepository : BaseRepository<USERROLES>, IDbUserRoleRepository
    {
        public DbUserRoleRepository(Entities focusContext) : base(focusContext)
        {
        }
    }
}