﻿using Focus.DAL.Common.DbModels;
using Focus.DAL.Common.Repositories;

namespace Focus.DAL.Repositories
{
    public class DbDeviceRepository : BaseRepository<DEVICE>, IDbDeviceRepository
    {
        public DbDeviceRepository(Entities focusContext) : base(focusContext)
        {
        }
    }
}