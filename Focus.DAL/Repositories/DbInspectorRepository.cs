﻿using Focus.DAL.Common.DbModels;
using Focus.DAL.Common.Repositories;

namespace Focus.DAL.Repositories
{
    public class DbInspectorRepository : BaseRepository<INSPECTOR>, IDbInspectorRepository
    {
        public DbInspectorRepository(Entities focusContext) : base(focusContext)
        {
        }
    }
}