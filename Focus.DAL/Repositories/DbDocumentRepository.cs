﻿using Focus.DAL.Common.DbModels;
using Focus.DAL.Common.Repositories;

namespace Focus.DAL.Repositories
{
    public class DbDocumentRepository : BaseRepository<DOCUMENT>, IDbDocumentRepository
    {
        public DbDocumentRepository(Entities focusContext) : base(focusContext)
        {
        }
    }
}