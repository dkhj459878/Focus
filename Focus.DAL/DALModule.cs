﻿using Focus.DAL.Common.Repositories;
using Focus.DAL.Common.UoW;
using Focus.DAL.Repositories;
using Focus.DAL.UoW;
using Focus.DAL.Common.DbModels;
using Ninject.Modules;
using System.Data.Entity;   
using Ninject.Web.Common;

namespace Rocket.DAL
{
    public class DALModule : NinjectModule
    {
        /// <summary>
        /// Настройка Ninject для DAL
        /// </summary>
        public override void Load()
        {
            //контекст
            Bind<DbContext>().To<Entities>();
            Bind<Entities>().ToSelf().InRequestScope();

            // данные
            Bind<IDbCarRepository>().To<DbCarRepository>();
            Bind<IDbContainerRepository>().To<DbContainerRepository>();
            Bind<IDbCountryRepository>().To<DbCountryRepository>();
            Bind<IDbCustombrockerRepository>().To<DbCustombrockerRepository>();
            Bind<IDbDeviceRepository>().To<DbDeviceRepository>();
            Bind<IDbDevicetypeRepository>().To<DbDevicetypeRepository>();
            Bind<IDbDocRepository>().To<DbDocRepository>();
            Bind<IDbDocumentkindRepository>().To<DbDocumentkindRepository>();
            Bind<IDbDocumentRepository>().To<DbDocumentRepository>();
            Bind<IDbInspectorRepository>().To<DbInspectorRepository>();
            Bind<IDbOrganizationRepository>().To<DbOrganizationRepository>();
            Bind<IDbPermissionRepository>().To<DbPermissionRepository>();
            Bind<IDbPostRepository>().To<DbPostRepository>();
            Bind<IDbReportRepository>().To<DbReportRepository>();
            Bind<IDbStorageRepository>().To<DbStorageRepository>();
            Bind<IDbVehiclekindRepository>().To<DbVehiclekindRepository>();
            Bind<IDbVehicleRepository>().To<DbVehicleRepository>();
            Bind<IDbVehicletypeRepository>().To<DbVehicletypeRepository>();
            Bind<IDbAudit_trailRepository>().To<DbAudit_trailRepository>();
            Bind<IDbVersionRepository>().To<DbVersionRepository>();
            Bind<IDbFeatureRepository>().To<DbFeatureRepository>();
            Bind<IDbUserRepository>().To<DbUserRepository>();
            Bind<IDbRoleRepository>().To<DbRoleRepository>();
            Bind<IDbUserRoleRepository>().To<DbUserRoleRepository>();


            //UoW
            Bind<IUnitOfWork>().To<UnitOfWorkAuditable>();
        }
    }
}
