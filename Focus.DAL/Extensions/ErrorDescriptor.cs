﻿using System.Data.Entity.Validation;

namespace Focus.DAL.Extensions
{
    public static class ErrorDescriptor
    {
        public static string GetDetails(this DbEntityValidationException dbException)
        {
            if (dbException == null)
            {
                return string.Empty;
            }

            string result = string.Empty;

            foreach (var eve in dbException.EntityValidationErrors)
            {
                result += $"Entity of type \"{eve.Entry.Entity.GetType().Name}\" in state \"{eve.Entry.State}\" has the following validation errors:";

                foreach (var ve in eve.ValidationErrors)
                {
                    result += $"/n- Property: \"{ve.PropertyName}\", Error: \"{ve.ErrorMessage}\"";
                }
            }

            return result;
        }
    }
}
