﻿using FluentAssertions;
using Focus.BL.Common.Services;
using Focus.BL.Services;
using Focus.BL.Tests.FakeData;
using Focus.DAL.Common.DbModels;
using Focus.DAL.Common.Repositories;
using Focus.DAL.Common.UoW;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Focus.BL.Tests.Vehiclekind
{
    /// <summary>
    /// Unit-тесты для сервиса <see cref="VehiclekindService"/>
    /// </summary>
    [TestFixture]
    public class VehiclekindServiceTests
    {
        private const int VehiclekindsCount = 3;
        private IVehiclekindService _vehiclekindService;
        private FakeVehiclekind _fakeVehiclekinds;
        private Mock<IDbVehiclekindRepository> mockvehiclekindRepository;

        /// <summary>
        /// Осуществляет настройки
        /// </summary>
        [OneTimeSetUp]
        public void SetUp()
        {


            _fakeVehiclekinds = new FakeVehiclekind();

            mockvehiclekindRepository = new Mock<IDbVehiclekindRepository>();
            mockvehiclekindRepository
                .Setup(mock => mock.Get(It.IsAny<Expression<Func<VEHICLEKIND, bool>>>(), null, It.IsAny<string>()))
                .Returns((Expression<Func<VEHICLEKIND, bool>> filter,
                    Func<IQueryable<VEHICLEKIND>, IOrderedQueryable<VEHICLEKIND>> orderBy,
                    string includeProperties) => _fakeVehiclekinds.Vehiclekinds.Where(filter.Compile()));
            mockvehiclekindRepository
                .Setup(mock => mock.Get(null, null, It.IsAny<string>()))
                .Returns((Expression<Func<VEHICLEKIND, bool>> filter,
                    Func<IQueryable<VEHICLEKIND>, IOrderedQueryable<VEHICLEKIND>> orderBy,
                    string includeProperties) => _fakeVehiclekinds.Vehiclekinds);
            mockvehiclekindRepository
                .Setup(mock => mock.GetPage(It.IsAny<int>(), It.IsAny<int>(), null, null, ""))
                .Returns((int pageSize,
                    int pageNumber,
                    Expression<Func<VEHICLEKIND, bool>> filter,
                    Func<IQueryable<VEHICLEKIND>, IOrderedQueryable<VEHICLEKIND>> orderBy,
                    string includeProperties) => _fakeVehiclekinds.Vehiclekinds.Skip((pageNumber - 1) * pageSize).Take(pageSize));
            mockvehiclekindRepository.Setup(mock => mock.GetById(It.IsAny<byte>()))
                .Returns((byte id) => _fakeVehiclekinds.Vehiclekinds.Find(f => f.VEHICLEKINDID == id));
            mockvehiclekindRepository.Setup(mock => mock.Insert(It.IsAny<VEHICLEKIND>()))
                .Callback((VEHICLEKIND f) => _fakeVehiclekinds.Vehiclekinds.Add(f));
            mockvehiclekindRepository.Setup(mock => mock.Update(It.IsAny<VEHICLEKIND>()))
                .Callback((VEHICLEKIND f) => _fakeVehiclekinds.Vehiclekinds.Find(d => d.VEHICLEKINDID == f.VEHICLEKINDID).NAME = f.NAME);
            mockvehiclekindRepository.Setup(mock => mock.Delete(It.IsAny<byte>()))
                .Callback((byte id) => _fakeVehiclekinds.Vehiclekinds
                    .Remove(_fakeVehiclekinds.Vehiclekinds.Find(f => f.VEHICLEKINDID == id)));

            var mockvehiclekindUnitOfWork = new Mock<IUnitOfWork>();
            mockvehiclekindUnitOfWork.Setup(mock => mock.VehiclekindRepository)
                .Returns(() => mockvehiclekindRepository.Object); //todo - закоментил, не знаю в чем дело

            _vehiclekindService = new VehiclekindService(mockvehiclekindUnitOfWork.Object);
        }

        /// <summary>
        /// Тест метода получения экземпляров типов транспортных средств
        /// выбранной страницы пейджинга. Выбрана может быть любая страница.
        /// </summary>
        [Test, Order(1)]
        public void GetExistedVehiclekindsPageTest([Random(1, 2, 1)]int pageNumber)
        {
            const int pagesize = 2;

            // Arrange
            var vehiclekinds = _fakeVehiclekinds.Vehiclekinds;

            var vehiclekindsPageIndexes = new List<int>();

            var startVehiclekindIndexInPage = pagesize * (pageNumber - 1);

            var finishVehiclekindIndexInPage = startVehiclekindIndexInPage + pagesize - 1;

            for (var i = startVehiclekindIndexInPage; i <= finishVehiclekindIndexInPage; i++)
            {
                vehiclekindsPageIndexes.Add(i);
            }

            List<VEHICLEKIND> vehiclekindsPage;
            vehiclekindsPage = vehiclekindsPageIndexes.Select(vehiclekindsPageIndex => vehiclekinds[vehiclekindsPageIndex]).ToList();


            // Act

            var actualVehiclekindsPage = _vehiclekindService.GetVehiclekindsPage(pageSize: pagesize, pageNumber: pageNumber).ToList();

            // Assert
            vehiclekindsPage.Should().BeEquivalentTo(actualVehiclekindsPage,
                options => options.ExcludingMissingMembers());
        }

        /// <summary>
        /// Тест метода получения экземпляров типов транспортных средств
        /// выбранной страницы пейджинга. Выбрана последняя страница.
        /// </summary>
        [Test, Order(1)]
        public void GetExistedVehiclekindsLastPageTest()
        {
            const int pagesize = 2;
            const int pageNumber = 2;

            // Arrange
            var dbVehiclekinds = _fakeVehiclekinds.Vehiclekinds;

            var vehiclekindsPageIndexes = new List<int>();

            var startVehiclekindIndexInPage = pagesize * (pageNumber - 1);

            var finishVehiclekindIndexInPage = startVehiclekindIndexInPage + VehiclekindsCount % pagesize - 1;

            for (var i = startVehiclekindIndexInPage; i <= finishVehiclekindIndexInPage; i++)
            {
                vehiclekindsPageIndexes.Add(i);
            }

            var dbVehiclekindsPage = vehiclekindsPageIndexes.Select(vehiclekindsPageIndex => dbVehiclekinds[vehiclekindsPageIndex]).ToList();

            // Act
            var actualVehiclekindsPage = _vehiclekindService.GetVehiclekindsPage(pageSize: pagesize, pageNumber: pageNumber).ToList();

            // Assert
            dbVehiclekindsPage.Should().BeEquivalentTo(actualVehiclekindsPage,
                options => options.ExcludingMissingMembers());
        }

        /// <summary>
        /// Тест метода получения экземпляров типов транспортных средств
        /// выбранной страницы пейджинга. Номер страницы отрицательный.
        /// </summary>
        [Test, Order(1)]
        public void GetNotExistedVehiclekindsPageWithNegativePageNumbersTest([Random(-100500, -1, 5)] int pageNumber)
        {
            const int pagesize = 16;

            // Act
            ICollection<VEHICLEKIND> actualVehiclekindsPage = _vehiclekindService.GetVehiclekindsPage(pageSize: pagesize, pageNumber: pageNumber).ToList();

            ICollection<VEHICLEKIND> expectedPage = _vehiclekindService.GetVehiclekindsPage(pageSize: pagesize, pageNumber: 1).ToList();

            // Assert
            actualVehiclekindsPage.Should().BeEquivalentTo(expectedPage,
                options => options.ExcludingMissingMembers());
        }

        /// <summary>
        /// Тест метода получения экземпляров типов транспортных средств
        /// выбранной страницы пейджинга. Номер страницы больше общего числа страниц.
        /// </summary>
        [Test, Order(1)]
        public void GetNotExistedVehiclekindsPageWithValueIfPageNumbersMoreThanTotalVehiclekindsPagesTest([Random(VehiclekindsCount, VehiclekindsCount + 100500, 5)]int pageNumber)
        {
            const int pagesize = 16;

            // Act
            ICollection<VEHICLEKIND> actualVehiclekindsPage = _vehiclekindService.GetVehiclekindsPage(pageSize: pagesize, pageNumber: pageNumber).ToList();

            // Assert
            actualVehiclekindsPage.Should().BeEmpty();
        }

        /// <summary>
        /// Тест метода получения экземпляров типов транспортных средств
        /// ввыбранной страницы пейджинга из пустого репозитария.
        /// </summary>
        [Test, Order(5)]
        public void GetNotExistedVehiclekindsPageFromEmptyRepositoryTest([Random(1, 18, 5)]int pageNumber)
        {
            const int pagesize = 16;

            // Arrange
            _fakeVehiclekinds.Vehiclekinds.Clear();

            // Act
            ICollection<VEHICLEKIND> actualVehiclekindsPage = _vehiclekindService.GetVehiclekindsPage(pageSize: pagesize, pageNumber: pageNumber).ToList();

            // Assert
            actualVehiclekindsPage.Should().BeEmpty();
        }

        /// <summary>
        /// Тест метода получения всех экземпляров типов транспортных средств
        /// в пустом репозитарии.
        /// </summary>
        [Test, Order(5)]
        public void GetAllVehiclekindsFromEmptyRepositoryTest()
        {
            // Arrange
            _fakeVehiclekinds.Vehiclekinds.Clear();

            // Act
            var actualVehiclekinds = _vehiclekindService.GetAllVehiclekinds(filter: null);

            // Assert
            actualVehiclekinds.Should().BeEmpty();
        }

        /// <summary>
        /// Тест метода получения всех экземпляров типов транспортных средств
        /// в не пустом репозитарии.
        /// </summary>
        [Test, Order(1)]
        public void GetAllExistedVehiclekindsTest()
        {
            // Arrange
            var expectedVehiclekinds = _fakeVehiclekinds.Vehiclekinds;

            // Act
            var actualVehiclekinds = _vehiclekindService.GetAllVehiclekinds().ToList();

            // Assert
            
            expectedVehiclekinds.Should().BeEquivalentTo(actualVehiclekinds,
                options => options.ExcludingMissingMembers());

            expectedVehiclekinds.Count.Should().Be(actualVehiclekinds.Count);
        }

        /// <summary>
        /// Тест метода получения экземпляра типа транспортного средства по заданному идентификатору.
        /// тип транспортного средства с передаваемым идентификатором существует
        /// </summary>
        /// <param name="id">Идентификатор типа транспортного средства</param>
        [Test, Order(1)]
        public void GetExistedVehiclekindTest([Random(0, VehiclekindsCount - 1, 5)] byte id)
        {
            // Arrange
            var expectedVehiclekind = _fakeVehiclekinds.Vehiclekinds.Find(f => f.VEHICLEKINDID == id);

            // Act
            var actualVehiclekind = _vehiclekindService.GetVehiclekind(id);

            // Assert


            actualVehiclekind.Should().BeEquivalentTo(expectedVehiclekind,
                options => options.ExcludingMissingMembers());
        }

        /// <summary>
        /// Тест метода получения экземпляра типа транспортного средства по заданному идентификатору.
        /// тип транспортного средства с передаваемым идентификатором не существует
        /// </summary>
        /// <param name="id">Идентификатор типа транспортного средства</param>
        [Test, Order(1)]
        public void GetNotExistedVehiclekindTest([Random(VehiclekindsCount, VehiclekindsCount + 300, 5)]
                    byte id)
        {
            var actualVehiclekind = _vehiclekindService.GetVehiclekind(id);

            actualVehiclekind.Should().BeNull();
        }

        /// <summary>
        /// Тест метода добавления типа транспортного средства в хранилище данных
        /// </summary>
        [Test, Repeat(5), Order(2)]
        public void AddVehiclekindTest()
        {

            byte vehiclekindId = 15;
            
            // Arrange
            var vehiclekindsFake = _fakeVehiclekinds.Vehiclekinds;

            var vehiclekindFake = vehiclekindsFake.Where(c => c.VEHICLEKINDID == vehiclekindId).FirstOrDefault();
            

            // Act

            var actualVehiclekind = _vehiclekindService.GetVehiclekind(vehiclekindId);

            // Assert

            actualVehiclekind.Should().BeEquivalentTo(actualVehiclekind,
                options => options.ExcludingMissingMembers());
        }

        /// <summary>
        /// Тест метода добавления типа транспортного средства в хранилище данных
        /// </summary>
        [Test, Repeat(5), Order(2)]
        public void AddVehiclekindIntegratedTest()
        {
            //IUnitOfWork _unitOfWork;

            //// Arrange
            //var vehiclekind = new FakeVehiclekind(1, false, false, false, false, 5, 5).Vehiclekinds[0];
            //var vehiclekindManagementService = new VehiclekindManagementService(_unitOfWork);
            //vehiclekind.Id = _fakeVehiclekinds.Vehiclekinds.Last().Id + 1;

            //// Act
            //var actualId = _vehiclekindService.AddVehiclekind(vehiclekind);

            //var actualVehiclekind = _vehiclekindService.GetVehiclekind(actualId);

            //// Assert
            //actualVehiclekind.VehiclekindDetail.EMailAddresses.Should().BeEquivalentTo(vehiclekind.VehiclekindDetail.EMailAddresses,
            //    options => options.ExcludingMissingMembers());
            //actualVehiclekind.Login.Should().BeEquivalentTo(vehiclekind.Login);
            //actualVehiclekind.FirstName.Should().BeEquivalentTo(vehiclekind.FirstName);
            //actualVehiclekind.LastName.Should().BeEquivalentTo(vehiclekind.LastName);
        }

        /// <summary>
        /// Тест метода обновления данных об типе транспортного средства
        /// </summary>
        /// <param name="id">Идентификатор типа транспортного средства для обновления</param>
        [Test, Order(2)]
        public void UpdateVehiclekindTest([Random(1, VehiclekindsCount, 5)] byte id)
        {
            // Arrange
            var vehiclekind = _vehiclekindService.GetVehiclekind(id);
            vehiclekind.NAME = new Bogus.Faker().Random.AlphaNumeric(17);

            // Act
            _vehiclekindService.UpdateVehiclekind(vehiclekind);
            var actualVehiclekind = _fakeVehiclekinds.Vehiclekinds.Find(f => f.VEHICLEKINDID == id);

            // Assert
            actualVehiclekind.NAME.Should().Be(vehiclekind.NAME);
        }

        /// <summary>
        /// Тест метода удаления типа транспортного средства из хранилища данных
        /// </summary>
        /// <param name="id">Идентификатор типа транспортного средства для удаления</param>
        [Test, Order(3)]
        public void DeleteVehiclekindTest([Random(1, VehiclekindsCount, 5)] byte id)
        {
            // Arrange
            _vehiclekindService.DeleteVehiclekind(id);

            // Act
            var actualVehiclekind = _fakeVehiclekinds.Vehiclekinds.Find(vehiclekind => vehiclekind.VEHICLEKINDID == id);

            // Assert
            actualVehiclekind.Should().BeNull();
        }

        /// <summary>
        /// Тест метода проверки наличия типа транспортного средства в хранилище данных.
        /// тип транспортного средства существует
        /// </summary>
        /// <param name="id">Идентификатор типа транспортного средства для поиска</param>
        [Test, Order(2)]
        public void VehiclekindExistsTest([Random(1, VehiclekindsCount, 5)] short id)
        {
            // Arrange
            var loginToFind = _fakeVehiclekinds.Vehiclekinds.Find(dbf => dbf.VEHICLEKINDID == id).NAME;

            // Act
            var actual = _vehiclekindService
                .VehiclekindExists(f => f.NAME == loginToFind);

            // Assert
            actual.Should().BeTrue();
        }

        /// <summary>
        /// Тест метода проверки наличия типа транспортного средства в хранилище данных.
        /// тип транспортного средства не существует
        /// </summary>
        /// <param name="login">Логин типа транспортного средства для поиска</param>
        [Test, Order(2)]
        public void VehiclekindNotExistsTest([Values("shameonyou", "qwer", "", "befastandclearver", "strangelogin")]
                    string VIN)
        {
            var actual = _vehiclekindService
                .VehiclekindExists(f => f.NAME == VIN);

            actual.Should().BeFalse();
        }
    }
}

