﻿using FluentAssertions;
using Focus.BL.Common.Services;
using Focus.BL.Services;
using Focus.BL.Tests.FakeData;
using Focus.DAL.Common.DbModels;
using Focus.DAL.Common.Repositories;
using Focus.DAL.Common.UoW;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Focus.BL.Tests.Documentkind
{
    /// <summary>
    /// Unit-тесты для сервиса <see cref="DocumentkindService"/>
    /// </summary>
    [TestFixture]
    public class DocumentkindServiceTests
    {
        private const int DocumentkindsCount = 4;
        private IDocumentkindService _documentkindService;
        private FakeDocumentkind _fakeDocumentkinds;
        private Mock<IDbDocumentkindRepository> mockdocumentkindRepository;

        /// <summary>
        /// Осуществляет настройки
        /// </summary>
        [OneTimeSetUp]
        public void SetUp()
        {


            _fakeDocumentkinds = new FakeDocumentkind(DocumentkindsCount);

            mockdocumentkindRepository = new Mock<IDbDocumentkindRepository>();
            mockdocumentkindRepository
                .Setup(mock => mock.Get(It.IsAny<Expression<Func<DOCUMENTKIND, bool>>>(), null, It.IsAny<string>()))
                .Returns((Expression<Func<DOCUMENTKIND, bool>> filter,
                    Func<IQueryable<DOCUMENTKIND>, IOrderedQueryable<DOCUMENTKIND>> orderBy,
                    string includeProperties) => _fakeDocumentkinds.Documentkinds.Where(filter.Compile()));
            mockdocumentkindRepository
                .Setup(mock => mock.Get(null, null, It.IsAny<string>()))
                .Returns((Expression<Func<DOCUMENTKIND, bool>> filter,
                    Func<IQueryable<DOCUMENTKIND>, IOrderedQueryable<DOCUMENTKIND>> orderBy,
                    string includeProperties) => _fakeDocumentkinds.Documentkinds);
            mockdocumentkindRepository
                .Setup(mock => mock.GetPage(It.IsAny<int>(), It.IsAny<int>(), null, null, ""))
                .Returns((int pageSize,
                    int pageNumber,
                    Expression<Func<DOCUMENTKIND, bool>> filter,
                    Func<IQueryable<DOCUMENTKIND>, IOrderedQueryable<DOCUMENTKIND>> orderBy,
                    string includeProperties) => _fakeDocumentkinds.Documentkinds.Skip((pageNumber - 1) * pageSize).Take(pageSize));
            mockdocumentkindRepository.Setup(mock => mock.GetById(It.IsAny<byte>()))
                .Returns((byte id) => _fakeDocumentkinds.Documentkinds.Find(f => f.DOCUMENTKINDID == id));
            mockdocumentkindRepository.Setup(mock => mock.Insert(It.IsAny<DOCUMENTKIND>()))
                .Callback((DOCUMENTKIND f) => _fakeDocumentkinds.Documentkinds.Add(f));
            mockdocumentkindRepository.Setup(mock => mock.Update(It.IsAny<DOCUMENTKIND>()))
                .Callback((DOCUMENTKIND f) => _fakeDocumentkinds.Documentkinds.Find(d => d.DOCUMENTKINDID == f.DOCUMENTKINDID).NAME = f.NAME);
            mockdocumentkindRepository.Setup(mock => mock.Delete(It.IsAny<byte>()))
                .Callback((byte id) => _fakeDocumentkinds.Documentkinds
                    .Remove(_fakeDocumentkinds.Documentkinds.Find(f => f.DOCUMENTKINDID == id)));

            var mockdocumentkindUnitOfWork = new Mock<IUnitOfWork>();
            mockdocumentkindUnitOfWork.Setup(mock => mock.DocumentkindRepository)
                .Returns(() => mockdocumentkindRepository.Object); //todo - закоментил, не знаю в чем дело

            _documentkindService = new DocumentkindService(mockdocumentkindUnitOfWork.Object);
        }

        /// <summary>
        /// Тест метода получения экземпляров типов транспортных средств
        /// выбранной страницы пейджинга. Выбрана может быть любая страница.
        /// </summary>
        [Test, Order(1)]
        public void GetExistedDocumentkindsPageTest([Random(1, 2, 1)]int pageNumber)
        {
            const int pagesize = 2;

            // Arrange
            var documentkinds = _fakeDocumentkinds.Documentkinds;

            var documentkindsPageIndexes = new List<int>();

            var startDocumentkindIndexInPage = pagesize * (pageNumber - 1);

            var finishDocumentkindIndexInPage = startDocumentkindIndexInPage + pagesize - 1;

            for (var i = startDocumentkindIndexInPage; i <= finishDocumentkindIndexInPage; i++)
            {
                documentkindsPageIndexes.Add(i);
            }

            List<DOCUMENTKIND> documentkindsPage;
            documentkindsPage = documentkindsPageIndexes.Select(documentkindsPageIndex => documentkinds[documentkindsPageIndex]).ToList();


            // Act

            var actualDocumentkindsPage = _documentkindService.GetDocumentkindsPage(pageSize: pagesize, pageNumber: pageNumber).ToList();

            // Assert
            documentkindsPage.Should().BeEquivalentTo(actualDocumentkindsPage,
                options => options.ExcludingMissingMembers());
        }

        /// <summary>
        /// Тест метода получения экземпляров типов транспортных средств
        /// выбранной страницы пейджинга. Выбрана последняя страница.
        /// </summary>
        [Test, Order(1)]
        public void GetExistedDocumentkindsLastPageTest()
        {
            const int pagesize = 2;
            const int pageNumber = 2;

            // Arrange
            var dbDocumentkinds = _fakeDocumentkinds.Documentkinds;

            var documentkindsPageIndexes = new List<int>();

            var startDocumentkindIndexInPage = pagesize * (pageNumber - 1);

            var finishDocumentkindIndexInPage = 3;

            for (var i = startDocumentkindIndexInPage; i <= finishDocumentkindIndexInPage; i++)
            {
                documentkindsPageIndexes.Add(i);
            }

            var dbDocumentkindsPage = documentkindsPageIndexes.Select(documentkindsPageIndex => dbDocumentkinds[documentkindsPageIndex]).ToList();

            // Act
            var actualDocumentkindsPage = _documentkindService.GetDocumentkindsPage(pageSize: pagesize, pageNumber: pageNumber).ToList();

            // Assert
            dbDocumentkindsPage.Should().BeEquivalentTo(actualDocumentkindsPage,
                options => options.ExcludingMissingMembers());
        }

        /// <summary>
        /// Тест метода получения экземпляров типов транспортных средств
        /// выбранной страницы пейджинга. Номер страницы отрицательный.
        /// </summary>
        [Test, Order(1)]
        public void GetNotExistedDocumentkindsPageWithNegativePageNumbersTest([Random(-100500, -1, 5)] int pageNumber)
        {
            const int pagesize = 16;

            // Act
            ICollection<DOCUMENTKIND> actualDocumentkindsPage = _documentkindService.GetDocumentkindsPage(pageSize: pagesize, pageNumber: pageNumber).ToList();

            ICollection<DOCUMENTKIND> expectedPage = _documentkindService.GetDocumentkindsPage(pageSize: pagesize, pageNumber: 1).ToList();

            // Assert
            actualDocumentkindsPage.Should().BeEquivalentTo(expectedPage,
                options => options.ExcludingMissingMembers());
        }

        /// <summary>
        /// Тест метода получения экземпляров типов транспортных средств
        /// выбранной страницы пейджинга. Номер страницы больше общего числа страниц.
        /// </summary>
        [Test, Order(1)]
        public void GetNotExistedDocumentkindsPageWithValueIfPageNumbersMoreThanTotalDocumentkindsPagesTest([Random(DocumentkindsCount, DocumentkindsCount + 100500, 5)]int pageNumber)
        {
            const int pagesize = 16;

            // Act
            ICollection<DOCUMENTKIND> actualDocumentkindsPage = _documentkindService.GetDocumentkindsPage(pageSize: pagesize, pageNumber: pageNumber).ToList();

            // Assert
            actualDocumentkindsPage.Should().BeEmpty();
        }

        /// <summary>
        /// Тест метода получения экземпляров типов транспортных средств
        /// ввыбранной страницы пейджинга из пустого репозитария.
        /// </summary>
        [Test, Order(5)]
        public void GetNotExistedDocumentkindsPageFromEmptyRepositoryTest([Random(1, 18, 5)]int pageNumber)
        {
            const int pagesize = 16;

            // Arrange
            _fakeDocumentkinds.Documentkinds.Clear();

            // Act
            ICollection<DOCUMENTKIND> actualDocumentkindsPage = _documentkindService.GetDocumentkindsPage(pageSize: pagesize, pageNumber: pageNumber).ToList();

            // Assert
            actualDocumentkindsPage.Should().BeEmpty();
        }

        /// <summary>
        /// Тест метода получения всех экземпляров типов транспортных средств
        /// в пустом репозитарии.
        /// </summary>
        [Test, Order(5)]
        public void GetAllDocumentkindsFromEmptyRepositoryTest()
        {
            // Arrange
            _fakeDocumentkinds.Documentkinds.Clear();

            // Act
            var actualDocumentkinds = _documentkindService.GetAllDocumentkinds(filter: null);

            // Assert
            actualDocumentkinds.Should().BeEmpty();
        }

        /// <summary>
        /// Тест метода получения всех экземпляров типов транспортных средств
        /// в не пустом репозитарии.
        /// </summary>
        [Test, Order(1)]
        public void GetAllExistedDocumentkindsTest()
        {
            // Arrange
            var expectedDocumentkinds = _fakeDocumentkinds.Documentkinds;

            // Act
            var actualDocumentkinds = _documentkindService.GetAllDocumentkinds().ToList();

            // Assert
            
            expectedDocumentkinds.Should().BeEquivalentTo(actualDocumentkinds,
                options => options.ExcludingMissingMembers());

            expectedDocumentkinds.Count.Should().Be(actualDocumentkinds.Count);
        }

        /// <summary>
        /// Тест метода получения экземпляра типа транспортного средства по заданному идентификатору.
        /// тип транспортного средства с передаваемым идентификатором существует
        /// </summary>
        /// <param name="id">Идентификатор типа транспортного средства</param>
        [Test, Order(1)]
        public void GetExistedDocumentkindTest([Random(0, DocumentkindsCount - 1, 5)] byte id)
        {
            // Arrange
            var expectedDocumentkind = _fakeDocumentkinds.Documentkinds.Find(f => f.DOCUMENTKINDID == id);

            // Act
            var actualDocumentkind = _documentkindService.GetDocumentkind(id);

            // Assert


            actualDocumentkind.Should().BeEquivalentTo(expectedDocumentkind,
                options => options.ExcludingMissingMembers());
        }

        /// <summary>
        /// Тест метода получения экземпляра типа транспортного средства по заданному идентификатору.
        /// тип транспортного средства с передаваемым идентификатором не существует
        /// </summary>
        /// <param name="id">Идентификатор типа транспортного средства</param>
        [Test, Order(1)]
        public void GetNotExistedDocumentkindTest([Random(DocumentkindsCount, DocumentkindsCount + 300, 5)]
                    byte id)
        {
            var actualDocumentkind = _documentkindService.GetDocumentkind(id);

            actualDocumentkind.Should().BeNull();
        }

        /// <summary>
        /// Тест метода добавления типа транспортного средства в хранилище данных
        /// </summary>
        [Test, Repeat(5), Order(2)]
        public void AddDocumentkindTest()
        {

            var documentkindId = (byte)15;
            
            // Arrange
            var documentkindsFake = _fakeDocumentkinds.Documentkinds;

            var documentkindFake = documentkindsFake.Where(c => c.DOCUMENTKINDID == documentkindId).FirstOrDefault();
            

            // Act

            var actualDocumentkind = _documentkindService.GetDocumentkind(documentkindId);

            // Assert

            actualDocumentkind.Should().BeEquivalentTo(actualDocumentkind,
                options => options.ExcludingMissingMembers());
        }

        /// <summary>
        /// Тест метода добавления типа транспортного средства в хранилище данных
        /// </summary>
        [Test, Repeat(5), Order(2)]
        public void AddDocumentkindIntegratedTest()
        {
            //IUnitOfWork _unitOfWork;

            //// Arrange
            //var documentkind = new FakeDocumentkind(1, false, false, false, false, 5, 5).Documentkinds[0];
            //var documentkindManagementService = new DocumentkindManagementService(_unitOfWork);
            //documentkind.Id = _fakeDocumentkinds.Documentkinds.Last().Id + 1;

            //// Act
            //var actualId = _documentkindService.AddDocumentkind(documentkind);

            //var actualDocumentkind = _documentkindService.GetDocumentkind(actualId);

            //// Assert
            //actualDocumentkind.DocumentkindDetail.EMailAddresses.Should().BeEquivalentTo(documentkind.DocumentkindDetail.EMailAddresses,
            //    options => options.ExcludingMissingMembers());
            //actualDocumentkind.Login.Should().BeEquivalentTo(documentkind.Login);
            //actualDocumentkind.FirstName.Should().BeEquivalentTo(documentkind.FirstName);
            //actualDocumentkind.LastName.Should().BeEquivalentTo(documentkind.LastName);
        }

        /// <summary>
        /// Тест метода обновления данных об типе транспортного средства
        /// </summary>
        /// <param name="id">Идентификатор типа транспортного средства для обновления</param>
        [Test, Order(2)]
        public void UpdateDocumentkindTest([Random(1, DocumentkindsCount, 5)] byte id)
        {
            // Arrange
            var documentkind = _documentkindService.GetDocumentkind(id);
            documentkind.NAME = new Bogus.Faker().Random.AlphaNumeric(17);

            // Act
            _documentkindService.UpdateDocumentkind(documentkind);
            var actualDocumentkind = _fakeDocumentkinds.Documentkinds.Find(f => f.DOCUMENTKINDID == id);

            // Assert
            actualDocumentkind.NAME.Should().Be(documentkind.NAME);
        }

        /// <summary>
        /// Тест метода удаления типа транспортного средства из хранилища данных
        /// </summary>
        /// <param name="id">Идентификатор типа транспортного средства для удаления</param>
        [Test, Order(3)]
        public void DeleteDocumentkindTest([Random(1, DocumentkindsCount, 5)] byte id)
        {
            // Arrange
            _documentkindService.DeleteDocumentkind(id);

            // Act
            var actualDocumentkind = _fakeDocumentkinds.Documentkinds.Find(documentkind => documentkind.DOCUMENTKINDID == id);

            // Assert
            actualDocumentkind.Should().BeNull();
        }

        /// <summary>
        /// Тест метода проверки наличия типа транспортного средства в хранилище данных.
        /// тип транспортного средства существует
        /// </summary>
        /// <param name="id">Идентификатор типа транспортного средства для поиска</param>
        [Test, Order(2)]
        public void DocumentkindExistsTest([Random(1, DocumentkindsCount, 5)] short id)
        {
            // Arrange
            var loginToFind = _fakeDocumentkinds.Documentkinds.Find(dbf => dbf.DOCUMENTKINDID == id).NAME;

            // Act
            var actual = _documentkindService
                .DocumentkindExists(f => f.NAME == loginToFind);

            // Assert
            actual.Should().BeTrue();
        }

        /// <summary>
        /// Тест метода проверки наличия типа транспортного средства в хранилище данных.
        /// тип транспортного средства не существует
        /// </summary>
        /// <param name="login">Логин типа транспортного средства для поиска</param>
        [Test, Order(2)]
        public void DocumentkindNotExistsTest([Values("shameonyou", "qwer", "", "befastandclearver", "strangelogin")]
                    string VIN)
        {
            var actual = _documentkindService
                .DocumentkindExists(f => f.NAME == VIN);

            actual.Should().BeFalse();
        }
    }
}

