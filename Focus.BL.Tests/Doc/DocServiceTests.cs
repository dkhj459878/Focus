﻿using FluentAssertions;
using Focus.BL.Common.Services;
using Focus.BL.Services;
using Focus.BL.Tests.FakeData;
using Focus.DAL.Common.Repositories;
using Focus.DAL.Common.UoW;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Focus.DAL.Common.DbModels;


namespace Focus.BL.Tests.Doc
{
    /// <summary>
    /// Unit-тесты для сервиса <see cref="DocService"/>
    /// </summary>
    [TestFixture]
    public class DocServiceTests
    {
        private const int DocsCount = 20;
        private IDocService _docService;
        private FakeDoc _fakeDocs;
        private Mock<IDbDocRepository> mockdocRepository;
        

        /// <summary>
        /// Осуществляет настройки
        /// </summary>
        [OneTimeSetUp]
        public void SetUp()
        {


            _fakeDocs = new FakeDoc(DocsCount);

            mockdocRepository = new Mock<IDbDocRepository>();
            mockdocRepository
                .Setup(mock => mock.Get(It.IsAny<Expression<Func<DOC, bool>>>(), null, It.IsAny<string>()))
                .Returns((Expression<Func<DOC, bool>> filter,
                    Func<IQueryable<DOC>, IOrderedQueryable<DOC>> orderBy,
                    string includeProperties) => _fakeDocs.Docs.Where(filter.Compile()));
            mockdocRepository
                .Setup(mock => mock.Get(null, null, It.IsAny<string>()))
                .Returns((Expression<Func<DOC, bool>> filter,
                    Func<IQueryable<DOC>, IOrderedQueryable<DOC>> orderBy,
                    string includeProperties) => _fakeDocs.Docs);
            mockdocRepository
                .Setup(mock => mock.GetPage(It.IsAny<int>(), It.IsAny<int>(), null, null, ""))
                .Returns((int pageSize,
                    int pageNumber,
                    Expression<Func<DOC, bool>> filter,
                    Func<IQueryable<DOC>, IOrderedQueryable<DOC>> orderBy,
                    string includeProperties) => _fakeDocs.Docs.Skip((pageNumber - 1) * pageSize).Take(pageSize));
            mockdocRepository.Setup(mock => mock.GetById(It.IsAny<short>()))
                .Returns((short id) => _fakeDocs.Docs.Find(f => f.DOCID == id));
            mockdocRepository.Setup(mock => mock.Insert(It.IsAny<DOC>()))
                .Callback((DOC f) => _fakeDocs.Docs.Add(f));
            mockdocRepository.Setup(mock => mock.Update(It.IsAny<DOC>()))
                .Callback((DOC f) => _fakeDocs.Docs.Find(d => d.DOCID == f.DOCID).CHIEFPHONENUMBER = f.CHIEFPHONENUMBER);
            mockdocRepository.Setup(mock => mock.Delete(It.IsAny<short>()))
                .Callback((short id) => _fakeDocs.Docs
                    .Remove(_fakeDocs.Docs.Find(f => f.DOCID == id)));

            var mockdocUnitOfWork = new Mock<IUnitOfWork>();
            mockdocUnitOfWork.Setup(mock => mock.DocRepository)
                .Returns(() => mockdocRepository.Object); //todo - закоментил, не знаю в чем дело

            _docService = new DocService(mockdocUnitOfWork.Object);
        }

        /// <summary>
        /// Тест метода получения экземпляров документов
        /// выбранной документовицы пейджинга. Выбрана может быть любая документовица.
        /// </summary>
        [Test, Order(1)]
        public void GetExistedDocsPageTest([Random(1, 5, 1)]int pageNumber)
        {
            const int pagesize = 3;

            // Arrange
            var docs = _fakeDocs.Docs;

            var docsPageIndexes = new List<int>();

            var startDocIndexInPage = pagesize * (pageNumber - 1);

            var finishDocIndexInPage = startDocIndexInPage + pagesize - 1;

            for (var i = startDocIndexInPage; i <= finishDocIndexInPage; i++)
            {
                docsPageIndexes.Add(i);
            }

            List<DOC> docsPage;
            docsPage = docsPageIndexes.Select(docsPageIndex => docs[docsPageIndex]).ToList();


            // Act

            var actualDocsPage = _docService.GetDocsPage(pageSize: pagesize, pageNumber: pageNumber).ToList();

            // Assert
            docsPage.Should().BeEquivalentTo(actualDocsPage,
                options => options.ExcludingMissingMembers());
        }

        /// <summary>
        /// Тест метода получения экземпляров документов
        /// выбранной документовицы пейджинга. Выбрана последняя документовица.
        /// </summary>
        [Test, Order(1)]
        public void GetExistedDocsLastPageTest()
        {
            const int pagesize = 19;
            const int pageNumber = 2;

            // Arrange
            var dbDocs = _fakeDocs.Docs;

            var docsPageIndexes = new List<int>();

            var startDocIndexInPage = pagesize * (pageNumber - 1);

            var finishDocIndexInPage = startDocIndexInPage + DocsCount % pagesize - 1;

            for (var i = startDocIndexInPage; i <= finishDocIndexInPage; i++)
            {
                docsPageIndexes.Add(i);
            }

            var dbDocsPage = docsPageIndexes.Select(docsPageIndex => dbDocs[docsPageIndex]).ToList();

            // Act
            var actualDocsPage = _docService.GetDocsPage(pageSize: pagesize, pageNumber: pageNumber).ToList();

            // Assert
            dbDocsPage.Should().BeEquivalentTo(actualDocsPage,
                options => options.ExcludingMissingMembers());
        }

        /// <summary>
        /// Тест метода получения экземпляров документов
        /// выбранной документовицы пейджинга. Номер документовицы отрицательный.
        /// </summary>
        [Test, Order(1)]
        public void GetNotExistedDocsPageWithNegativePageNumbersTest([Random(-100500, -1, 5)] int pageNumber)
        {
            const int pagesize = 16;

            // Act
            ICollection<DOC> actualDocsPage = _docService.GetDocsPage(pageSize: pagesize, pageNumber: pageNumber).ToList();

            ICollection<DOC> expectedPage = _docService.GetDocsPage(pageSize: pagesize, pageNumber: 1).ToList();

            // Assert
            actualDocsPage.Should().BeEquivalentTo(expectedPage,
                options => options.ExcludingMissingMembers());
        }

        /// <summary>
        /// Тест метода получения экземпляров документов
        /// выбранной документовицы пейджинга. Номер документовицы больше общего числа документовиц.
        /// </summary>
        [Test, Order(1)]
        public void GetNotExistedDocsPageWithValueIfPageNumbersMoreThanTotalDocsPagesTest([Random(DocsCount, DocsCount + 100500, 5)]int pageNumber)
        {
            const int pagesize = 16;

            // Act
            ICollection<DOC> actualDocsPage = _docService.GetDocsPage(pageSize: pagesize, pageNumber: pageNumber).ToList();

            // Assert
            actualDocsPage.Should().BeEmpty();
        }

        /// <summary>
        /// Тест метода получения экземпляров документов
        /// ввыбранной документовицы пейджинга из пустого репозитария.
        /// </summary>
        [Test, Order(5)]
        public void GetNotExistedDocsPageFromEmptyRepositoryTest([Random(1, 18, 5)]int pageNumber)
        {
            const int pagesize = 16;

            // Arrange
            _fakeDocs.Docs.Clear();

            // Act
            ICollection<DOC> actualDocsPage = _docService.GetDocsPage(pageSize: pagesize, pageNumber: pageNumber).ToList();

            // Assert
            actualDocsPage.Should().BeEmpty();
        }

        /// <summary>
        /// Тест метода получения всех экземпляров документов
        /// в пустом репозитарии.
        /// </summary>
        [Test, Order(5)]
        public void GetAllDocsFromEmptyRepositoryTest()
        {
            // Arrange
            _fakeDocs.Docs.Clear();

            // Act
            var actualDocs = _docService.GetAllDocs(filter: null);

            // Assert
            actualDocs.Should().BeEmpty();
        }

        /// <summary>
        /// Тест метода получения всех экземпляров документов
        /// в не пустом репозитарии.
        /// </summary>
        [Test, Order(1)]
        public void GetAllExistedDocsTest()
        {
            // Arrange
            var expectedDocs = _fakeDocs.Docs;

            // Act
            var actualDocs = _docService.GetAllDocs().ToList();

            // Assert
            //foreach (var expectedDoc in expectedDocs)
            //{
            //    expectedDoc.DOCID.Should().Be((actualDocs.Find(c => c.DOCID == expectedDoc.DOCID).DOCID));
            //    expectedDoc.CHIEFPHONENUMBER.Should().Be((actualDocs.Find(c => c.DOCID == expectedDoc.DOCID).CHIEFPHONENUMBER));
            //}

            expectedDocs.Should().BeEquivalentTo(actualDocs,
                options => options.ExcludingMissingMembers());

            expectedDocs.Count.Should().Be(actualDocs.Count);
        }

        /// <summary>
        /// Тест метода получения экземпляра документы по заданному идентификатору.
        /// документ с передаваемым идентификатором существует
        /// </summary>
        /// <param name="id">Идентификатор документы</param>
        [Test, Order(1)]
        public void GetExistedDocTest()
        {
            // Arrange
            var docId = _fakeDocs.Docs.ToArray()[(new Random()).Next(DocsCount)].DOCID;


            var expectedDoc = _fakeDocs.Docs.Find(f => f.DOCID == docId);

            // Act
            var actualDoc = _docService.GetDoc(docId);

            // Assert


            actualDoc.Should().BeEquivalentTo(expectedDoc,
                options => options.ExcludingMissingMembers());
        }

        /// <summary>
        /// Тест метода получения экземпляра документы по заданному идентификатору.
        /// Контейнер с передаваемым идентификатором не существует.
        /// </summary>
        /// <param name="id">Идентификатор документы</param>
        [Test, Order(1)]
        public void GetNotExistedDocTest([Random(DocsCount, DocsCount + DocsCount, 5)]
                    short id)
        {
            var actualDoc = _docService.GetDoc(id);

            actualDoc.Should().BeNull();
        }

        /// <summary>
        /// Тест метода добавления документы в хранилище данных
        /// </summary>
        [Test, Repeat(5), Order(2)]
        public void AddDocTest()
        {

            var docId = _fakeDocs.Docs.ToArray()[(new Random()).Next(DocsCount)].DOCID;
                        
            // Arrange
            var docsFake = _fakeDocs.Docs;

            var docFake = docsFake.Where(c => c.DOCID == docId).FirstOrDefault();
            

            // Act

            var actualDoc = _docService.GetDoc(docId);

            // Assert

            actualDoc.Should().BeEquivalentTo(actualDoc,
                options => options.ExcludingMissingMembers());
        }
       

        /// <summary>
        /// Тест метода обновления данных о документе
        /// </summary>
        /// <param name="id">Идентификатор документы для обновления</param>
        //[Test, Order(2)]
        //public void UpdateDocTest()
        //{
        //    // Arrange
        //    var docId = _fakeDocs.Docs.ToArray()[(new Random()).Next(DocsCount)].DOCID;

        //    var doc = _docService.GetDoc(docId);
        //    doc.CHIEFPHONENUMBER = new Bogus.Faker().Random.AlphaNumeric(17);

        //    // Act
        //    _docService.UpdateDoc(doc);
        //    var actualDoc = _fakeDocs.Docs.Find(f => f.DOCID == docId);

        //    // Assert
        //    actualDoc.CHIEFPHONENUMBER.Should().Be(doc.CHIEFPHONENUMBER);
        //}

        /// <summary>
        /// Тест метода удаления документы из хранилища данных
        /// </summary>
        /// <param name="id">Идентификатор документы для удаления</param>
        [Test, Order(3)]
        public void DeleteDocTest()
        {
            // Arrange

            var docId = _fakeDocs.Docs.ToArray()[(new Random()).Next(DocsCount)].DOCID;

            _docService.DeleteDoc(docId);

            // Act
            var actualDoc = _fakeDocs.Docs.Find(doc => doc.DOCID == docId);

            // Assert
            actualDoc.Should().BeNull();
        }

        /// <summary>
        /// Тест метода проверки наличия документы в хранилище данных.
        /// документ существует
        /// </summary>
        /// <param name="id">Идентификатор документы для поиска</param>
        [Test, Order(2)]
        public void DocExistsTest()
        {
            // Arrange

            var docId = _fakeDocs.Docs.ToArray()[(new Random()).Next(DocsCount)].DOCID;

            var loginToFind = _fakeDocs.Docs.Find(dbf => dbf.DOCID == docId).CHIEFPHONENUMBER;

            // Act
            var actual = _docService
                .DocExists(f => f.CHIEFPHONENUMBER == loginToFind);

            // Assert
            actual.Should().BeTrue();
        }

        /// <summary>
        /// Тест метода проверки наличия документы в хранилище данных.
        /// документ не существует
        /// </summary>
        /// <param name="login">Логин документы для поиска</param>
        [Test, Order(2)]
        public void DocNotExistsTest([Values("shameonyou", "qwer", "", "befastandclearver", "strangelogin")]
                    string VIN)
        {
            var actual = _docService
                .DocExists(f => f.CHIEFPHONENUMBER == VIN);

            actual.Should().BeFalse();
        }

        //[Test, Order(3)]
        //public void DocNotExistsTest([Values("shameonyou", "qwer", "", "befastandclearver", "strangelogin")]
        //            string VIN)
        //{
        //    var actual = _docService
        //        .DocExists(f => f.CHIEFPHONENUMBER == VIN);

        //    actual.Should().BeFalse();
        //}
    }
}

