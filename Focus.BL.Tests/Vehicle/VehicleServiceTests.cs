﻿using FluentAssertions;
using Focus.BL.Common.Services;
using Focus.BL.Services;
using Focus.BL.Tests.FakeData;
using Focus.DAL.Common.DbModels;
using Focus.DAL.Common.Repositories;
using Focus.DAL.Common.UoW;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Focus.BL.Tests.Vehicle
{
    /// <summary>
    /// Unit-тесты для сервиса <see cref="VehicleService"/>
    /// </summary>
    [TestFixture]
    public class VehicleServiceTests
    {
        private const int VehiclesCount = 300;
        private IVehicleService _vehicleService;
        private FakeVehicle _fakeVehicles;
        private Mock<IDbVehicleRepository> mockvehicleRepository;

        /// <summary>
        /// Осуществляет настройки
        /// </summary>
        [OneTimeSetUp]
        public void SetUp()
        {


            _fakeVehicles = new FakeVehicle(VehiclesCount);

            mockvehicleRepository = new Mock<IDbVehicleRepository>();
            mockvehicleRepository
                .Setup(mock => mock.Get(It.IsAny<Expression<Func<VEHICLE, bool>>>(), null, It.IsAny<string>()))
                .Returns((Expression<Func<VEHICLE, bool>> filter,
                    Func<IQueryable<VEHICLE>, IOrderedQueryable<VEHICLE>> orderBy,
                    string includeProperties) => _fakeVehicles.Vehicles.Where(filter.Compile()));
            mockvehicleRepository
                .Setup(mock => mock.Get(null, null, It.IsAny<string>()))
                .Returns((Expression<Func<VEHICLE, bool>> filter,
                    Func<IQueryable<VEHICLE>, IOrderedQueryable<VEHICLE>> orderBy,
                    string includeProperties) => _fakeVehicles.Vehicles);
            mockvehicleRepository
                .Setup(mock => mock.GetPage(It.IsAny<int>(), It.IsAny<int>(), null, null, ""))
                .Returns((int pageSize,
                    int pageNumber,
                    Expression<Func<VEHICLE, bool>> filter,
                    Func<IQueryable<VEHICLE>, IOrderedQueryable<VEHICLE>> orderBy,
                    string includeProperties) => _fakeVehicles.Vehicles.Skip((pageNumber - 1) * pageSize).Take(pageSize));
            mockvehicleRepository.Setup(mock => mock.GetById(It.IsAny<short>()))
                .Returns((short id) => _fakeVehicles.Vehicles.Find(f => f.VEHICLEID == id));
            mockvehicleRepository.Setup(mock => mock.Insert(It.IsAny<VEHICLE>()))
                .Callback((VEHICLE f) => _fakeVehicles.Vehicles.Add(f));
            mockvehicleRepository.Setup(mock => mock.Update(It.IsAny<VEHICLE>()))
                .Callback((VEHICLE f) => _fakeVehicles.Vehicles.Find(d => d.VEHICLEID == f.VEHICLEID).REGNUMBER = f.REGNUMBER);
            mockvehicleRepository.Setup(mock => mock.Delete(It.IsAny<short>()))
                .Callback((short id) => _fakeVehicles.Vehicles
                    .Remove(_fakeVehicles.Vehicles.Find(f => f.VEHICLEID == id)));

            var mockvehicleUnitOfWork = new Mock<IUnitOfWork>();
            mockvehicleUnitOfWork.Setup(mock => mock.VehicleRepository)
                .Returns(() => mockvehicleRepository.Object); //todo - закоментил, не знаю в чем дело

            _vehicleService = new VehicleService(mockvehicleUnitOfWork.Object);
        }

        /// <summary>
        /// Тест метода получения экземпляров автомобилей
        /// выбранной страницы пейджинга. Выбрана может быть любая страница.
        /// </summary>
        [Test, Order(1)]
        public void GetExistedVehiclesPageTest([Random(1, 14, 1)]int pageNumber)
        {
            const int pagesize = 16;

            // Arrange
            var vehicles = _fakeVehicles.Vehicles;

            var vehiclesPageIndexes = new List<int>();

            var startVehicleIndexInPage = pagesize * (pageNumber - 1);

            var finishVehicleIndexInPage = startVehicleIndexInPage + pagesize - 1;

            for (var i = startVehicleIndexInPage; i <= finishVehicleIndexInPage; i++)
            {
                vehiclesPageIndexes.Add(i);
            }

            List<VEHICLE> vehiclesPage;
            vehiclesPage = vehiclesPageIndexes.Select(vehiclesPageIndex => vehicles[vehiclesPageIndex]).ToList();


            // Act

            var actualVehiclesPage = _vehicleService.GetVehiclesPage(pageSize: pagesize, pageNumber: pageNumber).ToList();

            // Assert
            vehiclesPage.Should().BeEquivalentTo(actualVehiclesPage,
                options => options.ExcludingMissingMembers());
        }

        /// <summary>
        /// Тест метода получения экземпляров автомобилей
        /// выбранной страницы пейджинга. Выбрана последняя страница.
        /// </summary>
        [Test, Order(1)]
        public void GetExistedVehiclesLastPageTest()
        {
            const int pagesize = 16;
            const int pageNumber = 19;

            // Arrange
            var dbVehicles = _fakeVehicles.Vehicles;

            var vehiclesPageIndexes = new List<int>();

            var startVehicleIndexInPage = pagesize * (pageNumber - 1);

            var finishVehicleIndexInPage = startVehicleIndexInPage + VehiclesCount % pagesize - 1;

            for (var i = startVehicleIndexInPage; i <= finishVehicleIndexInPage; i++)
            {
                vehiclesPageIndexes.Add(i);
            }

            var dbVehiclesPage = vehiclesPageIndexes.Select(vehiclesPageIndex => dbVehicles[vehiclesPageIndex]).ToList();

            // Act
            var actualVehiclesPage = _vehicleService.GetVehiclesPage(pageSize: pagesize, pageNumber: pageNumber).ToList();

            // Assert
            dbVehiclesPage.Should().BeEquivalentTo(actualVehiclesPage,
                options => options.ExcludingMissingMembers());
        }

        /// <summary>
        /// Тест метода получения экземпляров автомобилей
        /// выбранной страницы пейджинга. Номер страницы отрицательный.
        /// </summary>
        [Test, Order(1)]
        public void GetNotExistedVehiclesPageWithNegativePageNumbersTest([Random(-100500, -1, 5)] int pageNumber)
        {
            const int pagesize = 16;

            // Act
            ICollection<VEHICLE> actualVehiclesPage = _vehicleService.GetVehiclesPage(pageSize: pagesize, pageNumber: pageNumber).ToList();

            ICollection<VEHICLE> expectedPage = _vehicleService.GetVehiclesPage(pageSize: pagesize, pageNumber: 1).ToList();

            // Assert
            actualVehiclesPage.Should().BeEquivalentTo(expectedPage,
                options => options.ExcludingMissingMembers());
        }

        /// <summary>
        /// Тест метода получения экземпляров автомобилей
        /// выбранной страницы пейджинга. Номер страницы больше общего числа страниц.
        /// </summary>
        [Test, Order(1)]
        public void GetNotExistedVehiclesPageWithValueIfPageNumbersMoreThanTotalVehiclesPagesTest([Random(VehiclesCount, VehiclesCount + 100500, 5)]int pageNumber)
        {
            const int pagesize = 16;

            // Act
            ICollection<VEHICLE> actualVehiclesPage = _vehicleService.GetVehiclesPage(pageSize: pagesize, pageNumber: pageNumber).ToList();

            // Assert
            actualVehiclesPage.Should().BeEmpty();
        }

        /// <summary>
        /// Тест метода получения экземпляров автомобилей
        /// ввыбранной страницы пейджинга из пустого репозитария.
        /// </summary>
        [Test, Order(5)]
        public void GetNotExistedVehiclesPageFromEmptyRepositoryTest([Random(1, 18, 5)]int pageNumber)
        {
            const int pagesize = 16;

            // Arrange
            _fakeVehicles.Vehicles.Clear();

            // Act
            ICollection<VEHICLE> actualVehiclesPage = _vehicleService.GetVehiclesPage(pageSize: pagesize, pageNumber: pageNumber).ToList();

            // Assert
            actualVehiclesPage.Should().BeEmpty();
        }

        /// <summary>
        /// Тест метода получения всех экземпляров автомобилей
        /// в пустом репозитарии.
        /// </summary>
        [Test, Order(5)]
        public void GetAllVehiclesFromEmptyRepositoryTest()
        {
            // Arrange
            _fakeVehicles.Vehicles.Clear();

            // Act
            var actualVehicles = _vehicleService.GetAllVehicles(filter: null);

            // Assert
            actualVehicles.Should().BeEmpty();
        }

        /// <summary>
        /// Тест метода получения всех экземпляров автомобилей
        /// в не пустом репозитарии.
        /// </summary>
        [Test, Order(1)]
        public void GetAllExistedVehiclesTest()
        {
            // Arrange
            var expectedVehicles = _fakeVehicles.Vehicles;

            // Act
            var actualVehicles = _vehicleService.GetAllVehicles().ToList();

            // Assert
            foreach (var expectedVehicle in expectedVehicles)
            {
                var i = expectedVehicle.VEHICLEID;

                expectedVehicles[i].COUNTRYCODE.Should().Be(actualVehicles[i].COUNTRYCODE);
                expectedVehicles[i].REGNUMBER.Should().Be(actualVehicles[i].REGNUMBER);
            }

            expectedVehicles.Should().BeEquivalentTo(actualVehicles,
                options => options.ExcludingMissingMembers());

            expectedVehicles.Count.Should().Be(actualVehicles.Count);
        }

        /// <summary>
        /// Тест метода получения экземпляра автомобиля по заданному идентификатору.
        /// автомобиль с передаваемым идентификатором существует
        /// </summary>
        /// <param name="id">Идентификатор автомобиля</param>
        [Test, Order(1)]
        public void GetExistedVehicleTest([Random(0, VehiclesCount - 1, 5)] short id)
        {
            // Arrange
            var expectedVehicle = _fakeVehicles.Vehicles.Find(f => f.VEHICLEID == id);

            // Act
            var actualVehicle = _vehicleService.GetVehicle(id);

            // Assert


            actualVehicle.Should().BeEquivalentTo(expectedVehicle,
                options => options.ExcludingMissingMembers());
        }

        /// <summary>
        /// Тест метода получения экземпляра автомобиля по заданному идентификатору.
        /// автомобиль с передаваемым идентификатором не существует
        /// </summary>
        /// <param name="id">Идентификатор автомобиля</param>
        [Test, Order(1)]
        public void GetNotExistedVehicleTest([Random(VehiclesCount, VehiclesCount + 300, 5)]
                    short id)
        {
            var actualVehicle = _vehicleService.GetVehicle(id);

            actualVehicle.Should().BeNull();
        }

        /// <summary>
        /// Тест метода добавления автомобиля в хранилище данных
        /// </summary>
        [Test, Repeat(5), Order(2)]
        public void AddVehicleTest()
        {

            short vehicleId = 15;
            
            // Arrange
            var vehiclesFake = _fakeVehicles.Vehicles;

            var vehicleFake = vehiclesFake.Where(c => c.VEHICLEID == vehicleId).FirstOrDefault();
            

            // Act

            var actualVehicle = _vehicleService.GetVehicle(vehicleId);

            // Assert

            actualVehicle.Should().BeEquivalentTo(actualVehicle,
                options => options.ExcludingMissingMembers());
        }

        /// <summary>
        /// Тест метода добавления автомобиля в хранилище данных
        /// </summary>
        [Test, Repeat(5), Order(2)]
        public void AddVehicleIntegratedTest()
        {
            //IUnitOfWork _unitOfWork;

            //// Arrange
            //var vehicle = new FakeVehicles(1, false, false, false, false, 5, 5).Vehicles[0];
            //var vehicleManagementService = new VehicleManagementService(_unitOfWork);
            //vehicle.Id = _fakeVehicles.Vehicles.Last().Id + 1;

            //// Act
            //var actualId = _vehicleService.AddVehicle(vehicle);

            //var actualVehicle = _vehicleService.GetVehicle(actualId);

            //// Assert
            //actualVehicle.VehicleDetail.EMailAddresses.Should().BeEquivalentTo(vehicle.VehicleDetail.EMailAddresses,
            //    options => options.ExcludingMissingMembers());
            //actualVehicle.Login.Should().BeEquivalentTo(vehicle.Login);
            //actualVehicle.FirstName.Should().BeEquivalentTo(vehicle.FirstName);
            //actualVehicle.LastName.Should().BeEquivalentTo(vehicle.LastName);
        }

        /// <summary>
        /// Тест метода обновления данных об автомобиле
        /// </summary>
        /// <param name="id">Идентификатор автомобиля для обновления</param>
        [Test, Order(2)]
        public void UpdateVehicleTest([Random(0, VehiclesCount - 1, 5)] short id)
        {
            // Arrange
            var vehicle = _vehicleService.GetVehicle(id);
            vehicle.REGNUMBER = new Bogus.Faker().Random.AlphaNumeric(17);

            // Act
            _vehicleService.UpdateVehicle(vehicle);
            var actualVehicle = _fakeVehicles.Vehicles.Find(f => f.VEHICLEID == id);

            // Assert
            actualVehicle.REGNUMBER.Should().Be(vehicle.REGNUMBER);
        }

        /// <summary>
        /// Тест метода удаления автомобиля из хранилища данных
        /// </summary>
        /// <param name="id">Идентификатор автомобиля для удаления</param>
        [Test, Order(3)]
        public void DeleteVehicleTest([Random(0, VehiclesCount - 1, 5)] short id)
        {
            // Arrange
            _vehicleService.DeleteVehicle(id);

            // Act
            var actualVehicle = _fakeVehicles.Vehicles.Find(vehicle => vehicle.VEHICLEID == id);

            // Assert
            actualVehicle.Should().BeNull();
        }

        /// <summary>
        /// Тест метода проверки наличия автомобиля в хранилище данных.
        /// автомобиль существует
        /// </summary>
        /// <param name="id">Идентификатор автомобиля для поиска</param>
        [Test, Order(2)]
        public void VehicleExistsTest([Random(0, VehiclesCount - 1, 5)] short id)
        {
            // Arrange
            var loginToFind = _fakeVehicles.Vehicles.Find(dbf => dbf.VEHICLEID == id).REGNUMBER;

            // Act
            var actual = _vehicleService
                .VehicleExists(f => f.REGNUMBER == loginToFind);

            // Assert
            actual.Should().BeTrue();
        }

        /// <summary>
        /// Тест метода проверки наличия автомобиля в хранилище данных.
        /// автомобиль не существует
        /// </summary>
        /// <param name="login">Логин автомобиля для поиска</param>
        [Test, Order(2)]
        public void VehicleNotExistsTest([Values("shameonyou", "qwer", "", "befastandclearver", "strangelogin")]
                    string VIN)
        {
            var actual = _vehicleService
                .VehicleExists(f => f.REGNUMBER == VIN);

            actual.Should().BeFalse();
        }
    }
}

