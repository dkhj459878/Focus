﻿using Focus.BL.Services;
using Focus.DAL.Common.DbModels;
using Focus.DAL.Common.Extensions;
using NUnit.Framework;
using System;
using System.Collections.Generic;

namespace Focus.BL.Tests.Audit
{
    /// <summary>
    /// Unit-тесты для экстенш методов класса Audit <see cref="CarService"/>
    /// </summary>
    [TestFixture]
    public class AuditTests
    {
        private class _someClass
        {
            public int intValue { get; set; }

            public long longValue { get; set; }

            public string stringValue { get; set; }

            public short shortValue { get; set; }

            public double doubleValue { get; set; }

            public float floatValue { get; set; }

            public decimal decimalValue { get; set; }

            public byte byteValue { get; set; }

            public bool boolValue { get; set; }

            public IEnumerable<string> stringCollection { get; set; }

            public class NestedClass
            {
                public int intValueOfNestedClass { get; set; }

                public string intstringValueOfNestedClass { get; set; }
            }
        }

        #region Instantiation of being tested classes.

        //Определяю классы с атрибутом Audited.
        [Audited]
        private class _someClassWithAuditedAttributeOne
        {
            public int MyProperty1 { get; set; }

            public int MyProperty2 { get; set; }

            public int MyProperty3 { get; set; }

            public int MyProperty4 { get; set; }

            public int MyProperty5 { get; set; }

            public int MyProperty6 { get; set; }

            public int MyProperty7 { get; set; }

            public int MyProperty8 { get; set; }
        }

        [Audited]
        private class _someClassWithAuditedAttributeTwo
        {
            public int MyProperty1 { get; set; }

            public int MyProperty2 { get; set; }

            public int MyProperty3 { get; set; }

            public int MyProperty4 { get; set; }

            public int MyProperty5 { get; set; }

            public int MyProperty6 { get; set; }

            public int MyProperty7 { get; set; }

            public int MyProperty8 { get; set; }
        }

        [Audited]
        private class _someClassWithAuditedAttributeThree
        {
            public int MyProperty1 { get; set; }

            public int MyProperty2 { get; set; }

            public int MyProperty3 { get; set; }

            public int MyProperty4 { get; set; }

            public int MyProperty5 { get; set; }

            public int MyProperty6 { get; set; }

            public int MyProperty7 { get; set; }

            public int MyProperty8 { get; set; }
        }

        [Audited]
        private class _someClassWithAuditedAttributeFour
        {
            public int MyProperty1 { get; set; }

            public int MyProperty2 { get; set; }

            public int MyProperty3 { get; set; }

            public int MyProperty4 { get; set; }

            public int MyProperty5 { get; set; }

            public int MyProperty6 { get; set; }

            public int MyProperty7 { get; set; }

            public int MyProperty8 { get; set; }
        }

        //Классы без аттрибута Audited.
        private class _someClassWithOutAuditedAttributeOne
        {
            public int MyProperty1 { get; set; }

            public int MyProperty2 { get; set; }

            public int MyProperty3 { get; set; }

            public int MyProperty4 { get; set; }

            public int MyProperty5 { get; set; }

            public int MyProperty6 { get; set; }

            public int MyProperty7 { get; set; }

            public int MyProperty8 { get; set; }
        }

        private class _someClassWithOutAuditedAttributeTwo
        {
            public int MyProperty1 { get; set; }

            public int MyProperty2 { get; set; }

            public int MyProperty3 { get; set; }

            public int MyProperty4 { get; set; }

            public int MyProperty5 { get; set; }

            public int MyProperty6 { get; set; }

            public int MyProperty7 { get; set; }

            public int MyProperty8 { get; set; }
        }

        private class _someClassWithOutAuditedAttributeThree
        {
            public int MyProperty1 { get; set; }

            public int MyProperty2 { get; set; }

            public int MyProperty3 { get; set; }

            public int MyProperty4 { get; set; }

            public int MyProperty5 { get; set; }

            public int MyProperty6 { get; set; }

            public int MyProperty7 { get; set; }

            public int MyProperty8 { get; set; }
        }

        private class _someClassWithOutAuditedAttributeFour
        {
            public int MyProperty1 { get; set; }

            public int MyProperty2 { get; set; }

            public int MyProperty3 { get; set; }

            public int MyProperty4 { get; set; }

            public int MyProperty5 { get; set; }

            public int MyProperty6 { get; set; }

            public int MyProperty7 { get; set; }

            public int MyProperty8 { get; set; }
        }

        private readonly List<object> _someClasses = new List<object>() {

            new _someClassWithAuditedAttributeOne(),

            new _someClassWithAuditedAttributeTwo(),

            new _someClassWithAuditedAttributeThree(),

            new _someClassWithAuditedAttributeFour(),

            new _someClassWithOutAuditedAttributeOne(),

            new _someClassWithOutAuditedAttributeTwo(),

            new _someClassWithOutAuditedAttributeThree(),

            new _someClassWithOutAuditedAttributeFour()
        };
        #endregion

        /// <summary>
        /// Осуществляет настройки
        /// </summary>
        [OneTimeSetUp]
        public void SetUp()
        {
            object o = new object();

            Type type = o.GetType();
        }

        #region CheckNull() test group.

        /// <summary>
        /// Тест метода расширения проверки объекта на null.
        /// </summary>
        /// 
        [Test, Order(1)]
        public void CheckNullFalseTest()
        {
            // Arrange
            var obj = new { Name = "Name", Age = 15 };
            bool result;

            // Act
            result = obj.CheckNull();

            // Assert
            Assert.IsFalse(result);
        }

        /// <summary>
        /// Тест метода расширения проверки объекта на null.
        /// </summary>
        [Test, Order(2)]
        public void CheckNullTrueTest()
        {
            // Arrange
            object obj = null;
            bool result;

            // Act
            result = obj.CheckNull();

            // Assert
            Assert.IsTrue(result);
        }

        /// Тест метода расширения проверки объекта на null.
        /// </summary>
        [Test, Order(3)]
        public void CheckNullStringIsNullTest()
        {
            // Arrange
            string str = null;
            bool result;

            // Act
            result = str.CheckNull();

            // Assert
            Assert.IsTrue(result);
        }

        /// Тест метода расширения проверки объекта на null.
        /// </summary>
        [Test, Order(4)]
        public void CheckNullStringIsNotNullTest()
        {
            // Arrange
            string str = "Test";
            bool result;

            // Act
            result = str.CheckNull();

            // Assert
            Assert.IsFalse(result);
        }

        /// Тест метода расширения проверки объекта на null.
        /// </summary>
        [Test, Order(5)]
        public void CheckIntIsNotNullTest()
        {
            // Arrange
            int number = 5;
            bool result;

            // Act
            result = number.CheckNull();

            // Assert
            Assert.IsFalse(result);
        }

        /// Тест метода расширения проверки объекта на null.
        /// </summary>
        [Test, Order(5)]
        public void CheckIntNullableIsNullTest()
        {
            // Arrange
            int? number = null;
            bool result;

            // Act
            result = number.CheckNull();

            // Assert
            Assert.IsTrue(result);
        }
        #endregion

        #region GetTypeName test group.
        /// <summary>
        /// Тест метода расширения получения наименования типа объекта.
        /// </summary>
        [Test, Order(1)]
        public void GetTypeNameExtensionMethodTest()
        {
            // Arrange
            _someClassWithAuditedAttributeOne _SomeClassWithAuditedAttributeOne = new _someClassWithAuditedAttributeOne();
            string expectedSomeClassWithAuditedAttributeOneTypeName = "_someClassWithAuditedAttributeOne";
            int integerNumber = 0;
            string expectedIntegerNumberTypeName = "Int32";
            int? integerNumberNullable = 0;
            string expectedIntegerNullableTypeName = "Int32";
            string str = string.Empty;
            string expectedStringTypeName = "String";
            DOC doc = new DOC();
            string expectedDOCTypeName = "DOC";
            long longNumber = 5;
            string expectedLongNumberTypeName = "Int64";
            object obj = new object();
            string expectedOblectTypeName = "Object";

            // Act
            string actualSomeClassWithAuditedAttributeOneTypeName = _SomeClassWithAuditedAttributeOne.GetTypeName();
            string actualIntegerNumberTypeName = integerNumber.GetTypeName();
            string actualIntegerNullableTypeName = integerNumberNullable.GetTypeName();
            string actualStringTypeName = str.GetTypeName();
            string actualDOCTypeName = doc.GetTypeName();
            string actualLongNumberTypeName = longNumber.GetTypeName();
            string actualOblectTypeName = obj.GetTypeName();

            // Assert
            Assert.AreEqual(expectedSomeClassWithAuditedAttributeOneTypeName, actualSomeClassWithAuditedAttributeOneTypeName);
            Assert.AreEqual(expectedIntegerNumberTypeName, actualIntegerNumberTypeName);
            Assert.AreEqual(expectedIntegerNullableTypeName, actualIntegerNullableTypeName);
            Assert.AreEqual(expectedStringTypeName, actualStringTypeName);
            Assert.AreEqual(expectedDOCTypeName, actualDOCTypeName);
            Assert.AreEqual(expectedLongNumberTypeName, actualLongNumberTypeName);
            Assert.AreEqual(expectedOblectTypeName, actualOblectTypeName);
        }
        #endregion

        #region HasAttribute() test group.

        /// <summary>
        /// Тест метода расширения проверка наличия аттрибута.
        /// </summary>
        [Test, Order(1)]
        public void HasAttributeExtensionMethodTrueTest()
        {
            // Arrange
            _someClassWithAuditedAttributeOne _SomeClassWithAuditedAttributeOne = new _someClassWithAuditedAttributeOne();

            // Act
            string attributeName = "Audited";
            string attributeNameLong = "AuditedAttribute";
            bool actualWithOutAttributePartWord = _SomeClassWithAuditedAttributeOne.HasAttribute(attributeName);
            bool actualWithAttributePartWord = _SomeClassWithAuditedAttributeOne.HasAttribute(attributeNameLong);

            // Assert
            Assert.IsTrue(actualWithOutAttributePartWord);
            Assert.IsTrue(actualWithAttributePartWord);
        }

        /// <summary>
        /// Тест метода расширения проверка наличия аттрибута.
        /// </summary>
        [Test, Order(1)]
        public void HasAttributeExtensionMethodFalseTest()
        {
            // Arrange
            _someClassWithOutAuditedAttributeOne _SomeClassWithOutAuditedAttributeOne = new _someClassWithOutAuditedAttributeOne();

            // Act
            string attributeName = "Audited";
            string attributeNameLong = "AuditedAttribute";
            bool actualWithOutAttributePartWord = _SomeClassWithOutAuditedAttributeOne.HasAttribute(attributeName);
            bool actualWithAttributePartWord = _SomeClassWithOutAuditedAttributeOne.HasAttribute(attributeNameLong);

            // Assert
            Assert.IsFalse(actualWithOutAttributePartWord);
            Assert.IsFalse(actualWithAttributePartWord);
        }
        #endregion

        #region GetContent() test group.
        #endregion
    }
}