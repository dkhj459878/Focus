﻿using Focus.BL.Services;
using NUnit.Framework;
using System;

namespace Focus.BL.Tests.ParametersProcessing
{
    /// <summary>
    /// Unit-тесты для сервиса <see cref="CarService"/>
    /// </summary>
    [TestFixture]
    public class AppParametersConfigService
    {
        //private const int CarsCount = 300;
        //private ICarService _carService;
        //private FakeCar _fakeCars;
        //private Mock<IDbCarRepository> mockcarRepository;

        /// <summary>
        /// Осуществляет настройки
        /// </summary>
        [OneTimeSetUp]
        public void SetUp()
        {
            //_fakeCars = new FakeCar(CarsCount);

            //mockcarRepository = new Mock<IDbCarRepository>();
            //mockcarRepository
            //    .Setup(mock => mock.Get(It.IsAny<Expression<Func<CAR, bool>>>(), null, It.IsAny<string>()))
            //    .Returns((Expression<Func<CAR, bool>> filter,
            //        Func<IQueryable<CAR>, IOrderedQueryable<CAR>> orderBy,
            //        string includeProperties) => _fakeCars.Cars.Where(filter.Compile()));
            //mockcarRepository
            //    .Setup(mock => mock.Get(null, null, It.IsAny<string>()))
            //    .Returns((Expression<Func<CAR, bool>> filter,
            //        Func<IQueryable<CAR>, IOrderedQueryable<CAR>> orderBy,
            //        string includeProperties) => _fakeCars.Cars);
            //mockcarRepository
            //    .Setup(mock => mock.GetPage(It.IsAny<int>(), It.IsAny<int>(), null, null, ""))
            //    .Returns((int pageSize,
            //        int pageNumber,
            //        Expression<Func<CAR, bool>> filter,
            //        Func<IQueryable<CAR>, IOrderedQueryable<CAR>> orderBy,
            //        string includeProperties) => _fakeCars.Cars.Skip((pageNumber - 1) * pageSize).Take(pageSize));
            //mockcarRepository.Setup(mock => mock.GetById(It.IsAny<int>()))
            //    .Returns((short id) => _fakeCars.Cars.Find(f => f.CARID == id));
            //mockcarRepository.Setup(mock => mock.Insert(It.IsAny<CAR>()))
            //    .Callback((CAR f) => _fakeCars.Cars.Add(f));
            //mockcarRepository.Setup(mock => mock.Update(It.IsAny<CAR>()))
            //    .Callback((CAR f) => _fakeCars.Cars.Find(D => D.CARID == f.CARID).VIN = f.VIN);
            //mockcarRepository.Setup(mock => mock.Delete(It.IsAny<int>()))
            //    .Callback((short id) => _fakeCars.Cars
            //        .Remove(_fakeCars.Cars.Find(f => f.CARID == id)));

            //var mockcarUnitOfWork = new Mock<IUnitOfWork>();
            //mockcarUnitOfWork.Setup(mock => mock.CarRepository)
            //    .Returns(() => mockcarRepository.Object); //todo - закоментил, не знаю в чем дело

            //_carService = new CarService(mockcarUnitOfWork.Object);
        }

        /// <summary>
        /// Тест метода получения экземпляров автомобилей
        /// выбранной страницы пейджинга. Выбрана может быть любая страница.
        /// </summary>
        [Test, Order(1)]
        public void GetExistedCarsPageTest([Random(1, 14, 1)]int pageNumber)
        {
            //const int pagesize = 16;

            //// Arrange
            //var cars = _fakeCars.Cars;

            //var carsPageIndexes = new List<int>();

            //var startCarIndexInPage = pagesize * (pageNumber - 1);

            //var finishCarIndexInPage = startCarIndexInPage + pagesize - 1;

            //for (var i = startCarIndexInPage; i <= finishCarIndexInPage; i++)
            //{
            //    carsPageIndexes.Add(i);
            //}

            //List<CAR> carsPage;
            //carsPage = carsPageIndexes.Select(carsPageIndex => cars[carsPageIndex]).ToList();


            //// Act

            //var actualCarsPage = _carService.GetCarsPage(pageSize: pagesize, pageNumber: pageNumber).ToList();

            //// Assert
            //carsPage.Should().BeEquivalentTo(actualCarsPage,
            //    options => options.ExcludingMissingMembers());
        }

        /// <summary>
        /// Тест метода получения экземпляров автомобилей
        /// выбранной страницы пейджинга. Выбрана последняя страница.
        /// </summary>
        [Test, Order(1)]
        public void GetExistedCarsLastPageTest()
        {
            //const int pagesize = 16;
            //const int pageNumber = 19;

            //// Arrange
            //var dbCars = _fakeCars.Cars;

            //var carsPageIndexes = new List<int>();

            //var startCarIndexInPage = pagesize * (pageNumber - 1);

            //var finishCarIndexInPage = startCarIndexInPage + CarsCount % pagesize - 1;

            //for (var i = startCarIndexInPage; i <= finishCarIndexInPage; i++)
            //{
            //    carsPageIndexes.Add(i);
            //}

            //var dbCarsPage = carsPageIndexes.Select(carsPageIndex => dbCars[carsPageIndex]).ToList();

            //// Act
            //var actualCarsPage = _carService.GetCarsPage(pageSize: pagesize, pageNumber: pageNumber).ToList();

            //// Assert
            //dbCarsPage.Should().BeEquivalentTo(actualCarsPage,
            //options => options.ExcludingMissingMembers());
        }
    }
}