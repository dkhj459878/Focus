﻿using FluentAssertions;
using Focus.BL.Common.Services;
using Focus.BL.Services;
using Focus.BL.Tests.FakeData;
using Focus.DAL.Common.DbModels;
using Focus.DAL.Common.Repositories;
using Focus.DAL.Common.UoW;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Focus.BL.Tests.Post
{
    /// <summary>
    /// Unit-тесты для сервиса <see cref="PostService"/>
    /// </summary>
    [TestFixture]
    public class PostServiceTests
    {
        private const int PostsCount = 15;
        private IPostService _postService;
        private FakePost _fakePosts;
        private Mock<IDbPostRepository> mockpostRepository;

        /// <summary>
        /// Осуществляет настройки
        /// </summary>
        [OneTimeSetUp]
        public void SetUp()
        {


            _fakePosts = new FakePost(PostsCount);

            mockpostRepository = new Mock<IDbPostRepository>();
            mockpostRepository
                .Setup(mock => mock.Get(It.IsAny<Expression<Func<POST, bool>>>(), null, It.IsAny<string>()))
                .Returns((Expression<Func<POST, bool>> filter,
                    Func<IQueryable<POST>, IOrderedQueryable<POST>> orderBy,
                    string includeProperties) => _fakePosts.Posts.Where(filter.Compile()));
            mockpostRepository
                .Setup(mock => mock.Get(null, null, It.IsAny<string>()))
                .Returns((Expression<Func<POST, bool>> filter,
                    Func<IQueryable<POST>, IOrderedQueryable<POST>> orderBy,
                    string includeProperties) => _fakePosts.Posts);
            mockpostRepository
                .Setup(mock => mock.GetPage(It.IsAny<int>(), It.IsAny<int>(), null, null, ""))
                .Returns((int pageSize,
                    int pageNumber,
                    Expression<Func<POST, bool>> filter,
                    Func<IQueryable<POST>, IOrderedQueryable<POST>> orderBy,
                    string includeProperties) => _fakePosts.Posts.Skip((pageNumber - 1) * pageSize).Take(pageSize));
            mockpostRepository.Setup(mock => mock.GetById(It.IsAny<byte>()))
                .Returns((byte id) => _fakePosts.Posts.Find(f => f.ID == id));
            mockpostRepository.Setup(mock => mock.Insert(It.IsAny<POST>()))
                .Callback((POST f) => _fakePosts.Posts.Add(f));
            mockpostRepository.Setup(mock => mock.Update(It.IsAny<POST>()))
                .Callback((POST f) => _fakePosts.Posts.Find(d => d.ID == f.ID).CODE = f.CODE);
            mockpostRepository.Setup(mock => mock.Delete(It.IsAny<byte>()))
                .Callback((byte id) => _fakePosts.Posts
                    .Remove(_fakePosts.Posts.Find(f => f.ID == id)));

            var mockpostUnitOfWork = new Mock<IUnitOfWork>();
            mockpostUnitOfWork.Setup(mock => mock.PostRepository)
                .Returns(() => mockpostRepository.Object); //todo - закоментил, не знаю в чем дело

            _postService = new PostService(mockpostUnitOfWork.Object);
        }

        /// <summary>
        /// Тест метода получения экземпляров ПТОов
        /// выбранной страницы пейджинга. Выбрана может быть любая страница.
        /// </summary>
        [Test, Order(1)]
        public void GetExistedPostsPageTest([Random(1, 3, 1)]int pageNumber)
        {
            const int pagesize = 4;

            // Arrange
            var posts = _fakePosts.Posts;

            var postsPageIndexes = new List<int>();

            var startPostIndexInPage = pagesize * (pageNumber - 1);

            var finishPostIndexInPage = startPostIndexInPage + pagesize - 1;

            for (var i = startPostIndexInPage; i <= finishPostIndexInPage; i++)
            {
                postsPageIndexes.Add(i);
            }

            List<POST> postsPage;
            postsPage = postsPageIndexes.Select(postsPageIndex => posts[postsPageIndex]).ToList();


            // Act

            var actualPostsPage = _postService.GetPostsPage(pageSize: pagesize, pageNumber: pageNumber).ToList();

            // Assert
            postsPage.Should().BeEquivalentTo(actualPostsPage,
                options => options.ExcludingMissingMembers());
        }

        /// <summary>
        /// Тест метода получения экземпляров ПТОов
        /// выбранной страницы пейджинга. Выбрана последняя страница.
        /// </summary>
        [Test, Order(1)]
        public void GetExistedPostsLastPageTest()
        {
            const int pagesize = 14;
            const int pageNumber = 2;

            // Arrange
            var dbPosts = _fakePosts.Posts;

            var postsPageIndexes = new List<int>();

            var startPostIndexInPage = pagesize * (pageNumber - 1);

            var finishPostIndexInPage = startPostIndexInPage + PostsCount % pagesize - 1;

            for (var i = startPostIndexInPage; i <= finishPostIndexInPage; i++)
            {
                postsPageIndexes.Add(i);
            }

            var dbPostsPage = postsPageIndexes.Select(postsPageIndex => dbPosts[postsPageIndex]).ToList();

            // Act
            var actualPostsPage = _postService.GetPostsPage(pageSize: pagesize, pageNumber: pageNumber).ToList();

            // Assert
            dbPostsPage.Should().BeEquivalentTo(actualPostsPage,
                options => options.ExcludingMissingMembers());
        }

        /// <summary>
        /// Тест метода получения экземпляров ПТОов
        /// выбранной страницы пейджинга. Номер страницы отрицательный.
        /// </summary>
        [Test, Order(1)]
        public void GetNotExistedPostsPageWithNegativePageNumbersTest([Random(-100500, -1, 5)] int pageNumber)
        {
            const int pagesize = 16;

            // Act
            ICollection<POST> actualPostsPage = _postService.GetPostsPage(pageSize: pagesize, pageNumber: pageNumber).ToList();

            ICollection<POST> expectedPage = _postService.GetPostsPage(pageSize: pagesize, pageNumber: 1).ToList();

            // Assert
            actualPostsPage.Should().BeEquivalentTo(expectedPage,
                options => options.ExcludingMissingMembers());
        }

        /// <summary>
        /// Тест метода получения экземпляров ПТОов
        /// выбранной страницы пейджинга. Номер страницы больше общего числа страниц.
        /// </summary>
        [Test, Order(1)]
        public void GetNotExistedPostsPageWithValueIfPageNumbersMoreThanTotalPostsPagesTest([Random(PostsCount, PostsCount + 100500, 5)]int pageNumber)
        {
            const int pagesize = 16;

            // Act
            ICollection<POST> actualPostsPage = _postService.GetPostsPage(pageSize: pagesize, pageNumber: pageNumber).ToList();

            // Assert
            actualPostsPage.Should().BeEmpty();
        }

        /// <summary>
        /// Тест метода получения экземпляров ПТОов
        /// ввыбранной страницы пейджинга из пустого репозитария.
        /// </summary>
        [Test, Order(5)]
        public void GetNotExistedPostsPageFromEmptyRepositoryTest([Random(1, 18, 5)]int pageNumber)
        {
            const int pagesize = 16;

            // Arrange
            _fakePosts.Posts.Clear();

            // Act
            ICollection<POST> actualPostsPage = _postService.GetPostsPage(pageSize: pagesize, pageNumber: pageNumber).ToList();

            // Assert
            actualPostsPage.Should().BeEmpty();
        }

        /// <summary>
        /// Тест метода получения всех экземпляров ПТОов
        /// в пустом репозитарии.
        /// </summary>
        [Test, Order(5)]
        public void GetAllPostsFromEmptyRepositoryTest()
        {
            // Arrange
            _fakePosts.Posts.Clear();

            // Act
            var actualPosts = _postService.GetAllPosts(filter: null);

            // Assert
            actualPosts.Should().BeEmpty();
        }

        /// <summary>
        /// Тест метода получения всех экземпляров ПТОов
        /// в не пустом репозитарии.
        /// </summary>
        [Test, Order(1)]
        public void GetAllExistedPostsTest()
        {
            // Arrange
            var expectedPosts = _fakePosts.Posts;

            // Act
            var actualPosts = _postService.GetAllPosts().ToList();

            // Assert
           
            expectedPosts.Should().BeEquivalentTo(actualPosts,
                options => options.ExcludingMissingMembers());

            expectedPosts.Count.Should().Be(actualPosts.Count);
        }

        /// <summary>
        /// Тест метода получения экземпляра ПТО по заданному идентификатору.
        /// ПТО с передаваемым идентификатором существует
        /// </summary>
        /// <param name="id">Идентификатор ПТО</param>
        [Test, Order(1)]
        public void GetExistedPostTest([Random(1, PostsCount, 5)] byte id)
        {
            // Arrange
            var expectedPost = _fakePosts.Posts.Find(f => f.ID == id);

            // Act
            var actualPost = _postService.GetPost(id);

            // Assert


            actualPost.Should().BeEquivalentTo(expectedPost,
                options => options.ExcludingMissingMembers());
        }

        /// <summary>
        /// Тест метода получения экземпляра ПТО по заданному идентификатору.
        /// Контейнер с передаваемым идентификатором не существует.
        /// </summary>
        /// <param name="id">Идентификатор ПТО</param>
        [Test, Order(1)]
        public void GetNotExistedPostTest([Random(20, 255, 5)]
                    byte id)
        {
            var actualPost = _postService.GetPost(id);

            actualPost.Should().BeNull();
        }

        /// <summary>
        /// Тест метода добавления ПТО в хранилище данных
        /// </summary>
        [Test, Repeat(5), Order(2)]
        public void AddPostTest()
        {

            byte postId = 15;
            
            // Arrange
            var postsFake = _fakePosts.Posts;

            var postFake = postsFake.Where(c => c.ID == postId).FirstOrDefault();
            

            // Act

            var actualPost = _postService.GetPost(postId);

            // Assert

            postFake.Should().BeEquivalentTo(actualPost,
                options => options.ExcludingMissingMembers());
        }

        /// <summary>
        /// Тест метода обновления данных о ПТО
        /// </summary>
        /// <param name="id">Идентификатор ПТО для обновления</param>
        [Test, Order(2)]
        public void UpdatePostTest([Random(1, PostsCount, 5)] byte id)
        {
            // Arrange
            var post = _postService.GetPost(id);
            post.CODE = new Bogus.Faker().Random.AlphaNumeric(17);

            // Act
            _postService.UpdatePost(post);
            var actualPost = _fakePosts.Posts.Find(f => f.ID == id);

            // Assert
            actualPost.CODE.Should().Be(post.CODE);
        }

        /// <summary>
        /// Тест метода удаления ПТО из хранилища данных
        /// </summary>
        /// <param name="id">Идентификатор ПТО для удаления</param>
        [Test, Order(3)]
        public void DeletePostTest([Random(1, PostsCount, 5)] byte id)
        {
            // Arrange
            _postService.DeletePost(id);

            // Act
            var actualPost = _fakePosts.Posts.Find(post => post.ID == id);

            // Assert
            actualPost.Should().BeNull();
        }

        /// <summary>ПТО
        /// Тест метода проверки наличия  в хранилище данных.
        /// ПТО существует
        /// </summary>
        /// <param name="id">Идентификатор ПТО для поиска</param>
        [Test, Order(2)]
        public void PostExistsTest([Random(1, PostsCount, 5)] short id)
        {
            // Arrange
            var loginToFind = _fakePosts.Posts.Find(dbf => dbf.ID == id).CODE;

            // Act
            var actual = _postService
                .PostExists(f => f.CODE == loginToFind);

            // Assert
            actual.Should().BeTrue();
        }

        /// <summary>
        /// Тест метода проверки наличия ПТО в хранилище данных.
        /// ПТО не существует
        /// </summary>
        /// <param name="login">Логин ПТО для поиска</param>
        [Test, Order(2)]
        public void PostNotExistsTest([Values("shameonyou", "qwer", "", "befastandclearver", "strangelogin")]
                    string VIN)
        {
            var actual = _postService
                .PostExists(f => f.CODE == VIN);

            actual.Should().BeFalse();
        }
    }
}

