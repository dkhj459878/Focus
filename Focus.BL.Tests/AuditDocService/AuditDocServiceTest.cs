﻿//using FluentAssertions;
//using Focus.BL.Common.Services;
//using Focus.BL.Enum;
//using Focus.BL.Services;
//using Focus.BL.Tests.FakeData;
//using Focus.DAL.Common.DbModels;
//using Focus.DAL.Common.Repositories;
//using Focus.DAL.Common.UoW;
//using Moq;
//using NUnit.Framework;
//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Linq.Expressions;

//namespace Focus.BL.Tests.AuditDocService
//{
//    /// <summary>
//    /// Unit-тесты для сервиса <see cref="CustombrockerService"/>
//    /// </summary>
//    [TestFixture]
//    public class AuditDocServiceTests
//    {
//        private const int CustombrockersCount = 300;
//        private const string TEST_USER_INFO = "administrator, Голуб Василий Иванович, инспектор, ОТОиК №7"; 
//        private List<AUDIT_TRAIL> auditTrailsCreated = new List<AUDIT_TRAIL>();
//        private List<AUDIT_TRAIL> auditTrailsWithModification = new List<AUDIT_TRAIL>();
//        private List<AUDIT_TRAIL> auditTrailsWithDeletion = new List<AUDIT_TRAIL>();
//        private ICustombrockerService _custombrockerService;
//        private DateTime timeStampOfCreation = DateTime.Now.AddDays(-5);
//        private DateTime timeStampOfCrudOperation = DateTime.Now.AddDays(-2);
//        private FakeCustombrocker _fakeCustombrockers;
//        private Mock<IDbCustombrockerRepository> mockcustombrockerRepository;
//        private readonly RetrievingEntityService _retrievingEntityService = new RetrievingEntityService();

//        /// <summary>
//        /// Осуществляет настройки
//        /// </summary>
//        [OneTimeSetUp]
//        public void SetUp()
//        {
//            _fakeCustombrockers = new FakeCustombrocker(CustombrockersCount);

//            mockcustombrockerRepository = new Mock<IDbCustombrockerRepository>();
//            mockcustombrockerRepository
//                .Setup(mock => mock.Get(It.IsAny<Expression<Func<CUSTOMBROCKER, bool>>>(), null, It.IsAny<string>()))
//                .Returns((Expression<Func<CUSTOMBROCKER, bool>> filter,
//                    Func<IQueryable<CUSTOMBROCKER>, IOrderedQueryable<CUSTOMBROCKER>> orderBy,
//                    string includeProperties) => _fakeCustombrockers.Custombrockers.Where(filter.Compile()));
//            mockcustombrockerRepository
//                .Setup(mock => mock.Get(null, null, It.IsAny<string>()))
//                .Returns((Expression<Func<CUSTOMBROCKER, bool>> filter,
//                    Func<IQueryable<CUSTOMBROCKER>, IOrderedQueryable<CUSTOMBROCKER>> orderBy,
//                    string includeProperties) => _fakeCustombrockers.Custombrockers);
//            mockcustombrockerRepository
//                .Setup(mock => mock.GetPage(It.IsAny<int>(), It.IsAny<int>(), null, null, ""))
//                .Returns((int pageSize,
//                    int pageNumber,
//                    Expression<Func<CUSTOMBROCKER, bool>> filter,
//                    Func<IQueryable<CUSTOMBROCKER>, IOrderedQueryable<CUSTOMBROCKER>> orderBy,
//                    string includeProperties) => _fakeCustombrockers.Custombrockers.Skip((pageNumber - 1) * pageSize).Take(pageSize));
//            mockcustombrockerRepository.Setup(mock => mock.GetById(It.IsAny<short>()))
//                .Returns((short id) => _fakeCustombrockers.Custombrockers.Find(f => f.CUSTOMBROCKERID == id));
//            mockcustombrockerRepository.Setup(mock => mock.Insert(It.IsAny<CUSTOMBROCKER>()))
//                .Callback((CUSTOMBROCKER f) => _fakeCustombrockers.Custombrockers.Add(f));
//            mockcustombrockerRepository.Setup(mock => mock.Update(It.IsAny<CUSTOMBROCKER>()))
//                .Callback((CUSTOMBROCKER f) => _fakeCustombrockers.Custombrockers.Find(d => d.CUSTOMBROCKERID == f.CUSTOMBROCKERID).FIRSTNAME = f.FIRSTNAME);
//            mockcustombrockerRepository.Setup(mock => mock.Delete(It.IsAny<short>()))
//                .Callback((short id) => _fakeCustombrockers.Custombrockers
//                    .Remove(_fakeCustombrockers.Custombrockers.Find(f => f.CUSTOMBROCKERID == id)));

//            var mockcustombrockerUnitOfWork = new Mock<IUnitOfWork>();
//            mockcustombrockerUnitOfWork.Setup(mock => mock.CustombrockerRepository)
//                .Returns(() => mockcustombrockerRepository.Object); //todo - закоментил, не знаю в чем дело

//            _custombrockerService = new CustombrockerService(mockcustombrockerUnitOfWork.Object);

//            #region Creation of the DOC instance.
//            auditTrailsCreated.Add(new AUDIT_TRAIL() {
//                ID = 1,
//                ENTITY = "DOC",
//                ENTITYID = 5,
//                FIELD = "INSPECTORDEVISION",
//                TIME_STAMP = timeStampOfCrudOperation,
//                DATE_ = timeStampOfCreation,
//                SID = "c1fa7deb - d31a - 4626 - be60 - 77292d0afae8",
//                TYPE = (short)CrudOperation.Added,
//                USERINFO = TEST_USER_INFO,
//                VALUENEW = "ОТОиК №5",
//                VALUEOLD = string.Empty
//            });
//            auditTrailsCreated.Add(new AUDIT_TRAIL()
//            {
//                ID = 2,
//                ENTITY = "DOC",
//                ENTITYID = 5,
//                FIELD = "FIRSTNAME",
//                TIME_STAMP = timeStampOfCrudOperation,
//                DATE_ = timeStampOfCreation,
//                SID = "c1fa7deb - d31a - 4626 - be60 - 77292d0afae8",
//                TYPE = (short)CrudOperation.Added,
//                USERINFO = TEST_USER_INFO,
//                VALUENEW = "Коля",
//                VALUEOLD = string.Empty
//            });
//            auditTrailsCreated.Add(new AUDIT_TRAIL()
//            {
//                ID = 3,
//                ENTITY = "DOC",
//                ENTITYID = 5,
//                FIELD = "CONSIGNEEID",
//                TIME_STAMP = timeStampOfCrudOperation,
//                DATE_ = timeStampOfCreation,
//                SID = "c1fa7deb - d31a - 4626 - be60 - 77292d0afae8",
//                TYPE = (short)CrudOperation.Added,
//                USERINFO = TEST_USER_INFO,
//                VALUENEW = "5",
//                VALUEOLD = string.Empty
//            });
//            auditTrailsCreated.Add(new AUDIT_TRAIL()
//            {
//                ID = 4,
//                ENTITY = "DOC",
//                ENTITYID = 5,
//                FIELD = "TYPE",
//                TIME_STAMP = timeStampOfCrudOperation,
//                DATE_ = timeStampOfCreation,
//                SID = "c1fa7deb - d31a - 4626 - be60 - 77292d0afae8",
//                TYPE = (short)CrudOperation.Added,
//                USERINFO = TEST_USER_INFO,
//                VALUENEW = "1",
//                VALUEOLD = string.Empty
//            });
//            auditTrailsCreated.Add(new AUDIT_TRAIL()
//            {
//                ID = 5,
//                ENTITY = "DOC",
//                ENTITYID = 5,
//                FIELD = "PERIODSTARTDATE",
//                TIME_STAMP = timeStampOfCrudOperation,
//                DATE_ = timeStampOfCreation,
//                SID = "c1fa7deb - d31a - 4626 - be60 - 77292d0afae8",
//                TYPE = (short)CrudOperation.Added,
//                USERINFO = TEST_USER_INFO,
//                VALUENEW = "27.08.19",
//                VALUEOLD = string.Empty
//            });
//            auditTrailsCreated.Add(new AUDIT_TRAIL()
//            {
//                ID = 6,
//                ENTITY = "DOC",
//                ENTITYID = 5,
//                FIELD = "PERIODSTARTTIME",
//                TIME_STAMP = timeStampOfCrudOperation,
//                DATE_ = timeStampOfCreation,
//                SID = "c1fa7deb - d31a - 4626 - be60 - 77292d0afae8",
//                TYPE = (short)CrudOperation.Added,
//                USERINFO = TEST_USER_INFO,
//                VALUENEW = "27.08.19 18:10:00,000000000",
//                VALUEOLD = string.Empty
//            });
//            auditTrailsCreated.Add(new AUDIT_TRAIL()
//            {
//                ID = 7,
//                ENTITY = "DOC",
//                ENTITYID = 5,
//                FIELD = "GOODSTOTALBRUTTO",
//                TIME_STAMP = timeStampOfCrudOperation,
//                DATE_ = timeStampOfCreation,
//                SID = "c1fa7deb - d31a - 4626 - be60 - 77292d0afae8",
//                TYPE = (short)CrudOperation.Added,
//                USERINFO = TEST_USER_INFO,
//                VALUENEW = "3000.50",
//                VALUEOLD = string.Empty
//            });
//            auditTrailsCreated.Add(new AUDIT_TRAIL()
//            {
//                ID = 8,
//                ENTITY = "DOC",
//                ENTITYID = 5,
//                FIELD = "TOTALWEIGHT",
//                TIME_STAMP = timeStampOfCrudOperation,
//                DATE_ = timeStampOfCreation,
//                SID = "c1fa7deb - d31a - 4626 - be60 - 77292d0afae8",
//                TYPE = (short)CrudOperation.Added,
//                USERINFO = TEST_USER_INFO,
//                VALUENEW = "15.125456",
//                VALUEOLD = string.Empty
//            });
//            auditTrailsCreated.Add(new AUDIT_TRAIL()
//            {
//                ID = 8,
//                ENTITY = "DOC",
//                ENTITYID = 5,
//                FIELD = "VEHICLE",
//                TIME_STAMP = timeStampOfCrudOperation,
//                DATE_ = timeStampOfCreation,
//                SID = "c1fa7deb - d31a - 4626 - be60 - 77292d0afae8",
//                TYPE = (short)CrudOperation.Added,
//                USERINFO = TEST_USER_INFO,
//                VALUENEW = "1;3;5",
//                VALUEOLD = string.Empty
//            });
//            #endregion

//            #region Modification of the DOC instance.
//            auditTrailsWithModification.Add(new AUDIT_TRAIL()
//            {
//                ID = 8,
//                ENTITY = "DOC",
//                ENTITYID = 5,
//                FIELD = "INSPECTORDEVISION",
//                TIME_STAMP = timeStampOfCrudOperation,
//                DATE_ = timeStampOfCreation,
//                SID = "c1fa7deb - d31a - 4626 - be60 - 77292d0afae8",
//                TYPE = (short)CrudOperation.Modified,
//                USERINFO = TEST_USER_INFO,
//                VALUENEW = "ОТОиК №6",
//                VALUEOLD = "ОТОиК №5"
//            });
//            auditTrailsWithModification.Add(new AUDIT_TRAIL()
//            {
//                ID = 9,
//                ENTITY = "DOC",
//                ENTITYID = 5,
//                FIELD = "FIRSTNAME",
//                TIME_STAMP = timeStampOfCrudOperation,
//                DATE_ = timeStampOfCreation,
//                SID = "c1fa7deb - d31a - 4626 - be60 - 77292d0afae8",
//                TYPE = (short)CrudOperation.Modified,
//                USERINFO = TEST_USER_INFO,
//                VALUENEW = "Саша",
//                VALUEOLD = "Коля"
//            });
//            auditTrailsWithModification.Add(new AUDIT_TRAIL()
//            {
//                ID = 10,
//                ENTITY = "DOC",
//                ENTITYID = 5,
//                FIELD = "CONSIGNEEID",
//                TIME_STAMP = timeStampOfCrudOperation,
//                DATE_ = timeStampOfCreation,
//                SID = "c1fa7deb - d31a - 4626 - be60 - 77292d0afae8",
//                TYPE = (short)CrudOperation.Modified,
//                USERINFO = TEST_USER_INFO,
//                VALUENEW = "6",
//                VALUEOLD = "5"
//            });
//            auditTrailsWithModification.Add(new AUDIT_TRAIL()
//            {
//                ID = 11,
//                ENTITY = "DOC",
//                ENTITYID = 5,
//                FIELD = "TYPE",
//                TIME_STAMP = timeStampOfCrudOperation,
//                DATE_ = timeStampOfCreation,
//                SID = "c1fa7deb - d31a - 4626 - be60 - 77292d0afae8",
//                TYPE = (short)CrudOperation.Modified,
//                USERINFO = TEST_USER_INFO,
//                VALUENEW = "0",
//                VALUEOLD = "1"
//            });
//            auditTrailsWithModification.Add(new AUDIT_TRAIL()
//            {
//                ID = 12,
//                ENTITY = "DOC",
//                ENTITYID = 5,
//                FIELD = "PERIODSTARTDATE",
//                TIME_STAMP = timeStampOfCrudOperation,
//                DATE_ = timeStampOfCreation,
//                SID = "c1fa7deb - d31a - 4626 - be60 - 77292d0afae8",
//                TYPE = (short)CrudOperation.Modified,
//                USERINFO = TEST_USER_INFO,
//                VALUENEW = "28.08.19",
//                VALUEOLD = "27.08.19",
//            });
//            auditTrailsWithModification.Add(new AUDIT_TRAIL()
//            {
//                ID = 13,
//                ENTITY = "DOC",
//                ENTITYID = 5,
//                FIELD = "PERIODSTARTTIME",
//                TIME_STAMP = timeStampOfCrudOperation,
//                DATE_ = timeStampOfCreation,
//                SID = "c1fa7deb - d31a - 4626 - be60 - 77292d0afae8",
//                TYPE = (short)CrudOperation.Modified,
//                USERINFO = TEST_USER_INFO,
//                VALUENEW = "28.08.19 18:10:00,000000000",
//                VALUEOLD = "27.08.19 18:10:00,000000000"
//            });
//            auditTrailsWithModification.Add(new AUDIT_TRAIL()
//            {
//                ID = 14,
//                ENTITY = "DOC",
//                ENTITYID = 5,
//                FIELD = "GOODSTOTALBRUTTO",
//                TIME_STAMP = timeStampOfCrudOperation,
//                DATE_ = timeStampOfCreation,
//                SID = "c1fa7deb - d31a - 4626 - be60 - 77292d0afae8",
//                TYPE = (short)CrudOperation.Modified,
//                USERINFO = TEST_USER_INFO,
//                VALUENEW = "3005.50",
//                VALUEOLD = "3000.50"
//            });
//            auditTrailsWithModification.Add(new AUDIT_TRAIL()
//            {
//                ID = 15,
//                ENTITY = "DOC",
//                ENTITYID = 5,
//                FIELD = "VEHICLE",
//                TIME_STAMP = timeStampOfCrudOperation,
//                DATE_ = timeStampOfCreation,
//                SID = "c1fa7deb - d31a - 4626 - be60 - 77292d0afae8",
//                TYPE = (short)CrudOperation.Modified,
//                USERINFO = TEST_USER_INFO,
//                VALUENEW = "1;5",
//                VALUEOLD = string.Empty
//            });
//            auditTrailsWithModification.Add(new AUDIT_TRAIL()
//            {
//                ID = 16,
//                ENTITY = "DOC",
//                ENTITYID = 5,
//                FIELD = "TOTALWEIGHT",
//                TIME_STAMP = timeStampOfCrudOperation,
//                DATE_ = timeStampOfCreation,
//                SID = "c1fa7deb - d31a - 4626 - be60 - 77292d0afae8",
//                TYPE = (short)CrudOperation.Modified,
//                USERINFO = TEST_USER_INFO,
//                VALUENEW = "16.125456",
//                VALUEOLD = "15.125456"
//            });
//            #endregion

//            #region Deletion of the DOC instance.
//            auditTrailsWithDeletion.AddRange(auditTrailsWithModification);
//            auditTrailsWithDeletion.Add(new AUDIT_TRAIL()
//            {
//                ID = 17,
//                ENTITY = "DOC",
//                ENTITYID = 5,
//                FIELD = "INSPECTORDEVISION",
//                TIME_STAMP = timeStampOfCrudOperation,
//                DATE_ = timeStampOfCreation,
//                SID = "c1fa7deb - d31a - 4626 - be60 - 77292d0afae8",
//                TYPE = (short)CrudOperation.Modified,
//                USERINFO = TEST_USER_INFO,
//                VALUENEW = string.Empty,
//                VALUEOLD = "ОТОиК №6"
//            });
//            auditTrailsWithDeletion.Add(new AUDIT_TRAIL()
//            {
//                ID = 18,
//                ENTITY = "DOC",
//                ENTITYID = 5,
//                FIELD = "FIRSTNAME",
//                TIME_STAMP = timeStampOfCrudOperation,
//                DATE_ = timeStampOfCreation,
//                SID = "c1fa7deb - d31a - 4626 - be60 - 77292d0afae8",
//                TYPE = (short)CrudOperation.Modified,
//                USERINFO = TEST_USER_INFO,
//                VALUENEW = string.Empty,
//                VALUEOLD = "Саша"
//            });
//            auditTrailsWithDeletion.Add(new AUDIT_TRAIL()
//            {
//                ID = 19,
//                ENTITY = "DOC",
//                ENTITYID = 5,
//                FIELD = "CONSIGNEEID",
//                TIME_STAMP = timeStampOfCrudOperation,
//                DATE_ = timeStampOfCreation,
//                SID = "c1fa7deb - d31a - 4626 - be60 - 77292d0afae8",
//                TYPE = (short)CrudOperation.Modified,
//                USERINFO = TEST_USER_INFO,
//                VALUENEW = string.Empty,
//                VALUEOLD = "6"
//            });
//            auditTrailsWithDeletion.Add(new AUDIT_TRAIL()
//            {
//                ID = 20,
//                ENTITY = "DOC",
//                ENTITYID = 5,
//                FIELD = "TYPE",
//                TIME_STAMP = timeStampOfCrudOperation,
//                DATE_ = timeStampOfCreation,
//                SID = "c1fa7deb - d31a - 4626 - be60 - 77292d0afae8",
//                TYPE = (short)CrudOperation.Modified,
//                USERINFO = TEST_USER_INFO,
//                VALUENEW = string.Empty,
//                VALUEOLD = "0"
//            });
//            auditTrailsWithDeletion.Add(new AUDIT_TRAIL()
//            {
//                ID = 21,
//                ENTITY = "DOC",
//                ENTITYID = 5,
//                FIELD = "PERIODSTARTDATE",
//                TIME_STAMP = timeStampOfCrudOperation,
//                DATE_ = timeStampOfCreation,
//                SID = "c1fa7deb - d31a - 4626 - be60 - 77292d0afae8",
//                TYPE = (short)CrudOperation.Modified,
//                USERINFO = TEST_USER_INFO,
//                VALUENEW = string.Empty,
//                VALUEOLD = "28.08.19",
//            });
//            auditTrailsWithDeletion.Add(new AUDIT_TRAIL()
//            {
//                ID = 22,
//                ENTITY = "DOC",
//                ENTITYID = 5,
//                FIELD = "PERIODSTARTTIME",
//                TIME_STAMP = timeStampOfCrudOperation,
//                DATE_ = timeStampOfCreation,
//                SID = "c1fa7deb - d31a - 4626 - be60 - 77292d0afae8",
//                TYPE = (short)CrudOperation.Modified,
//                USERINFO = TEST_USER_INFO,
//                VALUENEW = string.Empty,
//                VALUEOLD = "28.08.19 18:10:00,000000000"
//            });
//            auditTrailsWithDeletion.Add(new AUDIT_TRAIL()
//            {
//                ID = 23,
//                ENTITY = "DOC",
//                ENTITYID = 5,
//                FIELD = "GOODSTOTALBRUTTO",
//                TIME_STAMP = timeStampOfCrudOperation,
//                DATE_ = timeStampOfCreation,
//                SID = "c1fa7deb - d31a - 4626 - be60 - 77292d0afae8",
//                TYPE = (short)CrudOperation.Modified,
//                USERINFO = TEST_USER_INFO,
//                VALUENEW = string.Empty,
//                VALUEOLD = "3005.50"
//            });
//            auditTrailsWithDeletion.Add(new AUDIT_TRAIL()
//            {
//                ID = 25,
//                ENTITY = "DOC",
//                ENTITYID = 5,
//                FIELD = "TOTALWEIGHT",
//                TIME_STAMP = timeStampOfCrudOperation,
//                DATE_ = timeStampOfCreation,
//                SID = "c1fa7deb - d31a - 4626 - be60 - 77292d0afae8",
//                TYPE = (short)CrudOperation.Modified,
//                USERINFO = TEST_USER_INFO,
//                VALUENEW = string.Empty,
//                VALUEOLD = "15.125456"
//            });
//            auditTrailsWithDeletion.Add(new AUDIT_TRAIL()
//            {
//                ID = 25,
//                ENTITY = "DOC",
//                ENTITYID = 5,
//                FIELD = "VEHICLE",
//                TIME_STAMP = timeStampOfCrudOperation,
//                DATE_ = timeStampOfCreation,
//                SID = "c1fa7deb - d31a - 4626 - be60 - 77292d0afae8",
//                TYPE = (short)CrudOperation.Modified,
//                USERINFO = TEST_USER_INFO,
//                VALUENEW = string.Empty,
//                VALUEOLD = "1;5"
//            });
//            #endregion

//        }

//        /// <summary>
//        /// Тест метода получения коллекции типов свойсв вложенных классов.
//        /// </summary>
//        [Test, Order(1)]
//        public void GetNestedClassTypesTest()
//        {
//            // Arrange
//            DOC doc = new DOC();
//            CUSTOMBROCKER customBrocker = new CUSTOMBROCKER();
//            PERMISSION permission = new PERMISSION();

//            IEnumerable<string> docNestedClassTypeNamesExpected = new List<string>()
//            {
//                "CUSTOMBROCKER" ,
//                "INSPECTOR",
//                "ORGANIZATION",
//                "INSPECTOR1"
//            };
//            IEnumerable<string> customBrockerNestedClassTypeNamesExpected = new List<string>()
//            {
//                "COUNTRY"
//            };
//            IEnumerable<string> permissionNestedClassTypeNamesExpected = new List<string>()
//            {
//                "DOCUMENT"
//            };

//            IEnumerable<string> docNestedClassTypeNamesOrderedExpected = docNestedClassTypeNamesExpected.OrderBy(p => p);
//            IEnumerable<string> customBrockerNestedClassTypeNamesOrderedExpected = customBrockerNestedClassTypeNamesExpected.OrderBy(p => p);
//            IEnumerable<string> permissionNestedClassTypeNamesOrderedExpected = permissionNestedClassTypeNamesExpected.OrderBy(p => p);

//            // Act
//            IEnumerable<string> docNestedClassTypeNamesOrderedActual = _retrievingEntityService.GetNestedClassTypeNames(doc).OrderBy(p => p);
//            IEnumerable<string> customBrockerNestedClassTypeNamesOrderedActual = _retrievingEntityService.GetNestedClassTypeNames(customBrocker).OrderBy(p => p);
//            IEnumerable<string> permissionNestedClassTypeNamesOrderedActual = _retrievingEntityService.GetNestedClassTypeNames(permission).OrderBy(p => p);

//            // Assert
//            Assert.AreEqual(docNestedClassTypeNamesOrderedExpected, docNestedClassTypeNamesOrderedActual);
//            Assert.AreEqual(customBrockerNestedClassTypeNamesOrderedExpected, customBrockerNestedClassTypeNamesOrderedActual);
//            Assert.AreEqual(permissionNestedClassTypeNamesOrderedExpected, permissionNestedClassTypeNamesOrderedActual);
//        }

//        /// <summary>
//        /// Тест метода получения экземпляров таможенных представителей
//        /// ввыбранной страницы пейджинга из пустого репозитария.
//        /// </summary>
//        [Test, Order(5)]
//        public void GetNestedCollectionTypesTest()
//        {
//            // Arrange
//            DOC doc = new DOC();
//            CUSTOMBROCKER customBrocker = new CUSTOMBROCKER();
//            DOCUMENT document = new DOCUMENT();

//            IEnumerable<string> docNestedClassTypeNamesExpected = new List<string>()
//            {
//                "CAR" ,
//                "CONTAINER",
//                "DEVICE",
//                "VEHICLE"
//            };
//            IEnumerable<string> customBrockerNestedClassTypeNamesExpected = new List<string>()
//            {
//                "DOC" ,
//                "PERMISSION"
//            };
//            IEnumerable<string> documentNestedClassTypeNamesExpected = new List<string>()
//            {
//                "PERMISSION" ,
//                "ORGANIZATION"
//            };
//            IEnumerable<string> docNestedCollectionTypeNamesOrderedExpected = docNestedClassTypeNamesExpected.OrderBy(p => p);
//            IEnumerable<string> customBrockerNestedCollectionTypeNamesOrderedExpected = customBrockerNestedClassTypeNamesExpected.OrderBy(p => p);
//            IEnumerable<string> documentNestedCollectionTypeNamesOrderedExpected = documentNestedClassTypeNamesExpected.OrderBy(p => p);

//            // Act
//            IEnumerable<string> docNestedCollectionTypeNamesOrderedActual = _retrievingEntityService.GetCollectionTypeNames(doc).OrderBy(p => p);
//            IEnumerable<string> customBrockerNestedCollectionTypeNamesOrderedActual = _retrievingEntityService.GetCollectionTypeNames(customBrocker).OrderBy(p => p);
//            IEnumerable<string> documentNestedCollectionTypeNamesOrderedActual = _retrievingEntityService.GetCollectionTypeNames(document).OrderBy(p => p);

//            // Assert
//            Assert.AreEqual(docNestedCollectionTypeNamesOrderedExpected, docNestedCollectionTypeNamesOrderedActual);
//            Assert.AreEqual(customBrockerNestedCollectionTypeNamesOrderedExpected, customBrockerNestedCollectionTypeNamesOrderedActual);
//            Assert.AreEqual(documentNestedCollectionTypeNamesOrderedExpected, documentNestedCollectionTypeNamesOrderedActual);
//        }

//        [Test, Order(7)]
//        public void CreateInstanceTest()
//        {
//            //Arrange
//            CAR carExpected = new CAR() { CARID = 5 };
//            CONTAINER containerExpected = new CONTAINER() { CONTAINERID = 6 };
//            CUSTOMBROCKER customBrockerExpeted = new CUSTOMBROCKER() { CUSTOMBROCKERID = 7 };
//            DEVICE deviceExpected = new DEVICE() { DEVICEID = 8 };
//            DEVICETYPE deviceTypeExpected = new DEVICETYPE() { DEVICETYPEID = 9 };
//            DOC docExpected = new DOC() { DOCID = 10 };
//            DOCUMENT documentExpected = new DOCUMENT() { DOCUMENTID = 11 };
//            DOCUMENTKIND documentKindExpected = new DOCUMENTKIND() { DOCUMENTKINDID = 12 };
//            FEATURE featureExpected = new FEATURE() { ID = 13 };
//            INSPECTOR inspectorExpected = new INSPECTOR() { ID = 14 };
//            ORGANIZATION oganizationExpected = new ORGANIZATION() { ORGANIZATIONID = 15 };
//            PERMISSION permissionExpected = new PERMISSION() { PERMISSIONID = 16 };
//            POST postExpected = new POST() { ID = 17 };
//            REPORT reportExpected = new REPORT() { REPORTID = 18 };
//            STORAGE storageExpected = new STORAGE() { ID = 19 };
//            VEHICLE vehicle = new VEHICLE() { VEHICLEID = 20 };
//            VEHICLEKIND vehicleKind = new VEHICLEKIND() { VEHICLEKINDID = 21 };
//            VEHICLETYPE vehicleType = new VEHICLETYPE() { VEHICLETYPEID = 22 };

//            //Act
//            CAR carActual = _retrievingEntityService.CreateInstance(typeof(CAR).Name, 5) as CAR;
//            CONTAINER containerActual = _retrievingEntityService.CreateInstance(typeof(CONTAINER).Name, 6) as CONTAINER;
//            CUSTOMBROCKER customBrockerActual = _retrievingEntityService.CreateInstance(typeof(CUSTOMBROCKER).Name, 7) as CUSTOMBROCKER;
//            DEVICE deviceActual = _retrievingEntityService.CreateInstance(typeof(DEVICE).Name, 8) as DEVICE;
//            DEVICETYPE deviceTypeActual = _retrievingEntityService.CreateInstance(typeof(DEVICETYPE).Name, 9) as DEVICETYPE;
//            DOC docActual = _retrievingEntityService.CreateInstance(typeof(DOC).Name, 10) as DOC;
//            DOCUMENT documentActual = _retrievingEntityService.CreateInstance(typeof(DOCUMENT).Name,11) as DOCUMENT;
//            DOCUMENTKIND documentKindActual = _retrievingEntityService.CreateInstance(typeof(DOCUMENTKIND).Name, 12) as DOCUMENTKIND;
//            FEATURE featureActual = _retrievingEntityService.CreateInstance(typeof(FEATURE).Name, 13) as FEATURE;
//            INSPECTOR inspectorActual = _retrievingEntityService.CreateInstance(typeof(INSPECTOR).Name, 14) as INSPECTOR;
//            ORGANIZATION organizationActual = _retrievingEntityService.CreateInstance(typeof(ORGANIZATION).Name, 15) as ORGANIZATION;
//            PERMISSION permissionActual = _retrievingEntityService.CreateInstance(typeof(PERMISSION).Name, 16) as PERMISSION;
//            POST postActual = _retrievingEntityService.CreateInstance(typeof(POST).Name, 17) as POST;
//            REPORT reportActual = _retrievingEntityService.CreateInstance(typeof(REPORT).Name, 18) as REPORT;
//            STORAGE storageActual = _retrievingEntityService.CreateInstance(typeof(STORAGE).Name, 19) as STORAGE;
//            VEHICLE vehicleActual = _retrievingEntityService.CreateInstance(typeof(VEHICLE).Name, 20) as VEHICLE;
//            VEHICLEKIND vehicleKindActual = _retrievingEntityService.CreateInstance(typeof(VEHICLEKIND).Name, 21) as VEHICLEKIND;
//            VEHICLETYPE vehicleTypeActual = _retrievingEntityService.CreateInstance(typeof(VEHICLETYPE).Name, 22) as VEHICLETYPE;

//            //Assert
//            Assert.AreEqual(carExpected. GetType(), carActual.GetType()); Assert.AreEqual(carExpected.CARID, carActual.CARID);
//            Assert.AreEqual(containerExpected. GetType(), containerActual.GetType()); Assert.AreEqual(containerExpected.CONTAINERID, containerActual.CONTAINERID);
//            Assert.AreEqual(customBrockerExpeted. GetType(), customBrockerActual.GetType()); Assert.AreEqual(customBrockerExpeted.CUSTOMBROCKERID, customBrockerActual.CUSTOMBROCKERID);
//            Assert.AreEqual(deviceExpected. GetType(), deviceActual.GetType()); Assert.AreEqual(deviceExpected.DEVICEID, deviceActual.DEVICEID);
//            Assert.AreEqual(deviceTypeExpected. GetType(), deviceTypeActual.GetType()); Assert.AreEqual(deviceTypeExpected.DEVICETYPEID, deviceTypeActual.DEVICETYPEID);
//            Assert.AreEqual(docExpected. GetType(), docActual.GetType()); Assert.AreEqual(docExpected.DOCID, docActual.DOCID);
//            Assert.AreEqual(documentExpected.GetType(), documentActual.GetType()); Assert.AreEqual(documentExpected.DOCUMENTID, documentActual.DOCUMENTID);
//            Assert.AreEqual(documentKindExpected. GetType(), documentKindActual.GetType()); Assert.AreEqual(documentKindExpected.DOCUMENTKINDID, documentKindActual.DOCUMENTKINDID);
//            Assert.AreEqual(featureExpected. GetType(), featureActual.GetType()); Assert.AreEqual(featureExpected.ID, featureActual.ID);
//            Assert.AreEqual(inspectorExpected. GetType(), inspectorActual.GetType()); Assert.AreEqual(inspectorExpected.ID, inspectorActual.ID);
//            Assert.AreEqual(oganizationExpected. GetType(), organizationActual.GetType()); Assert.AreEqual(oganizationExpected.ORGANIZATIONID, organizationActual.ORGANIZATIONID);
//            Assert.AreEqual(permissionExpected. GetType(), permissionActual.GetType()); Assert.AreEqual(permissionExpected.PERMISSIONID, permissionActual.PERMISSIONID);
//            Assert.AreEqual(postExpected. GetType(), postActual.GetType()); Assert.AreEqual(postExpected.ID, postActual.ID);
//            Assert.AreEqual(reportExpected. GetType(), reportActual.GetType()); Assert.AreEqual(reportExpected.REPORTID, reportActual.REPORTID);
//            Assert.AreEqual(storageExpected. GetType(), storageActual.GetType()); Assert.AreEqual(storageExpected.ID, storageActual.ID);
//            Assert.AreEqual(vehicle. GetType(), vehicleActual.GetType()); Assert.AreEqual(vehicle.VEHICLEID, vehicleActual.VEHICLEID);
//            Assert.AreEqual(vehicleKind. GetType(), vehicleKindActual.GetType()); Assert.AreEqual(vehicleKind.VEHICLEKINDID, vehicleKindActual.VEHICLEKINDID);
//            Assert.AreEqual(vehicleType. GetType(), vehicleTypeActual.GetType()); Assert.AreEqual(vehicleType.VEHICLETYPEID, vehicleTypeActual.VEHICLETYPEID);
//        }

//        [Test, Order(8)]
//        public void CreateGenericCollectionInstanceTest()
//        {
//            //Arrange
//            ICollection<DOC> docsExpected = new List<DOC>();

//            //Act
//            ICollection<DOC> docsActual = _retrievingEntityService.CreateCollectionInstance<DOC>();

//            //Assert
//            Assert.AreEqual(docsExpected.GetType(), docsActual.GetType());
//        }

//        [Test, Order(8)]
//        public void CreateCollectionInstanceTest()
//        {
//            //Arrange
//            ICollection<DOC> docsExpected = new List<DOC>();

//            //Act
//            ICollection<DOC> docsActual = (ICollection<DOC>)_retrievingEntityService.CreateCollectionInstance("DOC");

//            //Assert
//            Assert.AreEqual(docsExpected.GetType(), docsActual.GetType());
//        }

//        [Test, Order(5)]
//        public void RetrieveCreatedEntityTest()
//        {
//            //Arrange
//            DOC docsExpected = new DOC()
//            {
//                INSPECTORDEVISION = "ОТОиК №5",
//                FIRSTNAME = "Коля",
//                USERINFO = "administrator, Голуб Василий Иванович, инспектор, ОТОиК №7",
//                SID = "c1fa7deb - d31a - 4626 - be60 - 77292d0afae8",
//                CONSIGNEEID = 5,
//                TYPE = 1,
//                LAST_UPDATE = timeStampOfCreation,
//                PERIODSTARTDATE = new DateTime(2019, 8, 27),
//                PERIODSTARTTIME = new DateTime(2019, 8, 27, 18, 10, 0),
//                GOODSTOTALBRUTTO = 3000.50M,
//                TOTALWEIGHT = 15.125456M,
//                VEHICLE = new List<VEHICLE>() { new VEHICLE() { VEHICLEID = 1}, new VEHICLE() { VEHICLEID = 3}, new VEHICLE() { VEHICLEID = 5}}
//            };

//            //Act
//            DOC docActual = _retrievingEntityService.RetrieveCreatedEntity<DOC>(auditTrailsCreated);

//            //Assert
//            Assert.AreEqual(docsExpected.INSPECTORDEVISION, docActual.INSPECTORDEVISION);
//            Assert.AreEqual(docsExpected.FIRSTNAME, docActual.FIRSTNAME);
//            Assert.AreEqual(docsExpected.PERIODSTARTDATE, docActual.PERIODSTARTDATE);
//            Assert.AreEqual(docsExpected.GOODSTOTALBRUTTO, docActual.GOODSTOTALBRUTTO);
//            Assert.AreEqual(docsExpected.VEHICLE.Count, docActual.VEHICLE.Count);
//        }

//        [Test, Order(5)]
//        public void RetrieveModifiedEntityTest()
//        {
//            //Arrange
//            DOC docsExpected = new DOC()
//            {
//                INSPECTORDEVISION = "ОТОиК №6",
//                FIRSTNAME = "Саша",
//                USERINFO = "administrator, Голуб Василий Иванович, инспектор, ОТОиК №7",
//                SID = "c1fa7deb - d31a - 4626 - be60 - 77292d0afae8",
//                CONSIGNEEID = 6,
//                TYPE = 0,
//                LAST_UPDATE = timeStampOfCreation,
//                PERIODSTARTDATE = new DateTime(2019, 8, 28),
//                PERIODSTARTTIME = new DateTime(2019, 8, 28, 18, 10, 0),
//                GOODSTOTALBRUTTO = 3005.50M,
//                TOTALWEIGHT = 16.125456M,
//                VEHICLE = new List<VEHICLE>() { new VEHICLE() { VEHICLEID = 1 }, new VEHICLE() { VEHICLEID = 5 } }
//            };
//            DOC docPrevious = _retrievingEntityService.RetrieveCreatedEntity<DOC>(auditTrailsCreated);

//            //Act
//            DOC docActual = _retrievingEntityService.ApplyChangesToEntity(docPrevious, auditTrailsWithModification);

//            //Assert
//            Assert.AreEqual(docsExpected.INSPECTORDEVISION, docActual.INSPECTORDEVISION);
//            Assert.AreEqual(docsExpected.FIRSTNAME, docActual.FIRSTNAME);
//            Assert.AreEqual(docsExpected.PERIODSTARTDATE, docActual.PERIODSTARTDATE);
//            Assert.AreEqual(docsExpected.GOODSTOTALBRUTTO, docActual.GOODSTOTALBRUTTO);
//            Assert.AreEqual(docsExpected.VEHICLE.Count, docActual.VEHICLE.Count);
//        }

//        [Test, Order(5)]
//        public void RetrieveDeletedEntityTest()
//        {
//            //Arrange
//            DOC docsExpected = new DOC()
//            {
//                INSPECTORDEVISION = "ОТОиК №6",
//                FIRSTNAME = "Саша",
//                USERINFO = "administrator, Голуб Василий Иванович, инспектор, ОТОиК №7",
//                SID = "c1fa7deb - d31a - 4626 - be60 - 77292d0afae8",
//                CONSIGNEEID = 6,
//                TYPE = 0,
//                LAST_UPDATE = timeStampOfCreation,
//                PERIODSTARTDATE = new DateTime(2019, 8, 28),
//                PERIODSTARTTIME = new DateTime(2019, 8, 28, 18, 10, 0),
//                GOODSTOTALBRUTTO = 3005.50M,
//                TOTALWEIGHT = 15.125456M,
//                VEHICLE = new List<VEHICLE>() { new VEHICLE() { VEHICLEID = 1 }, new VEHICLE() { VEHICLEID = 5 } }
//            };

//            //Act
//            DOC docActual = _retrievingEntityService.RetrieveDeletedEntity<DOC>(auditTrailsWithDeletion);

//            //Assert
//            Assert.AreEqual(docsExpected.INSPECTORDEVISION, docActual.INSPECTORDEVISION);
//            Assert.AreEqual(docsExpected.FIRSTNAME, docActual.FIRSTNAME);
//            Assert.AreEqual(docsExpected.PERIODSTARTDATE, docActual.PERIODSTARTDATE);
//            Assert.AreEqual(docsExpected.GOODSTOTALBRUTTO, docActual.GOODSTOTALBRUTTO);
//            Assert.AreEqual(docsExpected.VEHICLE.Count, docActual.VEHICLE.Count);
//        }
//    }
//}

