﻿using Bogus;
using Focus.DAL.Common.DbModels;
using System;
using System.Collections.Generic;

namespace Focus.BL.Tests.FakeData
{
    /// <summary>
    /// Представляет набор сгенерированных данных разрешениях.
    /// </summary>
    public class FakePermission
    {
        /// <summary>
        /// Возвращает генератор данных разрешениях.
        /// </summary>
        public Faker<PERMISSION> PermissionFaker { get; }

        /// <summary>
        /// Возвращает коллекцию сгенерированных разрешений.
        /// </summary>
        public List<PERMISSION> Permissions { get; }

        /// <summary>
        /// Создает новый экземпляр сгенерированных разрешений.
        /// </summary>
        /// <param name="permissionsCount">Необходимое количество разрешений.</param>
        public FakePermission(int permissionsCount)
        {

            PermissionFaker = new Faker<PERMISSION>()
                .RuleFor(c => c.PERMISSIONID, f => (short)f.IndexFaker)
                .RuleFor(c => c.CUSTOMBROCKER, new FakeCustombrocker(2).Custombrockers.ToArray())
                .RuleFor(c => c.DATEFINISH, f => f.Date.Future(2, DateTime.Now.AddMonths(5)))
                .RuleFor(c => c.DATESTART, f => f.Date.Past(2, DateTime.Now.AddMonths(-2)))
                .RuleFor(c => c.ISVALID, f => Convert.ToByte(f.PickRandomParam(true, false)));

            

            Permissions = PermissionFaker.Generate(permissionsCount);



        }
    }
}
