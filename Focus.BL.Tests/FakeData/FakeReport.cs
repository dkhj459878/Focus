﻿using Bogus;
using Focus.DAL.Common.DbModels;
using System;
using System.Collections.Generic;

namespace Focus.BL.Tests.FakeData
{
    /// <summary>
    /// Представляет набор сгенерированных протоколированных данных о действиях пользователя.
    /// </summary>
    public class FakeReport
    {
        /// <summary>
        /// Возвращает генератор данных о протоколированных событиях.
        /// </summary>
        public Faker<REPORT> ReportFaker { get; }

        /// <summary>
        /// Возвращает коллекцию сгенерированных протоколированных событиях.
        /// </summary>
        public List<REPORT> Reports { get; }

        /// <summary>
        /// Создает новый экземпляр сгенерированного протоколированного события.
        /// </summary>
        /// <param name="reportsCount">Необходимое количество протоколированных событий.</param>
        public FakeReport(int reportsCount)
        {

            ReportFaker = new Faker<REPORT>()
                .RuleFor(c => c.REPORTID, f => (short)f.IndexFaker)
                .RuleFor(c => c.CUSTOMBROCKERID, f => Convert.ToInt16(f.IndexFaker))
                .RuleFor(c => c.FULLNAME, f => f.Person.FullName.ToString())
                .RuleFor(c => c.NUMBER_, f => f.Random.Digits(7).ToString())
                .RuleFor(c => c.PASSPORTNUMBER, f => f.Random.Chars(count: 2) + f.Random.Digits(7).ToString());
            

            Reports = ReportFaker.Generate(reportsCount);



        }
    }
}
