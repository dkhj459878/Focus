﻿using Bogus;
using Focus.DAL.Common.DbModels;
using System;
using System.Collections.Generic;

namespace Focus.BL.Tests.FakeData
{
    /// <summary>
    /// Представляет набор сгенерированных данных о таможенных представителях.
    /// </summary>
    public class FakeCustombrocker
    {
        /// <summary>
        /// Возвращает генератор данных о таможенных представителях.
        /// </summary>
        public Faker<CUSTOMBROCKER> CustombrockerFaker { get; }

        /// <summary>
        /// Возвращает коллекцию сгенерированных таможенных представителей.
        /// </summary>
        public List<CUSTOMBROCKER> Custombrockers { get; }

        /// <summary>
        /// Создает новый экземпляр сгенерированных таможенных представителей.
        /// </summary>
        /// <param name="custombrockersCount">Необходимое количество таможенных представителей.</param>
        public FakeCustombrocker(int custombrockersCount)
        {

            CustombrockerFaker = new Faker<CUSTOMBROCKER>()
                .RuleFor(c => c.CUSTOMBROCKERID, f => (short)f.IndexFaker)
                .RuleFor(c => c.ADDRESS, f => f.Address.FullAddress())
                .RuleFor(c => c.ADDRESS_FULL, f => f.Address.FullAddress())
                .RuleFor(c => c.BUILDING, f => f.Address.BuildingNumber())
                .RuleFor(c => c.STREET_TYPE, f => f.Address.StreetSuffix())
                .RuleFor(c => c.STREET, f => f.Address.StreetName())
                .RuleFor(c => c.COUNTRY, f => f.PickRandomParam(new FakeCountry(15).Countries.ToArray()))
                .RuleFor(c => c.BUILDING_BLOCK, f => f.Address.BuildingNumber())
                .RuleFor(c => c.LAST_UPDATE, f => f.Date.Past(2, DateTime.Now.AddMonths(-2)))
                .RuleFor(c => c.CITY, f => f.Address.City())
                .RuleFor(c => c.CITY_TYPE, f => f.Address.CityPrefix())
                .RuleFor(c => c.COUNTRY_ID, (f, c) => c.COUNTRY.ABBR2)
                .RuleFor(c => c.DISTRICT, f => f.Address.State())
                .RuleFor(c => c.EMAIL, f => f.Internet.Email())
                .RuleFor(c => c.FATHERSNAME, f => f.Person.FirstName)
                .RuleFor(c => c.LASTNAME, f => f.Person.LastName)
                .RuleFor(c => c.FATHERSNAME, f => f.Person.FirstName)
                .RuleFor(c => c.FAX, f => f.Person.Phone)
                .RuleFor(c => c.WORKPLACE, f => f.Person.Company.Name)

                .RuleFor(c => c.PASSPORTBODYISSUE, f => f.Company.CompanyName())
                .RuleFor(c => c.PASSPORTISSUED, f => f.Date.Past(2, DateTime.Now.AddMonths(-2)))
                .RuleFor(c => c.PASSPORTNUMBER, f => f.Lorem.Letter(2) + f.Random.Digits(7))
                .RuleFor(c => c.OFFICE, f => f.Address.SecondaryAddress())
                
                .RuleFor(c => c.WORKPLACE, f => f.Person.Company.Name)
                .RuleFor(c => c.POST, f => f.Random.Word())
                .RuleFor(c => c.POST_INDEX, f => f.Address.ZipCode());

            

            Custombrockers = CustombrockerFaker.Generate(custombrockersCount);



        }
    }
}
