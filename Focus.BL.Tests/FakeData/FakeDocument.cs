﻿using Bogus;
using Focus.BL.Tests.FakeData;
using Focus.DAL.Common.DbModels;
using System;
using System.Collections.Generic;

namespace Focus.BL.Tests.FakeData
{
    /// <summary>
    /// Представляет набор сгенерированных данных о документах, подтверждающих полномочия.
    /// </summary>
    public class FakeDocument
    {
        /// <summary>
        /// Возвращает генератор данных о документах, подтверждающих полномочия.
        /// </summary>
        public Faker<DOCUMENT> DocumentFaker { get; }

        /// <summary>
        /// Возвращает коллекцию сгенерированных документов, подтверждающих полномочия.
        /// </summary>
        public List<DOCUMENT> Documents { get; }

        /// <summary>
        /// Создает новый экземпляр сгенерированных документов, подтверждающих полномочия.
        /// </summary>
        /// <param name="documentsCount">Необходимое количество документов, подтверждающих полномочия.</param>
        public FakeDocument(int documentsCount)
        {

            DocumentFaker = new Faker<DOCUMENT>()
                .RuleFor(c => c.DOCUMENTID, f => (short)f.IndexFaker)
                .RuleFor(c => c.BODYISSUE, f => f.Company.CompanyName())
                .RuleFor(c => c.DATE_, f => f.Date.Past(2, DateTime.Now.AddMonths(-2)))
                .RuleFor(c => c.DOCUMENTKIND, f => f.PickRandomParam(new FakeDocumentkind(5).Documentkinds.ToArray()))
                .RuleFor(c => c.DOCUMENTKINDID, (f, c) => c.DOCUMENTKIND.DOCUMENTKINDID)
                .RuleFor(c => c.NUMBER_, f => f.Random.Digits(5).ToString())
                .RuleFor(c => c.ORGANIZATION, f => new FakeOrganization(5).Organizations.ToArray())
                .RuleFor(c => c.PERMISSION, f => new FakePermission(5).Permissions.ToArray())
                .RuleFor(c => c.LAST_UPDATE, f => f.Date.Past(2, DateTime.Now.AddMonths(-2)));

            

            Documents = DocumentFaker.Generate(documentsCount);



        }
    }
}
