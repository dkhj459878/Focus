﻿using Bogus;
using Focus.DAL.Common.DbModels;
using System.Collections.Generic;

namespace Focus.BL.Tests.FakeData
{
    /// <summary>
    /// Представляет набор сгенерированных данных о странах,
    /// в моделях домена
    /// </summary>
    public class FakeCountry
    {
        /// <summary>
        /// Возвращает генератор данных о странах
        /// </summary>
        public Faker<COUNTRY> CountryFaker { get; }

        /// <summary>
        /// Возвращает коллекцию сгенерированных стран
        /// </summary>
        public List<COUNTRY> Countries { get; }

        /// <summary>
        /// Создает новый экземпляр сгенерированных данных о странах
        /// </summary>
        /// <param name="countriesCount">Необходимое количество стран для генерации</param>
        public FakeCountry(int countriesCount)
        {

            var firstLetterArray = new string[] {"A", "B", "C", "D", "E", "F", "G", "H", "I", "L", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"  };
            var secondLetterArray = new string[] { "A", "B", "C", "D", "E", "F", "G", "H", "I", "L", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z" };

            var countryFakeCodes = new List<string>();


            foreach (var first in firstLetterArray)
            {
                foreach (var second in secondLetterArray)
                {
                    countryFakeCodes.Add(first + second);
                }
            }

            CountryFaker = new Faker<COUNTRY>()
                .RuleFor(c => c.ABBR2, f => countryFakeCodes[f.IndexFaker])    
                .RuleFor(c => c.NAME, f => f.Address.Country());

            Countries = CountryFaker.Generate(countriesCount);
        }
    }
}