﻿using Bogus;
using Focus.DAL.Common.DbModels;
using System;
using System.Collections.Generic;

namespace Focus.BL.Tests.FakeData
{
    /// <summary>
    /// Представляет набор сгенерированных данных о видах документов, подтверждающих полномочия..
    /// </summary>
    public class FakeDocumentkind
    {
        /// <summary>
        /// Возвращает генератор данных о видах документов, подтверждающих полномочия..
        /// </summary>
        public Faker<DOCUMENTKIND> DocumentkindFaker { get; }

        /// <summary>
        /// Возвращает коллекцию сгенерированных видов документов, подтверждающих полномочия.
        /// </summary>
        public List<DOCUMENTKIND> Documentkinds { get; }

        /// <summary>
        /// Создает новый экземпляр сгенерированных видов документов, подтверждающих полномочия.
        /// </summary>
        /// <param name="documentkindsCount">Необходимое количество видов документов, подтверждающих полномочия.</param>
        public FakeDocumentkind(int documentkindsCount)
        {

            Documentkinds = new List<DOCUMENTKIND>()
            { new DOCUMENTKIND() { DOCUMENTKINDID = 1, NAME= "ДОВЕРЕННОСТЬ" , LAST_UPDATE = DateTime.Now },
              new DOCUMENTKIND() { DOCUMENTKINDID = 2, NAME = "ПИСЬМО", LAST_UPDATE = DateTime.Now },
              new DOCUMENTKIND() { DOCUMENTKINDID = 3, NAME = "ПРИКАЗ", LAST_UPDATE = DateTime.Now },
              new DOCUMENTKIND() { DOCUMENTKINDID = 4, NAME = "ДОГОВОР ПОРУЧЕНИЯ", LAST_UPDATE = DateTime.Now },
           };
        }
    }
}
