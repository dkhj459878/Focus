﻿using Bogus;
using Focus.DAL.Common.DbModels;
using System;
using System.Collections.Generic;

namespace Focus.BL.Tests.FakeData
{
    /// <summary>
    /// Представляет набор сгенерированных данных о таможенных представителях.
    /// </summary>
    public class FakeDoc
    {
        /// <summary>
        /// Возвращает генератор данных о документах.
        /// </summary>
        public Faker<DOC> DocFaker { get; }

        /// <summary>
        /// Возвращает коллекцию сгенерированных документов.
        /// </summary>
        public List<DOC> Docs { get; }

        /// <summary>
        /// Создает новый экземпляр сгенерированных документов.
        /// </summary>
        /// <param name="docsCount">Необходимое количество документов.</param>
        public FakeDoc(int docsCount)
        {

            DocFaker = new Faker<DOC>()
                .RuleFor(c => c.DOCID, f => (short)f.IndexFaker)
                .RuleForType(typeof(string), f => f.Random.Word())
                .RuleForType(typeof(DateTime), f => f.Date.Past(2, DateTime.Now.AddMonths(-2)))
                .RuleForType(typeof(DateTime?), f => (DateTime?)f.Date.Past(2, DateTime.Now.AddMonths(-2)))
                .RuleForType(typeof(decimal), f => f.Random.Decimal(min: (decimal)0.35, max: (decimal)1500))
                .RuleForType(typeof(sbyte), f => f.PickRandomParam(new sbyte[] { (sbyte)0, (sbyte)1 }))
                .RuleForType(typeof(sbyte?), f => (sbyte?)f.PickRandomParam(new sbyte?[] { (sbyte)0, (sbyte)1 }))
                .RuleForType(typeof(short?), f => (short?)f.Random.Short())
                .RuleFor(d => d.CAR, f => new FakeCar(5).Cars)
                .RuleFor(d => d.CONTAINER, f => new FakeContainer(5).Containers)
                .RuleFor(d => d.DEVICE, f => new FakeDevice(5).Devices)
                .RuleFor(d => d.VEHICLE, f => new FakeVehicle(5).Vehicles)
                .RuleFor(d => d.CUSTOMBROCKER, f => f.PickRandomParam(new FakeCustombrocker(5).Custombrockers.ToArray()))
                .RuleFor(d => d.ORGANIZATION, f => f.PickRandomParam(new FakeOrganization(5).Organizations.ToArray()))
                .RuleFor(d => d.INSPECTOR, f => f.PickRandomParam(new FakeInspector(5).Inspectors.ToArray()));

            Docs = DocFaker.Generate(docsCount);



        }
    }
}
