﻿using Bogus;
using Focus.DAL.Common.DbModels;
using System;
using System.Collections.Generic;

namespace Focus.BL.Tests.FakeData
{
    /// <summary>
    /// Представляет набор сгенерированных данных о ТСТК
    /// </summary>
    public class FakeDevice
    {
        /// <summary>
        /// Возвращает генератор данных о ТСТК.
        /// </summary>
        public Faker<DEVICE> DeviceFaker { get; }

        /// <summary>
        /// Возвращает коллекцию сгенерированных ТСТК.
        /// </summary>
        public List<DEVICE> Devices { get; }

        /// <summary>
        /// Создает новый экземпляр сгенерированных ТСТК.
        /// </summary>
        public FakeDevice(int deviceCount)
        {
            DeviceFaker = new Faker<DEVICE>()
                .RuleFor(c => c.DEVICEID, f => (short)f.IndexFaker)
                .RuleFor(c => c.CALLIBRATION, f => f.Date.Past(2, DateTime.Now.AddMonths(-2)))
                .RuleFor(c => c.DEVICETYPE, f => f.PickRandomParam(new FakeDevicetype().Devicetypes.ToArray()))
                .RuleFor(c => c.DEVICETYPEID, (f, c) => c.DEVICETYPE.DEVICETYPEID)
                .RuleFor(c => c.MARK, f => f.Commerce.Categories(1)[0])
                .RuleFor(c => c.MODEL, f => f.Commerce.Categories(1)[0])
                .RuleFor(c => c.NAME, f => f.Lorem.Word())
                .RuleFor(c => c.PARAMETERSOFCAMERA, f => f.Lorem.Word())
                .RuleFor(c => c.PARAMETERSOFSCALES, f => f.Lorem.Word())
                .RuleFor(c => c.PARAMETERSXRAYCOMPLEXES, f => f.Lorem.Word())
                .RuleFor(c => c.PHONE, f => f.Person.Phone)
                .RuleFor(c => c.SERIALNUMBER, f => f.Random.Digits(3).ToString());

            Devices = DeviceFaker.Generate(deviceCount);

        }
    }
}