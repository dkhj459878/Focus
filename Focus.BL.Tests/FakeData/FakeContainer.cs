﻿using Bogus;
using Focus.DAL.Common.DbModels;
using System;
using System.Collections.Generic;

namespace Focus.BL.Tests.FakeData
{
    /// <summary>
    /// Представляет набор сгенерированных данных об автомобилях.
    /// </summary>
    public class FakeContainer
    {
        /// <summary>
        /// Возвращает генератор данных об автомобилях.
        /// </summary>
        public Faker<CONTAINER> ContainerFaker { get; }

        /// <summary>
        /// Возвращает коллекцию сгенерированных автомобилей.
        /// </summary>
        public List<CONTAINER> Containers { get; }

        /// <summary>
        /// Создает новый экземпляр сгенерированных автомобилей.
        /// </summary>
        /// <param name="containersCount">Необходимое количество автомобилей.</param>
        public FakeContainer(int containersCount)
        {
            
            ContainerFaker = new Faker<CONTAINER>()
                .RuleFor(c => c.CONTAINERID, f => (short)f.IndexFaker)
                .RuleFor(c => c.COUNTRY, f => f.PickRandomParam(new FakeCountry(15).Countries.ToArray()))
                .RuleFor(c => c.COUNTRYCODE, (f, c) => c.COUNTRY.ABBR2)
                .RuleFor(c => c.DEMENSIONS, f => f.Random.Word())
                .RuleFor(c => c.NAME, f => f.Random.Word())
                .RuleFor(c => c.REGNUMBER, f => f.Random.Word())
                .RuleFor(c => c.LAST_UPDATE, f => f.Date.Past(2, DateTime.Now.AddMonths(-2)));

            Containers = ContainerFaker.Generate(containersCount);



        }
    }
}
