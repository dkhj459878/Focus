﻿using Focus.DAL.Common.DbModels;
using System.Collections.Generic;

namespace Focus.BL.Tests.FakeData
{
    /// <summary>
    /// Представляет набор сгенерированных данных о транспортных средствах.
    /// </summary>
    public class FakeVehicletype
    {

        /// <summary>
        /// Возвращает коллекцию сгенерированных транспортных средств.
        /// </summary>
        public List<VEHICLETYPE> Vehicletypes { get; }

        /// <summary>
        /// Создает новый экземпляр сгенерированных транспортных средств.
        /// </summary>
        public FakeVehicletype()
        {
            Vehicletypes = new List<VEHICLETYPE>()
                {new VEHICLETYPE() { VEHICLETYPEID = 1, NAME = "ЛЕГКОВАЯ" },
                {new VEHICLETYPE() { VEHICLETYPEID = 2, NAME = "ГРУЗОВАЯ" } }
           };
        }
    }
}