﻿using Bogus;
using Focus.DAL.Common.DbModels;
using System;
using System.Collections.Generic;

namespace Focus.BL.Tests.FakeData
{
    /// <summary>
    /// Представляет набор сгенерированных данных о ПТО.
    /// </summary>
    public class FakePost
    {
        /// <summary>
        /// Возвращает генератор данных о ПТО.
        /// </summary>
        public Faker<POST> PostFaker { get; }

        /// <summary>
        /// Возвращает коллекцию сгенерированных ПТО.
        /// </summary>
        public List<POST> Posts { get; }

        /// <summary>
        /// Создает новый экземпляр сгенерированных ПТО.
        /// </summary>
        /// <param name="postCount">Необходимое количество ПТО.</param>
        public FakePost(int postsCount)
        {
            Posts = new List<POST>()
                {new POST() { ID = 1, CODE = "06650", NAME = "Минск-Белтаможсервис-2", DEVISION  = "ОТОиК №1", ADDRESS = "Минский р-н, Сенницкий с/с, р-н д. Щитомиричи",  LAST_UPDATE = DateTime.Now },
                 new POST() { ID = 2, CODE = "06544", NAME = "Белювелирторг", DEVISION  = "ОТОиК №1", ADDRESS = "г. Минск, ул. Смоленская, 33", LAST_UPDATE = DateTime.Now},
                 new POST() { ID = 3, CODE = "06505", NAME = "Минская почта", DEVISION  = "ОТОиК №2", ADDRESS = "г. Минск, ул. Вокзальная, 22", LAST_UPDATE = DateTime.Now },
                 new POST() { ID = 4, CODE = "06533", NAME = "Минск-СЭЗ", DEVISION  = "ОТОиК №3", ADDRESS = "г. Минск, ул. Вокзальная, 22", LAST_UPDATE = DateTime.Now },
                 new POST() { ID = 5, CODE = "06542", NAME = "Кунцевщина", DEVISION  = "ОТОиК №4", ADDRESS = "г. Минск, ул. Брикета, 25", LAST_UPDATE = DateTime.Now },
                 new POST() { ID = 6, CODE = "06529", NAME = "Колядичи-авто", DEVISION  = "ОТОиК №5", ADDRESS = "г. Минск, ул. Бабушкина, 39 (промузел Колядичи)", LAST_UPDATE = DateTime.Now },
                 new POST() { ID = 7, CODE = "06503", NAME = "Степянка", DEVISION  = "ОТОиК №6", ADDRESS = "г. Минск, ул. Анаева, 84", LAST_UPDATE = DateTime.Now },
                 new POST() { ID = 8, CODE = "06604", NAME = "Колядичи", DEVISION  = "ОТОиК №6", ADDRESS = "Минский р-н, южнее д. Колядичи, ж/д ст. Колядичи", LAST_UPDATE = DateTime.Now },
                 new POST() { ID = 9, CODE = "06649", NAME = "Белтаможсервис-ТЛЦ", DEVISION  = "ОТОиК №7", ADDRESS = "Минский р-н, 17-й км. дороги Р-1 Минск-Дзержинск", LAST_UPDATE = DateTime.Now },
                 new POST() { ID = 10, CODE = "06611", NAME = "Белкультторг", DEVISION  = "ОТОиК №8", ADDRESS = "Минский р-н, пос. Озерцо", LAST_UPDATE = DateTime.Now },
                 new POST() { ID = 11, CODE = "06601", NAME = "Молодечно", DEVISION  = "Молодечненский таможенный пост, ОТОиК №1", ADDRESS = "г. Молодечно, ул. Вокзальная,7", LAST_UPDATE = DateTime.Now },
                 new POST() { ID = 12, CODE = "06648", NAME = "Молодечно-Белтамож-сервис", DEVISION  = "Молодечненский таможенный пост, ОТОиК №2", ADDRESS = "г. Молодечно, ул. Тамары Дудко, 2", LAST_UPDATE = DateTime.Now },
                 new POST() { ID = 13, CODE = "06613", NAME = "БелАЗ", DEVISION  = "Борисовский таможенный пост", ADDRESS = "г. Жодино, р-н улицы Богуслав поле, 2", LAST_UPDATE = DateTime.Now },
                 new POST() { ID = 14, CODE = "06608", NAME = "Борисов-Авто", DEVISION  = "Борисовский таможенный пост", ADDRESS = "г. Борисов,  ул. Гагарина, 105", LAST_UPDATE = DateTime.Now },
                 new POST() { ID = 15, CODE = "06641", NAME = "Солигорск", DEVISION  = "Солигорский таможенный пост", ADDRESS = "г. Солигорск ст. Калий-1 база Райагропромснаб", LAST_UPDATE = DateTime.Now }
             };
        }
    }
}
