﻿using FluentAssertions;
using Focus.BL.Common.Services;
using Focus.BL.Services;
using Focus.BL.Tests.FakeData;
using Focus.DAL.Common.Repositories;
using Focus.DAL.Common.UoW;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Focus.DAL.Common.DbModels;

namespace Focus.BL.Tests.Inspector
{
    /// <summary>
    /// Unit-тесты для сервиса <see cref="InspectorService"/>
    /// </summary>
    [TestFixture]
    public class InspectorServiceTests
    {
        private const int InspectorsCount = 30;
        private IInspectorService _inspectorService;
        private FakeInspector _fakeInspectors;
        private Mock<IDbInspectorRepository> mockinspectorRepository;

        /// <summary>
        /// Осуществляет настройки
        /// </summary>
        [OneTimeSetUp]
        public void SetUp()
        {


            _fakeInspectors = new FakeInspector(InspectorsCount);

            mockinspectorRepository = new Mock<IDbInspectorRepository>();
            mockinspectorRepository
                .Setup(mock => mock.Get(It.IsAny<Expression<Func<INSPECTOR, bool>>>(), null, It.IsAny<string>()))
                .Returns((Expression<Func<INSPECTOR, bool>> filter,
                    Func<IQueryable<INSPECTOR>, IOrderedQueryable<INSPECTOR>> orderBy,
                    string includeProperties) => _fakeInspectors.Inspectors.Where(filter.Compile()));
            mockinspectorRepository
                .Setup(mock => mock.Get(null, null, It.IsAny<string>()))
                .Returns((Expression<Func<INSPECTOR, bool>> filter,
                    Func<IQueryable<INSPECTOR>, IOrderedQueryable<INSPECTOR>> orderBy,
                    string includeProperties) => _fakeInspectors.Inspectors);
            mockinspectorRepository
                .Setup(mock => mock.GetPage(It.IsAny<int>(), It.IsAny<int>(), null, null, ""))
                .Returns((int pageSize,
                    int pageNumber,
                    Expression<Func<INSPECTOR, bool>> filter,
                    Func<IQueryable<INSPECTOR>, IOrderedQueryable<INSPECTOR>> orderBy,
                    string includeProperties) => _fakeInspectors.Inspectors.Skip((pageNumber - 1) * pageSize).Take(pageSize));
            mockinspectorRepository.Setup(mock => mock.GetById(It.IsAny<short>()))
                .Returns((short id) => _fakeInspectors.Inspectors.Find(f => f.ID == id));
            mockinspectorRepository.Setup(mock => mock.Insert(It.IsAny<INSPECTOR>()))
                .Callback((INSPECTOR f) => _fakeInspectors.Inspectors.Add(f));
            mockinspectorRepository.Setup(mock => mock.Update(It.IsAny<INSPECTOR>()))
                .Callback((INSPECTOR f) => _fakeInspectors.Inspectors.Find(d => d.ID == f.ID).FIRSTNAME = f.FIRSTNAME);
            mockinspectorRepository.Setup(mock => mock.Delete(It.IsAny<short>()))
                .Callback((short id) => _fakeInspectors.Inspectors
                    .Remove(_fakeInspectors.Inspectors.Find(f => f.ID == id)));

            var mockinspectorUnitOfWork = new Mock<IUnitOfWork>();
            mockinspectorUnitOfWork.Setup(mock => mock.InspectorRepository)
                .Returns(() => mockinspectorRepository.Object); //todo - закоментил, не знаю в чем дело

            _inspectorService = new InspectorService(mockinspectorUnitOfWork.Object);
        }

        /// <summary>
        /// Тест метода получения экземпляров инспекторов
        /// выбранной инспекторовицы пейджинга. Выбрана может быть любая инспекторовица.
        /// </summary>
        [Test, Order(1)]
        public void GetExistedInspectorsPageTest([Random(1, 5, 1)]int pageNumber)
        {
            const int pagesize = 3;

            // Arrange
            var inspectors = _fakeInspectors.Inspectors;

            var inspectorsPageIndexes = new List<int>();

            var startInspectorIndexInPage = pagesize * (pageNumber - 1);

            var finishInspectorIndexInPage = startInspectorIndexInPage + pagesize - 1;

            for (var i = startInspectorIndexInPage; i <= finishInspectorIndexInPage; i++)
            {
                inspectorsPageIndexes.Add(i);
            }

            List<INSPECTOR> inspectorsPage;
            inspectorsPage = inspectorsPageIndexes.Select(inspectorsPageIndex => inspectors[inspectorsPageIndex]).ToList();


            // Act

            var actualInspectorsPage = _inspectorService.GetInspectorsPage(pageSize: pagesize, pageNumber: pageNumber).ToList();

            // Assert
            inspectorsPage.Should().BeEquivalentTo(actualInspectorsPage,
                options => options.ExcludingMissingMembers());
        }

        /// <summary>
        /// Тест метода получения экземпляров инспекторов
        /// выбранной инспекторовицы пейджинга. Выбрана последняя инспекторовица.
        /// </summary>
        [Test, Order(1)]
        public void GetExistedInspectorsLastPageTest()
        {
            const int pagesize = 19;
            const int pageNumber = 2;

            // Arrange
            var dbInspectors = _fakeInspectors.Inspectors;

            var inspectorsPageIndexes = new List<int>();

            var startInspectorIndexInPage = pagesize * (pageNumber - 1);

            var finishInspectorIndexInPage = startInspectorIndexInPage + InspectorsCount % pagesize - 1;

            for (var i = startInspectorIndexInPage; i <= finishInspectorIndexInPage; i++)
            {
                inspectorsPageIndexes.Add(i);
            }

            var dbInspectorsPage = inspectorsPageIndexes.Select(inspectorsPageIndex => dbInspectors[inspectorsPageIndex]).ToList();

            // Act
            var actualInspectorsPage = _inspectorService.GetInspectorsPage(pageSize: pagesize, pageNumber: pageNumber).ToList();

            // Assert
            dbInspectorsPage.Should().BeEquivalentTo(actualInspectorsPage,
                options => options.ExcludingMissingMembers());
        }

        /// <summary>
        /// Тест метода получения экземпляров инспекторов
        /// выбранной инспекторовицы пейджинга. Номер инспекторовицы отрицательный.
        /// </summary>
        [Test, Order(1)]
        public void GetNotExistedInspectorsPageWithNegativePageNumbersTest([Random(-100500, -1, 5)] int pageNumber)
        {
            const int pagesize = 16;

            // Act
            ICollection<INSPECTOR> actualInspectorsPage = _inspectorService.GetInspectorsPage(pageSize: pagesize, pageNumber: pageNumber).ToList();

            ICollection<INSPECTOR> expectedPage = _inspectorService.GetInspectorsPage(pageSize: pagesize, pageNumber: 1).ToList();

            // Assert
            actualInspectorsPage.Should().BeEquivalentTo(expectedPage,
                options => options.ExcludingMissingMembers());
        }

        /// <summary>
        /// Тест метода получения экземпляров инспекторов
        /// выбранной инспекторовицы пейджинга. Номер инспекторовицы больше общего числа инспекторовиц.
        /// </summary>
        [Test, Order(1)]
        public void GetNotExistedInspectorsPageWithValueIfPageNumbersMoreThanTotalInspectorsPagesTest([Random(InspectorsCount, InspectorsCount + 100500, 5)]int pageNumber)
        {
            const int pagesize = 16;

            // Act
            ICollection<INSPECTOR> actualInspectorsPage = _inspectorService.GetInspectorsPage(pageSize: pagesize, pageNumber: pageNumber).ToList();

            // Assert
            actualInspectorsPage.Should().BeEmpty();
        }

        /// <summary>
        /// Тест метода получения экземпляров инспекторов
        /// ввыбранной инспекторовицы пейджинга из пустого репозитария.
        /// </summary>
        [Test, Order(5)]
        public void GetNotExistedInspectorsPageFromEmptyRepositoryTest([Random(1, 18, 5)]int pageNumber)
        {
            const int pagesize = 16;

            // Arrange
            _fakeInspectors.Inspectors.Clear();

            // Act
            ICollection<INSPECTOR> actualInspectorsPage = _inspectorService.GetInspectorsPage(pageSize: pagesize, pageNumber: pageNumber).ToList();

            // Assert
            actualInspectorsPage.Should().BeEmpty();
        }

        /// <summary>
        /// Тест метода получения всех экземпляров инспекторов
        /// в пустом репозитарии.
        /// </summary>
        [Test, Order(5)]
        public void GetAllInspectorsFromEmptyRepositoryTest()
        {
            // Arrange
            _fakeInspectors.Inspectors.Clear();

            // Act
            var actualInspectors = _inspectorService.GetAllInspectors(filter: null);

            // Assert
            actualInspectors.Should().BeEmpty();
        }

        /// <summary>
        /// Тест метода получения всех экземпляров инспекторов
        /// в не пустом репозитарии.
        /// </summary>
        [Test, Order(1)]
        public void GetAllExistedInspectorsTest()
        {
            // Arrange
            var expectedInspectors = _fakeInspectors.Inspectors;

            // Act
            var actualInspectors = _inspectorService.GetAllInspectors().ToList();

            // Assert
            //foreach (var expectedInspector in expectedInspectors)
            //{
            //    expectedInspector.ID.Should().Be((actualInspectors.Find(c => c.ID == expectedInspector.ID).ID));
            //    expectedInspector.FIRSTNAME.Should().Be((actualInspectors.Find(c => c.ID == expectedInspector.ID).FIRSTNAME));
            //}

            expectedInspectors.Should().BeEquivalentTo(actualInspectors,
                options => options.ExcludingMissingMembers());

            expectedInspectors.Count.Should().Be(actualInspectors.Count);
        }

        /// <summary>
        /// Тест метода получения экземпляра инспектора по заданному идентификатору.
        /// инспектор с передаваемым идентификатором существует
        /// </summary>
        /// <param name="id">Идентификатор инспектора</param>
        [Test, Order(1)]
        public void GetExistedInspectorTest()
        {
            // Arrange
            var inspectorId = _fakeInspectors.Inspectors.ToArray()[(new Random()).Next(InspectorsCount)].ID;


            var expectedInspector = _fakeInspectors.Inspectors.Find(f => f.ID == inspectorId);

            // Act
            var actualInspector = _inspectorService.GetInspector(inspectorId);

            // Assert


            actualInspector.Should().BeEquivalentTo(expectedInspector,
                options => options.ExcludingMissingMembers());
        }

        /// <summary>
        /// Тест метода получения экземпляра инспектора по заданному идентификатору.
        /// Контейнер с передаваемым идентификатором не существует.
        /// </summary>
        /// <param name="id">Идентификатор инспектора</param>
        [Test, Order(1)]
        public void GetNotExistedInspectorTest([Random(InspectorsCount, InspectorsCount + InspectorsCount, 5)]
                    short id)
        {
            var actualInspector = _inspectorService.GetInspector(id);

            actualInspector.Should().BeNull();
        }

        /// <summary>
        /// Тест метода добавления инспектора в хранилище данных
        /// </summary>
        [Test, Repeat(5), Order(2)]
        public void AddInspectorTest()
        {

            var inspectorId = _fakeInspectors.Inspectors.ToArray()[(new Random()).Next(InspectorsCount)].ID;
                        
            // Arrange
            var inspectorsFake = _fakeInspectors.Inspectors;

            var inspectorFake = inspectorsFake.Where(c => c.ID == inspectorId).FirstOrDefault();
            

            // Act

            var actualInspector = _inspectorService.GetInspector(inspectorId);

            // Assert

            actualInspector.Should().BeEquivalentTo(actualInspector,
                options => options.ExcludingMissingMembers());
        }

        /// <summary>
        /// Тест метода обновления данных о инспекторе
        /// </summary>
        /// <param name="id">Идентификатор инспектора для обновления</param>
        [Test, Order(2)]
        public void UpdateInspectorTest()
        {
            // Arrange
            var inspectorId = _fakeInspectors.Inspectors.ToArray()[(new Random()).Next(InspectorsCount)].ID;

            var inspector = _inspectorService.GetInspector(inspectorId);
            inspector.FIRSTNAME = new Bogus.Faker().Random.AlphaNumeric(17);

            // Act
            _inspectorService.UpdateInspector(inspector);
            var actualInspector = _fakeInspectors.Inspectors.Find(f => f.ID == inspectorId);

            // Assert
            actualInspector.FIRSTNAME.Should().Be(inspector.FIRSTNAME);
        }

        /// <summary>
        /// Тест метода удаления инспектора из хранилища данных
        /// </summary>
        /// <param name="id">Идентификатор инспектора для удаления</param>
        [Test, Order(3)]
        public void DeleteInspectorTest()
        {
            // Arrange

            var inspectorId = _fakeInspectors.Inspectors.ToArray()[(new Random()).Next(InspectorsCount)].ID;

            _inspectorService.DeleteInspector(inspectorId);

            // Act
            var actualInspector = _fakeInspectors.Inspectors.Find(inspector => inspector.ID == inspectorId);

            // Assert
            actualInspector.Should().BeNull();
        }

        /// <summary>
        /// Тест метода проверки наличия инспектора в хранилище данных.
        /// инспектор существует
        /// </summary>
        /// <param name="id">Идентификатор инспектора для поиска</param>
        [Test, Order(2)]
        public void InspectorExistsTest()
        {
            // Arrange

            var inspectorId = _fakeInspectors.Inspectors.ToArray()[(new Random()).Next(InspectorsCount)].ID;

            var loginToFind = _fakeInspectors.Inspectors.Find(dbf => dbf.ID == inspectorId).FIRSTNAME;

            // Act
            var actual = _inspectorService
                .InspectorExists(f => f.FIRSTNAME == loginToFind);

            // Assert
            actual.Should().BeTrue();
        }

        /// <summary>
        /// Тест метода проверки наличия инспектора в хранилище данных.
        /// инспектор не существует
        /// </summary>
        /// <param name="login">Логин инспектора для поиска</param>
        [Test, Order(2)]
        public void InspectorNotExistsTest([Values("shameonyou", "qwer", "", "befastandclearver", "strangelogin")]
                    string VIN)
        {
            var actual = _inspectorService
                .InspectorExists(f => f.FIRSTNAME == VIN);

            actual.Should().BeFalse();
        }
    }
}

