﻿using FluentAssertions;
using Focus.BL.Common.Services;
using Focus.BL.Services;
using Focus.BL.Tests.FakeData;
using Focus.DAL.Common.DbModels;
using Focus.DAL.Common.Repositories;
using Focus.DAL.Common.UoW;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Focus.BL.Tests.Car
{
    /// <summary>
    /// Unit-тесты для сервиса <see cref="CarService"/>
    /// </summary>
    [TestFixture]
    public class CarServiceTests
    {
        private const int CarsCount = 300;
        private ICarService _carService;
        private FakeCar _fakeCars;
        private Mock<IDbCarRepository> mockcarRepository;

        /// <summary>
        /// Осуществляет настройки
        /// </summary>
        [OneTimeSetUp]
        public void SetUp()
        {


            _fakeCars = new FakeCar(CarsCount);

            mockcarRepository = new Mock<IDbCarRepository>();
            mockcarRepository
                .Setup(mock => mock.Get(It.IsAny<Expression<Func<CAR, bool>>>(), null, It.IsAny<string>()))
                .Returns((Expression<Func<CAR, bool>> filter,
                    Func<IQueryable<CAR>, IOrderedQueryable<CAR>> orderBy,
                    string includeProperties) => _fakeCars.Cars.Where(filter.Compile()));
            mockcarRepository
                .Setup(mock => mock.Get(null, null, It.IsAny<string>()))
                .Returns((Expression<Func<CAR, bool>> filter,
                    Func<IQueryable<CAR>, IOrderedQueryable<CAR>> orderBy,
                    string includeProperties) => _fakeCars.Cars);
            mockcarRepository
                .Setup(mock => mock.GetPage(It.IsAny<int>(), It.IsAny<int>(), null, null, ""))
                .Returns((int pageSize,
                    int pageNumber,
                    Expression<Func<CAR, bool>> filter,
                    Func<IQueryable<CAR>, IOrderedQueryable<CAR>> orderBy,
                    string includeProperties) => _fakeCars.Cars.Skip((pageNumber - 1) * pageSize).Take(pageSize));
            mockcarRepository.Setup(mock => mock.GetById(It.IsAny<short>()))
                .Returns((short id) => _fakeCars.Cars.Find(f => f.CARID == id));
            mockcarRepository.Setup(mock => mock.Insert(It.IsAny<CAR>()))
                .Callback((CAR f) => _fakeCars.Cars.Add(f));
            mockcarRepository.Setup(mock => mock.Update(It.IsAny<CAR>()))
                .Callback((CAR f) => _fakeCars.Cars.Find(D => D.CARID == f.CARID).VIN = f.VIN);
            mockcarRepository.Setup(mock => mock.Delete(It.IsAny<short>()))
                .Callback((short id) => _fakeCars.Cars
                    .Remove(_fakeCars.Cars.Find(f => f.CARID == id)));

            var mockcarUnitOfWork = new Mock<IUnitOfWork>();
            mockcarUnitOfWork.Setup(mock => mock.CarRepository)
                .Returns(() => mockcarRepository.Object); //todo - закоментил, не знаю в чем дело

            _carService = new CarService(mockcarUnitOfWork.Object);
        }

        /// <summary>
        /// Тест метода получения экземпляров автомобилей
        /// выбранной страницы пейджинга. Выбрана может быть любая страница.
        /// </summary>
        [Test, Order(1)]
        public void GetExistedCarsPageTest([Random(1, 14, 1)]int pageNumber)
        {
            const int pagesize = 16;

            // Arrange
            var cars = _fakeCars.Cars;

            var carsPageIndexes = new List<int>();

            var startCarIndexInPage = pagesize * (pageNumber - 1);

            var finishCarIndexInPage = startCarIndexInPage + pagesize - 1;

            for (var i = startCarIndexInPage; i <= finishCarIndexInPage; i++)
            {
                carsPageIndexes.Add(i);
            }

            List<CAR> carsPage;
            carsPage = carsPageIndexes.Select(carsPageIndex => cars[carsPageIndex]).ToList();


            // Act

            var actualCarsPage = _carService.GetCarsPage(pageSize: pagesize, pageNumber: pageNumber).ToList();

            // Assert
            carsPage.Should().BeEquivalentTo(actualCarsPage,
                options => options.ExcludingMissingMembers());
        }

        /// <summary>
        /// Тест метода получения экземпляров автомобилей
        /// выбранной страницы пейджинга. Выбрана последняя страница.
        /// </summary>
        [Test, Order(1)]
        public void GetExistedCarsLastPageTest()
        {
            const int pagesize = 16;
            const int pageNumber = 19;

            // Arrange
            var dbCars = _fakeCars.Cars;

            var carsPageIndexes = new List<int>();

            var startCarIndexInPage = pagesize * (pageNumber - 1);

            var finishCarIndexInPage = startCarIndexInPage + CarsCount % pagesize - 1;

            for (var i = startCarIndexInPage; i <= finishCarIndexInPage; i++)
            {
                carsPageIndexes.Add(i);
            }

            var dbCarsPage = carsPageIndexes.Select(carsPageIndex => dbCars[carsPageIndex]).ToList();

            // Act
            var actualCarsPage = _carService.GetCarsPage(pageSize: pagesize, pageNumber: pageNumber).ToList();

            // Assert
            dbCarsPage.Should().BeEquivalentTo(actualCarsPage,
                options => options.ExcludingMissingMembers());
        }

        /// <summary>
        /// Тест метода получения экземпляров автомобилей
        /// выбранной страницы пейджинга. Номер страницы отрицательный.
        /// </summary>
        [Test, Order(1)]
        public void GetNotExistedCarsPageWithNegativePageNumbersTest([Random(-100500, -1, 5)] int pageNumber)
        {
            const int pagesize = 16;

            // Act
            ICollection<CAR> actualCarsPage = _carService.GetCarsPage(pageSize: pagesize, pageNumber: pageNumber).ToList();

            ICollection<CAR> expectedPage = _carService.GetCarsPage(pageSize: pagesize, pageNumber: 1).ToList();

            // Assert
            actualCarsPage.Should().BeEquivalentTo(expectedPage,
                options => options.ExcludingMissingMembers());
        }

        /// <summary>
        /// Тест метода получения экземпляров автомобилей
        /// выбранной страницы пейджинга. Номер страницы больше общего числа страниц.
        /// </summary>
        [Test, Order(1)]
        public void GetNotExistedCarsPageWithValueIfPageNumbersMoreThanTotalCarsPagesTest([Random(CarsCount, CarsCount + 100500, 5)]int pageNumber)
        {
            const int pagesize = 16;

            // Act
            ICollection<CAR> actualCarsPage = _carService.GetCarsPage(pageSize: pagesize, pageNumber: pageNumber).ToList();

            // Assert
            actualCarsPage.Should().BeEmpty();
        }

        /// <summary>
        /// Тест метода получения экземпляров автомобилей
        /// ввыбранной страницы пейджинга из пустого репозитария.
        /// </summary>
        [Test, Order(5)]
        public void GetNotExistedCarsPageFromEmptyRepositoryTest([Random(1, 18, 5)]int pageNumber)
        {
            const int pagesize = 16;

            // Arrange
            _fakeCars.Cars.Clear();

            // Act
            ICollection<CAR> actualCarsPage = _carService.GetCarsPage(pageSize: pagesize, pageNumber: pageNumber).ToList();

            // Assert
            actualCarsPage.Should().BeEmpty();
        }

        /// <summary>
        /// Тест метода получения всех экземпляров автомобилей
        /// в пустом репозитарии.
        /// </summary>
        [Test, Order(5)]
        public void GetAllCarsFromEmptyRepositoryTest()
        {
            // Arrange
            _fakeCars.Cars.Clear();

            // Act
            var actualCars = _carService.GetAllCars(filter: null);

            // Assert
            actualCars.Should().BeEmpty();
        }

        /// <summary>
        /// Тест метода получения всех экземпляров автомобилей
        /// в не пустом репозитарии.
        /// </summary>
        [Test, Order(1)]
        public void GetAllExistedCarsTest()
        {
            // Arrange
            var expectedCars = _fakeCars.Cars;

            // Act
            var actualCars = _carService.GetAllCars().ToList();

            // Assert
            foreach (var expectedCar in expectedCars)
            {
                var i = expectedCar.CARID;

                expectedCars[i].COUNTRYCODE.Should().Be(actualCars[i].COUNTRYCODE);
                expectedCars[i].VIN.Should().Be(actualCars[i].VIN);
            }

            expectedCars.Should().BeEquivalentTo(actualCars,
                options => options.ExcludingMissingMembers());

            expectedCars.Count.Should().Be(actualCars.Count);
        }

        /// <summary>
        /// Тест метода получения экземпляра автомобиля по заданному идентификатору.
        /// автомобиль с передаваемым идентификатором существует
        /// </summary>
        /// <param name="id">Идентификатор автомобиля</param>
        [Test, Order(1)]
        public void GetExistedCarTest([Random(0, CarsCount - 1, 5)] short id)
        {
            // Arrange
            var expectedCar = _fakeCars.Cars.Find(f => f.CARID == id);

            // Act
            var actualCar = _carService.GetCar(id);

            // Assert


            actualCar.Should().BeEquivalentTo(expectedCar,
                options => options.ExcludingMissingMembers());
        }

        /// <summary>
        /// Тест метода получения экземпляра автомобиля по заданному идентификатору.
        /// автомобиль с передаваемым идентификатором не существует
        /// </summary>
        /// <param name="id">Идентификатор автомобиля</param>
        [Test, Order(1)]
        public void GetNotExistedCarTest([Random(CarsCount, CarsCount + 300, 5)]
                    short id)
        {
            var actualCar = _carService.GetCar(id);

            actualCar.Should().BeNull();
        }

        /// <summary>
        /// Тест метода добавления автомобиля в хранилище данных
        /// </summary>
        [Test, Repeat(5), Order(2)]
        public void AddCarTest()
        {

            short carId = 15;
            
            // Arrange
            var carsFake = _fakeCars.Cars;

            var carFake = carsFake.Where(c => c.CARID == carId).FirstOrDefault();
            

            // Act

            var actualCar = _carService.GetCar(carId);

            // Assert

            actualCar.Should().BeEquivalentTo(actualCar,
                options => options.ExcludingMissingMembers());
        }

        /// <summary>
        /// Тест метода добавления автомобиля в хранилище данных
        /// </summary>
        [Test, Repeat(5), Order(2)]
        public void AddCarIntegratedTest()
        {
            //IUnitOfWork _unitOfWork;

            //// Arrange
            //var car = new FakeCars(1, false, false, false, false, 5, 5).Cars[0];
            //var carManagementService = new CarManagementService(_unitOfWork);
            //car.Id = _fakeCars.Cars.Last().Id + 1;

            //// Act
            //var actualId = _carService.AddCar(car);

            //var actualCar = _carService.GetCar(actualId);

            //// Assert
            //actualCar.CarDetail.EMailAddresses.Should().BeEquivalentTo(car.CarDetail.EMailAddresses,
            //    options => options.ExcludingMissingMembers());
            //actualCar.Login.Should().BeEquivalentTo(car.Login);
            //actualCar.FirstName.Should().BeEquivalentTo(car.FirstName);
            //actualCar.LastName.Should().BeEquivalentTo(car.LastName);
        }

        /// <summary>
        /// Тест метода обновления данных об автомобиле
        /// </summary>
        /// <param name="id">Идентификатор автомобиля для обновления</param>
        [Test, Order(2)]
        public void UpdateCarTest([Random(0, CarsCount - 1, 5)] short id)
        {
            // Arrange
            var car = _carService.GetCar(id);
            car.VIN = new Bogus.Faker().Random.AlphaNumeric(17);

            // Act
            _carService.UpdateCar(car);
            var actualCar = _fakeCars.Cars.Find(f => f.CARID == id);

            // Assert
            actualCar.VIN.Should().Be(car.VIN);
        }

        /// <summary>
        /// Тест метода удаления автомобиля из хранилища данных
        /// </summary>
        /// <param name="id">Идентификатор автомобиля для удаления</param>
        [Test, Order(3)]
        public void DeleteCarTest([Random(0, CarsCount - 1, 5)] short id)
        {
            // Arrange
            _carService.DeleteCar(id);

            // Act
            var actualCar = _fakeCars.Cars.Find(car => car.CARID == id);

            // Assert
            actualCar.Should().BeNull();
        }

        /// <summary>
        /// Тест метода проверки наличия автомобиля в хранилище данных.
        /// автомобиль существует
        /// </summary>
        /// <param name="id">Идентификатор автомобиля для поиска</param>
        [Test, Order(2)]
        public void CarExistsTest([Random(0, CarsCount - 1, 5)] short id)
        {
            // Arrange
            var loginToFind = _fakeCars.Cars.Find(dbf => dbf.CARID == id).VIN;

            // Act
            var actual = _carService
                .CarExists(f => f.VIN == loginToFind);

            // Assert
            actual.Should().BeTrue();
        }

        /// <summary>
        /// Тест метода проверки наличия автомобиля в хранилище данных.
        /// автомобиль не существует
        /// </summary>
        /// <param name="login">Логин автомобиля для поиска</param>
        [Test, Order(2)]
        public void CarNotExistsTest([Values("shameonyou", "qwer", "", "befastandclearver", "strangelogin")]
                    string VIN)
        {
            var actual = _carService
                .CarExists(f => f.VIN == VIN);

            actual.Should().BeFalse();
        }
    }
}

