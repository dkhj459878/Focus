﻿using FluentAssertions;
using Focus.BL.Common.Services;
using Focus.BL.Services;
using Focus.BL.Tests.FakeData;
using Focus.DAL.Common.DbModels;
using Focus.DAL.Common.Repositories;
using Focus.DAL.Common.UoW;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Focus.BL.Tests.Storage
{
    /// <summary>
    /// Unit-тесты для сервиса <see cref="StorageService"/>
    /// </summary>
    [TestFixture]
    public class StorageServiceTests
    {
        private const int StoragesCount = 15;
        private IStorageService _storageService;
        private FakeStorage _fakeStorages;
        private Mock<IDbStorageRepository> mockstorageRepository;

        /// <summary>
        /// Осуществляет настройки
        /// </summary>
        [OneTimeSetUp]
        public void SetUp()
        {


            _fakeStorages = new FakeStorage(StoragesCount);

            mockstorageRepository = new Mock<IDbStorageRepository>();
            mockstorageRepository
                .Setup(mock => mock.Get(It.IsAny<Expression<Func<STORAGE, bool>>>(), null, It.IsAny<string>()))
                .Returns((Expression<Func<STORAGE, bool>> filter,
                    Func<IQueryable<STORAGE>, IOrderedQueryable<STORAGE>> orderBy,
                    string includeProperties) => _fakeStorages.Storages.Where(filter.Compile()));
            mockstorageRepository
                .Setup(mock => mock.Get(null, null, It.IsAny<string>()))
                .Returns((Expression<Func<STORAGE, bool>> filter,
                    Func<IQueryable<STORAGE>, IOrderedQueryable<STORAGE>> orderBy,
                    string includeProperties) => _fakeStorages.Storages);
            mockstorageRepository
                .Setup(mock => mock.GetPage(It.IsAny<int>(), It.IsAny<int>(), null, null, ""))
                .Returns((int pageSize,
                    int pageNumber,
                    Expression<Func<STORAGE, bool>> filter,
                    Func<IQueryable<STORAGE>, IOrderedQueryable<STORAGE>> orderBy,
                    string includeProperties) => _fakeStorages.Storages.Skip((pageNumber - 1) * pageSize).Take(pageSize));
            mockstorageRepository.Setup(mock => mock.GetById(It.IsAny<byte>()))
                .Returns((byte id) => _fakeStorages.Storages.Find(f => f.ID == id));
            mockstorageRepository.Setup(mock => mock.Insert(It.IsAny<STORAGE>()))
                .Callback((STORAGE f) => _fakeStorages.Storages.Add(f));
            mockstorageRepository.Setup(mock => mock.Update(It.IsAny<STORAGE>()))
                .Callback((STORAGE f) => _fakeStorages.Storages.Find(d => d.ID == f.ID).NUMBER_ = f.NUMBER_);
            mockstorageRepository.Setup(mock => mock.Delete(It.IsAny<byte>()))
                .Callback((byte id) => _fakeStorages.Storages
                    .Remove(_fakeStorages.Storages.Find(f => f.ID == id)));

            var mockstorageUnitOfWork = new Mock<IUnitOfWork>();
            mockstorageUnitOfWork.Setup(mock => mock.StorageRepository)
                .Returns(() => mockstorageRepository.Object); //todo - закоментил, не знаю в чем дело

            _storageService = new StorageService(mockstorageUnitOfWork.Object);
        }

        /// <summary>
        /// Тест метода получения экземпляров СВХ
        /// выбранной страницы пейджинга. Выбрана может быть любая страница.
        /// </summary>
        [Test, Order(1)]
        public void GetExistedStoragesPageTest([Random(1, 4, 1)]int pageNumber)
        {
            const int pagesize = 3;

            // Arrange
            var storages = _fakeStorages.Storages;

            var storagesPageIndexes = new List<int>();

            var startStorageIndexInPage = pagesize * (pageNumber - 1);

            var finishStorageIndexInPage = startStorageIndexInPage + pagesize - 1;

            for (var i = startStorageIndexInPage; i <= finishStorageIndexInPage; i++)
            {
                storagesPageIndexes.Add(i);
            }

            List<STORAGE> storagesPage;
            storagesPage = storagesPageIndexes.Select(storagesPageIndex => storages[storagesPageIndex]).ToList();


            // Act

            var actualStoragesPage = _storageService.GetStoragesPage(pageSize: pagesize, pageNumber: pageNumber).ToList();

            // Assert
            storagesPage.Should().BeEquivalentTo(actualStoragesPage,
                options => options.ExcludingMissingMembers());
        }

        /// <summary>
        /// Тест метода получения экземпляров СВХ
        /// выбранной страницы пейджинга. Выбрана последняя страница.
        /// </summary>
        [Test, Order(1)]
        public void GetExistedStoragesLastPageTest()
        {
            const int pagesize = 4;
            const int pageNumber = 4;

            // Arrange
            var dbStorages = _fakeStorages.Storages;

            var storagesPageIndexes = new List<int>();

            var startStorageIndexInPage = pagesize * (pageNumber - 1);

            var finishStorageIndexInPage = startStorageIndexInPage + StoragesCount % pagesize - 1;

            for (var i = startStorageIndexInPage; i <= finishStorageIndexInPage; i++)
            {
                storagesPageIndexes.Add(i);
            }

            var dbStoragesPage = storagesPageIndexes.Select(storagesPageIndex => dbStorages[storagesPageIndex]).ToList();

            // Act
            var actualStoragesPage = _storageService.GetStoragesPage(pageSize: pagesize, pageNumber: pageNumber).ToList();

            // Assert
            dbStoragesPage.Should().BeEquivalentTo(actualStoragesPage,
                options => options.ExcludingMissingMembers());
        }

        /// <summary>
        /// Тест метода получения экземпляров СВХ
        /// выбранной страницы пейджинга. Номер страницы отрицательный.
        /// </summary>
        [Test, Order(1)]
        public void GetNotExistedStoragesPageWithNegativePageNumbersTest([Random(-100500, -1, 5)] int pageNumber)
        {
            const int pagesize = 16;

            // Act
            ICollection<STORAGE> actualStoragesPage = _storageService.GetStoragesPage(pageSize: pagesize, pageNumber: pageNumber).ToList();

            ICollection<STORAGE> expectedPage = _storageService.GetStoragesPage(pageSize: pagesize, pageNumber: 1).ToList();

            // Assert
            actualStoragesPage.Should().BeEquivalentTo(expectedPage,
                options => options.ExcludingMissingMembers());
        }

        /// <summary>
        /// Тест метода получения экземпляров СВХ
        /// выбранной страницы пейджинга. Номер страницы больше общего числа страниц.
        /// </summary>
        [Test, Order(1)]
        public void GetNotExistedStoragesPageWithValueIfPageNumbersMoreThanTotalStoragesPagesTest([Random(StoragesCount, StoragesCount + 100500, 5)]int pageNumber)
        {
            const int pagesize = 16;

            // Act
            ICollection<STORAGE> actualStoragesPage = _storageService.GetStoragesPage(pageSize: pagesize, pageNumber: pageNumber).ToList();

            // Assert
            actualStoragesPage.Should().BeEmpty();
        }

        /// <summary>
        /// Тест метода получения экземпляров СВХ
        /// ввыбранной страницы пейджинга из пустого репозитария.
        /// </summary>
        [Test, Order(5)]
        public void GetNotExistedStoragesPageFromEmptyRepositoryTest([Random(1, 18, 5)]int pageNumber)
        {
            const int pagesize = 16;

            // Arrange
            _fakeStorages.Storages.Clear();

            // Act
            ICollection<STORAGE> actualStoragesPage = _storageService.GetStoragesPage(pageSize: pagesize, pageNumber: pageNumber).ToList();

            // Assert
            actualStoragesPage.Should().BeEmpty();
        }

        /// <summary>
        /// Тест метода получения всех экземпляров СВХ
        /// в пустом репозитарии.
        /// </summary>
        [Test, Order(5)]
        public void GetAllStoragesFromEmptyRepositoryTest()
        {
            // Arrange
            _fakeStorages.Storages.Clear();

            // Act
            var actualStorages = _storageService.GetAllStorages(filter: null);

            // Assert
            actualStorages.Should().BeEmpty();
        }

        /// <summary>
        /// Тест метода получения всех экземпляров СВХ
        /// в не пустом репозитарии.
        /// </summary>
        [Test, Order(1)]
        public void GetAllExistedStoragesTest()
        {
            // Arrange
            var expectedStorages = _fakeStorages.Storages;

            // Act
            var actualStorages = _storageService.GetAllStorages().ToList();

            // Assert
            foreach (var expectedStorage in expectedStorages)
            {
                var i = expectedStorage.ID;

                expectedStorages[i].NAME.Should().Be(actualStorages[i].NAME);
                expectedStorages[i].NUMBER_.Should().Be(actualStorages[i].NUMBER_);
            }

            expectedStorages.Should().BeEquivalentTo(actualStorages,
                options => options.ExcludingMissingMembers());

            expectedStorages.Count.Should().Be(actualStorages.Count);
        }

        /// <summary>
        /// Тест метода получения экземпляра СВХ по заданному идентификатору.
        /// СВХ с передаваемым идентификатором существует
        /// </summary>
        /// <param name="id">Идентификатор СВХ</param>
        [Test, Order(1)]
        public void GetExistedStorageTest([Random(0, StoragesCount - 1, 5)] byte id)
        {
            // Arrange
            var expectedStorage = _fakeStorages.Storages.Find(f => f.ID == id);

            // Act
            var actualStorage = _storageService.GetStorage(id);

            // Assert


            actualStorage.Should().BeEquivalentTo(expectedStorage,
                options => options.ExcludingMissingMembers());
        }

        /// <summary>
        /// Тест метода получения экземпляра СВХ по заданному идентификатору.
        /// Контейнер с передаваемым идентификатором не существует.
        /// </summary>
        /// <param name="id">Идентификатор СВХ</param>
        [Test, Order(1)]
        public void GetNotExistedStorageTest([Random(StoragesCount + 1, StoragesCount + 220, 5)]
                    byte id)
        {
            var actualStorage = _storageService.GetStorage(id);

            actualStorage.Should().BeNull();
        }

        /// <summary>
        /// Тест метода добавления СВХ в хранилище данных
        /// </summary>
        [Test, Repeat(5), Order(2)]
        public void AddStorageTest()
        {

            byte storageId = 15;
            
            // Arrange
            var storagesFake = _fakeStorages.Storages;

            var storageFake = storagesFake.Where(c => c.ID == storageId).FirstOrDefault();
            

            // Act

            var actualStorage = _storageService.GetStorage(storageId);

            // Assert

            storageFake.Should().BeEquivalentTo(actualStorage,
                options => options.ExcludingMissingMembers());
        }

        /// <summary>
        /// Тест метода обновления данных об автомобиле
        /// </summary>
        /// <param name="id">Идентификатор СВХ для обновления</param>
        [Test, Order(2)]
        public void UpdateStorageTest([Random(0, StoragesCount - 1, 5)] byte id)
        {
            // Arrange
            var storage = _storageService.GetStorage(id);
            storage.NUMBER_ = new Bogus.Faker().Random.AlphaNumeric(17);

            // Act
            _storageService.UpdateStorage(storage);
            var actualStorage = _fakeStorages.Storages.Find(f => f.ID == id);

            // Assert
            actualStorage.NUMBER_.Should().Be(storage.NUMBER_);
        }

        /// <summary>
        /// Тест метода удаления СВХ из хранилища данных
        /// </summary>
        /// <param name="id">Идентификатор СВХ для удаления</param>
        [Test, Order(3)]
        public void DeleteStorageTest([Random(0, StoragesCount - 1, 5)] byte id)
        {
            // Arrange
            _storageService.DeleteStorage(id);

            // Act
            var actualStorage = _fakeStorages.Storages.Find(storage => storage.ID == id);

            // Assert
            actualStorage.Should().BeNull();
        }

        /// <summary>
        /// Тест метода проверки наличия СВХ в хранилище данных.
        /// СВХ существует
        /// </summary>
        /// <param name="id">Идентификатор СВХ для поиска</param>
        [Test, Order(2)]
        public void StorageExistsTest([Random(0, StoragesCount - 1, 5)] short id)
        {
            // Arrange
            var loginToFind = _fakeStorages.Storages.Find(dbf => dbf.ID == id).NUMBER_;

            // Act
            var actual = _storageService
                .StorageExists(f => f.NUMBER_ == loginToFind);

            // Assert
            actual.Should().BeTrue();
        }

        /// <summary>
        /// Тест метода проверки наличия СВХ в хранилище данных.
        /// СВХ не существует
        /// </summary>
        /// <param name="login">Логин СВХ для поиска</param>
        [Test, Order(2)]
        public void StorageNotExistsTest([Values("shameonyou", "qwer", "", "befastandclearver", "strangelogin")]
                    string VIN)
        {
            var actual = _storageService
                .StorageExists(f => f.NUMBER_ == VIN);

            actual.Should().BeFalse();
        }
    }
}

