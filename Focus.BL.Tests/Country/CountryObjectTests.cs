﻿//using FluentAssertions;
//using Focus.BL.Common.Services;
//using Focus.BL.Services;
//using Focus.BL.Tests.FakeData;
//using Focus.DAL.Common.DbModels;
//using Focus.DAL.Common.Repositories;
//using Focus.DAL.Common.UoW;
//using Moq;
//using AutoMapper;
//using NUnit.Framework;
//using Focus.Web.Models;
//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Linq.Expressions;

//namespace Focus.BL.Tests.Country
//{
//    /// <summary>
//    /// Unit-тесты для сервиса <see cref="CountryService"/>
//    /// </summary>
//    [TestFixture]
//    public class CountryMapperTests
//    {
//        private const int CountriesCount = 20;
//        private ICountryService _countryService;
//        private CountryMapper _countryMapper;
//        private FakeCountry _fakeCountries;
//        private Mock<IDbCountryRepository> mockcountryRepository;

//        /// <summary>
//        /// Осуществляет настройки
//        /// </summary>
//        [OneTimeSetUp]
//        public void SetUp()
//        {



//            mockcountryRepository = new Mock<IDbCountryRepository>();
//            mockcountryRepository
//                .Setup(mock => mock.Get(It.IsAny<Expression<Func<COUNTRY, bool>>>(), null, It.IsAny<string>()))
//                .Returns((Expression<Func<COUNTRY, bool>> filter,
//                    Func<IQueryable<COUNTRY>, IOrderedQueryable<COUNTRY>> orderBy,
//                    string includeProperties) => _fakeCountries.Countries.Where(filter.Compile()));
//            mockcountryRepository
//                .Setup(mock => mock.Get(null, null, It.IsAny<string>()))
//                .Returns((Expression<Func<COUNTRY, bool>> filter,
//                    Func<IQueryable<COUNTRY>, IOrderedQueryable<COUNTRY>> orderBy,
//                    string includeProperties) => _fakeCountries.Countries);
//            mockcountryRepository
//                .Setup(mock => mock.GetPage(It.IsAny<int>(), It.IsAny<int>(), null, null, ""))
//                .Returns((int pageSize,
//                    int pageNumber,
//                    Expression<Func<COUNTRY, bool>> filter,
//                    Func<IQueryable<COUNTRY>, IOrderedQueryable<COUNTRY>> orderBy,
//                    string includeProperties) => _fakeCountries.Countries.Skip((pageNumber - 1) * pageSize).Take(pageSize));
//            mockcountryRepository.Setup(mock => mock.GetById(It.IsAny<string>()))
//                .Returns((string id) => _fakeCountries.Countries.Find(f => f.ABBR2 == id));
//            mockcountryRepository.Setup(mock => mock.Insert(It.IsAny<COUNTRY>()))
//                .Callback((COUNTRY f) => _fakeCountries.Countries.Add(f));
//            mockcountryRepository.Setup(mock => mock.Update(It.IsAny<COUNTRY>()))
//                .Callback((COUNTRY f) => _fakeCountries.Countries.Find(d => d.ABBR2 == f.ABBR2).NAME = f.NAME);
//            mockcountryRepository.Setup(mock => mock.Delete(It.IsAny<string>()))
//                .Callback((string id) => _fakeCountries.Countries
//                    .Remove(_fakeCountries.Countries.Find(f => f.ABBR2 == id)));

//            var mockcountryUnitOfWork = new Mock<IUnitOfWork>();
//            mockcountryUnitOfWork.Setup(mock => mock.CountryRepository)
//                .Returns(() => mockcountryRepository.Object); //todo - закоментил, не знаю в чем дело

//            _countryService = new CountryService(mockcountryUnitOfWork.Object);
//            _countryMapper = new CountryMapper();
//        }

//        /// <summary>
//        /// Тест метода маппинга коллеции стран в модели базы данных
//        /// в коллекцию стран модели представления.
//        /// </summary>
//        [Test, Order(1)]
//        public void MapTest([Random(1, 30, 1)]int countryCount)
//        {

//            // Arrange

//            _fakeCountries = new FakeCountry(countryCount);
//            IEnumerable<COUNTRY> countriesDbModel = _fakeCountries.Countries;

//            // Act
//            IEnumerable<Web.Models.Country> countriesViewModel = _countryMapper.Map(countriesDbModel);

//            // Assert
            

//        }
//    }
//}

