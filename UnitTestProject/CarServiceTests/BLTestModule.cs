﻿using Focus.BL.Common.Services;
using Focus.BL.Services;
using Ninject.Modules;

namespace Rocket.BL
{
    public class BLModule : NinjectModule
    {
        public override void Load()
        {
            Bind<IDocService>().To<DocService>();
            Bind<ICarService>().To<CarService>();
            Bind<IContainerService>().To<ContainerService>();
            Bind<IDeviceService>().To<DeviceService>();
            Bind<IDevicetypeService>().To<DevicetypeService>();
            Bind<IVehicleService>().To<VehicleService>();
            Bind<IVehiclekindService>().To<VehiclekindService>();
            Bind<ICountryService>().To<CountryService>();
            Bind<ICustombrockerService>().To<CustombrockerService>();
            Bind<IDocumentkindService>().To<DocumentkindService>();
            Bind<IDocumentService>().To<DocumentService>();
            Bind<IInspectorService>().To<InspectorService>();
            Bind<IOrganizationService>().To<OrganizationService>();
            Bind<IPermissionService>().To<PermissionService>();
            Bind<IPostService>().To<PostService>();
            Bind<IReportService>().To<ReportService>();
            Bind<IStorageService>().To<StorageService>();
        }
    }
}