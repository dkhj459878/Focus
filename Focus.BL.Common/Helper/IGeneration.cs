﻿using System;
using System.Collections.Generic;

namespace Focus.BL.Common.Helpers
{
    public interface IGeneration<TType> where TType : Type
    {
        TType Type { get; set; }

        ICollection<Version<TType>> Versions { get; set; }

        bool IsNextGeneration { get; set; }
    }
}
