﻿using System;

namespace Focus.BL.Common.Helpers
{
    public interface IVersion<TType> where TType : Type
    {
        DateTime VersionDateTime { get; set; }

        TType Entity { get; set; }

        short Id { get; set; }
    }
}
