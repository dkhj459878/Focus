﻿using System;
using System.Collections.Generic;

namespace Focus.BL.Common.Helpers
{
    public interface ILebel
    {
        ICollection<IGeneration<Type>> Generations { get; set;}
    }
}
