﻿/// <summary>
/// Processing strings.
/// </summary>
namespace Focus.BL.Common.Validators
{
    public interface IStringMaster
    {
        // Convert сirillica similar letter to latins.
        string ConvertSimilarLatins(string str);


        /// <summary>
        /// Get last folder in folders' path.
        /// For example, when method get 'E:\\Tests\\SampleMySQlidentity\\' value
        /// it returns 'SampleMySQlidentity' value.
        /// </summary>
        /// <param name="path">Folder path.</param>
        /// <returns>Name of last folder.</returns>
        string GetLastFolderName(string path); 
    }
}
