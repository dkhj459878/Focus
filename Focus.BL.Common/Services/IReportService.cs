﻿using Focus.DAL.Common.DbModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Focus.BL.Common.Services
{
    /// <summary>
    /// Представляет сервис для работы с протоколированием событий.
    /// </summary>
    public interface IReportService : IDisposable
    {
        /// <summary>
        /// Возвращает протоколированные события
        /// из хранилища данных для пейджинга.
        /// </summary>
        /// <param name="filter">Лямбда-выражение определяющее фильтрацию экземпляров.</param>
        /// <param name="orderBy">Лямбда-выражение определяющее сортировку экземпляров.</param>
        /// <param name="includeProperties">Список связанных свойств экземпляров, разделенный запятыми.</param>
        /// <returns>Коллекция экземпляров протоколированных событий для пейджинга.</returns>
        IEnumerable<REPORT> GetAllReports(
            Expression<Func<REPORT, bool>> filter = null,
            Func<IQueryable<REPORT>, IOrderedQueryable<REPORT>> orderBy = null,
            string includeProperties = "");

        /// <summary>
        /// Возвращает протоколированные события с заданным идентификатором
        /// из хранилища данных.
        /// </summary>
        /// <param name="pageSize">Количество сведений об автомобилях, выводимых на страницу.</param>
        /// <param name="pageNumber">Номер выводимой страницы со сведениями о документе.</param>
        /// <returns>Экземпляр протоколированного события.</returns>
        IEnumerable<REPORT> GetReportsPage(
           int pageSize,
           int pageNumber,
           Expression<Func<REPORT, bool>> filter = null,
           Func<IQueryable<REPORT>, IOrderedQueryable<REPORT>> orderBy = null,
           string includeProperties = "");

        /// <summary>
        /// Возвращает протоколированное событие с заданным идентификатором
        /// из хранилища данных.
        /// </summary>
        /// <param name="id">Идентификатор документа.</param>
        /// <returns>Экземпляр документа.</returns>
        REPORT GetReport(short id);

        /// <summary>
        /// Добавляет заданное протоколированное событие в хранилище данных
        /// и возвращает идентификатор добавленного протоколированного события.
        /// </summary>
        /// <param name="user">Экземпляр протоколированного события.</param>
        /// <returns>Уникальный идентификатор протоколированного события.</returns>
        short AddReport(REPORT report, string userId, string userInfo);

        /// <summary>
        /// Обновляет информацию заданного протоколированного события в хранилище данных.
        /// </summary>
        /// <param name="user">Экземпляр протоколированного события.</param>
        /// <returns> Task </returns>
        void UpdateReport(REPORT report, string userId, string userInfo);

        /// <summary>
        /// Удаляет протоколированное событие с заданным идентификатором
        /// из хранилища данных.
        /// </summary>
        /// <param name="id">Идентификатор протоколированного события.</param>
        /// <returns> Task </returns>
        void DeleteReport(short id, string userId, string userInfo);

        /// <summary>
        /// Проверяет наличие протоколированного события в хранилище данных
        /// соответствующего заданному фильтру.
        /// </summary>
        /// <param name="filter">Лямбда-выражение определяющее фильтр для поиска протоколированного события.</param>
        /// <returns>Возвращает <see langword="true"/>, если протоколированное событие существует в хранилище данных.</returns>
        bool ReportExists(Expression<Func<REPORT, bool>> filter);
    }
}