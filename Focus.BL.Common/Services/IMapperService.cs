﻿using System;
using System.Collections.Generic;

namespace Focus.BL.Common.Services
{   
    public interface IMapperService
    {
        Dictionary<Type, IEnumerable<Type>> GetMap { get; }

        Dictionary<Type, IEnumerable<Type>> GetCollection { get; }
    }
}
