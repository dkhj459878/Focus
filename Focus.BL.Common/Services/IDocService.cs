﻿using Focus.DAL.Common.DbModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Focus.BL.Common.Services
{
    /// <summary>
    /// Представляет сервис для работы с для документами.
    /// </summary>
    public interface IDocService : IDisposable
    {
        /// <summary>
        /// Возвращает все документы
        /// из хранилища данных.
        /// <param name="filter">Лямбда-выражение определяющее фильтрацию экземпляров.</param>
        /// <param name="orderBy">Лямбда-выражение определяющее сортировку экземпляров.</param>
        /// <param name="includeProperties">Список связанных свойств экземпляров, разделенный запятыми.</param>
        /// </summary>
        /// <returns>Коллекцию всех экземпляров пользователей.</returns>
        IEnumerable<DOC> GetAllDocs(
            Expression<Func<DOC, bool>> filter = null,
            Func<IQueryable<DOC>, IOrderedQueryable<DOC>> orderBy = null,
            string includeProperties = "");

        /// <summary>
        /// Возвращает документы
        /// из хранилища данных для пейджинга.
        /// </summary>
        /// <param name="pageSize">Количество сведений об автомобилях, выводимых на страницу.</param>
        /// <param name="pageNumber">Номер выводимой страницы со сведениями о документе.</param>
        /// <returns>Коллекция экземпляров документов для пейджинга.</returns>
        IEnumerable<DOC> GetDocsPage(
            int pageSize,
            int pageNumber,
            Expression<Func<DOC, bool>> filter = null,
            Func<IQueryable<DOC>, IOrderedQueryable<DOC>> orderBy = null,
            string includeProperties = "");

        /// <summary>
        /// Возвращает документ с заданным идентификатором
        /// из хранилища данных.
        /// </summary>
        /// <param name="id">Идентификатор документа.</param>
        /// <returns>Экземпляр документа.</returns>
        DOC GetDoc(short id);

        /// <summary>
        /// Добавляет заданного документа в хранилище данных
        /// и возвращает идентификатор добавленного документа.
        /// </summary>
        /// <param name="user">Экземпляр документа.</param>
        /// <returns>Уникальный идентификатор документа.</returns>
        short AddDoc(DOC doc,
            string userId = "",
            string userInfo = "");

        /// <summary>
        /// Обновляет информацию заданного документа в хранилище данных.
        /// </summary>
        /// <param name="user">Экземпляр документа.</param>
        /// <returns> Task </returns>
        void UpdateDoc(DOC doc,
            string userId = "",
            string userInfo = "");

        /// <summary>
        /// Обновляет только одно поле документа с заданным идентификатором.
        /// </summary>
        /// <param name="id">Идентификатор экземпляра документа.</param>
        /// <param name="field">Лямбда-выражение, определяющее имя поля.</param>
        /// <param name="value">Обновляемое значение.</param>
        void UpdateOneField(short id, Expression<Func<DOC, string>> field, string value);

        /// <summary>
        /// Возвращает количество документов,
        /// соответствующих заданному фильтру.
        /// </summary>
        /// <param name="filter">Лямбда-выражение определяющее фильтрацию экземпляров документов <see cref="TEntity"/>.</param>
        /// <returns>Количество документов.</returns>
        int DocsCount(Expression<Func<DOC, bool>> filter = null);

        /// <summary>
        /// Удаляет документа с заданным идентификатором
        /// из хранилища данных.
        /// </summary>
        /// <param name="id">Идентификатор документа.</param>
        /// <returns> Task </returns>
        void DeleteDoc(short id,
            string userId = "",
            string userInfo = "");

        /// <summary>
        /// Удаляет дкоумент с заданным идентификатором из хранилища данных,
        /// но перед этим отмечает идентификатор пользователя, который удаляет 
        /// документ.
        /// </summary>
        void DeleteDocWithUserIdMarking(short id, string userId);

        /// <summary>
        /// Проверяет наличие документа в хранилище данных
        /// соответствующего заданному фильтру.
        /// </summary>
        /// <param name="filter">Лямбда-выражение определяющее фильтр для поиска документа.</param>
        /// <returns>Возвращает <see langword="true"/>, если документсуществует в хранилище данных.</returns>
        bool DocExists(Expression<Func<DOC, bool>> filter);
    }
}