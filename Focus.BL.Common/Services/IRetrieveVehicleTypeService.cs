﻿using Focus.DAL.Common.DbModels;
using System;
using System.Collections.Generic;

namespace Focus.BL.Common.Services
{
    public interface IRetrieveVehicleTypeService
    {
        Dictionary<short?, Dictionary<DateTime, VEHICLETYPE>> GetVehicleTypesVersions();
    }
}
