﻿using Focus.DAL.Common.DbModels;
using System.Collections.Generic;

namespace Focus.BL.Common.Services
{
    public interface IAuditVehicletypeService
    {
        List<VEHICLETYPE> GetVehicletypeVersions(short id);
    }
}
