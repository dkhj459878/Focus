﻿using Focus.DAL.Common.DbModels;
using System;
using System.Collections.Generic;

namespace Focus.BL.Common.Services
{
    public interface IRequestService
    {
        Dictionary<Type, IEnumerable<short?>> GetIdentitiesNextGeneration(Dictionary<DateTime?, List<AUDIT_TRAIL>> audit_TrailsGroups);
    }
}
