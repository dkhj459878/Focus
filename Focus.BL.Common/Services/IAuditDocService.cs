﻿using Focus.DAL.Common.DbModels;
using System.Collections.Generic;

namespace Focus.BL.Common.Services
{
    public interface IAuditDocService
    {
        List<DOC> GetDocVersions(short id);
    }
}
