﻿using Focus.DAL.Common.DbModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Focus.BL.Common.Services
{
    /// <summary>
    /// Представляет сервис для работы с разрешениями.
    /// </summary>
    public interface IPermissionService : IDisposable
    {
        /// <summary>
        /// Возвращает разрешения
        /// из хранилища данных для пейджинга.
        /// </summary>
        /// <param name="filter">Лямбда-выражение определяющее фильтрацию экземпляров.</param>
        /// <param name="orderBy">Лямбда-выражение определяющее сортировку экземпляров.</param>
        /// <param name="includeProperties">Список связанных свойств экземпляров, разделенный запятыми.</param>
        /// <returns>Коллекция экземпляров разрешений для пейджинга.</returns>
        IEnumerable<PERMISSION> GetAllPermissions(
            Expression<Func<PERMISSION, bool>> filter = null,
            Func<IQueryable<PERMISSION>, IOrderedQueryable<PERMISSION>> orderBy = null,
            string includeProperties = "");

        /// <summary>
        /// Возвращает разрешения с заданным идентификатором
        /// из хранилища данных.
        /// </summary>
        /// <param name="pageSize">Количество сведений о разрешениях, выводимых на страницу.</param>
        /// <param name="pageNumber">Номер выводимой страницы со сведениями о разрешениях.</param>
        /// <returns>Экземпляр разрешения.</returns>
        IEnumerable<PERMISSION> GetPermissionsPage(
           int pageSize,
           int pageNumber,
           Expression<Func<PERMISSION, bool>> filter = null,
           Func<IQueryable<PERMISSION>, IOrderedQueryable<PERMISSION>> orderBy = null,
           string includeProperties = "");

        /// <summary>
        /// Возвращает разрешение с заданным идентификатором
        /// из хранилища данных.
        /// </summary>
        /// <param name="id">Идентификатор разрешения.</param>
        /// <returns>Экземпляр разрешения.</returns>
        PERMISSION GetPermission(short id);

        /// <summary>
        /// Добавляет заданное разрешение в хранилище данных
        /// и возвращает идентификатор добавленного разрешения.
        /// </summary>
        /// <param name="user">Экземпляр разрешения.</param>
        /// <returns>Уникальный идентификатор разрешения.</returns>
        short AddPermission(PERMISSION permission,
            string userId = "",
            string userInfo = "");

        /// <summary>
        /// Обновляет информацию заданного разрешения в хранилище данных.
        /// </summary>
        /// <param name="user">Экземпляр разрешения.</param>
        /// <returns> Task </returns>
        void UpdatePermission(PERMISSION permission,
            string userId = "",
            string userInfo = "");

        /// <summary>
        /// Удаляет разрешение с заданным идентификатором
        /// из хранилища данных.
        /// </summary>
        /// <param name="id">Идентификатор разрешения.</param>
        /// <returns> Task </returns>
        void DeletePermission(short id,
            string userId = "",
            string userInfo = "");

        /// <summary>
        /// Проверяет наличие разрешения в хранилище данных
        /// соответствующего заданному фильтру.
        /// </summary>
        /// <param name="filter">Лямбда-выражение определяющее фильтр для поиска разрешения.</param>
        /// <returns>Возвращает <see langword="true"/>, если разрешение существует в хранилище данных.</returns>
        bool PermissionExists(Expression<Func<PERMISSION, bool>> filter);
    }
}