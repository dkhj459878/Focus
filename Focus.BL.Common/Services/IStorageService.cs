﻿using Focus.DAL.Common.DbModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Focus.BL.Common.Services
{
    /// <summary>
    /// Представляет сервис для работы с СВХ.
    /// </summary>
    public interface IStorageService : IDisposable
    {
        /// <summary>
        /// Возвращает СВХ
        /// из хранилища данных для пейджинга.
        /// </summary>
        /// <param name="filter">Лямбда-выражение определяющее фильтрацию экземпляров.</param>
        /// <param name="orderBy">Лямбда-выражение определяющее сортировку экземпляров.</param>
        /// <param name="includeProperties">Список связанных свойств экземпляров, разделенный запятыми.</param>
        /// <returns>Коллекция СВХ контейнеров для пейджинга.</returns>
        IEnumerable<STORAGE> GetAllStorages(
            Expression<Func<STORAGE, bool>> filter = null,
            Func<IQueryable<STORAGE>, IOrderedQueryable<STORAGE>> orderBy = null,
            string includeProperties = "");

        /// <summary>
        /// Возвращает СВХ с заданным идентификатором
        /// из хранилища данных.
        /// </summary>
        /// <param name="pageSize">Количество сведений о СВХ, выводимых на страницу.</param>
        /// <param name="pageNumber">Номер выводимой страницы со сведениями о СВХ.</param>
        /// <returns>Экземпляр контейнера.</returns>
        IEnumerable<STORAGE> GetStoragesPage(
           int pageSize,
           int pageNumber,
           Expression<Func<STORAGE, bool>> filter = null,
           Func<IQueryable<STORAGE>, IOrderedQueryable<STORAGE>> orderBy = null,
           string includeProperties = "");

        /// <summary>
        /// Возвращает СВХ с заданным идентификатором
        /// из хранилища данных.
        /// </summary>
        /// <param name="id">Идентификатор СВХ.</param>
        /// <returns>Экземпляр СВХ.</returns>
        STORAGE GetStorage(byte id);

        /// <summary>
        /// Добавляет заданный СВХ в хранилище данных
        /// и возвращает идентификатор добавленного СВХ.
        /// </summary>
        /// <param name="user">Экземпляр СВХ.</param>
        /// <returns>Уникальный идентификатор СВХ.</returns>
        byte AddStorage(STORAGE storage,
            string userId = "",
            string userInfo = "");

        /// <summary>
        /// Обновляет информацию заданного СВХ в хранилище данных.
        /// </summary>
        /// <param name="user">Экземпляр СВХ.</param>
        /// <returns> Task </returns>
        void UpdateStorage(STORAGE storage,
            string userId = "",
            string userInfo = "");

        /// <summary>
        /// Удаляет СВХ с заданным идентификатором
        /// из хранилища данных.
        /// </summary>
        /// <param name="id">Идентификатор СВХ.</param>
        /// <returns> Task </returns>
        void DeleteStorage(byte id,
            string userId = "",
            string userInfo = "");

        /// <summary>
        /// Проверяет наличие СВХ в хранилище данных
        /// соответствующего заданному фильтру.
        /// </summary>
        /// <param name="filter">Лямбда-выражение определяющее фильтр для поиска СВХ.</param>
        /// <returns>Возвращает <see langword="true"/>, если СВХ существует в хранилище данных.</returns>
        bool StorageExists(Expression<Func<STORAGE, bool>> filter);
    }
}