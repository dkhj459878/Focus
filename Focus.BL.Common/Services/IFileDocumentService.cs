﻿using Focus.DAL.Common.DbModels;

namespace Focus.BL.Common.Services
{
    public interface IFileDocumentService
    {
        string GetDocumentFilePath(DOC document, string templatePath);
    }
}
