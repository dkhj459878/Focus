﻿using Focus.DAL.Common.DbModels;
using System.Collections.Generic;

namespace Focus.BL.Common.Services
{
    public interface IAuditDevicetypeService
    {
        List<DEVICETYPE> GetDevicetypeVersions(short id);
    }
}
