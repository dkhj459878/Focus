﻿using Focus.DAL.Common.DbModels;
using System.Collections.Generic;

namespace Focus.BL.Common.Services
{
    public interface IAuditInspectorService
    {
        List<INSPECTOR> GetInspectorVersions(short id);
    }
}
