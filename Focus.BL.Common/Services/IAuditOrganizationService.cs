﻿using Focus.DAL.Common.DbModels;
using System.Collections.Generic;

namespace Focus.BL.Common.Services
{
    public interface IAuditOrganizationService
    {
        List<ORGANIZATION> GetOrganizationVersions(short id);
    }
}
