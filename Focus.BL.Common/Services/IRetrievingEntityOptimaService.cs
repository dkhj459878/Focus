﻿using Focus.DAL.Common.DbModels;
using System.Collections.Generic;

namespace Focus.BL.Common.Services
{
    /// <summary>
    /// Defines interface of services which provide methods for converting audit trails 
    /// information to the collection of the entity versions ordered by time stamp 
    /// of CRUD operation.
    /// </summary>
    public interface IRetrievingEntityOptimaService
    {
        IEnumerable<TEntity> RetrieveEntityVersions<TEntity>(IEnumerable<AUDIT_TRAIL> auditTrails) where TEntity : class;
    }
}