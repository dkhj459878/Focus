﻿using Focus.DAL.Common.DbModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Focus.BL.Common.Services
{
    /// <summary>
    /// Представляет сервис для работы с видами транспортных средств.
    /// </summary>
    public interface IVehiclekindService : IDisposable
    {
        /// <summary>
        /// Возвращает виды транспортных средств
        /// из хранилища данных для пейджинга.
        /// </summary>
        /// <param name="filter">Лямбда-выражение определяющее фильтрацию экземпляров.</param>
        /// <param name="orderBy">Лямбда-выражение определяющее сортировку экземпляров.</param>
        /// <param name="includeProperties">Список связанных свойств экземпляров, разделенный запятыми.</param>
        /// <returns>Коллекция экземпляров видов транспортных средств для пейджинга.</returns>
        IEnumerable<VEHICLEKIND> GetAllVehiclekinds(
            Expression<Func<VEHICLEKIND, bool>> filter = null,
            Func<IQueryable<VEHICLEKIND>, IOrderedQueryable<VEHICLEKIND>> orderBy = null,
            string includeProperties = "");

        /// <summary>
        /// Возвращает вид транспортного средства с заданным идентификатором
        /// из хранилища данных.
        /// </summary>
        /// <param name="pageSize">Количество сведений о видах транспортных средств, выводимых на страницу.</param>
        /// <param name="pageNumber">Номер выводимой страницы со сведениями о виде транспортного средства.</param>
        /// <returns>Экземпляр вида транспортного средства.</returns>
        IEnumerable<VEHICLEKIND> GetVehiclekindsPage(
           int pageSize,
           int pageNumber,
           Expression<Func<VEHICLEKIND, bool>> filter = null,
           Func<IQueryable<VEHICLEKIND>, IOrderedQueryable<VEHICLEKIND>> orderBy = null,
           string includeProperties = "");

        /// <summary>
        /// Возвращает вид транспортного средства с заданным идентификатором
        /// из хранилища данных.
        /// </summary>
        /// <param name="id">Идентификатор документа.</param>
        /// <returns>Экземпляр документа.</returns>
        VEHICLEKIND GetVehiclekind(byte id);

        /// <summary>
        /// Добавляет заданный контейнер в хранилище данных
        /// и возвращает идентификатор добавленного контейнера.
        /// </summary>
        /// <param name="user">Экземпляр вида транспортного средства.</param>
        /// <returns>Уникальный идентификатор вида транспортного средства.</returns>
        byte AddVehiclekind(VEHICLEKIND vehiclekind,
            string userId = "",
            string userInfo = "");

        /// <summary>
        /// Обновляет информацию заданного вида транспортного средства в хранилище данных.
        /// </summary>
        /// <param name="user">Экземпляр вида транспортного средства.</param>
        /// <returns> Task </returns>
        void UpdateVehiclekind(VEHICLEKIND vehiclekind,
            string userId = "",
            string userInfo = "");

        /// <summary>
        /// Удаляет вид транспортного средства с заданным идентификатором
        /// из хранилища данных.
        /// </summary>
        /// <param name="id">Идентификатор вида транспортного средства.</param>
        /// <returns> Task </returns>
        void DeleteVehiclekind(byte id,
            string userId = "",
            string userInfo = "");

        /// <summary>
        /// Проверяет наличие вида транспортного средства в хранилище данных
        /// соответствующего заданному фильтру.
        /// </summary>
        /// <param name="filter">Лямбда-выражение определяющее фильтр для поиска вида транспортного средства.</param>
        /// <returns>Возвращает <see langword="true"/>, если вид транспортного средства существует в хранилище данных.</returns>
        bool VehiclekindExists(Expression<Func<VEHICLEKIND, bool>> filter);
    }
}