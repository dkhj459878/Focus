﻿using Focus.DAL.Common.DbModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Focus.BL.Common.Services
{
    /// <summary>
    /// Представляет сервис для работы с организациями.
    /// </summary>
    public interface IOrganizationService : IDisposable
    {
        /// <summary>
        /// Возвращает все организации,
        /// из хранилища данных.  
         /// <param name="filter">Лямбда-выражение определяющее фильтрацию экземпляров.</param>
        /// <param name="orderBy">Лямбда-выражение определяющее сортировку экземпляров.</param>
        /// <param name="includeProperties">Список связанных свойств экземпляров, разделенный запятыми.</param>
        /// </summary>
        /// <returns>Коллекцию всех экземпляров организаций.</returns>
        IEnumerable<ORGANIZATION> GetAllOrganizations(
            Expression<Func<ORGANIZATION, bool>> filter = null,
            Func<IQueryable<ORGANIZATION>, IOrderedQueryable<ORGANIZATION>> orderBy = null,
            string includeProperties = "");

        /// <summary>
        /// Возвращает организации
        /// из хранилища данных для пейджинга.
        /// </summary>
        /// <param name="pageSize">Количество сведений об организациях, выводимых на страницу.</param>
        /// <param name="pageNumber">Номер выводимой страницы со сведениями об организации.</param>
        /// <returns>Коллекция экземпляров организацийа для пейджинга.</returns>
        IEnumerable<ORGANIZATION> GetOrganizationsPage(
            int pageSize,
            int pageNumber,
            Expression<Func<ORGANIZATION, bool>> filter = null,
            Func<IQueryable<ORGANIZATION>, IOrderedQueryable<ORGANIZATION>> orderBy = null,
            string includeProperties = "");

        /// <summary>
        /// Возвращает организации с заданным идентификатором,0,
        /// из хранилища данных.
        /// </summary>
        /// <param name="id">Идентификатор контейнера.</param>
        /// <returns>Экземпляр контейнера.</returns>
        ORGANIZATION GetOrganization(short id);

        /// <summary>
        /// Добавляет заданный контейнер в хранилище данных
        /// и возвращает идентификатор добавленного контейнера.
        /// </summary>
        /// <param name="user">Экземпляр контейнера.</param>
        /// <returns>Уникальный идентификатор контейнера.</returns>
        short AddOrganization(ORGANIZATION organization,
            string userId = "",
            string userInfo = "");

        /// <summary>
        /// Обновляет информацию заданного контейнера в хранилище данных.
        /// </summary>
        /// <param name="user">Экземпляр контейнера.</param>
        /// <returns> Task </returns>
        void UpdateOrganization(ORGANIZATION organization,
            string userId = "",
            string userInfo = "");

        /// <summary>
        /// Удаляет контейнер с заданным идентификатором
        /// из хранилища данных.
        /// </summary>
        /// <param name="id">Идентификатор контейнера.</param>
        /// <returns> Task </returns>
        void DeleteOrganization(short id,
            string userId = "",
            string userInfo = "");

        /// <summary>
        /// Проверяет наличие контейнера в хранилище данных
        /// соответствующего заданному фильтру.
        /// </summary>
        /// <param name="filter">Лямбда-выражение определяющее фильтр для поиска контейнера.</param>
        /// <returns>Возвращает <see langword="true"/>, если контейнер существует в хранилище данных.</returns>
        bool OrganizationExists(Expression<Func<ORGANIZATION, bool>> filter);
    }
}