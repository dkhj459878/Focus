﻿//using System;
//using System.Linq;
//using System.Threading.Tasks;

//namespace Focus.DAL.Comparison
//{
//    /// <summary>
//    /// Class that implements the key ASP.NET Identity role store iterfaces
//    /// </summary>
//    public class ComparisonStore: IDisposable
//    {
//        private ComparisonTable comparisonTable;
//        public AccessDatabase Database { get; private set; }

//        public IQueryable Comparisons
//        {
//            get
//            {
//                return comparisonTable.GetCustomBrockersWithComparison().AsQueryable() as IQueryable;
//            }
//        }


//        /// <summary>
//        /// Default constructor that initializes a new MySQLDatabase
//        /// instance using the Default Connection string
//        /// </summary>
//        public ComparisonStore()
//        {
//            new ComparisonTable(new AccessDatabase());
//        }

//        /// <summary>
//        /// Constructor that takes a MySQLDatabase as argument 
//        /// </summary>
//        /// <param name="database"></param>
//        public ComparisonStore(AccessDatabase database)
//        {
//            Database = database;
//            comparisonTable = new ComparisonTable(database);
//        }

//        public Task CreateAsync(Comparison comparison)
//        {
//            if (comparison == null)
//            {
//                throw new ArgumentNullException("comparison");
//            }

//            comparisonTable.Insert(comparison);

//            return Task.FromResult<object>(null);
//        }

//        public Task DeleteAsync(Comparison comparison)
//        {
//            if (comparison == null)
//            {
//                throw new ArgumentNullException("comparison");
//            }

//            comparisonTable.Delete(comparison.CUSTOMBROCKERID);

//            return Task.FromResult<Object>(null);
//        }

//        public Task<Comparison> FindByIdAsync(int comparisonId)
//        {
//            Comparison result = comparisonTable.GetCustomBrockerWithComparison(comparisonId) as Comparison;

//            return Task.FromResult<Comparison>(result);
//        }

//        public Task UpdateAsync(Comparison comparison)
//        {
//            if (comparison == null)
//            {
//                throw new ArgumentNullException("comparison");
//            }

//            comparisonTable.Update(comparison);

//            return Task.FromResult<Object>(null);
//        }

//        public void Dispose()
//        {
//            if (Database != null)
//            {
//                Database.Dispose();
//                Database = null;
//            }
//        }

//    }
//}
