﻿using AutoMapper;
using Focus.BL.Common.Services;
using Focus.BL.Common.Validators;
using Focus.DAL.Common.DbModels;
using Focus.DAL.Extensions;
using Focus.SSO.Models;
using log4net;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Net.Http;
//using OpenQA.Selenium;
//using OpenQA.Selenium.Chrome;
//using OpenQA.Selenium.Support.UI;
using System.Text.RegularExpressions;
using System.Web.Mvc;
using Messages = Focus.SSO.Properties.Resources;

namespace Focus.Controllers
{
    public class custombrockersController : Controller
    {
        protected static readonly ILog _logger =
       LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);


        private readonly ICustombrockerService _custombrockerService;
        private readonly IPermissionService _permissionService;
        private readonly IReportService _reportService;
        private readonly IDocumentService _documentService;
        private readonly IDocumentkindService _documentkindService;
        private readonly ICountryService _countryService;
        private readonly IInspectorService _inspectorService;
        private readonly IPostService _postService;
        private readonly IStringMaster _stringMaster;
        private static readonly HttpClient _client = new HttpClient();

        public custombrockersController(ICustombrockerService custombrockerService, IPermissionService permissionService, IReportService reportService, IDocumentService documentService,
            IDocumentkindService documentkindService, ICountryService countryService, IStringMaster stringMaster, IInspectorService inspectorService, IPostService postService)
        {
            _custombrockerService = custombrockerService;
            _permissionService = permissionService;
            _reportService = reportService;
            _documentService = documentService;
            _documentkindService = documentkindService;
            _countryService = countryService;
            _inspectorService = inspectorService;
            _postService = postService;
            _stringMaster = stringMaster;
        }

        // GET: custombrockers/NotRepeatedPassportNumber/
        public JsonResult NotRepeatedPassportNumber([Bind()]string PASSPORTNUMBER, int? CUSTOMBROCKERID, string COUNTRY_ID)
        {
            // Check on passport number format.
            (bool IsTrue, string Error) resultFormat;
            resultFormat = IsCorrectPassportNumber(PASSPORTNUMBER, COUNTRY_ID);

            if (!resultFormat.IsTrue)
            {
                return Json(resultFormat.Error, JsonRequestBehavior.AllowGet);
            }

            if (IsEniquePassport(PASSPORTNUMBER, CUSTOMBROCKERID, COUNTRY_ID))
            {
                return Json(true, JsonRequestBehavior.AllowGet);
            }

            return Json(
                    string.Format(
                        Messages.ErrorRemoteCustomBrockerRepeated,
                        PASSPORTNUMBER
                        ), JsonRequestBehavior.AllowGet);
        }

        private bool IsEniquePassport(string passport, int? customBrockerId, string COUNTRY_ID)
        {
            if (customBrockerId == null)
            {
                return !_custombrockerService.CustombrockerExists(
                    o => o.PASSPORTNUMBER == (passport == string.Empty ? null : passport) && o.COUNTRY_ID == COUNTRY_ID);
            }

            return !_custombrockerService.CustombrockerExists(
                o => o.PASSPORTNUMBER == (passport == string.Empty ? null : passport) && o.COUNTRY_ID == COUNTRY_ID &&
                o.CUSTOMBROCKERID != customBrockerId);
        }

        private (bool IsCorrect, string Error) IsCorrectPassportNumber(string PASSPORTNUMBER, string COUNTRY_ID)
        {
            if (string.IsNullOrEmpty(COUNTRY_ID))
            {
                return (true, string.Empty);
            }

            Match match;


            switch (COUNTRY_ID)
            {
                case "BY":
                    Regex regexBLR = new Regex(@"^[ABCXETIOPKHAMСМТБОРАВКЕНХ]{2}[0-9]{7}$");
                    match = regexBLR.Match(PASSPORTNUMBER);
                    if (!match.Success)
                    {
                        return (false, "Номер паспорта должден состоять из двух букв верхнего регистра и семи цифр.");
                    }
                    break;
                default:
                    break;
            }

            return (true, string.Empty);
        }

        // Remote diagnostics.
        // GET: Inspector /NotRepeated/
        public JsonResult NotRepeated(string FIRSTNAME, string LASTNAME, string FATHERSNAME, int? CUSTOMBROCKERID)
        {
            if (IsEnique(FIRSTNAME, LASTNAME, FATHERSNAME, CUSTOMBROCKERID))
            {
                return Json(true,
                    JsonRequestBehavior.AllowGet);
            }

            return Json(
                    string.Format(
                        Messages.ErrorRemoteBrockerRepeated,
                        LASTNAME ?? string.Empty,
                        FIRSTNAME ?? string.Empty,
                        FATHERSNAME ?? string.Empty),
                        JsonRequestBehavior.AllowGet);
        }

        private bool IsEnique(string firstName, string lastName, string fatherName, int? id)
        {
            if (id == null)
            {
                return !_custombrockerService.CustombrockerExists(
                    i => i.FIRSTNAME == (firstName == string.Empty ? null : firstName) &&
                    i.LASTNAME == (lastName == string.Empty ? null : lastName) &&
                    i.FATHERSNAME == (fatherName == string.Empty ? null : fatherName));
            }

            return !_custombrockerService.CustombrockerExists(
                    i => i.FIRSTNAME == (firstName == string.Empty ? null : firstName) &&
                    i.LASTNAME == (lastName == string.Empty ? null : lastName) &&
                    i.FATHERSNAME == (fatherName == string.Empty ? null : fatherName) &&
                    i.CUSTOMBROCKERID != id);

        }

        // GET: custombrockers
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Search(string lastName)
        {

            if (string.IsNullOrEmpty(lastName))
            {
                return Content($"<br /><p><b>Поисковая строка не должна быть пустой. Введите в поисковую строку тест, содержащий более <span style = 'background-color: #ffff00; color: Red;'>3</span> символов, исключая пробелы, и вновь нажмите клавишу «Ввод» либо кнопку «Поиск».</b></p>");
            }
            if (lastName.Trim().Length < 4)
            {
                return Content($"<br /><p><b>Поисковая строка должна содержать не менее 4 символов, за исключением пробела. Введите в поисковую строку тест, содержащий более <span style = 'background-color: #ffff00; color: Red;'>3</span> символов, исключая пробелы, и вновь нажмите клавишу «Ввод» либо кнопку «Поиск».</b></p>");
            }

            string lastNameUpperCase = lastName.ToUpper();
            var custombrockers = _custombrockerService.GetAllCustombrockers(filter: o => o.LASTNAME.ToUpper().Contains(lastNameUpperCase))
                .OrderBy(b => b.LASTNAME)
                .OrderBy(b => b.FIRSTNAME)
                .OrderBy(b => b.FATHERSNAME)
                .OrderBy(b => b.PASSPORTISSUED)
                .ToList();

            if (custombrockers.Count <= 0)
            {
                return Content($"<br /><p><b>Уполномоченные представители с частями наименований (фамилий) <span style = 'background-color: #ffff00;'>{lastName}</span> в базе данных не обнаружены.</b></p>");
            }

            List<CUSTOMBROCKER> actualCustomBrockers = new List<CUSTOMBROCKER>();

            var customBrockersGroups = custombrockers.GroupBy(b => b.LASTNAME).Select(g => new { CustomBrockersGroupedByFirstName = g.GroupBy(b => b.FIRSTNAME) });
            foreach (var groupByLastNameAndFirstName in customBrockersGroups)
            {
                foreach (var group in groupByLastNameAndFirstName.CustomBrockersGroupedByFirstName)
                {
                    var customBrockersGroupsByLastNameFirstNameAndFartherName = group.GroupBy(b => b.FATHERSNAME).Select(g => new { ActualBrockers = g.OrderBy(b => b.PASSPORTISSUED).LastOrDefault() });
                    foreach (var item in customBrockersGroupsByLastNameFirstNameAndFartherName)
                    {
                        actualCustomBrockers.Add(item.ActualBrockers);
                    }
                }
            }


            return PartialView(Mapper.Map<IEnumerable<CUSTOMBROCKER>, IEnumerable<Custombrocker>>(actualCustomBrockers));
        }

        [HttpPost]
        public ActionResult SearchMove(string number)
        {
            if (string.IsNullOrEmpty(number))
            {
                return PartialView("SearchMoveErrNullString");
            }
            else
            {
                if (number.Length != 7 || !IsAnyCharIsDigit(number))
                {
                    return PartialView("SearchMoveErr");
                }
            }
            List<CUSTOMBROCKER> brockers = _custombrockerService.GetAllCustombrockers(filter: o => o.PASSPORTNUMBER.Contains(number)).ToList();

            string userId = User.Identity.GetUserId();
            //var user = userManager.FindById(userId);
            //short personId = (short)user.PERSONID;
            string ip = Request.UserHostAddress;
            const short PERSON_ID_FOR_TEST_PURPOSES = 9;
            INSPECTOR inspector = _inspectorService.GetInspector(PERSON_ID_FOR_TEST_PURPOSES);
            string codePost = string.Empty;
            string postName = string.Empty;
            string division = string.Empty;

            if (inspector != null)
            {
                POST post = _postService.GetPost(inspector.POSTID ?? 0);
                if (post != null)
                {
                    codePost = post.CODE;
                    postName = post.NAME;
                    division = post.DEVISION;
                }
            }

            REPORT record = new REPORT() { NUMBER_ = number, CODEPOST = codePost, NAMEPOST = postName, DEVISION = division, USERID = userId, PERSONID = Convert.ToInt16(PERSON_ID_FOR_TEST_PURPOSES), IP = ip };
            if (brockers.Count <= 0)
            {
                record.LAST_UPDATE = DateTime.Now;
                _reportService.AddReport(record, userId: User.Identity.GetUserId() ?? string.Empty, userInfo: "" ?? string.Empty);
                ViewBag.PassportNumberDigitalPart = number;
                return PartialView("SearchMoveNotFound");
            }

            CUSTOMBROCKER brocker = brockers.FirstOrDefault();

            record.CUSTOMBROCKERID = brocker.CUSTOMBROCKERID;
            record.PASSPORTNUMBER = brocker.PASSPORTNUMBER;
            record.FULLNAME = (brocker.LASTNAME ?? "") + (string.IsNullOrEmpty(brocker.FIRSTNAME) ? "" : " " + brocker.FIRSTNAME) + (string.IsNullOrEmpty(brocker.FATHERSNAME) ? "" : (brocker.FATHERSNAME.Contains(".") ? brocker.FATHERSNAME : " " + brocker.FATHERSNAME));
            record.LAST_UPDATE = DateTime.Now;
            _reportService.AddReport(record, userId: User.Identity.GetUserId() ?? string.Empty, userInfo: "" ?? string.Empty);
            return PartialView(Mapper.Map<IEnumerable<CUSTOMBROCKER>, IEnumerable<Custombrocker>>(brockers));
        }

        //public async Task Check()
        //{
        //    const int START_ID_FOR_TESTING = 1157   ;
        //    IWebDriver driver = new ChromeDriver();
        //    List<Comparison> comparisons = new List<Comparison>();
        //    using (AccessDatabase database = new AccessDatabase())
        //    {
        //        using (ComparisonStore comparisonStore = new ComparisonStore(database))
        //        {
        //            comparisons.AddRange((comparisonStore.Comparisons as IQueryable<Comparison>).ToList());

        //            List<Comparison> comparisonsFiltered = comparisons.Where(c => c.CUSTOMBROCKERID > START_ID_FOR_TESTING).OrderBy(c => c.CUSTOMBROCKERID).ToList();


        //            string homeURL = "http:\\112.112.112.1";
        //            driver.Navigate().GoToUrl(homeURL);
        //            string result = string.Empty;

        //            foreach (var comparison in comparisonsFiltered)
        //            {
        //                Comparison comparisonLocal = (Comparison)(comparison.Clone());
        //                string passportURL = @"http:\\112.112.112.2:8585";
        //                driver.Navigate().GoToUrl(passportURL);
        //                SetSearchParameters(driver, comparison);
        //                SendSearchParameters(driver);
        //                WebDriverWait wait = new WebDriverWait(driver,
        //                    System.TimeSpan.FromSeconds(15));
        //                wait.Until(d =>
        //                    d.FindElement(By.Id("txt_st")));
        //                IWebElement elementsIsFound =
        //                    driver.FindElement(By.Id("txt_st"));





        //                result = elementsIsFound.Text;
        //                int intResult = GetNumberFromResult(result);
        //                switch (intResult)
        //                {
        //                    case -1:
        //                        comparisonLocal.MISTAKE = "Ошибка записи";
        //                        break;
        //                    case 0:
        //                      comparisonLocal.MISTAKE = "Запись не найдена";
        //                        break;
        //                    case 1:
        //                        comparisonLocal.MISTAKE = "+";
        //                        IWebElement linkPassportElement = driver.FindElement(By.XPath("//table"));
        //                        string value = linkPassportElement.Text;
        //                        if (value.Contains("[недействителен]"))
        //                        {
        //                            comparisonLocal.MISTAKE = "-";
        //                        }
        //                        break;
        //                    default:
        //                        comparisonLocal.MISTAKE = "Найдено больше одной записи";
        //                        break;
        //                }
        //                await comparisonStore.UpdateAsync(comparisonLocal);
        //            }
        //        }
        //    }
        //    driver.Dispose();
        //    return;
        //}



        //private bool IsExpired(WebDriverWait wait)
        //{
        //    return false;
        //}

        //private int GetNumberFromResult(string str)
        //{

        //    if (string.IsNullOrEmpty(str))
        //    {
        //        return -1;
        //    }

        //    const string IS_FOUND_RECORDS = "Найдено записей: ";

        //    string result = string.Empty;

        //    int intResult = -1;

        //    if (str.Contains(IS_FOUND_RECORDS))
        //    {
        //        result = str.Replace(@"<b>", string.Empty).Replace(@"</b>", string.Empty).Replace(IS_FOUND_RECORDS, string.Empty);
        //        intResult = Convert.ToInt32(result);
        //        return intResult;
        //    }

        //    return intResult;

        //}

        //private void SetSearchParameters(IWebDriver driver, Comparison custombrocker)
        //{
        //    const int POS_OF_START_OF_THE_NUMERIC_PART_OF_PASSPORT = 2;

        //    if (driver == null || custombrocker == null)
        //    {
        //        return;
        //    }

        //    string lastName = custombrocker.LASTNAME ?? string.Empty;
        //    string firstName = custombrocker.FIRSTNAME ?? string.Empty; ;
        //    string fartherName = custombrocker.FATHERSNAME ?? string.Empty; ;
        //    string passportNumber = custombrocker.PASSPORTNUMBER.Substring(POS_OF_START_OF_THE_NUMERIC_PART_OF_PASSPORT) ?? string.Empty; ;

        //    IWebElement lastNameElement = driver.FindElement(By.Name("Last_Name"));
        //    IWebElement firstNameElement = driver.FindElement(By.Name("First_Name"));
        //    IWebElement midNameElement = driver.FindElement(By.Name("Mid_Name"));
        //    IWebElement passportNumberElement = driver.FindElement(By.Name("Num"));

        //    if (lastNameElement == null || firstNameElement == null || midNameElement == null || passportNumberElement == null)
        //    {
        //        return;
        //    }

        //    lastNameElement.Clear();
        //    firstNameElement.Clear();
        //    midNameElement.Clear();
        //    passportNumberElement.Clear();

        //    lastNameElement.SendKeys(lastName);
        //    firstNameElement.SendKeys(firstName);
        //    midNameElement.SendKeys(fartherName);
        //    passportNumberElement.SendKeys(passportNumber);
        //}

        //private void SendSearchParameters(IWebDriver driver)
        //{
        //    if (driver == null)
        //    {
        //        return;
        //    }

        //    IWebElement btnFindElement = driver.FindElement(By.Name("BtnFind"));

        //    if (btnFindElement == null)
        //    {
        //        return;
        //    }

        //    btnFindElement.Click();
        //}

        public ActionResult Details(short id)
        {
            CUSTOMBROCKER custombrocker = _custombrockerService.GetCustombrocker(id);
            if (custombrocker == null)
            {   
                return HttpNotFound();
            }
            return View(Mapper.Map<Custombrocker>(custombrocker));
        }

        // GET: custombrockers/Create
        public ActionResult Create()
        {
            ViewBag.COUNTRY_ID = new SelectList(Mapper.Map<IEnumerable<COUNTRY>, IEnumerable<Country>>(_countryService.GetAllCountries()), "ABBR2", "NAME", "BY");
            return View();
        }

        // POST: custombrockers/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "CUSTOMBROCKERID,FIRSTNAME,LASTNAME,FATHERSNAME,WORKPLACE,POST,PASSPORTNUMBER,PASSPORTISSUED,PASSPORTBODYISSUE,ADDRESS_FULL,ADDRESS,COUNTRY_ID,POST_INDEX,REGION,DISTRICT,CITY_TYPE,CITY,STREET_TYPE,STREET,BUILDING,BUILDING_BLOCK,OFFICE,PHONE,FAX,EMAIL,LAST_UPDATE")] Custombrocker custombrocker)
        {
            if (ModelState.IsValid)
            {
                // Проверка на наличие уполномоченного представителя с номером паспорта создаваемого уполномоченного представителя
                // в базе данных.
                if (!string.IsNullOrEmpty(custombrocker.PASSPORTNUMBER))
                {
                    var result = _custombrockerService.GetAllCustombrockers(filter: c => c.PASSPORTNUMBER.Contains(custombrocker.PASSPORTNUMBER)).Count() == 0;
                    if (!result)
                    {
                        var brocker = _custombrockerService.GetAllCustombrockers(filter: c => c.PASSPORTNUMBER.Contains(custombrocker.PASSPORTNUMBER)).FirstOrDefault();
                        return PartialView("PartialViewPassportNumberDoubling", brocker);
                    }
                }

                try
                {
                    // Your code...
                    // Could also be before try if you know the exception occurs in SaveChanges
                    var passportNumber = _stringMaster.ConvertSimilarLatins(custombrocker.PASSPORTNUMBER.ToUpper());
                    custombrocker.PASSPORTNUMBER = passportNumber;
                    custombrocker.USERID = User.Identity.GetUserId(); // Adding user id to row of container for auditing purpose.
                    // Remove whitespaces befor and after parts of the names.
                    string firstName = string.Empty;
                    string fartherName = string.Empty;
                    string lastName = string.Empty;
                    if (!string.IsNullOrEmpty(custombrocker.FIRSTNAME))
                    {
                        firstName = custombrocker.FIRSTNAME.Trim();
                    }
                    if (!string.IsNullOrEmpty(custombrocker.FATHERSNAME))
                    {
                        fartherName = custombrocker.FATHERSNAME.Trim();
                    }
                    if (!string.IsNullOrEmpty(custombrocker.LASTNAME))
                    {
                        lastName = custombrocker.LASTNAME.Trim();
                    }
                    custombrocker.FIRSTNAME = firstName;
                    custombrocker.FATHERSNAME = fartherName;
                    custombrocker.LASTNAME = lastName;
                    custombrocker.LAST_UPDATE = DateTime.Now;
                    custombrocker.CUSTOMBROCKERID = _custombrockerService.AddCustombrocker(Mapper.Map<CUSTOMBROCKER>(custombrocker));
                }
                catch (DbEntityValidationException e)
                {
                    _logger.Error(e.GetDetails(), e);
                    throw;
                }
                // Остаемся в нашем же модельном окне.
                return PartialView("Search", new List<Custombrocker>() { custombrocker });
            }
            ViewBag.COUNTRY_ID = new SelectList(Mapper.Map<IEnumerable<COUNTRY>, IEnumerable<Country>>(_countryService.GetAllCountries()), "ABBR2", "NAME");
            return View(custombrocker);
        }
        
        public ActionResult PartialCreate(string workplace)
        {
            ViewBag.COUNTRY_ID = new SelectList(Mapper.Map<IEnumerable<COUNTRY>, IEnumerable<Country>>(_countryService.GetAllCountries()), "ABBR2", "NAME", "BY");
            return PartialView(new Custombrocker() { WORKPLACE = workplace});
        }

        // POST: custombrockers/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult PartialCreate([Bind(Include = "CUSTOMBROCKERID,FIRSTNAME,LASTNAME,FATHERSNAME,WORKPLACE,POST,PASSPORTNUMBER,PASSPORTISSUED,PASSPORTBODYISSUE,ADDRESS_FULL,ADDRESS,COUNTRY_ID,POST_INDEX,REGION,DISTRICT,CITY_TYPE,CITY,STREET_TYPE,STREET,BUILDING,BUILDING_BLOCK,OFFICE,PHONE,FAX,EMAIL,LAST_UPDATE")] Custombrocker custombrocker)
        {
            if (ModelState.IsValid)
            {

                // Проверка на наличие уполномоченного представителя с номером паспорта создаваемого уполномоченного представителя
                // в базе данных.
                if (!string.IsNullOrEmpty(custombrocker.PASSPORTNUMBER))
                {
                    var result = _custombrockerService.GetAllCustombrockers(filter: c => c.PASSPORTNUMBER.Contains(custombrocker.PASSPORTNUMBER)).Count() == 0;
                    if (!result)
                    {
                        var brocker = _custombrockerService.GetAllCustombrockers(filter: c => c.PASSPORTNUMBER.Contains(custombrocker.PASSPORTNUMBER)).FirstOrDefault();
                        return PartialView("PartialViewPassportNumberDoubling", brocker);
                    }
                }

                try
                {
                    // Your code...
                    // Could also be before try if you know the exception occurs in SaveChanges
                    var passportNumber = _stringMaster.ConvertSimilarLatins(custombrocker.PASSPORTNUMBER.ToUpper());
                    custombrocker.PASSPORTNUMBER = passportNumber;
                    custombrocker.USERID = User.Identity.GetUserId(); // Adding user id to row of container for auditing purpose. 
                    custombrocker.LAST_UPDATE = DateTime.Now;
                    custombrocker.CUSTOMBROCKERID  = _custombrockerService.AddCustombrocker(Mapper.Map<CUSTOMBROCKER>(custombrocker));
                }
                catch (DbEntityValidationException e)
                {
                    _logger.Error(e.GetDetails(), e);
                    throw;
                }
                // Остаемся в нашем же модельном окне.
                return PartialView("Search", new List<Custombrocker>() { custombrocker });
            }
            ViewBag.COUNTRY_ID = new SelectList(Mapper.Map<IEnumerable<COUNTRY>, IEnumerable<Country>>(_countryService.GetAllCountries()), "ABBR2", "NAME");
            return PartialView(custombrocker);
        }

        public JsonResult JsonOrgMove(short id)
        {
            CUSTOMBROCKER custombrocker = _custombrockerService.GetCustombrocker(id);
            if (custombrocker == null)
            {
                return Json( new { }, JsonRequestBehavior.AllowGet);
            }

            CUSTOMBROCKER custombrockerActual = _custombrockerService.GetAllCustombrockers(filter: o =>
                        o.LASTNAME.Contains(custombrocker.LASTNAME) &&
                        o.FIRSTNAME.Contains(custombrocker.FIRSTNAME) &&
                        o.FATHERSNAME.Contains(custombrocker.FATHERSNAME)
                        ).OrderBy(c => c.PASSPORTISSUED).LastOrDefault();

            if (custombrocker.PASSPORTISSUED < custombrockerActual.PASSPORTISSUED)
            {
                string jslastName = custombrocker.LASTNAME ?? "";
                string jsFirstName = custombrocker.FIRSTNAME ?? "";
                string jsFatherName = custombrocker.FATHERSNAME ?? "";
                string jsPassportNumber = custombrocker.PASSPORTNUMBER ?? "";
                string jsPassportIssued = custombrocker.PASSPORTISSUED.Value.ToShortDateString();
                string jsPassportBodyIssue = custombrocker.PASSPORTBODYISSUE ?? "";

                return Json(new { id = -5, lastName = jslastName, firstName = jsFirstName, fatherName = jsFatherName, passportNumber = jsPassportNumber, passportBodyIssue = jsPassportBodyIssue, passportIssued = jsPassportIssued}, JsonRequestBehavior.AllowGet);
            }

            if (custombrocker.PERMISSION.Count < 1)
            {
                string jslastName = custombrocker.LASTNAME ?? "";
                string jsFirstName = custombrocker.FIRSTNAME ?? "";
                string jsFatherName = custombrocker.FATHERSNAME ?? "";
                return Json(new { id = -1, lastName = jslastName, firstName =  jsFirstName, fatherName = jsFatherName}, JsonRequestBehavior.AllowGet);
            }

            var permissions = new List<(
                string UNP,
                string Name,
                string Address,
                string DocNumber,
                string DocType,
                string DocDate,
                string Start,
                string Finish,
                bool IsValid
                )>();



            foreach (var item in custombrocker.PERMISSION)
            {
                var doc = _documentService.GetAllDocuments(filter: o => o.DOCUMENTID == item.DOCUMENTID).FirstOrDefault();
                if (doc ==  null)
                {
                    continue;
                }
                var docNumber = doc.NUMBER_ ?? "";
                string docDate = doc.DATE_.ToShortDateString();
                var org = doc.ORGANIZATION.FirstOrDefault();
                string orgUnp = org.UNP;
                string start = item.DATESTART == null ? "" : item.DATESTART.Value.ToShortDateString();
                string finish = item.DATEFINISH == null ? "" : item.DATEFINISH.Value.ToShortDateString();
                string docType = _documentkindService.GetDocumentkind(doc.DOCUMENTKINDID).NAME;
                string orgName = org.NAME;
                string orgAddress = org.ADDRESS;
                bool isValid = item.ISVALID == null ? false : Convert.ToBoolean(item.ISVALID.Value);
                var permissionItem = (
                    UNP: orgUnp,
                    Name: orgName,
                    Address: orgAddress,
                    DocNumber: docNumber,
                    DocType: docType,
                    DocDate: docDate,
                    Start: start,
                    Finish: finish,
                    IsValid: isValid);
                permissions.Add(permissionItem);
            }

            if (permissions.Count == 0)
            {
                string jslastName = custombrocker.LASTNAME ?? "";
                string jsFirstName = custombrocker.FIRSTNAME ?? "";
                string jsFatherName = custombrocker.FATHERSNAME ?? "";
                return Json(new { id = -1, lastName = jslastName, firstName = jsFirstName, fatherName = jsFatherName }, JsonRequestBehavior.AllowGet);
            }

            var jsondata = _custombrockerService.GetCustombrocker(id);
            return Json(
                new
                {

                    id = jsondata?.CUSTOMBROCKERID,
                    lastName = jsondata?.LASTNAME,
                    firstName = jsondata?.FIRSTNAME,
                    fthName = jsondata?.FATHERSNAME,
                    wrkPlace = jsondata?.WORKPLACE,
                    country = jsondata?.COUNTRY_ID,
                    address = jsondata?.ADDRESS,
                    passnumber = jsondata?.PASSPORTNUMBER,
                    passportdate = (jsondata?.PASSPORTISSUED == null) ? "" : jsondata?.PASSPORTISSUED.Value.ToShortDateString(),
                    passportauthoritybodyissue = jsondata?.PASSPORTBODYISSUE,
                    post = jsondata?.POST,
                    phone = jsondata?.PHONE,
                    fax = jsondata?.FAX,
                    permissionjson = permissions
                },
                JsonRequestBehavior.AllowGet);
        }

        public JsonResult JsonOrg(short id)
        {
            var jsondata = _custombrockerService.GetCustombrocker(id);
            return Json(
                new
                {
                    id = jsondata?.CUSTOMBROCKERID,
                    lastName = jsondata?.LASTNAME,
                    firstName = jsondata?.FIRSTNAME,
                    fthName = jsondata?.FATHERSNAME,
                    wrkPlace = jsondata?.WORKPLACE,
                    address = jsondata?.ADDRESS,
                    country = jsondata?.COUNTRY_ID,
                    passnumber = jsondata?.PASSPORTNUMBER,
                    passportdate = (jsondata?.PASSPORTISSUED == null) ? "" : jsondata?.PASSPORTISSUED.Value.ToShortDateString(),
                    passportauthoritybodyissue = jsondata?.PASSPORTBODYISSUE,
                    post = jsondata?.POST,
                    phone = jsondata?.PHONE,
                    fax = jsondata?.FAX
                },
                JsonRequestBehavior.AllowGet);
        }

        // GET: custombrockers/Edit/5
        public ActionResult Edit(short id)
        {
            CUSTOMBROCKER custombrocker = _custombrockerService.GetCustombrocker(id);
            if (custombrocker == null)
            {
                return HttpNotFound();
            }
            ViewBag.COUNTRY_ID = new SelectList(Mapper.Map<IEnumerable<COUNTRY>, IEnumerable<Country>>(_countryService.GetAllCountries()), "ABBR2", "NAME", custombrocker.COUNTRY_ID);
            return View(Mapper.Map<Custombrocker>(custombrocker));
        }

        // POST: custombrockers/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "CUSTOMBROCKERID,FIRSTNAME,LASTNAME,FATHERSNAME,WORKPLACE,POST,PASSPORTNUMBER,PASSPORTISSUED,PASSPORTBODYISSUE,ADDRESS_FULL,ADDRESS,COUNTRY_ID,POST_INDEX,REGION,DISTRICT,CITY_TYPE,CITY,STREET_TYPE,STREET,BUILDING,BUILDING_BLOCK,OFFICE,PHONE,FAX,EMAIL,LAST_UPDATE")] Custombrocker custombrocker)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    custombrocker.USERID = User.Identity.GetUserId(); // Adding user id to row of container for auditing purpose. 
                    var passportNumber = _stringMaster.ConvertSimilarLatins(custombrocker.PASSPORTNUMBER.ToUpper());
                    custombrocker.PASSPORTNUMBER = passportNumber;
                    custombrocker.LAST_UPDATE = DateTime.Now;
                    _custombrockerService.UpdateCustombrocker(Mapper.Map<CUSTOMBROCKER>(custombrocker));
                }
                catch (DbEntityValidationException e)
                {
                    _logger.Error(e.GetDetails(), e);
                    throw;
                }

                return RedirectToAction("Index");
            }
            ViewBag.COUNTRY_ID = new SelectList(Mapper.Map<IEnumerable<COUNTRY>, IEnumerable<Country>>(_countryService.GetAllCountries()), "ABBR2", "NAME");
            return View(custombrocker);
        }

        // GET: custombrockers/Delete/5
        public ActionResult Delete(short id)
        {
            CUSTOMBROCKER custombrocker = _custombrockerService.GetCustombrocker(id);
            if (custombrocker == null)
            {
                return HttpNotFound();
            }
            return View(Mapper.Map<Custombrocker>(custombrocker));
        }

        // POST: custombrockers/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(short id)
        {
            CUSTOMBROCKER custombrocker = _custombrockerService.GetCustombrocker(id);
            _custombrockerService.DeleteCustombrocker(id);
            return RedirectToAction("Index");
        }

        // GET: vehicles/JsonDelete/5
        public ActionResult JsonDelete(short id)
        {
            CUSTOMBROCKER jsondata = _custombrockerService.GetCustombrocker(id);

            var custombrockerJSON = $"{jsondata.LASTNAME} {jsondata.FIRSTNAME} {jsondata.FATHERSNAME}";

            var docCount = jsondata.DOC?.Count;

            return Json(
               new
               {
                   custombrockerId = id,
                   custombrockerName = custombrockerJSON,

                   docCount,
                   docs = jsondata.DOC.Select(o => $"Номер АТД '{o.NUM}' от {o.PERIODSTARTDATE.Value.ToShortDateString()} {o.CONSIGNEENAME}")
               },
               JsonRequestBehavior.AllowGet);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _custombrockerService.Dispose();
                _permissionService.Dispose();
                _reportService.Dispose();
                _documentService.Dispose();
                _documentkindService.Dispose();
                _countryService.Dispose();
                _inspectorService.Dispose();
                _postService.Dispose();
                _client.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool IsAnyCharIsDigit(string str)
        {
            if (string.IsNullOrEmpty(str))
            {
                return false;
            }
            Regex r = new Regex(@"\d");
            Match match = r.Match(str);
            if (match.Success)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
