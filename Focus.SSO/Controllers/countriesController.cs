﻿using AutoMapper;
using Focus.BL.Common.Services;
using Focus.DAL.Common.DbModels;
using Focus.SSO.Models;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using Messages = Focus.SSO.Properties.Resources;

namespace Focus.Controllers
{
    public class countriesController : Controller
    {

        private readonly IVehiclekindService _vehiclekindService;

        private readonly ICarService _carService;
        
        private readonly ICountryService _countryService;

        private readonly IContainerService _containerService;

        private readonly IDeviceService _deviceService;

        private readonly IDevicetypeService _devicetypeService;

        private readonly IVehicleService _vehicleService;

        public countriesController(ICountryService countryService, IVehiclekindService vehiclekindService, ICarService carService, IContainerService containerService, IDeviceService deviceService, IDevicetypeService devicetypeService, IVehicleService vehicleService)
        {
            _countryService = countryService;
            _vehiclekindService = vehiclekindService;
            _carService = carService;
            _containerService = containerService;
            _deviceService = deviceService;
            _devicetypeService = devicetypeService;
            _vehicleService = vehicleService;
        }

        // Remote diagnostics.
        // GET: countriesController /NotRepeatedAbbr2/
        [HttpPost]
        public ActionResult NotRepeatedAbbr2(string ABBR2)
        {
            
            var result = _countryService.CountryExists
                (filter: c => c.ABBR2 == ABBR2);

            if (!result)
            {
                return Json(true, JsonRequestBehavior.AllowGet);
            }
            
            return Json(
                    string.Format(
                        Messages.ErrorRemoteCountryCodeRepeated,
                        ABBR2
                        ), JsonRequestBehavior.AllowGet);
        }

        // Remote diagnostics.
        // GET: countriesController /NotRepeatedAbbr2/
        [HttpPost]
        public ActionResult NotRepeatedName(string NAME)
        {

            var result = _countryService.CountryExists
                (filter: c => c.NAME == NAME);

            if (!result)
            {
                return Json(true, JsonRequestBehavior.AllowGet);
            }

            return Json(
                    string.Format(
                        Messages.ErrorRemoteCountryNameRepeated,
                        NAME
                        ), JsonRequestBehavior.AllowGet);
        }

        // GET: countries
        public ActionResult Index()
        {
            var countries = _countryService.GetAllCountries().ToList(); 


            return View(Mapper.Map<IEnumerable<COUNTRY>, IEnumerable<Country>>(countries));
        }

        // GET: countries/Details/5
        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            COUNTRY country = _countryService.GetCountry(id);
            if (country == null)
            {
                return HttpNotFound();
            }
            return View(Mapper.Map<Country>(country));
        }

        // GET: countries/Create
        public ActionResult Create()
        {                               
            return View();
        }

        // POST: countries/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ABBR2,NAME,NAMEFULL,NAMEEN,ABBR3,CODELETTERS,CODEDIGITS,CONTINENTS,REGION")] Country country)
        {
            if (ModelState.IsValid)
            {
                country.LAST_UPDATE = DateTime.Now;
                _countryService.AddCountry(Mapper.Map<COUNTRY>(country));
                return RedirectToAction("Index");
            }

            return View(country);
        }

        // GET: countries/Edit/5
        public ActionResult Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            COUNTRY country = _countryService.GetCountry(id);
            if (country == null)
            {
                return HttpNotFound();
            }
            return View(Mapper.Map<Country>(country));
        }

        // POST: countries/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ABBR2,NAME,NAMEFULL,NAMEEN,ABBR3,CODELETTERS,CODEDIGITS,CONTINENTS,REGION")] Country country)
        {
            if (ModelState.IsValid)
            {
                country.USERID = User.Identity.GetUserId(); // Adding user id to row of container for auditing purpose. 
                country.LAST_UPDATE = DateTime.Now;
                _countryService.UpdateCountry(Mapper.Map<COUNTRY>(country));
                return RedirectToAction("Index");
            }
            return View(country);
        }

        // GET: countries/Delete/5
        public ActionResult Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            COUNTRY country = _countryService.GetCountry(id);
            if (country == null)
            {
                return HttpNotFound();
            }
            return View(Mapper.Map<Country>(country));
        }

        // POST: countries/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            _countryService.DeleteCountry(id);
            return RedirectToAction("Index");
        }

        // GET: vehicles/JsonDelete/5
        public ActionResult JsonDelete(string id)
        {
            COUNTRY jsondata = _countryService.GetCountry(id);

            var title = new MvcHtmlString($"<p><b>Данные о стране с двухзначным кодом '{jsondata.ABBR2}', " +
                        $"с наименованием {jsondata.NAME}, " +
                        $"поскольку от них зависят:</b></p>");

            var anonArray = new[] { new {
                message = new MvcHtmlString($"<p><b>– следующие автомобили:</b></p>"),
                name = $"{jsondata.ABBR2} {jsondata.NAME}",
                action =  @"\countries\Delete\" + id.ToString(),
                enitities  = jsondata.CAR.Select(c => $"VIN номер '{c.VIN}' {_vehiclekindService.GetVehiclekind(c.VEHICLEKINDID).NAME}, страна {((c.COUNTRYCODE ?? "") == "" ? _countryService.GetCountry(c.COUNTRYCODE).NAME : "")}") }
            };

            return Json(
               new
               {
                   anonArray
               },
               JsonRequestBehavior.AllowGet);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _countryService.Dispose();
                _vehiclekindService.Dispose();
                _carService.Dispose();
                _containerService.Dispose();
                _deviceService.Dispose();
                _devicetypeService.Dispose();
                _vehicleService.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
