﻿using AutoMapper;
using Focus.BL.Common.Services;
using Focus.DAL.Common.DbModels;
using Focus.DAL.Extensions;
using Focus.SSO.Models;
using log4net;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Web.Mvc;
using Messages = Focus.SSO.Properties.Resources;

namespace Focus.Controllers
{
    public class documentsController : Controller
    {
        protected static readonly ILog _logger =
           LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private readonly IDocumentService _documentService;
        private readonly IDocumentkindService _documentkindService;

        public documentsController(IDocumentService documentService, IDocumentkindService documentkindService)
        {
            _documentService = documentService;
            _documentkindService = documentkindService;
        }

        // // Remote diagnostics.
        // GET: docs /NotRepeated/
        public ActionResult NotRepeated(string NUMBER_, DateTime? DATE, byte DOCUMENTKINDID, int? DOCUMENTID)
        {
            if (DATE == null)
            {
                return Json(true, JsonRequestBehavior.AllowGet);
            }

            if (IsUnique(NUMBER_, DATE, DOCUMENTKINDID, DOCUMENTID))
            {
                return Json(true, JsonRequestBehavior.AllowGet);
            }

            var documentKind = _documentkindService.GetDocumentkind(DOCUMENTKINDID).NAME;
            return Json(
                   string.Format(
                       Messages.ErrorRemoteDocumentRepeated,
                       NUMBER_, DATE.Value.ToShortDateString(), documentKind
                       ), JsonRequestBehavior.AllowGet);

        }

        private bool IsUnique(string number, DateTime? date, byte documentKindId, int? documentId)
        {
            if (documentId == null)
            {
                return !_documentService.DocumentExists
                (filter: d => d.NUMBER_ == number &&
                d.DATE_ == date.Value &&
                d.DOCUMENTKINDID  == documentKindId);
            }

            return !_documentService.DocumentExists
                (filter: d => d.NUMBER_ == number &&
                d.DATE_ == date.Value &&
                d.DOCUMENTKINDID == documentKindId &&
                d.DOCUMENTID != documentId);
        }

        // GET: documents
        public ActionResult Index()
        {
            var documents = _documentService.GetAllDocuments().ToList();
            return View(Mapper.Map<IEnumerable<DOCUMENT>, IEnumerable<Document>>(documents));
        }

        // GET: documents/Details/5
        public ActionResult Details(short id)
        {
            DOCUMENT document = _documentService.GetDocument(id);
            if (document == null)
            {
                return HttpNotFound();
            }
            return View(Mapper.Map<Document>(document));
        }

        // GET: documents/Create
        public ActionResult Create()
        {
            ViewBag.DOCUMENTKINDID = new SelectList(Mapper.Map<IEnumerable<DOCUMENTKIND>, IEnumerable<Documentkind>>(_documentkindService.GetAllDocumentkinds()), "DOCUMENTKINDID", "NAME");
            return View();
        }

        // POST: docs/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Document document)
        {

            if (ModelState.IsValid)
            {
                try
                {
                    document.USERID = User.Identity.GetUserId();
                    Document localDoc = document;
                    localDoc.BODYISSUE = localDoc.ORGANIZATION.FirstOrDefault().NAME;
                    localDoc.LAST_UPDATE = DateTime.Now;
                    document.LAST_UPDATE = DateTime.Now;
                    _documentService.AddDocument(Mapper.Map<DOCUMENT>(localDoc));
                }
                catch (DbEntityValidationException e)
                {
                    _logger.Error(e.GetDetails(), e);
                    throw;
                }
            }

                ViewBag.DOCUMENTKINDID = new SelectList(Mapper.Map<IEnumerable<DOCUMENTKIND>, IEnumerable<Documentkind>>(_documentkindService.GetAllDocumentkinds()), "DOCUMENTKINDID", "NAME");
                return RedirectToAction("Index");
            //}
            //ViewBag.DOCUMENTKINDID = new SelectList(db.documentkind, "DOCUMENTKINDID", "NAME");
            //return View(document);
        }

        // GET: documents/Edit/5
        public ActionResult Edit(short id)
        {
            DOCUMENT document = _documentService.GetDocument(id);
            if (document == null)
            {
                return HttpNotFound();
            }
            ViewBag.DOCUMENTKINDID = new SelectList(Mapper.Map<IEnumerable<DOCUMENTKIND>, IEnumerable<Documentkind>>(_documentkindService.GetAllDocumentkinds()), "DOCUMENTKINDID", "NAME", document.DOCUMENTKINDID);
            return View(Mapper.Map<Document>(document));
        }
        // irina + 
        // POST: documents/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "DOCUMENTID,NUMBER_,DATE,DOCUMENTKINDID,BODYISSUE")] Document document)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    document.USERID = User.Identity.GetUserId();
                    document.LAST_UPDATE = DateTime.Now;
                    _documentService.UpdateDocument(Mapper.Map<DOCUMENT>(document));
                }
                catch (DbEntityValidationException e)
                {
                    _logger.Error(e.GetDetails(), e);
                    throw;
                }
                return RedirectToAction("Index");
            }
            ViewBag.DOCUMENTKINDID = new SelectList(Mapper.Map<IEnumerable<DOCUMENTKIND>, IEnumerable<Documentkind>>(_documentkindService.GetAllDocumentkinds()), "DOCUMENTKINDID", "NAME");
            return View(document);
        }

        // GET: documents/Delete/5
        public ActionResult Delete(short id)
        {
            DOCUMENT document = _documentService.GetDocument(id);
            if (document == null)
            {
                return HttpNotFound();
            }
            return View(Mapper.Map<Document>(document));
        }

        // POST: documents/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(short id)
        {
            DOCUMENT document = _documentService.GetDocument(id);
            _documentService.DeleteDocument(id);
            return RedirectToAction("Index");
        }

        private void FillOrganizationName()
        {
            //Инициализировать список всех документов с организацией, как вложенным классом.
            IEnumerable<DOCUMENT> allDocuments = _documentService.GetAllDocuments(includeProperties: "organization");
            //Изменить изменить в списке документов наименование организации.
            try
            {
                foreach (DOCUMENT document in allDocuments)
                {
                    document.BODYISSUE = document.ORGANIZATION.First().NAME;
                    document.LAST_UPDATE = DateTime.Now;
                    _documentService.UpdateDocument(document);
                }
            }
            catch (Exception error)
            {
                _logger.Error(error.Message);
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _documentService.Dispose();
                _documentkindService.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
