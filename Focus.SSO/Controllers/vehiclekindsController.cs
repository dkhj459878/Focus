﻿using AutoMapper;
using Focus.BL.Common.Services;
using Focus.DAL.Common.DbModels;
using Focus.SSO.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Messages = Focus.SSO.Properties.Resources;

namespace Focus.Controllers
{
    public class vehiclekindsController : Controller
    {
        private IVehiclekindService _vehiclekindService;

        private IVehicletypeService _vehicletypeService;

        public vehiclekindsController(IVehiclekindService vehiclekindService, IVehicletypeService vehicletypeService)
        {
            _vehiclekindService = vehiclekindService;
            _vehicletypeService = vehicletypeService;
        }

        public ActionResult NotRepeated(string NAME, byte? VEHICLETYPEID, byte? VEHICLEKINDID)
        {
            string vehicleType;
            if (IsUnique(NAME, VEHICLETYPEID, VEHICLEKINDID))
            {
                return Json(true, JsonRequestBehavior.AllowGet);
            }

            if (VEHICLETYPEID == null)
            {
                vehicleType = string.Empty;
            }
            else
            {
                vehicleType = _vehicletypeService.GetVehicletype((byte)VEHICLETYPEID)?.NAME;
            }

            string vehicleTypeName = _vehicletypeService.GetVehicletype(
                (byte)VEHICLETYPEID).NAME;

            return Json(
                    string.Format(
                        Messages.ErrorRemoteVehiclekindRepeated,
                        vehicleTypeName,
                        NAME
                        ), JsonRequestBehavior.AllowGet);
        }

        private bool IsUnique(string name, byte? vehicleTypeId, byte? vehicleKindId)
        {

            if (vehicleKindId == null)
            {
                return !_vehiclekindService.VehiclekindExists
                (filter: v => v.NAME == name &&
                v.VEHICLETYPEID == vehicleTypeId);
            }

            return !_vehiclekindService.VehiclekindExists
                 (filter: v => v.NAME == name &&
                 v.VEHICLETYPEID == vehicleTypeId &&
                 v.VEHICLEKINDID != vehicleKindId);
        }

        // GET: vehiclekinds
        public ActionResult Index()
        {
            var vehiclekinds = _vehiclekindService.GetAllVehiclekinds().ToList();
            return View(Mapper.Map<IEnumerable<VEHICLEKIND>, IEnumerable<Vehiclekind>>(vehiclekinds));
        }
        

        // GET: vehiclekinds/Details/5
        public ActionResult Details(byte id)
        {
            VEHICLEKIND vehiclekind = _vehiclekindService.GetVehiclekind(id);
            if (vehiclekind == null)
            {
                return HttpNotFound();
            }
            return View(Mapper.Map<Vehiclekind>(vehiclekind));
        }

        // GET: vehiclekinds/Create
        public ActionResult Create()
        {
            ViewBag.VEHICLETYPEID = new SelectList(Mapper.Map<IEnumerable<VEHICLETYPE>, IEnumerable<Vehicletype>>(_vehicletypeService.GetAllVehicletypes()), "VEHICLETYPEID", "NAME");
            return View();
        }

        // POST: vehiclekinds/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "VEHICLEKINDID,VEHICLETYPEID,NAME,DESCRIPTION,LAST_UPDATE")] Vehiclekind vehiclekind)
        {
            if (ModelState.IsValid)
            {
                vehiclekind.LAST_UPDATE = DateTime.Now;
                _vehiclekindService.AddVehiclekind(Mapper.Map<VEHICLEKIND>(vehiclekind));
                return RedirectToAction("Index");
            }

            ViewBag.VEHICLETYPEID = new SelectList(Mapper.Map<IEnumerable<VEHICLETYPE>, IEnumerable<Vehicletype>>(_vehicletypeService.GetAllVehicletypes()), "VEHICLETYPEID", "NAME");
            return View(vehiclekind);
        }

        // GET: vehiclekinds/Edit/5
        public ActionResult Edit(byte id)
        {
            VEHICLEKIND vehiclekind = _vehiclekindService.GetVehiclekind(id);
            if (vehiclekind == null)
            {
                return HttpNotFound();
            }
            ViewBag.VEHICLETYPEID = new SelectList(Mapper.Map<IEnumerable<VEHICLETYPE>, IEnumerable<Vehicletype>>(_vehicletypeService.GetAllVehicletypes()), "VEHICLETYPEID", "NAME", vehiclekind.VEHICLETYPEID);
            return View(Mapper.Map<Vehiclekind>(vehiclekind));
        }

        // POST: vehiclekinds/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "VEHICLEKINDID,VEHICLETYPEID,NAME,DESCRIPTION,LAST_UPDATE")] Vehiclekind vehiclekind)
        {
            if (ModelState.IsValid)
            {
                vehiclekind.LAST_UPDATE = DateTime.Now;
                _vehiclekindService.UpdateVehiclekind(Mapper.Map<VEHICLEKIND>(vehiclekind));
                return RedirectToAction("Index");
            }
            ViewBag.VEHICLETYPEID = new SelectList(Mapper.Map<IEnumerable<VEHICLETYPE>, IEnumerable<Vehicletype>>(_vehicletypeService.GetAllVehicletypes()), "VEHICLETYPEID", "NAME", vehiclekind.VEHICLETYPEID);
            return View(vehiclekind);
        }

        // GET: vehiclekinds/Delete/5
        public ActionResult Delete(byte id)
        {
             VEHICLEKIND vehiclekind = _vehiclekindService.GetVehiclekind(id);
            if (vehiclekind == null)
            {
                return HttpNotFound();
            }
            return View(Mapper.Map<Vehiclekind>(vehiclekind));
        }

        // POST: vehiclekinds/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(byte id)
        {
            VEHICLEKIND vehiclekind = _vehiclekindService.GetVehiclekind(id);
            _vehiclekindService.DeleteVehiclekind(id);
            return RedirectToAction("Index");
        }

        public ActionResult JsonDelete(byte id)
        {
            VEHICLEKIND jsondata = _vehiclekindService.GetVehiclekind(id);

            var vehicleKind = jsondata.NAME;

            var carCount = jsondata.CAR?.Count;
			
			var vehicleCount = jsondata.VEHICLE?.Count;

            return Json(
               new
               {
                   vehicleKindId = id,
                   vehicleKindName = vehicleKind,

                   carCount,
                   cars = jsondata.CAR?.Select(c =>  $"VIN: {c.VIN} {c.COUNTRYCODE}"),

                   vehicleCount,
                   vehicles = jsondata.VEHICLE?.Select(v => $"Рег.номер: {v.REGNUMBER} {v.COUNTRYCODE}")
               },
               JsonRequestBehavior.AllowGet);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _vehiclekindService.Dispose();
                _vehicletypeService.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
