﻿using AutoMapper;
using Focus.DAL.Common.DbModels;
using Focus.SSO.Models;

namespace Focus.Web.Mappings
{
    /// <summary>
    /// Профиль сопоставления веб модели автомобиля с моделью хранения данных автомобиля.
    /// </summary>
    public class CarMappingProfile : Profile
    {
        public CarMappingProfile()
        {
            CreateMap<CAR, Car>()
                .ForMember(cfg => cfg.DOC, act => act.Ignore())
                .MaxDepth(1);
            ;
        }
    }
}