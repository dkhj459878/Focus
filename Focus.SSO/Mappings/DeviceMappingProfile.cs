﻿using AutoMapper;
using Focus.DAL.Common.DbModels;
using Focus.SSO.Models;

namespace Focus.Web.Mappings
{
    /// <summary>
    /// Профиль сопоставления веб модели ТСТК с моделью хранения данных ТСТК
    /// </summary>
    public class DeviceMappingProfile : Profile
    {
        public DeviceMappingProfile()
        {
            CreateMap<DEVICE, Device>()
                .ForMember(cfg => cfg.DOC, act => act.Ignore())
                .MaxDepth(1)
                .ReverseMap()
                .MaxDepth(1);
            ;
        }
    }
}