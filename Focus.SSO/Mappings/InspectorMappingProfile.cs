﻿using AutoMapper;
using Focus.DAL.Common.DbModels;
using Focus.SSO.Models;

namespace Focus.Web.Mappings
{
    /// <summary>
    /// Профиль сопоставления веб модели инспектора с моделью хранения данных инспектора.
    /// </summary>
    public class InspectorMappingProfile : Profile
    {
        public InspectorMappingProfile()
        {
            CreateMap<INSPECTOR, Inspector>()
                .ForMember(cfg => cfg.DOC, act => act.Ignore())
                .ForMember(cfg => cfg.DOC1, act => act.Ignore())
                .MaxDepth(1);
        }
    }
}