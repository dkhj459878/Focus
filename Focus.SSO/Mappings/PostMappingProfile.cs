﻿using AutoMapper;
using Focus.DAL.Common.DbModels;
using Focus.SSO.Models;

namespace Focus.Web.Mappings
{
    /// <summary>
    /// Профиль сопоставления веб модели таможенного поста (ПТО, другого подразделения) с моделью хранения данных таможенного поста (ПТО, другого подразделения).
    /// </summary>
    public class PostMappingProfile : Profile
    {
        public PostMappingProfile()
        {
            CreateMap<POST, Post>()
                .ForMember(cfg => cfg.INSPECTOR, act => act.Ignore())
                .MaxDepth(1);
        }
    }
}