﻿using AutoMapper;
using Focus.DAL.Common.DbModels;
using Focus.SSO.Models;

namespace Focus.Web.Mappings
{
    /// <summary>
    /// Профиль сопоставления веб модели данных регистрации действий пользователя с моделью хранения данных регистрации данных пользователя.
    /// </summary>
    public class ReportMappingProfile : Profile
    {
        public ReportMappingProfile()
        {
            CreateMap<REPORT, Report>()
                .MaxDepth(1);
        }
    }
}