﻿using AutoMapper;
using Focus.DAL.Common.DbModels;
using Focus.SSO.Models;

namespace Focus.Web.Mappings
{
    /// <summary>
    /// Профиль сопоставления веб модели типов транспортных средств с моделью хранения данных типов транспортнызх средств.
    /// </summary>
    public class VehicletypeMappingProfile : Profile
    {
        public VehicletypeMappingProfile()
        {
            CreateMap<VEHICLETYPE, Vehicletype>()
                .ForMember(cfg => cfg.VEHICLEKIND, act => act.Ignore())
                .MaxDepth(1);
        }
    }
}