﻿using AutoMapper;
using Focus.DAL.Common.DbModels;
using Focus.SSO.Models;

namespace Focus.Web.Mappings
{
    /// <summary>
    /// Профиль сопоставления веб модели страны с моделью хранения данных страны.
    /// </summary>
    public class СountryMappingProfile : Profile
    {
        public СountryMappingProfile()
        {
            CreateMap<COUNTRY, Country>()
                .ForMember(cfg => cfg.CUSTOMBROCKER, act => act.Ignore())
                .ForMember(cfg => cfg.CONTAINER, act => act.Ignore())
                .ForMember(cfg => cfg.VEHICLE, act => act.Ignore())
                .ForMember(cfg => cfg.CAR, act => act.Ignore())
                .MaxDepth(1);
            ;
        }
    }
}