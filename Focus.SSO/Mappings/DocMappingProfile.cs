﻿using AutoMapper;
using Focus.DAL.Common.DbModels;
using Focus.SSO.Models;

namespace Focus.Web.Mappings
{
    /// <summary>
    /// Профиль сопоставления веб модели документа с моделью хранения данных документа.
    /// </summary>
    public class DocMappingProfile : Profile
    {
        public DocMappingProfile()
        {
            CreateMap<DOC, Doc>()
                .MaxDepth(1)
                .ReverseMap()
                .MaxDepth(1);
        }
    }
}