﻿using AutoMapper;
using Focus.DAL.Common.DbModels;
using Focus.SSO.Models;

namespace Focus.Web.Mappings
{
    /// <summary>
    /// Профиль сопоставления веб модели типа ТСТК с моделью хранения данных типа ТСТК
    /// </summary>
    public class DevicetypeMappingProfile : Profile
    {
        public DevicetypeMappingProfile()
        {
            CreateMap<DEVICETYPE, Devicetype>()
                .ForMember(cfg => cfg.DEVICE, act => act.Ignore())
                .MaxDepth(1)
                .ReverseMap()
                .MaxDepth(1);
            ;
        }
    }
}