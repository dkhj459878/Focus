﻿using System;
using System.Collections.Generic;
using System.Reflection;

namespace Focus.Web.Extensions
{

    internal static class ReflectionExtensions
    {
        internal static readonly List<Type> SimplePropertyTypes = new List<Type>()
        {
            typeof(byte),
            typeof(byte?),
            typeof(int),
            typeof(int?),
            typeof(short),
            typeof(short?),
            typeof(DateTime),
            typeof(DateTime?),
            typeof(string),
            typeof(bool),
            typeof(bool?),
            typeof(decimal),
            typeof(decimal?),
            typeof(float),
            typeof(float?),
            typeof(double),
            typeof(double?)
        };

        internal static bool IsNestedProperty(this PropertyInfo property)
        {
            Type propertyType = property.PropertyType;
            bool isSimpleProperty = SimplePropertyTypes.Contains(propertyType);
            return !isSimpleProperty;
        }
    }
}