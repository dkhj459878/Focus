﻿namespace Focus.SSO.Models
{
    using System;
    using System.ComponentModel.DataAnnotations;

    public partial class Report
    {
        [Display(Name = "Код")]
        public short REPORTID { get; set; }
        [Display(Name = "Номер паспорта")]
        public string PASSPORTNUMBER { get; set; }
        [Display(Name = "Код представителя")]
        public Nullable<short> CUSTOMBROCKERID { get; set; }
        [Display(Name = "Цифровая часть номера паспорта")]
        public string NUMBER_ { get; set; }
        [Display(Name = "Полное имя")]
        public string FULLNAME { get; set; }
        [Display(Name = "Пользователь")]
        public string USERID { get; set; }
        [Display(Name = "IP")]
        public string IP { get; set; }
        [Display(Name = "Должностное лицо")]
        public Nullable<short> PERSONID { get; set; }
        [Display(Name = "Код ПТО")]
        public string CODEPOST { get; set; }
        [Display(Name = "ПТО")]
        public string NAMEPOST { get; set; }
        [Display(Name = "Подразделение/отдел")]
        public string DEVISION { get; set; }
        [Display(Name = "Обновлен")]
        public DateTime LAST_UPDATE { get; set; }
    }
}