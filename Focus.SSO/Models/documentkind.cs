﻿namespace Focus.SSO.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public partial class Documentkind
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Documentkind()
        {
        }

        [Display(Name = "Код")]
        public byte DOCUMENTKINDID { get; set; }
        [Required(ErrorMessage = "Требуется ввести вид документа.")]
        [Display(Name = "Вид документа")]
        [MaxLength(255)]
        public string NAME { get; set; }
        [Display(Name = "Пользователь")]
        [MaxLength(256)]
        public string USERID { get; set; }
        [Display(Name = "Обновлен")]
        public DateTime LAST_UPDATE { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Document> DOCUMENT { get; set; }
    }
}
