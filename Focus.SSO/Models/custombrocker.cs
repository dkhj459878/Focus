﻿namespace Focus.SSO.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Web.Mvc;

    public partial class Custombrocker
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Custombrocker()
        {
        }

        [Display(Name = "Код")]
        public short CUSTOMBROCKERID { get; set; }
        [Required(ErrorMessage = "Требуется ввести имя.")]
        [Display(Name = "Имя")]
        [MaxLength(45)]
        [MinLength(2)]
        [Remote("NotRepeated", "custombrockers", HttpMethod = "POST", AdditionalFields = "LASTNAME, FATHERSNAME, CUSTOMBROCKERID")]
        public string FIRSTNAME { get; set; }
        [Required(ErrorMessage = "Требуется ввести фамилию.")]
        [Display(Name = "Фамилия")]
        [MaxLength(45)]
        [MinLength(2)]
        public string LASTNAME { get; set; }
        [Required(ErrorMessage = "Требуется ввести отчество.")]
        [Display(Name = "Отчество")]
        [MaxLength(45)]
        [MinLength(2)]
        public string FATHERSNAME { get; set; }
        [Required(ErrorMessage = "Требуется ввести место работы.")]
        [Display(Name = "Организация")]
        [MaxLength(255)]
        public string WORKPLACE { get; set; }
        [Required(ErrorMessage = "Требуется ввести должность.")]
        [Display(Name = "Должность")]
        [MaxLength(255)]
        public string POST { get; set; }
        [Required(ErrorMessage = "Требуется ввести номер паспорта.")]
        [Remote("NotRepeatedPassportNumber", "custombrockers", HttpMethod = "POST",
            AdditionalFields = "CUSTOMBROCKERID,COUNTRY_ID")]
        [Display(Name = "Номер")]
        [MaxLength(45)]
        public string PASSPORTNUMBER { get; set; }
        [Required(ErrorMessage = "Требуется ввести дату выдачи.")]
        [Display(Name = "Выдан")]
        [DataType(DataType.Date)]
        public DateTime? PASSPORTISSUED { get; set; }
        [Required(ErrorMessage = "Требуется ввести орган выдачи.")]
        [Display(Name = "Кем выдан")]
        [MaxLength(255)]
        public string PASSPORTBODYISSUE { get; set; }
        [Display(Name = "Полный адрес")]
        [MaxLength(500)]
        public string ADDRESS_FULL { get; set; }
        [Required(ErrorMessage = "Требуется ввести адрес.")]
        [Display(Name = "Адрес")]
        [MaxLength(255)]
        public string ADDRESS { get; set; }
        [Display(Name = "Индекс")]
        [MaxLength(255)]
        public string POST_INDEX { get; set; }
        [Display(Name = "Страна")]
        [MaxLength(2)]
        [Required(ErrorMessage = "Требуется ввести страну регистрации.")]
        public string COUNTRY_ID { get; set; }
        [Display(Name = "Регион")]
        [MaxLength(255)]
        public string REGION { get; set; }
        [Display(Name = "Район")]
        [MaxLength(255)]
        public string DISTRICT { get; set; }
        [Display(Name = "Тип населенного пункта")]
        [MaxLength(15)]
        public string CITY_TYPE { get; set; }
        [Display(Name = "Наименование населенного пункта")]
        [MaxLength(255)]
        public string CITY { get; set; }
        [Display(Name = "Тип улицы")]
        [MaxLength(255)]
        public string STREET_TYPE { get; set; }
        [Display(Name = "Улица")]
        [MaxLength(255)]
        public string STREET { get; set; }
        [Display(Name = "Здание / строение")]
        [MaxLength(255)]
        public string BUILDING { get; set; }
        [Display(Name = "Корпус")]
        [MaxLength(255)]
        public string BUILDING_BLOCK { get; set; }
        [Display(Name = "Офис")]
        [MaxLength(255)]
        public string OFFICE { get; set; }
        [Display(Name = "Тел.")]
        [MaxLength(255)]
        public string PHONE { get; set; }
        [Display(Name = "Факс")]
        [MaxLength(255)]
        public string FAX { get; set; }
        [Display(Name = "Email")]
        [EmailAddress(ErrorMessage = "Формат записи не соответствует формату адреса электронной почты.")]
        [MaxLength(255)]
        public string EMAIL { get; set; }
        [Display(Name = "Пользователь")]
        [MaxLength(256)]
        public string USERID { get; set; }
        [Display(Name = "Обновлен")]
        public DateTime LAST_UPDATE { get; set; }

        public virtual Country COUNTRY { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Doc> DOC { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Permission> PERMISSION { get; set; }
    }
}
