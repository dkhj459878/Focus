﻿namespace Focus.SSO.Models
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Web.Mvc;

    public partial class Container
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Container()
        {
        }

        [Display(Name = "Код")]
        public short CONTAINERID { get; set; }
        [Display(Name = "Рег. номер")]
        [Required(ErrorMessage = "Требуется ввести регистрационный номер контейнера.")]
        [Remote("NotRepeated", "containers", HttpMethod = "POST", AdditionalFields = "COUNTRYCODE, CONTAINERID")]
        [MaxLength(45)]
        public string REGNUMBER { get; set; }
        [Display(Name = "Наименование")]
        [MaxLength(45)]
        public string NAME { get; set; }
        [Display(Name = "Страна регистрации")]
        public string COUNTRYCODE { get; set; }
        [Display(Name = "Размер")]
        [MaxLength(45)]
        public string DEMENSIONS { get; set; }
        [Display(Name = "Обновлен")]
        [MaxLength(256)]
        public string USERID { get; set; }
        public System.DateTime LAST_UPDATE { get; set; }

        public virtual Country COUNTRY { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Doc> DOC { get; set; }
    }
}
