﻿namespace Focus.SSO.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Web.Mvc;

    public partial class Document
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Document()
        {
        }

        [Display(Name = "Код")]
        public short DOCUMENTID { get; set; }
        [Required(ErrorMessage = "Требуется ввести регистрационный номер документа.")]
        [Display(Name = "Номер документа")]
        [Remote("NotRepeated", "documents", HttpMethod = "POST", AdditionalFields = "DOCUMENTID, DATE, DOCUMENTKINDID")]
        [MaxLength(45)]
        public string NUMBER_ { get; set; }
        [Required(ErrorMessage = "Требуется ввести дату регистрации документа.")]
        [Display(Name = "Дата регистрации документа")]
        [DataType(DataType.Date)]
        public DateTime DATE_ { get; set; }
        [Required(ErrorMessage = "Требуется ввести код вида документа.")]
        [Display(Name = "Вид документа")]
        public byte DOCUMENTKINDID { get; set; }
        [Display(Name = "Орган выдачи документа")]
        [MaxLength(255)]
        public string BODYISSUE { get; set; }
        [Display(Name = "Пользователь")]
        [MaxLength(256)]
        public string USERID { get; set; }
        [Display(Name = "Обновлен")]
        public DateTime LAST_UPDATE { get; set; }

        public virtual Documentkind DOCUMENTKIND { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Permission> PERMISSION { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Organization> ORGANIZATION { get; set; }
    }
}
